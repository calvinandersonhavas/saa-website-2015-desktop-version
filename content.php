<?php
ob_start();

include('includes/header.php');
include('includes/content.php');
include('includes/footer.php');

$contents = ob_get_clean();
file_put_contents('content.html', $contents);

echo $contents;