

$(document).ready(function(){
    //Load the Slider
    loadSliderImages();
    //Load the Bottom content
    loadFlightDeals();
    loadSpecialOffer();
    loadLeftBlocks();

    //loadAdserverBanners();
    //TODO Element does not exist
    //loadAdSpaceImage();
    //TODO Element does not exist
    //loadGlobalImage();
    //TODO Element does not exist
    //Load the links
    //loadHomeLinks();

});



/**
 *Load OpenCMS image for adspace content using Jquery
 *
 */
function loadAdSpaceImage() {
    $("#adSpaceImage").load(relPath + "/cms/ZA/leisure/AdSpaceImage.html");
}

function loadGlobalImage() {
    $("#specials").load(relPath + "/cms/ZA/leisure/AdGlobalHighlights.html");
}

function loadHomeLinks() {
    $("#homeLinks").load(relPath + "/cms/US/leisure/flysaa_homeLinks.html");
}
function loadAdserverBanners() {
    $("#banners").load(relPath + "/cms/" + country + "/leisure/AdServerImage.html", function (response, status, xhr) {
        if (status == "error") {
            $("#banners").load(relPath + "/cms/ZA/leisure/AdServerImage.html");
        }
    });
}

function loadFlightDeals(){
    $("#flightDeals").load(relPath+"/cms/ZA/HomePageDetails/flysaa_FlightDealsData.html");
}

function loadSpecialOffer(){
    $("#specialOffers").load(relPath+"/cms/ZA/HomePageDetails/flysaa_SpecialOfferData.jsp");
}

function loadSliderImages(){
    $("#slider").load(relPath+"/cms/ZA/HomePageDetails/flysaa_SliderImages.jsp", function(){

        $(".flexslider").flexslider({
            animation: "slide",
            start: function() {
                $("body").removeClass("loading");
            }
        })

    });
}

function loadLeftBlocks(){
    $("#featuredArticles").load(relPath+"/cms/ZA/HomePageDetails/flysaa_LeftBlocks.jsp");
}