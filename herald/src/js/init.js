/**
 * Init Javascript
 *
 * This contains init code for initialising the page js and variables
 *
 * This was built from original website source code and migrated
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/09
 */

/**
 * Relative path
 *
 * @type {String}
 */
var relPath="";

/**
 * ?
 *
 * @type {String}
 */
var conLan="";

/**
 * The site language
 *
 * This should be upper case
 *
 * @type {string}
 */
var language = config.language.toUpperCase();

/**
 * The site country
 *
 * This should be upper case
 *
 * @type {string}
 */
var country = config.country.toUpperCase();


/**
 * A list of regions
 *
 * Key = Region code
 * Value = Region Name
 *
 * This is populated by loadRegionXML
 *
 * @type {Array}
 */
var regions = [];

/**
 * A list of countries
 *
 * @type {Array}
 */
var countries = [];

/**
 * Determine if we are a device
 * This variable is used to do *lots* of checks because we want to use the native <select>s on devices instead of the linkselect plugin.
 *
 * @type {Boolean} True if the device is an iPhone, iPad or Android device, False if not a device
 */
var iDevice = (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Android/i)) ? true : false;

/**
 * All the translated short-months for the datepicker.
 *
 * @type {Array}
 */
// The datepicker translations are set in lib/libraries.js (right at the bottom of the file)
var monthArrays = [];
monthArrays['en'] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
monthArrays['fr'] = ['Jan', 'F\u00E9v', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Ao\u00FB', 'Sep', 'Oct', 'Nov', 'D\u00E9c'];
monthArrays['de'] = ['Jan', 'Feb', 'M\u00E4r', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'];
monthArrays['it'] = ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'];
monthArrays['es'] = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
monthArrays['pt'] = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
monthArrays['zh'] = ['\u4E00\u6708', '\u4E8C\u6708', '\u4E09\u6708', '\u56DB\u6708', '\u4E94\u6708', '\u516D\u6708', '\u4E03\u6708', '\u516B\u6708', '\u4E5D\u6708', '\u5341\u6708', '\u5341\u4E00\u6708', '\u5341\u4E8C\u6708'];
monthArrays['zh_cn'] = ['\u4E00\u6708', '\u4E8C\u6708', '\u4E09\u6708', '\u56DB\u6708', '\u4E94\u6708', '\u516D\u6708', '\u4E03\u6708', '\u516B\u6708', '\u4E5D\u6708', '\u5341\u6708', '\u5341\u4E00\u6708', '\u5341\u4E8C\u6708'];
monthArrays['ja'] = ['1\u6708', '2\u6708', '3\u6708', '4\u6708', '5\u6708', '6\u6708', '7\u6708', '8\u6708', '9\u6708', '10\u6708', '11\u6708', '12\u6708'];
// Populate the 2 arrays used in the Booking & Meta Blocks with the correct (translated) months
// mbMonths	- Meta Block
// months	- Booking Block
//TODO Need to determine if this is a valid way of doing this
var months = monthArrays[config.language.toLowerCase()]; //mbMonths

/**
 * Cookie Enabled
 *
 * ?? Why is this a string value ??
 *
 * @type {string}
 */
var isCookieEnabled = config.isCookieEnabled;


/**
 * Get todays date
 *
 * @type {Date}
 */
var today = new Date();

/**
 * Helper variable to set the day with a leading 0
 *
 * This always spits out a 2 character string
 * If the day is under 10, it just adds a zero to the front of the string (eg: 03 or 16)
 *
 * @type {string}
 */
var leadingZeroDay = ('0' + today.getDate()).slice(-2);


/**
 * Get the formatted version of today
 *
 * @type {string}
 */
var todayFormatted = leadingZeroDay + '-' + monthArrays['en'][today.getMonth()] + '-' + today.getFullYear();


/**
 * Set the Ajax Load Leisure image
 *
 * ?? TODO Is this actually needed ??
 *
 * @type {string}
 */
var ajax_load_leisure = "<img class='loading' src='" + relPath + "/images/ajaxloader/ajax-loader.gif' alt='loading...' />";

/**
 * Messages Array
 *
 * This Array is used For internationalization of validation messages in JS..
 * Required properties are read and pushed to this array such that they can be
 * accessed form js files..
 *
 * @type {Array}
 */
var propsArray = [];


/**
 * The destinations xml for populating the options
 *
 * @type {xml} An xml object
 */
var xmlDestinations = null;


/**
 * The flight status xml
 *
 * @type {xml} An xml object
 */
var xmlFlightStatus = null;


/**
 * The flight status xml
 *
 * @type {xml} An xml object
 */
var xmlChkInStatus = null

/**
 * What is this controlling???
 *
 * @type {boolean}
 */
var isMatrixSearch = false;


/**
 * Maximum allowed guests for hotel bookings
 *
 * @type {number}
 */
var hotelMaxPax = 9;


/**
 * Date Format
 *
 * @type {string}
 */
var dateFormat = 'dd-M y';


/**
 * URL for the global notices
 *
 * @type {string}
 */
var globalNoticeUrl = '';

/**
 * URL for the local notices
 *
 * @type {string}
 */
var localNoticeUrl = '';



//need to investigate these
var tab1 = [], tab2 = [], tab3 = [], tab4 = [], tabVal1 = [], tabVal2 = [], tabVal3 = [], tabVal4 = [], regionXml = [], departXml = [];
var depCountry = '';
var carRentalPosConnId = '';
var posConnId;
// Append the language to the body
//TODO Is this actually needed?
$('body').removeClass('en').addClass(language);


/**
 * Browser properties
 */

/**
 * User Agent
 *
 * @type {string}
 */
var agt = navigator.userAgent.toLowerCase();

/**
 * Browser version
 *
 * @type {string}
 */
var appVer = navigator.appVersion.toLowerCase();

/**
 * Index of IE in the app Version
 *
 * @type {string}
 */
var iePos = appVer.indexOf('msie');

/**
 * The list of cabin classes
 *
 * @type {string}
 */
var cabinClass = '';








/**
 * Load bindings
 */
$(window).load(function(){
    if (iDevice) {
        //TODO This should use language translation!!!
        alert("We've detected you're on a mobile touch browser, please use two fingers to scroll the drop-down options.");
    }
});


/**
 * Document ready bindings
 */
$(document).ready(function(){

    //TODO Check if this can be changed as the wrapping form should be removed
    if (config.mobileUser == true) {

        //TODO this is missing from the page currently! And it should be in propsArray
        if (confirm(document.getElementById('MsgMU').innerHTML)) {
            //TODO Update to redirect without a form submittion
            //document.forms[0].action = relPath + '/splashPageAction!mobileRedirect.action';
            //document.forms[0].submit();
            window.location = relPath + '/splashPageAction!mobileRedirect.action';
        }
    }

    // Determine the browser ie version
    var is_minor, is_major; //TODO Check if these are needed as variables
    if (iePos != -1) {
        is_minor = parseFloat(appVer.substring(iePos + 5, appVer.indexOf(';', iePos)));
        is_major = parseInt(is_minor);
        if (is_major <= 8) {
            alert(propsArray['ieVersion_error']);
        }
    }

    // A neat little trick to open external links in new tabs
    // Just add rel="external" to an <a>
    $('a[rel|=external]').click(function () {
        window.open(this.href);
        return false
    });

    //create an event for the overlay open and close
    $.Event("modal-open");
    $.Event("modal-closed");
    $('body').on('modal-close', function(){
        $('#disabler').fadeOut();
        $('.overlay').fadeOut();
    });

    // Close the modals when you click the body
    $('#disabler, .overlay').click(function () {
        closePopups()
    });
    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            closePopups();
        }
    });
    $('#iframePopup .close').click(function(){
        iframePopupClose();
        return false;
    })



    /**
     * Update the page location and language and get/set the cookie
     */
    // THIS IS NO LONGER NEEDED
    //update the country set to the page
    //if ($.trim($("#sessionCountry").val()) == "") {
    //    $("#sessionCountry").val($("#selectedCountryDiv").html());
    //}
    //$('#selLanguage').val($('.current').attr('rel'));

    if (country.length > 0) {
        conLan = "/" + country.toLowerCase() + "/" + language.toLowerCase();
    }
    if (language != "EN") {
        $('body').removeClass('en');
        $('body').addClass(language.toLowerCase());
    }

    //Update locale and country based on cookie
    config.isCookieEnabled = checkCookie();
    toggleRememberDiv(config.isCookieEnabled);
    updateLocationHtml(language, country);


    // Load the datepicker's translated defaults from lib/libraries.js
    $.datepicker.setDefaults($.datepicker.regional[language.toLowerCase()]);

    // JS Trick to keep Search Engine crawlers out of the Snoopy page
    // The Snoopy page's link can be found in the very bottom left corner of the home page
    // It looks like this: § - click it for lots of information!
    $('#snoopy').click(function () {
        viewSnoopy()
    });



    //TODO This should rather be loaded at server run time perhaps?
    popEssInfo(config.language, config.country);




    //TODO Need to check this again with the new design
    $('.icon-info').hoverIntent(function () {
        var head = $(this).find('span .head').text();
        var copy = $(this).find('span .inner').html();
        $('#bb .info-popup header').html('<span class="icon-info"></span>' + head);
        $('#bb .info-popup div').html(copy);
        $('#bb .info-popup').fadeIn();
    }, function () {
        $('#bb .info-popup').fadeOut();
    });







//Calling the function to populate the Global Emergency Notice
    populateGlobalNotice();
//Calling the function to populate the Global Emergency Notice
    populateLocalNotice();







});

