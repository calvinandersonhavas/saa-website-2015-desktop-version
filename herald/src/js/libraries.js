









(function ($) {
	$.fn.hoverIntent = function (f, g) {
		var cfg = {sensitivity: 7, interval: 100, timeout: 0};
		cfg = $.extend(cfg, g ? {over: f, out: g} : f);
		var cX, cY, pX, pY;
		var track = function (ev) {
			cX = ev.pageX;
			cY = ev.pageY
		};
		var compare = function (ev, ob) {
			ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
			if ((Math.abs(pX - cX) + Math.abs(pY - cY)) < cfg.sensitivity) {
				$(ob).unbind("mousemove", track);
				ob.hoverIntent_s = 1;
				return cfg.over.apply(ob, [ev])
			} else {
				pX = cX;
				pY = cY;
				ob.hoverIntent_t = setTimeout(function () {
					compare(ev, ob)
				}, cfg.interval)
			}
		};
		var delay = function (ev, ob) {
			ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
			ob.hoverIntent_s = 0;
			return cfg.out.apply(ob, [ev])
		};
		var handleHover = function (e) {
			var p = (e.type == "mouseover" ? e.fromElement : e.toElement) || e.relatedTarget;
			while (p && p != this) {
				try {
					p = p.parentNode
				} catch (e) {
					p = this
				}
			}
			if (p == this) {
				return false
			}
			var ev = jQuery.extend({}, e);
			var ob = this;
			if (ob.hoverIntent_t) {
				ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t)
			}
			if (e.type == "mouseover") {
				pX = ev.pageX;
				pY = ev.pageY;
				$(ob).bind("mousemove", track);
				if (ob.hoverIntent_s != 1) {
					ob.hoverIntent_t = setTimeout(function () {
						compare(ev, ob)
					}, cfg.interval)
				}
			} else {
				$(ob).unbind("mousemove", track);
				if (ob.hoverIntent_s == 1) {
					ob.hoverIntent_t = setTimeout(function () {
						delay(ev, ob)
					}, cfg.timeout)
				}
			}
		};
		return this.mouseover(handleHover).mouseout(handleHover).click(handleHover);
	}
})(jQuery);


//an extenion to change a select field value and trigger the change
$.fn.changeVal = function (v) {
	if ($(this).attr('type') == 'hidden') {
		return $(this).linkselect('val', v).trigger('change');
	} else {
		return $(this).val(v);
	}
}

// Console log
;window.log=function(){log.history=log.history||[];log.history.push(arguments);arguments.callee=arguments.callee.caller;if(this.console)console.log(Array.prototype.slice.call(arguments))};(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();)b[a]=b[a]||c})(window.console=window.console||{});

// iPhone style - deprecated.
;(function($,iphoneStyle){$[iphoneStyle]=function(elem,options){this.$elem=$(elem);var obj=this;$.each(options,function(key,value){obj[key]=value});this.wrapCheckboxWithDivs();this.attachEvents();this.disableTextSelection();if(this.resizeHandle){this.optionallyResize('handle')}
if(this.resizeContainer){this.optionallyResize('container')}
this.initialPosition()};$.extend($[iphoneStyle].prototype,{wrapCheckboxWithDivs:function(){this.$elem.wrap('<div class="'+this.containerClass+'" />');this.container=this.$elem.parent();this.offLabel=$('<label class="'+this.labelOffClass+'">'+'<span>'+this.uncheckedLabel+'</span>'+'</label>').appendTo(this.container);this.offSpan=this.offLabel.children('span');this.onLabel=$('<label class="'+this.labelOnClass+'">'+'<span>'+this.checkedLabel+'</span>'+'</label>').appendTo(this.container);this.onSpan=this.onLabel.children('span');this.handle=$('<div class="'+this.handleClass+'">'+'<div class="'+this.handleRightClass+'">'+'<div class="'+this.handleCenterClass+'" />'+'</div>'+'</div>').appendTo(this.container)},disableTextSelection:function(){if(!$.browser.msie){return}
$.each([this.handle,this.offLabel,this.onLabel,this.container],function(){$(this).attr("unselectable","on")})},optionallyResize:function(mode){var onLabelWidth=this.onLabel.width(),offLabelWidth=this.offLabel.width();if(mode=='container'){var newWidth=(onLabelWidth>offLabelWidth)?onLabelWidth:offLabelWidth;newWidth+=this.handle.width()+15}else{var newWidth=(onLabelWidth<offLabelWidth)?onLabelWidth:offLabelWidth}
this[mode].css({width:newWidth})},attachEvents:function(){var obj=this;this.container.bind('mousedown touchstart',function(event){event.preventDefault();if(obj.$elem.is(':disabled')){return}
var x=event.pageX||event.originalEvent.changedTouches[0].pageX;$[iphoneStyle].currentlyClicking=obj.handle;$[iphoneStyle].dragStartPosition=x;$[iphoneStyle].handleLeftOffset=parseInt(obj.handle.css('left'),10)||0;$[iphoneStyle].dragStartedOn=obj.$elem}).bind('iPhoneDrag',function(event,x){event.preventDefault();if(obj.$elem.is(':disabled')){return}
if(obj.$elem!=$[iphoneStyle].dragStartedOn){return}
var p=(x+$[iphoneStyle].handleLeftOffset-$[iphoneStyle].dragStartPosition)/obj.rightSide;if(p<0){p=0}
if(p>1){p=1}
obj.handle.css({left:p*obj.rightSide});obj.onLabel.css({width:p*obj.rightSide+4});obj.offSpan.css({marginRight:-p*obj.rightSide});obj.onSpan.css({marginLeft:-(1-p)*obj.rightSide})}).bind('iPhoneDragEnd',function(event,x){if(obj.$elem.is(':disabled')){return}
var checked;if($[iphoneStyle].dragging){var p=(x-$[iphoneStyle].dragStartPosition)/obj.rightSide;checked=(p<0)?Math.abs(p)<0.5:p>=0.5}else{checked=!obj.$elem.attr('checked')}
obj.$elem.attr('checked',checked);$[iphoneStyle].currentlyClicking=null;$[iphoneStyle].dragging=null;obj.$elem.change()});this.$elem.change(function(){if(obj.$elem.is(':disabled')){obj.container.addClass(obj.disabledClass);return false}else{obj.container.removeClass(obj.disabledClass)}
var new_left=obj.$elem.attr('checked')?obj.rightSide:0;obj.handle.animate({left:new_left},obj.duration);obj.onLabel.animate({width:new_left+4},obj.duration);obj.offSpan.animate({marginRight:-new_left},obj.duration);obj.onSpan.animate({marginLeft:new_left-obj.rightSide},obj.duration)})},initialPosition:function(){this.offLabel.css({width:this.container.width()-5});var offset=($.browser.msie&&$.browser.version<7)?3:6;this.rightSide=this.container.width()-this.handle.width()-offset;if(this.$elem.is(':checked')){this.handle.css({left:this.rightSide});this.onLabel.css({width:this.rightSide+4});this.offSpan.css({marginRight:-this.rightSide})}else{this.onLabel.css({width:0});this.onSpan.css({marginLeft:-this.rightSide})}
if(this.$elem.is(':disabled')){this.container.addClass(this.disabledClass)}}});$.fn[iphoneStyle]=function(options){var checkboxes=this.filter(':checkbox');if(!checkboxes.length){return this}
var opt=$.extend({},$[iphoneStyle].defaults,options);checkboxes.each(function(){$(this).data(iphoneStyle,new $[iphoneStyle](this,opt))});if(!$[iphoneStyle].initComplete){$(document).bind('mousemove touchmove',function(event){if(!$[iphoneStyle].currentlyClicking){return}
event.preventDefault();var x=event.pageX||event.originalEvent.changedTouches[0].pageX;if(!$[iphoneStyle].dragging&&(Math.abs($[iphoneStyle].dragStartPosition-x)>opt.dragThreshold)){$[iphoneStyle].dragging=true}
$(event.target).trigger('iPhoneDrag',[x])}).bind('mouseup touchend',function(event){if(!$[iphoneStyle].currentlyClicking){return}
event.preventDefault();var x=event.pageX||event.originalEvent.changedTouches[0].pageX;$($[iphoneStyle].currentlyClicking).trigger('iPhoneDragEnd',[x])});$[iphoneStyle].initComplete=true}
return this};$[iphoneStyle].defaults={duration:100,checkedLabel:'Yes',uncheckedLabel:'No',resizeHandle:true,resizeContainer:true,disabledClass:'chkDisabled',containerClass:'chkContainer',labelOnClass:'chkLabelYes',labelOffClass:'chkLabelNo',handleClass:'chkHandle',handleCenterClass:'chkHandleCenter',handleRightClass:'chkHandleRight',dragThreshold:5}})(jQuery,'iphoneStyle');;


/*

	Combobox functionality

*/

(function($){
	$.widget("ui.combobox",{_create:
		function(){
			var self = this,
				select = this.element.hide(),
				selected = select.children(":selected"),
				value = selected.val() ? selected.text() : "";

			accentMap={'Á':'a','Â':'a','Ã':'a','Ä':'a','Ç':'c','É':'e','Ê':'e','Ë':'e','Í':'i','Î':'i','Ï':'i','Ñ':'n','Ó':'o','Ô':'o','Õ':'o','Ö':'o','Ú':'u','Û':'u','Ü':'u','ß':'ss','á':'a','â':'a','ã':'a','ä':'a','ç':'c','è':'e','é':'e','ê':'e','ë':'e','í':'i','î':'i','ï':'i','ñ':'n','ó':'o','ô':'o','õ':'o','ö':'o','ù':'u','ú':'u','û':'u','ü':'u'};

			var normalize=function(term){
				var ret="";
				for(var i=0;i<term.length;i++){ret+=accentMap[term.charAt(i)]||term.charAt(i);}
				return ret;
			};

			var input = this.input = $("<input placeholder=\""+select.attr('title')+"\" title=\""+select.attr('title')+"\" autocomplete=\"off\">")
				.insertAfter(select)
				.val('')
				.autocomplete({
					appendTo: $(select).parent(),
					delay:0,
					minLength:0,

					source: function(request,response){
						var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term),"i");
						response(select.children("option").map(function(){
							var text = $(this).text();
							if(this.value && (!request.term||matcher.test(text)) || matcher.test(normalize(text)) || matcher.test(this.value))
								return {
									label:	text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)","gi"), "$1"),
									value:	text,
									option:	this
								};
							})
						);
					},

					select:function(event,ui){
						ui.item.option.selected = true;
						self._trigger("selected",event,{item:ui.item.option});
						$(this).removeClass('invalid');
					},

					focus:function(event,ui){
						$(this).val(ui.item.value);
						return false;
					},

					change:function(event,ui){
						var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$","i"),
							valid = false;
						select.children("option").each(function(){
							if($(this).text().match(matcher)){
								this.selected = valid = true;
								return false;
							}
						});
						if(!valid){
							if(Modernizr.input.placeholder){
								$(this).val("");
								select.val("");
							}else{
								$(this).val(input.attr('title'));
								select.val(input.attr('title'));
								$(this).css('color','darkGrey');
							}
							input.data("autocomplete").term="";
							return false;
						}
					}
				})
				.addClass("ui-widget ui-widget-content");

			input.data("autocomplete")._renderItem = function(ul,item){
				if (item.label != null) {
					$li = $("<li></li>").data("item.autocomplete",item);
					if (item.label.indexOf(',') == -1) {
						$li.append("<a>"+item.label+"</a>");
					} else {
						items = item.label.split(', ');
						$li.append("<a>"+items[0]+"<span>, "+items[1]+"</span></a>");
					}
					$li.appendTo(ul);
					return $li;
				}
			};

			this.button = $('<button type="button" class="ui-button icon-arrow-down"> </button>')
				.attr("tabIndex",-1)
				.attr("title","Show All Items")
				.insertAfter(input)
				//.button({text:false})
				.removeClass("ui-corner-all")
				.addClass("ui-button-icon")
				.click(function(){
					if(input.autocomplete("widget").is(":visible")){
						input.autocomplete("close");
						return
					}
					//TODO This should be more generic than using the select ID
					if(select.attr('id')=='departCity'){
						// Find all the locale based items here
						input.autocomplete("search",", " + config.countryLong);
					} else {
						// Show all the items in the list
						input.autocomplete("search","");
					}
					input.focus();
				});
		},

			destroy:function(){
				this.input.remove();
				this.button.remove();
				this.element.show();
				$.Widget.prototype.destroy.call(this);
			}
	});
})(jQuery);




jQuery(function($) {
	$.datepicker.regional['en'] = {
		closeText: 'Done',
		prevText: 'Prev',
		nextText: 'Next',
		currentText: 'Today',
		monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
		dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
		dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
		dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
		weekHeader: 'Wk',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
});

jQuery(function($){
	$.datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: '&#x3c;Préc',
		nextText: 'Suiv&#x3e;',
		currentText: 'Courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort:['Jan','F\u00E9v','Mar','Avr','Mai','Jun','Jul','Ao\u00FB','Sep','Oct','Nov','D\u00E9c'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'mm/dd/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
});

/* German initialisation for the jQuery UI date picker plugin. */
/* Written by Milian Wolff (mail@milianw.de). */
jQuery(function($){
	$.datepicker.regional['de'] = {
		closeText: 'schließen',
		prevText: '&#x3c;zurück',
		nextText: 'Vor&#x3e;',
		currentText: 'Heute',
		monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
		monthNamesShort:['Jan','Feb','M\u00E4r','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
		dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
		dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
		dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
		weekHeader: 'Wo',
		dateFormat: 'mm/dd/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
});

/* Spanish initialisation for the jQuery UI date picker plugin. */
/* Traducido por Vester (xvester@gmail.com). */
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		weekHeader: 'Sm',
		dateFormat: 'mm/dd/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
});


/* Italian initialisation for the jQuery UI date picker plugin. */
/* Written by Antonello Pasella (antonello.pasella@gmail.com). */
jQuery(function($){
	$.datepicker.regional['it'] = {
		closeText: 'Chiudi',
		prevText: '&#x3c;Prec',
		nextText: 'Succ&#x3e;',
		currentText: 'Oggi',
		monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settebre','Ottobre','Novembre','Dicembre'],
		monthNamesShort:['Gen','Feb','Mar','Apr','Mag','Giu','Lug','Ago','Set','Ott','Nov','Dic'],
		dayNames: ['Domenica','Lunedì','Martedì','Mercoledì','Giovedì','Venerdì','Sabato'],
		dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
		dayNamesMin: ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'mm/dd/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
});

/* Brazilian initialisation for the jQuery UI date picker plugin. */
/* Written by Leonildo Costa Silva (leocsilva@gmail.com). */
jQuery(function($){
	$.datepicker.regional['pt'] = {
		closeText: 'Fechar',
		prevText: '&#x3c;Anterior',
		nextText: 'Próximo&#x3e;',
		currentText: 'Hoje',
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort:['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		dayNames: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
		dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
		weekHeader: 'Sm',
		dateFormat: 'mm/dd/yy',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
});

/* Chinese initialisation for the jQuery UI date picker plugin. */
/* Written by Ressol (ressol@gmail.com). */
jQuery(function($){
	$.datepicker.regional['zh'] = {
		closeText: '關閉',
		prevText: '&#x3c;上月',
		nextText: '下月&#x3e;',
		currentText: '今天',
		monthNames: ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708','\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
		monthNamesShort:['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708','\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
		dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
		dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
		dayNamesMin: ['日','一','二','三','四','五','六'],
		weekHeader: '周',
		dateFormat: 'mm/dd/yy',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '年'};
});

jQuery(function($){
	$.datepicker.regional['zh_cn'] = {
		closeText: '關閉',
		prevText: '&#x3c;上月',
		nextText: '下月&#x3e;',
		currentText: '今天',
		monthNames: ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708','\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
		monthNamesShort:['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708','\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
		dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
		dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
		dayNamesMin: ['日','一','二','三','四','五','六'],
		weekHeader: '周',
		dateFormat: 'mm/dd/yy',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '年'};
});

/* Japanese initialisation for the jQuery UI date picker plugin. */
/* Written by Kentaro SATO (kentaro@ranvis.com). */
jQuery(function($){
	$.datepicker.regional['ja'] = {
		closeText: '閉じる',
		prevText: '&#x3c;前',
		nextText: '次&#x3e;',
		currentText: '今日',
		monthNames: ['1\u6708','2\u6708','3\u6708','4\u6708','5\u6708','6\u6708','7\u6708','8\u6708','9\u6708','10\u6708','11\u6708','12\u6708'],
		monthNamesShort: ['1\u6708','2\u6708','3\u6708','4\u6708','5\u6708','6\u6708','7\u6708','8\u6708','9\u6708','10\u6708','11\u6708','12\u6708'],
		dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
		dayNamesShort: ['日','月','火','水','木','金','土'],
		dayNamesMin: ['日','月','火','水','木','金','土'],
		weekHeader: '週',
		dateFormat: 'mm/dd/yy',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '年'};
	//$.datepicker.setDefaults($.datepicker.regional['ja']);
});