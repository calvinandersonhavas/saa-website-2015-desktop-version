


// From flysaa_common.js

/**
 * Determine the key that was pressed
 *
 * @param event
 * @returns {integer} The int value of the key pressed
 */
function getKeyCode(event) {
    return  event.keyCode ? event.keyCode :
                event.charCode ? event.charCode :
                    event.which ? event.which : void 0;
}

/**
 * Is the key pressed an integer?
 *
 * @param event
 * @returns {boolean}
 */
function numeric(event) {
    var keyCode = getKeyCode(event)
    if (((keyCode >= 48) && (keyCode <= 57) || (keyCode == 46)) || (keyCode == 8) || keyCode == 9) {
        event.returnValue = true;
        return true;
    } else {
        event.preventDefault;
        return false;
    }
}


/**
 * Override alert function for showing modalwindow based popup for alert mesages
 *
 * @param {string} The message to display
 */
function alert(msg) {
    //TODO update this to refer to the propsArray
    modalShow('scriptAlert', getLocalizedMessageUnformatted('Message', 'msgAlertHeader'), msg);
}

/**
 * Get localised message
 *
 * This method can be used to get the localized messages in javascript.This
 * method takes 2 parameters.. 1. defaultMessage - message to be used if
 * localized message is not available 2. keyIndex - index of the message in
 * propsArray
 *
 * @param {string} The default message to display
 * @param {string} Get the key for finding the translated message
 */
function getLocalizedMessage(defaultMessage, keyIndex) {
    if (propsArray[keyIndex]) {
        //return propsArray[keyIndex]+'\n';
        return propsArray[keyIndex] + '<br />';
    } else {
        //return defaultMessage+'\n';
        return defaultMessage + '<br />';
    }
}

/**
 * Get localised message
 *
 * This method can be used to get the localized messages in javascript.This
 * method takes 2 parameters.. 1. defaultMessage - message to be used if
 * localized message is not available 2. keyIndex - index of the message in
 * propsArray
 *
 * @param {string} The default message to display
 * @param {string} Get the key for finding the translated message
 */
function getLocalizedMessageUnformatted(defaultMessage, keyIndex) {
    if (propsArray[keyIndex]) {
        return propsArray[keyIndex];
    } else {
        return defaultMessage;
    }
}

/**
 * Get localised message using only the keyIndex
 *
 * @param keyIndex
 * @returns {*}
 */
function getLocalizedMessageFromKey(keyIndex) {
    if (propsArray[keyIndex]) {
        return propsArray[keyIndex];
    } else {
        return '';
    }
}


/**
 * Fix to calculate the center of the page
 *
 * Used for center() in modalShow()
 *
 * @returns {jQuery.fn}
 */
jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
    this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
    return this;
};

/**
 * Helper function to Fade elements in or out
 *
 * @param {string} The element id
 * @param {string} type of fade direction: fadeIn, fadeOut
 * @param {mixed} The speed of the fade. @see jQuery.fadeIn or jQuery.fadeOut for supported values
 */
function adjustedFade(id, direction, speed) {
    if ($.browser.msie && ($.browser.version > 6)) {
        if (direction == 'fadeIn') {
            $(id).show(0);
        } else {
            $(id).hide(0);
        }
    } else if ($.browser.msie && ($.browser.version == 6)) {
        if (direction == 'fadeIn') {
            $(id).show();
        } else {
            $(id).hide();
        }
    } else {
        if (direction == 'fadeIn') {
            $(id).fadeIn(speed);
        } else {
            $(id).fadeOut(speed);
        }
    }
}


/**
 * This method is added for the Snoopy Information action
 *
 * TODO Is this actually needed???
 */
function viewSnoopy() {
    var url = relPath + conLan + "/viewSnoopy!snoopy.action";
    window.open(url, 'subWin', 'height=800,width=1000,resizable=yes,scrollbars=yes');
}


/**
 * Checks the cookie for if the remembered Locale and Lang exists
 *
 * This will update the global country and language as well
 *
 * @returns {boolean} True if the cookie was found and valid, false if not
 */
function checkCookie() {
    var rememberLocale = getCookie('rememberLocale');
    var rememberLang = getCookie('rememberLang');
    if (rememberLocale != null && rememberLocale != "" && rememberLang != null && rememberLang != "") {
        language = rememberLang;
        country = rememberLocale;
        return true;
    } else {
        return false;
    }
}

/**
 * Set a cookie key with its value
 *
 * @param c_name The name of the cookie variable
 * @param value The value to set
 * @param expiremonths Expiry date
 * @param path The path
 */
function setCookie(c_name, value, expiremonths, path) {
    var exdate = new Date();
    exdate.setMonth(exdate.getMonth() + expiremonths);
// var pathString = ((path == null) ? "" : ("; path=" + path));
    document.cookie = c_name + "=" + escape(value) + ((path == null) ? "" : ("; path=" + path)) +
    ((expiremonths == null) ? "" : ";expires=" + exdate.toUTCString());
}

/**
 * Get a value from the cookie
 *
 * @param c_name The variable name to fetch the value for
 * @returns {mixed} The value of the cookie if found or an empty string
 */
function getCookie(c_name) {
    var cValue = "", tmpArray;
    if (document.cookie.length > 0) {
        tmpArray = document.cookie.split(";");
        for (var i = 0; i < tmpArray.length; i++) {
            if (tmpArray[i].indexOf(c_name + "=") == 1) {
                cValue = tmpArray[i].split("=");
            }
        }
        if (cValue.length > 0) {
            return cValue[1];
        }
    }
    return "";
}

/**
 * Checks for selected radio button in the button group
 *
 * This method cannot be used for radio values
 *
 * @param buttonGroup
 * @returns {boolean}
 */
function checkRadioSelection(buttonGroup) {
    if (buttonGroup) {
        for (var i = 0; i < buttonGroup.length; i++) {
            if (buttonGroup[i].checked) {
                return true;
            }
        }
    }
    return false;
}


/**
 * A series of date functions
 */

/**
 * Gets the days in the current month
 *
 * Used for Select box-Calendat component
 *
 * @param {int} The month
 * @param {int} The year
 * @returns {number}
 */
function daysInMonth(month, year) {
    return 32 - new Date(year, month, 32).getDate();
}

/**
 * Is the date valid
 *
 * @param {string} The date to check as a string
 * @param {string} The date format, Only supports strings in the format "dd-M y"
 * @returns {boolean} true if valid, false if not a valid date
 */
function isValidDate(dateToCheck, format) {
    if (format == 'dd-M y' && dateToCheck != "") {
        var dateArray = dateToCheck.split('-');
        if (dateArray[0].length == 0 || dateArray[1].length == 0) {
            return false;
        }
        if (dateArray[0] > daysInMonth(getMonthIndex(dateArray[1].split(' ')[0]), '20' + dateArray[1].split(' ')[1])) {
            return false;
        }
    }
    return true;
}

/**
 * Compare date with current date
 *
 *  Note: Only supports strings in the format "dd-M y"
 *
 * @param {string} The date as a string
 * @param {string} The format of the strings supplied, Only supports strings in the format "dd-M y"
 * @returns {number} @see compareDateObjects return, 1 if date2 in future, -1 if date2 is past date, 0 if both the same
 */
function compareWithCurrentDate(dateStr, format) {
    var date1;
    var dateArray;
    if (format == 'dd-M y') {
        if (dateStr != "") {
            dateArray = dateStr.split('-');

            date1 = new Date('20' + dateArray[1].split(' ')[1], getMonthIndex(dateArray[1].split(' ')[0]), dateArray[0]);
        }
    } else {
        alert('Date format not supported by function..');
        return -1;
    }
    return compareDateObjects(new Date(), date1);
}

/**
 * Compares the given String formated dates to determine if greater, lesser, or equal
 *
 * Note: Only supports strings in the format "dd-M y"
 *
 * @param {string} The date as a string
 * @param {string} The date to compare against as a string
 * @param {string} The format of the strings supplied, only supports strings in the format "dd-M y"
 * @returns {number} @see compareDateObjects return, 1 if date2 in future, -1 if date2 is past date, 0 if both the same
 */
function compareDateStrings(date1, date2, format) {
    var dateObj1;
    var dateObj2;
    if (format == 'dd-M y') {
        if (date1 != "") {
            var dateArray = date1.split('-');
            dateObj1 = new Date('20' + dateArray[1].split(' ')[1], getMonthIndex(dateArray[1].split(' ')[0]), dateArray[0]);
        }
        if (date2 != "") {
            dateArray = date2.split('-');
            dateObj2 = new Date('20' + dateArray[1].split(' ')[1], getMonthIndex(dateArray[1].split(' ')[0]), dateArray[0]);
        }
    } else {
        alert('Date format not supported by function..');
        return -1;
    }
    return compareDateObjects(dateObj1, dateObj2);
}

/**
 * Compares 2 date objects with each other to greater or less or if equal
 *
 * @param {object} The date object to check
 * @param {object} The date object to compare against
 * @returns {number} -1 if date1 > date2, 0 if dates the same, 1 if date1 < date2
 */
function compareDateObjects(date1, date2) {
    if (date1 != "" && date2 != undefined) {
        d1 = date1.getDate();
        d2 = date2.getDate();
        m1 = date1.getMonth();
        m2 = date2.getMonth();
        y1 = date1.getFullYear();
        y2 = date2.getFullYear();
        if (y2 < y1) {
            return -1;
        } else if ((y2 == y1) && (m2 < m1)) {
            return -1;
        } else if ((y2 == y1) && (m2 == m1)) {
            if ((d2 < d1)) {
                return -1;
            } else if ((d2 == d1)) {
                return 0;
            }
        }
        return 1;
    }
}


/**
 * Get the Month Index
 *
 * @param {object} A Date object
 * @returns {void|number}
 */
function getMonthIndex(monthVal) {
    switch (monthVal.toUpperCase()) {
        case 'JAN':
            return Number(0);
        case 'FEB':
            return Number(1);
        case 'MAR':
            return Number(2);
        case 'APR':
            return Number(3);
        case 'MAY':
            return Number(4);
        case 'JUN':
            return Number(5);
        case 'JUL':
            return Number(6);
        case 'AUG':
            return Number(7);
        case 'SEP':
            return Number(8);
        case 'OCT':
            return Number(9);
        case 'NOV':
            return Number(10);
        case 'DEC':
            return Number(11);
    }
}

/**
 * Get the day with a # on the front
 *
 * @param {string} The day
 * @returns {string}
 */
function getDayAndMonthval(day) {
    //TODO this variable is not needed
    var strVal = day;
    var subStr = strVal.split('#');
    var dayValue = '#' + subStr[1];
    return dayValue;
}
/**
 * Get the month index
 *
 * @param {string} The month value
 * @returns {integer}
 */
function getAllMonthIndex(monthValue) {
    var valMonthIndex = getMonthIndex(monthValue.split('-')[0]) + 1;
    return valMonthIndex;
}
/**
 * Get the Year index
 *
 * @param {string} the year
 * @returns {string}
 */
function getAllyearValue(yearValue) {
    var yearVal = yearValue.split('-')[1];
    return yearVal;
}















/**
 *
 * @type {object} Object array of Modal buttons
 */
var modalButtons = {Ok: 1, Back: 2, Next: 3, Continue: 4};

/**
 * Show the Modal
 *
 * @param {string} The element id of the Modal to show
 */
function modalShow(id) {
    var heading = arguments[1];
    if (typeof (heading) != 'undefined') {
        var inner = '<div>' + arguments[2] + '</div>';
        var customButton = arguments[3];
        var buttons = '<div class="actions"><a class="button" onclick="modalClose(\'' + id + '\')"><span>{BUTTON}</span></a></div>';
        $('#' + id + ' h4').first().text(heading);
        if (typeof (customButton) != 'undefined') {
            switch (customButton) {
                case modalButtons.Ok:
                    customButton = 'Ok';
                    break;
                case modalButtons.Back:
                    customButton = 'Back';
                    break;
                case modalButtons.Next:
                    customButton = 'Next';
                    break;
                default:
                    customButton = 'Continue';
                    break;
            }
            buttons = buttons.replace('{BUTTON}', customButton);
        } else {
            buttons = buttons.replace('{BUTTON}', getLocalizedMessageUnformatted('Continue', 'msgButtonContinue'));
        }
        $('#' + id + ' h4').text(heading);
        $('#' + id + ' .inner').html(inner + buttons);
    }
    $('#' + id).center();
    adjustedFade('#disabler', 'fadeIn', 200);
    adjustedFade('#' + id, 'fadeIn', 200);
}
/**
 * Close the Modal
 *
 * @param {string} The element id of the Modal to close
 */
function modalClose(id) {
    adjustedFade('#disabler', 'fadeOut', 200);
    adjustedFade('#' + id, 'fadeOut', 200);
    return false;
}




function iframePopupOpen(url){
    $('#iframePopup .inner').html('<span></span><iframe src="' + url + '" frameborder="0" scrolling="auto"></iframe>');
    $('#iframePopup').fadeIn();
    adjustedFade('#disabler', 'fadeIn', 200);
}

function iframePopupClose(){
    $('#iframePopup .inner').html('');
    $('#iframePopup').fadeOut();
    adjustedFade('#disabler', 'fadeOut', 200);
}



/**
 * General Validation Functions
 */

/**
 * Is valid Email
 *
 * @param {element} The email address jquery element to check
 * @returns {boolean}
 */
function isVEM(field) {
    var pattern = /^(.+)@([^\(\);:,<>]+\.[a-zA-Z]{2,4})/;
    if (field.val().length > 0) {
        var result = field.val().match(pattern);
        if (result == null) {
            field.focus();
            return false;
        }
        return true;
    } else {
        return true;
    }
}



function oc(a) {
    var o = {};
    for (var i = 0; i < a.length; i++) {
        o[a[i]] = '';
    }
    return o;
}


/**
 * Close all popups
 *
 * TODO Check if this is infact global or only homepage
 * TODO Check also if any of these have changed or are no longer relevant
 *
 */
function closePopups() {
    //these should be bound to an event that is fired!!!!
    $('.overlay').fadeOut('fast');
    $('.calendar').fadeOut();
    iframePopupClose();
    modalClose('scriptAlert');
    /*
    $('.linkselect-link').each(function () {
        $(this).blur();
    });
    //$('.linkselect-link').siblings('input').linkselect("close");
     */
    //$('#checkInWrap').slideUp(100);
}






/**
 * Change the language on the page
 *
 * TODO This should be updated to use a different form for posting tha page and setting the locale/language
 *
 * @param langCode
 */
function changeLang(language) {
    if ($('#chkRemember').checked) {
        config.isCookieEnabled = true;
        rememberLocation(language, country);
    } else {
        config.isCookieEnabled = false;
        forgetLocation(false);
    }
    //TODO This is a form update, so needs to be checked!!!
    $('#selLanguage').val(language);
    document.forms.bbFlights.action = relPath + "/" + country.toLowerCase() + "/" + language.toLowerCase() + "/home!loadCountryLanguage.action?request_locale=" + language.toLowerCase() + "_" + country.toUpperCase() + "&splashLocale=" + language + "&splashCntry=" + country + "&isCookieEnabled=" + ((isCookieEnabled) ? "true":"false") + "&langChanged=true";
    document.forms.bbFlights.submit();
}


/**
 * Forget the location
 *
 * @param toggle Toggle the display of the remember/forget section
 */
function forgetLocation(toggle) {
    //clearing cookie
    setCookie('rememberLocale', '', -1, '/');
    setCookie('rememberLang', '', -1, '/');
    //toggle remember effects off
    if (typeof toggle != 'undefined' && toggle == true) {
        toggleRememberDiv(false);
    }
}

/**
 * Remember the location and language
 *
 * @param country The country code
 * @param language The language code
 */
function rememberLocation(language, country)
{
    forgetLocation(false);
    //setting new cookie
    setCookie('rememberLang', language.toLowerCase(), 11, '/');
    setCookie('rememberLocale', country.toLowerCase(), 11, '/');
    //update the html
    updateLocationHtml(language, country);
    //toggle remember effects on
    toggleRememberDiv(true);
}

/**
 * Toggles the remember the user location preference
 *
 * @param remember
 */
function toggleRememberDiv(remember){
    if (typeof remember !== 'undefined' && remember == true) {
        $('#rememberDiv').hide();
        $('#savedDiv').show();
        $('#chkRemember').attr('checked', 'checked');
        $('#rememberDiv span.check').removeClass("un");
    } else {
        $('#rememberDiv').show();
        $('#savedDiv').hide();
        $('#chkRemember').removeAttr('checked');
        $('#rememberDiv span.check').addClass("un");
    }
}

/**
 * Updates the text in the header to show the language
 *
 * @param language
 */
function updateLocationHtml(language, country)
{
    $('#currentLoc').attr('class', country.toLowerCase());
    //update the selected language
    $('#dropLangNav .code .text').html(language.toUpperCase());
    $('#dropLangNav li.active').removeClass('selected');
    $('#dropLangNav a.' + language.toUpperCase()).addClass('selected');
}


/**
 * Load an xml response from the server, run the callback if success or display a message if not
 *
 * @param url The url to get the xml data from
 * @param callback The function to run on success
 * @param responseFormat The type of ajax response to expect and interpret
 * @param failMsg The failed message to display. If not set, a default message of "No matching region found."
 * @param params The parameters to send
 */
function loadXmlResponseAndUpdate(url, callback, responseFormat, failMsg, params){

    $.ajaxSetup({
        cache: false
    });

    var defaultParams = {localeSeltd : config.language};

    //set the detaults
    if(typeof responseFormat == 'undefined' || responseFormat == null) {
        responseFormat = 'xml';
    }
    if (typeof failMsg == 'undefined' || failMsg == null) {
        failMsg = "No matching region found.";
    }

    if (typeof params == 'undefined' || params == null) {
        params = defaultParams;
    } else {
        params = $.extend({}, defaultParams, params);
    }

    $.get(url, params, function(response, status){
        if (status == "success") {
            callback(response);
        } else {
            alert(failMsg);
        }
    }, responseFormat);
}



/**
 * Style the selects
 *
 * This also binds the change event of the select to refresh the styled effect
 *
 * @param e The element(s) to style
 */
function styleSelect(e) {
    if (!iDevice) {
        $(e).linkselect();
    }
}

/**
 * Style the element with a combobox
 *
 * @param e The element(s) to style
 */
function styleSelectCombo(e) {
    $(e).combobox();
}


/**
 * Set the date picker with the dates found in the inputs next to the calendar link
 *
 * @param el The Calendar element to make a datepicker from
 * @param ob The options to pass to the date picker on create
 */
function setDatePicker(el, ob)
{
    if (typeof ob == 'undefined') {
        ob = {};
    }
    $(el).datepicker(ob);
    $(el).click(function (e) {
        e.stopPropagation();
    });
    $(el).parent().click(function(e){

        // This checks whether the Return Date block is disabled
        if ($(this).parent().hasClass('disabled')) return false;

        // Don't auto-close this datepicker
        e.stopPropagation();

        // Close all the other modals though
        //closePopups();

        date = new Date();

        if (iDevice) {
            var dayInput = $(el).parents('.input').find('.day select');
            var monthYearInput = $(el).parents('.input').find('.month-year select');
        } else {
            var dayInput = $(el).parents('.input').find('.day input');
            var monthYearInput = $(el).parents('.input').find('.month-year input');
        }

        // Force the string to be sure IE understands what we're doing... *sigh*
        // This spits out something like: "14-Jan-2012"
        var update = $(dayInput).val() + '-' + $(monthYearInput).val();
        //console.log(update);

        // Check if this if the date is a *valid* date string.
        // Fail if it is not (eg: 31 February 2011) - yay for the drop downs.
        try {
            calDate = $.datepicker.parseDate('dd-M-yy', update, {
                //Due to how the date picker works, if it is localised it won't accept a different localised value if
                //passed to it. If the date picker is in French, it would need "15-Mai-2015" to be valid.
                //This line allows us to pass the english value in
                monthNamesShort: $.datepicker.regional[ "en" ].monthNamesShort
            });
        } catch (e) {
            alert(propsArray['bookingBlock_14']);
            // the datepicker doesnt parse when M is localized,eg: Dezember,Marz,Mai in deutsch.
            return false;
        }

        // Here, we know the date is valid! So..
        // Update the datepicker.
        $(this).children('.calendar').datepicker('setDate', calDate);

        $cal = $(this).children('.calendar:not(.disabled)');

        if (typeof ob.onBeforeFadeIn !== 'undefined') {
            ob.onBeforeFadeIn(function(){
                $cal.fadeIn();
            });
        } else {
            // ..and fade it in!
            $cal.fadeIn();
        }

    });
}


/**
 * Update the dates next to the calendar pop up link
 *
 * @param c The Calendar element. Used to find the inputs to update
 * @param d The date object
 */
function updateDates(c, d) {
    var day = ('0' + d.getDate()).slice(-2);
    var monthYear = monthArrays['en'][d.getMonth()] + '-' + d.getFullYear();

    if (iDevice) {
        var dayInput = $(c).parents('.input').find('.day select');
        var monthYearInput = $(c).parents('.input').find('.month-year select');
    } else {
        var dayInput = $(c).parents('.input').find('.day input');
        var monthYearInput = $(c).parents('.input').find('.month-year input');
    }

    $(dayInput).changeVal(day);
    $(monthYearInput).changeVal(monthYear);
}

/**
 * Creates a formatted date from the given values
 *
 * @param day The day
 * @param monthYear Month Year value as "Jan-2010"
 * @param format The format wanted. Defaults to the global variable dateFormat
 * @returns {String} The date formatted
 */
function getFormatedDate(day, monthYear, format)
{
    if (typeof format == 'undefined') {
        format = dateFormat;
    }
    return $.datepicker.formatDate(format, new Date(day + ' ' + monthYear), {
        monthNamesShort: $.datepicker.regional[ "en" ].monthNamesShort
    });
}

/**
 * Creates a formatted date from the given values
 *
 * @param date The date object
 * @returns {String} The date formatted
 */
function getFormatedDateFromObject(date)
{
    if (typeof format == 'undefined') {
        format = dateFormat;
    }
    return $.datepicker.formatDate(dateFormat, date, {
        monthNamesShort: $.datepicker.regional[ "en" ].monthNamesShort
    });
}



/**
 * Update the Month Year selects to have the translated months
 *
 * @param e The element to the selects to update
 */
function updateMonthYearSelects(e) {
    var dates = [], values = [], now = new Date(), option = new String();
    // February bug
    now.setDate(28);
    //clear them
    $(e).html('');
    // Loop through 13 months. (eg: December 2011 => December 2012)
    for (var i = 0; i < 13; i++) {
        values[i] = monthArrays['en'][now.getMonth()] + '-' + now.getFullYear();
        dates[i] = months[now.getMonth()] + ' ' + now.getFullYear();
        option = '<option value="' + values[i] + '">' + dates[i] + '</option>';
        $(e).append(option);
        now.setMonth(now.getMonth() + 1)
    }
}

/**
 * Update the Month Year selects to have the translated months
 *
 * @param e The element to the selects to update
 */
function updateDaySelects(e) {
    var option = new String();
    //clear them
    $(e).html('');
    // Loop through 13 months. (eg: December 2011 => December 2012)
    for (var i = 1; i < 32; i++) {
        var d = '0' + i.toString();
        option = '<option value="' + d.substr(-2) + '">' + i + '</option>';
        $(e).append(option);
    }
}


/**
 * Updates select input options with a new list
 *
 * @param elm The select element
 * @param elms {object|string} The list of options to update with
 * @param first_text The text to display in the first option
 */
function updateSelectOptions(elm, elms, first_text) {
    $(elm).html('');
    //TODO This may need to be updated to support the ability to customise adding this or not
    if (typeof first_text !== 'undefined') {
        $(elm).append('<option value="">' + first_text + '</option>');
    }
    if (typeof elms == 'string') {
        $(elm).append(elms);
    } else {
        $(elms).each(function () {
            $(elm).append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
        });
    }
}


/**
 * Redirect to the Hotels URL
 */
function goToHotelsURL() {
    var url = "http://clk.tradedoubler.com/click?p=250485&a=2414975&g=18488210&url=http://za.hotels.com/?rffrid=AFF.HCOM.ZA.001.000.2414975.18488210.SAA";
    window.open(url);
    return false;
}


/**
 * Toggle the return files
 *
 * @param enabled
 */
function toggleReturnFields(enabled, el) {
    $container = $(el).parents('.datepicker').find('> div');
    if (iDevice) {
        if (enabled) {
            $($container).show();
        } else {
            $($container).hide();
        }
    } else {
        if (enabled) {
            $($container).find('input').linkselect('disable', false);
            $($container).find('.calendar').datepicker('enable').removeClass('disabled');
            $($container).fadeTo(100, 1).css('cursor', 'pointer').removeClass('disabled');
        }
        else {
            $($container).find('input').linkselect('disable', true);
            $($container).find('.calendar').datepicker('disable').addClass('disabled');
            $($container).fadeTo(100, 0.5).css('cursor', 'default').addClass('disabled');
        }
    }
}








/**
 * Deprecated Functions
 */

/**
 * Forget the Locale and Language for the user
 *
 * TODO Rename this function
 *
 * @deprecated  This is no longer used, use forgetLocation
 */
function forgetMyLoc() {
    forgetLocation();
}



/**
 * Update given selects
 *
 * @param e The elements to loop through
 * @param x The elements to make a combobox
 * @param y The elements exclude
 */
function styleSelects(e, x, y) {
    return;


    $(e).each(function () {
        id = '#' + $(this).attr('id');
        if (!(id in oc(y))) {
            if ($.browser.msie && $.browser.version < 7) {
                $(this).linkselect()
            } else {
                if (id in oc(x)) {
                    $(this).combobox();
                } else {
                    //$(this).linkselect();
                    $(this).selectric();
                }
            }
        }
    });
}



//TODO Check and refactor this
function popEssInfo(locale, country) {
    var essential = new String();
    if (country == 'default') {
        essential = relPath + "/cms/ZA/routeSpecific/IncludeCMS_Content.jsp?template=ESSENTIALINFO&locale=en&country=ZA";
        tryAgain = false;
    } else {
        essential = relPath + "/cms/ZA/routeSpecific/IncludeCMS_Content.jsp?template=ESSENTIALINFO&locale=" + locale + "&country=" + country;
        tryAgain = true;
    }
    $.ajax({
        url: essential, success: function (data) {
            loadEssInfo(data)
        }, error: function (data) {
            errorEssInfo(data, tryAgain)
        }
    });
}
function loadEssInfo(data) {
    $('#essentialInfoLinks').html('');
    $(data).find('a').each(function () {
        val = $(this).attr('href')
        name = $(this).text();
        $('#essentialInfoLinks').append('<li><a href="' + val + '">' + name + '</a></li>');
    });
}
function errorEssInfo(data, tryAgain) {
    if (tryAgain) {
        setTimeout("popEssInfo('','default')", 1000);
    } else {
        $('#essentialInfoLinks').html('');
        //TODO this should use language translation
        $('#menu nav .essential-info').html('<a href="#">Essential Info</a>');
    }
}






/*
 * This method is used to load the Global Emergency notice.
 * This should be called on page load
 *
 * TODO Don't have the styling for this currently
 */
function populateGlobalNotice() {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadGlobalEmergencyXmlFile.action", function(response)
    {
        $(response).find('notice').each(function () {
            $("#globalNoticeDesc").html($(this).text());
            globalNoticeUrl = $(this).attr('value');
            $("#globalnoticediv").show();
        });

    }, 'xml', null, {
        localeSeltd: config.language
    });
}


/*
 * This method will be called when the user clicks the 'more info'
 * tab for getting the details of the Global Emergency Notice.
 */
function cmsPageForGlobalNotice() {
    var cmsUrlForGlobalNotice = globalNoticeUrl;
    /*
     * modifying the code assuming that the user will enter the correct url after rewriting for the news page in the below format say,
     * For pages inside ZA folder: http://www.flysaa.com/$country/$language/emergency/test.html (rewriten url)
     * For pages outside ZA folder: http://test.flysaa.com/cms/common/test.html(exact url)
     */
    if (cmsUrlForGlobalNotice != null && $.trim(cmsUrlForGlobalNotice).length > 0) {
        if (cmsUrlForGlobalNotice.indexOf("$") >= 0) {
            cmsUrlForGlobalNotice = cmsUrlForGlobalNotice.replace("$country", config.country.toLowerCase());
            cmsUrlForGlobalNotice = cmsUrlForGlobalNotice.replace("$language", config.language);
        }
        window.open(cmsUrlForGlobalNotice, "_self");
    } else {
        window.location.href = "#";
    }
}



/*
 * This method is used to load the Local Emergency notice.
 * This should be called on page load
 *
 * TODO Don't have the styling for this currently
 */
function populateLocalNotice() {

    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadLocalEmergencyXmlFile.action", function(response)
    {
        $(response).find('notice').each(function () {
            $("#globalNoticeDesc").html($(this).text());
            globalNoticeUrl = $(this).attr('value');
            $("#globalnoticediv").show();
        });


        $(response).find('notice').each(function () {
            $("#localNoticeDesc").html($(this).text());
            //$("#globalNoticeUrl").html($(this).attr('value'));
            localNoticeUrl = $(this).attr('value');
            $("#localnoticediv").show();
        });

    }, 'xml', null, {
        localeSeltd: config.language,
        localNoticeCntry: config.country.toUpperCase()
    });
}

/*
 * This method will be called when the user clicks the 'more info'
 * tab for getting the details of the Local Emergency Notice.
 */
function cmsPageForLocalNotice() {
    var cmsUrlForLocalNotice = localNoticeUrl;

    /*
     * modifying the code assuming that the user will enter the correct url after rewriting for the news page in the below format say,
     * For pages inside ZA folder: http://www.flysaa.com/$country/$language/emergency/test.html (rewriten url)
     * For pages outside ZA folder: http://test.flysaa.com/cms/common/test.html(exact url)
     */
    if (cmsUrlForLocalNotice != null && $.trim(cmsUrlForLocalNotice).length > 0) {

        if (cmsUrlForLocalNotice.indexOf("$") >= 0) {
            cmsUrlForLocalNotice = cmsUrlForLocalNotice.replace("$country", config.country.toLowerCase());
            cmsUrlForLocalNotice = cmsUrlForLocalNotice.replace("$language", locale);
        }
        window.open(cmsUrlForLocalNotice, "_self");
    } else {
        window.location.href = "#";
    }
}



