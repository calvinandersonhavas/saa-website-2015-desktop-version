/**
 * Header
 *
 * This contains all relevant code for handling the header javascript of the SAA Website
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/09
 */
$(document).ready(function(){

    //bind to the language dropdown
    $('#dropLang a').click(function(){
        changeLang(this.hash.substring(1).toUpperCase());
    });
    //bind to the remember and forget elements
    $('#regionNav .forget a').click(function(){
        forgetLocation(true);
    })
    $('#chkRemember').change(function () {
        if ($(this).prop('checked')) {
            rememberLocation(language, country);
        } else {
            forgetLocation(true);
        }
    });

    //bind to the search field
    $('#searchInput').on('keyup', function(event){
        if (event.keyCode == 13) {
            searchFlysaa();
        }
    });
    $('#searchButton').click(function(){
        searchFlysaa();
    });
    /*
    $('#searchInput').click(function(){
        $(this).value('');
    });
    $('#searchInput').onblur(function(){
        if ($(this.value() == '')) {
            $(this).value($(this).data('value'));
        }
    });
    */

    //TODO This should rather run all server side and be sent with the html on page download
    loadHomeLangXML();
    //update the region menu
    populateMegaMenu();

    //setup the region tabs
    $('#regionNav .tabs').tabs({
        fx: {
            opacity: 'toggle',
            duration: 'fast'
        },
        select: function () {
            // This fixes a bug where the adspace images sometimes don't show
            //TODO need to check why this happens
            //$('.adSpace').hide().show();
        }
    });














});

















/**
 * Search Bar
 *
 * When a user enters in a value in the Search Bar, redirect the user to the search url with what they entered
 */
function searchFlysaa() {
    var i = $('#searchInput').val();
    var e = encodeURIComponent(i);
    window.location.href = relPath + "/cms/commons/flysaa_search.html?searchQuery=" + e;
}



/**
 * Loads the list of languages to display in the dropdown for the current language
 */
/*
//this are replaced by the function call below
function loadHomeLangXML() {
    $.ajaxSetup({
        cache: false
    });
    var params = {
        localeSeltd : config.language
    };
    var url = relPath + "/splashPageAction!loadLangXmlFile.action";
    $.get(url, params, getHomeLangXmlValues, 'xml');
}
function getHomeLangXmlValues(XMLResponse, status) {
    if (status == "success") {
        parseHomeLangXMLResponse(XMLResponse);
    } else {
        alert("No matching region found.");
    }
}
function parseHomeLangXMLResponse(xml) {
    var langCode = '', langDesc = '';
    var selLang = config.language;
    $('#dropLang').html('');
    $(xml).find('language').each(
        function () {
            langDesc = $(this).text();
            langCode = $(this).attr('value');
            if (selLang != langCode.toLowerCase()) {
                $('#dropLang').append('<li><a  onclick="changeLang(' + "'" + langCode + "'" + ');" href="#' + langCode.toLowerCase() + '">' + langDesc + '</a></li>');
            }
        }
    );
}
*/
function loadHomeLangXML(){
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadLangXmlFile.action", function(xml) {
        var langCode = '', langDesc = '';
        $('#dropLang').html('');
        $(xml).find('language').each(
            function () {
                langDesc = $(this).text();
                langCode = $(this).attr('value');
                if (config.language != langCode.toLowerCase()) {
                    $('#dropLang').append('<li><a  onclick="changeLang(' + "'" + langCode + "'" + ');" href="#' + langCode.toLowerCase() + '">' + langDesc + '</a></li>');
                }
            }
        );
    })
}





/**
 * The Region Drop Down and related functions
 *
 * @depends ???
 */


/**
 * Changes Region Mega Menu at Home page
 */
function populateMegaMenu(){
    //TODO refactor where these two variables are collected from
    /*
    var selectedCountry = config.country;
    var selectedLang = config.language;
    if(selectedCountry == null || selectedCountry == "")
    {
        selectedCountry = "ZA";
    }
    if(selectedLang == null || selectedLang == "")
    {
        selectedLang = "EN";
    }
    //loadXMLforCDesc(selectedLang);*/
    //loadPageLabels();
    loadRegionXML();

}

/**
 * Load the Region XML and update the page
 *
 */
function loadRegionXML(){
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadRegionXmlFile.action", function(response)
    {
        //NOTE This could be made into a single data set and avoid the split
        xml =  (response).split('CACHEDELIMITER');

        // for populating car countries
        //TODO Move this to its own section
        //$('#carCountry').html('');
        //country = $("#countryResHeaderId").html();
        //code = '';
        //$('#carCountry').append('<option value="' + code + '">' + country + '</option>');
        //TODO move to its own location and reference the list of countries
        //$('#carCountry').append('<option value="'+code+'">'+country+'</option>');

        //get the countries
        xmlDoc = $.parseXML(xml[1]);
        $countryXml = $(xmlDoc);
        $($countryXml).find('country').each(function(){
            countries[$(this).attr('value')] = $(this).text();
        });

        if (countries[config.country]) {
            //$('#currentLoc').html(countries[config.country]);
            config.countryLong = countries[config.country];
            $('#savedLocale').html(config.countryLong);
        }

        //Get the regions and update the countries for each region on the page
        xmlDoc = $.parseXML(xml[0]);
        $newRegionXml = $(xmlDoc);
        var regionTabs = [];
        $($newRegionXml).find('region').each(function(){
            var regionCode = $(this).attr('value');
            var regionName = $(this).find('region_name').text();

            $('#regionNav .tabs li.' + regionCode + ' a').html(regionName);
            //add to global regions
            regions[regionCode] = regionName;
            //update countries for this region
            regionTabs[regionCode] = [];
            $(this).find("country").each(function(){
                regionTabs[regionCode][$(this).attr('value')] = $(this).text();
            });
        });

        //update all the regions with their countries
        for (var regionCode in regionTabs) {
            var $region = $('#region-' + regionCode.toLowerCase() + ' ul');
            $region.html('');
            for (var countryCode in regionTabs[regionCode]) {
                $region.append('<li><a href="#" onclick="changeCountry(' + "'" + countryCode + "'" + ')">' + regionTabs[regionCode][countryCode] + '</a></li>');
            }
        }
    }, 'html');
}


/**
 * Change the country the user is in.
 *
 * Used with the header and when users click on a new country
 *
 * @param countryCode
 */
function changeCountry(countryCode) {
    updateCookieAndRedirect(config.language, countryCode);
    return false;
}


/**
 * Update the cookie if remember is set and redirect the user to the new URL
 *
 * @param newCountryCode The new country code
 * @param newLanguageCode The new language code
 */
function updateCookieAndRedirect(newLanguageCode, newCountryCode)
{
    rememberLocation(newLanguageCode, newCountryCode);
    if ($('#chkRemember').prop('checked')) {
        isCookieEnabled = "true";
        rememberLocation(newLanguageCode, newCountryCode);
    } else {
        isCookieEnabled = "false";
        forgetLocation(false);
    }
    //TODO this should use a correct form name to avoid any confusions. This should also be changed to a universal function if this function can't be it
    var conLanForHome = "/" + newCountryCode.toLowerCase() + "/" + newLanguageCode.toLowerCase();
    document.forms.bbFlights.elements.country.value = newCountryCode;
    document.forms.bbFlights.action = relPath + conLanForHome + "/home!loadCountryLanguage.action?request_locale=" + newLanguageCode.toLowerCase() + "_" + newCountryCode.toUpperCase() + "&splashLocale=" + newLanguageCode + "&splashCntry=" + newCountryCode + "&isCookieEnabled=" + isCookieEnabled;
    document.forms.bbFlights.submit();
}

