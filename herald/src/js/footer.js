

$(document).ready(function(){

    //determine if the footer is above the bottom of the window and if so, absolute it to the bottom
    $( window ).resize(function() {
        adjustFooter();
    });

    //adjustFooter();

})

function adjustFooter(){
    //position: absolute; width: 100%; bottom: 0;
    var h = $('#footer').offset();
    if ((h.top + $('#footer').height()) < $('body').height()) {
        $('#footer').css({
            position: 'absolute',
            bottom: '0'
        })
    } else {
        $('#footer').css({
            position: 'relative',
            bottom: 'auto'
        })
    }
}