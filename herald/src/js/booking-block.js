/**
 * Booking Block
 *
 * This contains all relevant code for handling the booking block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function(){

    // Instantiate the Meta Block & List of Locations' tabs
    $('#bb').tabs({
        fx: {
            opacity: 'toggle',
            duration: 'fast'
        }
    });

    //update the destinations, flight status cities, status cities
    loadHomePageXmls(config.language, config.country);

});


function getDepartDate() {
    var departMonthYear = $('#departMonthYear').val().split('-');
    return new Date(departMonthYear[1], getMonthIndex(departMonthYear[0]), $('#departDay').val());
}

function getReturnDate() {
    var returnMonthYear = $('#returnMonthYear').val().split('-');
    return new Date(returnMonthYear[1], getMonthIndex(returnMonthYear[0]), $('#destDay').val());
}

/**
 * Updates several booking block selects with applicable data
 *
 * NOTE: This should be broken up into several separate calls/functions to allow the blocks to be loaded dynamically
 *
 * @param locale
 * @param country
 */
function loadHomePageXmls(locale, country) {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadHomePageXmlFiles.action", function(response) {
        xml = (response).split('CACHEDELIMITER');

        // to populate check flight status
        xmlFlightStatus = $.parseXML(xml[1]);
        updateFlightStatus(xmlFlightStatus);

        // Check in status cities
        xmlChkInStatus = $.parseXML(xml[2]);
        updateChkInStatus(xml);

        // to populate departure cities
        //TODO This is currently returning null
        xmlDestinations = $.parseXML(xml[3]);
        updateDepartDestCities(xml[3]);

    }, 'html', null, {
        localeSeltd: locale,
        country: country
    });
}

/**
 * Update the Checkin Status Selects
 *
 * @param xml
 */
function updateChkInStatus(xml) {
    var elms = $(xml).find('checkinpoint');
    updateSelectOptions("#IBoardPoint", elms, getLocalizedMessageFromKey('bookingBlock_boardPoint'));
}

/**
 * UPdate the Flight Status Selects
 *
 * @param xml
 */
function updateFlightStatus(xml) {
    var elms = $(xml).find('point');
    updateSelectOptions("#statusDepartCity", elms, getLocalizedMessageFromKey('bookingBlock_departCity'));
    updateSelectOptions("#statusDestCity", elms, getLocalizedMessageFromKey('bookingBlock_destinationCity'));
}

/**
 * Update the Destination and Depart City Selects
 *
 * @param xml
 */
function updateDepartDestCities(xml){
    var elms = $(xml).find('city');
    updateSelectOptions("#departCity", elms, getLocalizedMessageFromKey('bookingBlock_departCity'));
    updateSelectOptions("#destCity", elms, getLocalizedMessageFromKey('bookingBlock_destinationCity'));
}


/**
 * Plugin to auto style the required fields in a form
 *
 */
(function($) {
    // Plugin definition.
    $.fn.blockform = function(options) {

        /**
         * The state of the object
         *
         * @type {null}
         */
        var state = null;

        /**
         * The detaults
         *
         * @type {Object}
         */
        var defaults = {
            comboboxInputs: [],
            selectricInputs: [],
            autocompleteInputs: {},
            dateInputs: {},
            dateDefaults: {
                onBeforeFadeIn: function(callback){
                    $('#bb .overlay').fadeIn('fast', callback);
                },
                onAfterFadeOut: function(){
                    $('#bb .overlay').fadeOut('fast');
                }
            },
            afterInit: function(){}
        };
        /**
         * The settings
         *
         * @type {Object}
         */
        var settings = $.extend( {}, defaults, options );

        var comboboxInputs = [];

        initComboBoxInputs = function(){
            var elms = settings.comboboxInputs;
            for (var i in elms) {
                styleSelectCombo(elms[i]);
                comboboxInputs.push($(elms[i]).attr('id'));
            }
        }

        initSelectricInputs = function(){
            var elms = settings.selectricInputs;
            for (var i in elms) {
                $(elms[i]).each(function(){
                    if ($.inArray($(this).attr('id'), comboboxInputs) == -1) {
                        styleSelect(this);
                    }
                });
            }
        }

        initAutocompleteInputs = function() {
            var elms = settings.autocompleteInputs;
            for (var i in elms) {
                if (elms.hasOwnProperty(i)) {
                    $(i).autocomplete(elms[i]);
                } else {
                    $(elms[i]).autocomplete();
                }
            }
        }

        initDateInputs = function() {
            var elms = settings.dateInputs;
            for (var i in elms) {
                var params = settings.dateDefaults;
                if (elms.hasOwnProperty(i)) {
                    $.extend(params, elms[i]);
                    setDatePicker(i, params);
                } else {
                    setDatePicker(elms[i], params);
                }
            }
        }

        this.state = 'loaded';

        /**
         * Initialise the block and run the standard init calls
         *
         */
        init = function() {
            if (settings.comboboxInputs.length > 0)
                initComboBoxInputs();
            if (settings.selectricInputs.length > 0)
                initSelectricInputs();
            if (!$.isEmptyObject(settings.autocompleteInputs))
                initAutocompleteInputs();
            if (!$.isEmptyObject(settings.dateInputs))
                initDateInputs();
        }

        //$('#bb .overlay').addClass('loading').fadeIn('fast');
        settings.beforeInit();
        init();
        settings.afterInit();
        $('#bb .overlay').fadeOut('fast', function(){
            $(this).removeClass('loading');
        });
    };
// End of closure.
})(jQuery);








/**
 * This method is the callback fucntion configured in the $.get() method. This
 * will be called by the Ajax framework, if the ajax call returned a success. By
 * default two values are passed into this fucntion : The response data and
 * status.
 *
 * @deprecated
 */
function filterFlexiSearchOption(matrixOrTdp, status) {
    //NOTE This conditional would never have been met!!!
    matrixOrTdp = 'TDP';
    if (matrixOrTdp == 'Matrix') {
        //isMatrixSearch	= true;
        //$('.flexibility .flex').fadeOut();
        $('input[name="flexible"][value="true"]').prop('checked', true);
        var flexibleGrp = $('input[name="flexible"]');
        if (flexibleGrp) {
            flexibleGrp[0].checked = true;
        }
    } else {
        checkFlexiOption();
    }
    if ($.trim($("#destCity").val()).length > 0) {
        populateCabinClass();
    }
}
