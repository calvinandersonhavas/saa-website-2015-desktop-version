/**
 * Check In Block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {

    $('#voyager .loadingIndicator').fadeOut();

    loadVoyagerInfo("voyagerInfo");

    if($('#isHomeVoyLogin').val()=="Y"){
        var name=$('#nameInitial').val() +' ' + $('#name').val();
        $('#voyager').addClass('loggedin');
        $('#memName span').html(name);
        $('#memNum').html('<span></span>'+ $('#membrshpNum').val());
        $('#memMiles').html('<span></span>'+$('#miles').val());
        $('#memStatus').html('<span></span>'+$('#currenttierid').val());
        $('#memStatus').addClass($('#status').val());
        $('#voyager .in').css('display','block');
        $('#voyager .out').css('display','none');

    } else {

        $('#bb-log-in a.forgot').click(function(){
            $('#bb-log-in form[name="voyagerLogin"]').slideUp();
            $('#bb-log-in form[name="voyagerForgot"]').fadeIn();
            return false;
        });

        $('#bb-log-in .back').click(function(){
            $('#bb-log-in form[name="voyagerLogin"]').slideDown();
            $('#bb-log-in form[name="voyagerForgot"]').fadeOut();
            return false;
        });

        $('#bb-log-in form[name="voyagerLogin"]').submit(function(){

// Remove all the styling from the inputs pre-validation.
            $('#pin, #voyagerId').removeClass('invalid');

            if ($('#voyagerId').val().length==0){
                alert(getLocalizedMessage("The voyager number field is empty.","voylogin_usename"));
                $('#voyagerId').addClass('invalid');
                return false;
            }

            if(($('#voyagerId').val().length < 6) || ($('#voyagerId').val().length > 15)){
                alert(getLocalizedMessage("The voyager number is not valid.","voyLogin_usename3"));
                $('#voyagerId').addClass('invalid');
                return false;
            }

// Voyager number must be a number
            if(isNaN($('#voyagerId').val())){
                alert(getLocalizedMessage("The voyager number should be a Number.","voylogin_usename2"));
                $('#voyagerId').addClass('invalid');
                return false;
            }
            if ($('#pin').val().length==0){
                alert(getLocalizedMessage("The PIN field is empty.","voylogin_pin"));
                $('#pin').addClass('invalid');
                return false;
            }
// Voyager PIN must be at least 4 characters long
            if($('#pin').val().length<4){
                alert(getLocalizedMessage("The PIN number may not be less than 4 characters.","voylogin_pin2"));
                $('#pin').addClass('invalid');
                return false;
            }

// Voyager PIN must be a number
            if(isNaN($('#pin').val())){
                alert(getLocalizedMessage("The PIN should be a Number.","voylogin_pin3"));
                $('#pin').addClass('invalid');
                return false;
            }
            checkVoyagerLoginTab();
            return false;
        });}
});


function checkVoyagerLoginTab(){

    document.forms['voyagerLogin'].action = conLanPath+"/voyagerloginaction!voyagerloginaction.action";
    document.forms['voyagerLogin'].target = "_parent";
    document.forms['voyagerLogin'].submit();

}
function checkJoinVoyager() {

    document.forms['voyagerLogin'].action = conLanPath+"/voyager!registration.action";
    document.forms['voyagerLogin'].target = "_parent";
    document.forms['voyagerLogin'].submit();

}
function goToVoyHelpDesk()
{
    document.forms['voyagerLogin'].action = "/splashPageAction!goToVoyHelpDesk.action";
    document.forms['voyagerLogin'].target = "_parent";
    document.forms['voyagerLogin'].submit();
}

/**
 *Load OpenCMS Voyager Info content using Jquery
 *
 */
function loadVoyagerInfo(id){
    var ajax_load = "<img class='loading' src='/images/processing-loading.gif' alt='loading...' />";
    var selectedCnt = config.country;
    var langSelected = config.language.toUpperCase();
    var template		= "common";
    var cmsParam={};
    cmsParam["country"] = selectedCnt;
    cmsParam["locale"] = langSelected;
    cmsParam["template"] = template;
    cmsParam["id"] = id;
    $("#"+id).html(ajax_load).load("/cms/ZA/routeSpecific/IncludeCMS_Content.jsp",cmsParam);
}