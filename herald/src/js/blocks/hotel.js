/**
 * Book a hotel room
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {

    $('#bb-hotels form').blockform({
        comboboxInputs: [
            '#hotelDestination'
        ],
        selectricInputs: [
            '#bb-hotels form select',
        ],
        dateInputs: {
            "#bb-hotels .calendar": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);

                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();

                    // If the date selected was the checkin date.
                    if ($(this).hasClass('check-in')) {
                        // Update the drop downs of the returning date too!
                        // Makes sure impossible dates don't happen by accident, basicaly.
                        var $cal = $('#bb-hotels .calendar.check-out');
                        $cal.datepicker("option", "minDate", date);
                        updateDates($cal, dateObj);

                        // Show the leave date calendar after half a second
                        // The timeout is there to wait for the other calendar to fade out
                        setTimeout("$('#bb-hotels .calendar.check-out').parent().click()", 500);
                    }

                    $('#bb .overlay').fadeOut('fast');
                }
            }
        },
        beforeInit: function(){
            updateMonthYearSelects('#bb-hotels .month-year select');
            updateDaySelects('#bb-hotels .day select');
        },
        afterInit: function(){
            updateDates('#bb-hotels .calendar', today);
            updateGuests();

            $('#bb-hotels .hotel-guests').on('change', function(){
                updateGuests();
            });

            $('#checkinDay').on('change', function(){
                $('#checkoutDay').changeVal($(this).val());
            });
            $('#checkinMonthYear').on('change', function(){
                $('#checkoutMonthYear').changeVal($(this).val());
            });

            $('#hotelDestination').siblings('input').keyup(function (event) {
                loadHotelDestination($(this).val());
            });
        }
    });

    $('#bb-hotels form').submit(function () {
        var valMsg = '';
        var error = false;
        var chkInDate = getCheckInDate();
        var chkOutDate = getCheckOutDate();

        //TODO This is a callback function which may never even get to update the values on the page before submission!!!!
        //updateDestinationCityDesc();

        $('#hotelDestination').siblings('input').removeClass('invalid');

        //validate the destination is ok
        if ($('#hotelDestination').val().length == 0) {
            $('#hotelDestination').siblings('input').addClass('invalid');
            valMsg = getLocalizedMessage("Please provide the hotel destination.", "hotelDest_1");
            error = true;
        }


        //validate the dates selected
        if (compareDateObjects(chkInDate, chkOutDate) == -1) {
            valMsg += getLocalizedMessage("Please choose valid dates.", "bookingBlock_14");
            error = true;
        }

        //show error if present
        if (error) {
            alert(valMsg);
            return false;
        }

        var numberOfNights = Math.ceil((chkOutDate.getTime() - chkInDate.getTime()) / (1000 * 60 * 60 * 24));

        var url = "http://www.flysaa.com/saacui/HotelSearchExternal.do?isNewSearchFlow=true&pos=SAACUI-ZAF&lang=en&hotelSearchDestinationType=IAN_HOTELS&hotelSearchDestination=" + $('#txtHotelDes_loc').val() +
            "&hotelSearchDestinationCode=" + $('#hotelDestination').val() +
            "&numberOfNights=" + numberOfNights +
            "&hotelSearchNumberOfRooms=" + $('#numberoffrooms').val() +
            "&guestTypeBeans[1].guestTypes[0].type=10.AQC&guestTypeBeans[1].guestTypes[0].amount=" + $('#hotelAdults').val() +
            "&guestTypeBeans[1].guestTypes[1].type=8.AQC&guestTypeBeans[1].guestTypes[1].amount=" + $('#hotelChildren').val() +
            "&guestTypeBeans[1].guestTypes[2].type=7.AQC&guestTypeBeans[1].guestTypes[2].amount=" + $('#hotelInfants').val() +
                //TODO Check that the date format of this is correct!!!
            "&checkinDate=" + $.datepicker.formatDate(chkInDate, 'd/m/y') + //$('#checkinDay').val() + "/" + chkInMonthIndex + "/" + chkInYear +
            "&checkoutDate=" + $.datepicker.formatDate(chkOutDate, 'd/m/y'); //$('#checkoutDay').val() + "/" + chkOutMonthIndex + "/" + chkOutYear;

        window.open(url, "_self");
        return false;
    });

    $('#bb-hotels form .button.reset').click(function () {
        $('#hotelDestination').siblings('input').removeClass('invalid');
        $("#hotelDestination").val('');
        //$("#hotelDestination").siblings('input').val(getLocalizedMessageFromKey('bookingBlock_hotelHeaderTxt'));
        updateDates($('#bb-hotels .calendar'), today);
        $("#hotelAdults,#hotelChildren,#hotelInfants,#numberoffrooms").changeVal('-1');
        return false;
    });

});




/**
 * Update the options for the hotel guest fields to eliminate invalid quantity of guests selectable
 *
 * Refers to global maxPax variable for the maximum
 *
 */
function updateGuests() {

    var adults = parseInt($('#hotelAdults').val());
    var children = parseInt($('#hotelChildren').val());
    var infants = parseInt($('#hotelInfants').val());
    var remainder = hotelMaxPax - adults - children - infants;

    if (!iDevice) {
        //TODO Update to be universal and work on iDevice as well
        $('#hotelAdults_list li').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + adults;
            if (i > selectable)$(this).addClass('hidden');
        });
        $('#hotelChildren_list li').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + children;
            if (i > selectable)$(this).addClass('hidden');
        });
        $('#hotelInfants_list li').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + infants;
            if (i > selectable)$(this).addClass('hidden');
        });
    } else {
        $('#hotelAdults option').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + adults;
            if (i > selectable)$(this).addClass('hidden');
        });
        $('#hotelChildren option').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + children;
            if (i > selectable)$(this).addClass('hidden');
        });
        $('#hotelInfants option').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + infants;
            if (i > selectable)$(this).addClass('hidden');
        });
    }
}

/**
 * Get the checkin date
 *
 * @returns {Date}
 */
function getCheckInDate()
{
    return new Date($('#checkinDay').val() + '-' + $('#checkinMonthYear').val())
}

/**
 * Get the checkout date
 *
 * @returns {Date}
 */
function getCheckOutDate()
{
    return new Date($('#checkoutDay').val() + '-' + $('#checkoutMonthYear').val())
}


/**
 * Update the list of Hotel Destinations
 *
 * Fired when the autocomplete field is entered
 *
 * TODO This could be moved directly into th autocomplete function
 *
 * @param value The value of the autocomplete input
 */
function loadHotelDestination(value) {
    if (value.length <= 2) {
        return;
    }
    loadXmlResponseAndUpdate(relPath + conLan + "/crossSellAction!getAccomodationDetails.action", function(response)
    {
        $('#hotelDestination').html('');
        $('#hotelDestination').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_destinationHeader') + '</option>');
        $(response).find('airport').each(function () {
            var $xml = $(this);
            $('#hotelDestination').append('<option value="' + $xml.find("cityCode").text() + '">' + $xml.find("cityDesc").text() + '</option>');
        });
    }, 'xml', null, {
        accommodationDest: value
    });
}


/**
 * Update the Destination City Description
 *
 */
function updateDestinationCityDesc() {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadDepartureXmlFile.action", function(response)
    {
        var selectedHotDes = $('#hotelDestination').val();
        $(XMLResponse).find('departure').each(function () {
            code = $(this).attr('code');
            if (code == selectedHotDes) {
                $('#txtHotelDes_loc').val($(this).text());
            }
        });
    }, 'xml', null, {
        localeSeltd: config.language,
        country: country
    });


}
