/**
 * Flight Status Block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {
    $('#bb-flight-status form').blockform({
        comboboxInputs: [
            '#statusDepartCity',
            '#statusDestCity'
        ],
        selectricInputs: [
            '#bb-flight-status form select',
        ],
        dateInputs: {
            /*
            "#bb-flight-status .calendar": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();
                    $('#bb .overlay').fadeOut('fast');
                }
            }
            */
        },
        beforeInit: function(){
            updateMonthYearSelects('#bb-flight-status .month-year select');
            updateDaySelects('#bb-flight-status .day select');
        },
        afterInit: function(){
            updateDates('#bb-flight-status .calendar', today);

            $('#statusDepartCity').siblings('input').keyup(function (event) {
                if ($(this).val().length > 2) {
                    loadFlightDestinations($('#statusDepartCity'), getLocalizedMessageFromKey('statusDepartCityId'));
                }
            });
            $('#statusDestCity').siblings('input').keyup(function (event) {
                if ($(this).val().length > 2) {
                    loadFlightDestinations($('#statusDestCity'), getLocalizedMessageFromKey('statusDestCityId'));
                }
            });

            //switch the flight status
            $('#flightChecked').change(function () {
                if ($(this).prop('checked')) {
                    $('#FlightNo').show();
                    $('#NoFlight').hide();
                } else {
                    $('#NoFlight').show();
                    $('#FlightNo').hide();
                }
            });

            //override the exist autocomplete function
            $('#statusDepartCity').siblings('input').autocomplete('option', 'source', function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response($("#statusDepartCity option").map(function () {
                    var text = $(this).text();
                    var code = $(this).val();
                    if ((matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text) && ($.trim(code).length > 0)))
                        return {
                            label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "$1"),
                            value: text,
                            option: this
                        };
                }));
            })

            $('#statusDestCity').siblings('input').autocomplete('option', 'source', function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response($("#statusDestCity option").map(function () {
                    var text = $(this).text();
                    var code = $(this).val();
                    if ((matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text) && ($.trim(code).length > 0)))
                        return {
                            label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "$1"),
                            value: text,
                            option: this
                        };
                }));
            });
        }
    });


    //only numeric( no space)
    $('#flightNumber').keyup(function () {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });


    $('#bb-flight-status footer .button.route').click(function () {
        var url = 'http://timetables.oag.com/saaroutemapper/"';
        iframePopupOpen(url);
    });


    $('#bb-flight-status form').submit(function () {
        var valMsg = "", statusValMsg = "";
        var error = false;
        var flightNum = $('#FlightChecked').prop('checked');

        $('#carrierCode').removeClass('invalid');
        $('#flightNumber').removeClass('invalid');

        $('#statusDepartCity').siblings('input').removeClass('invalid');
        $('#statusDestCity').siblings('input').removeClass('invalid');

        var d = new Date();
        d.setDate(d.getDate() + parseInt($('#fromDateFLT').linkselect('val')));
        var date = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();

        if (flightNum) {
            if (!$('#flightNumber').val().length) {
                valMsg += getLocalizedMessage("Please enter the Flight number.", "statusVal_2");
                //alert("Please enter the Flight number.");
                $('#flightNumber').addClass('invalid');
                error = true;
            }
            else if (isNaN($('#flightNumber').val())) {
                valMsg += getLocalizedMessage("Flight Number should be a number.", "statusVal_3");
                //alert("Flight Number should be a number.");
                $('#flightNumber').addClass('invalid');
                error = true;
            }
            if (error) {
                alert(valMsg);
                return false;
            }

            $(this).action = relPath + conLan + "/checkFlightStatusHome!checkFlightStatus_FltNum.action?request_locale=" + locale + "&fromDateFLT=" + date;
            /*$('body').append('<form id="frmStatus" methor="post" action="http://www.flysaa.com/Journeys/checkFlightStatus!checkFlightStatus_FltNum.action">').hide();$('#frmStatus').append('<input name="carrierCode" value="'+$('#carrierCode').val()+'" />');$('#frmStatus').append('<input name="flightNumber" value="'+$('#flightNumber').val()+'" />');$('#frmStatus').append('<input name="fromDateFLT" value="'+date+'" />');$('#frmStatus').submit();*/
        }
        else {
            if (!$('#statusDepartCity').val().length) {
                statusValMsg = getLocalizedMessage("Please enter the Departure City.", "statusVal_4");
                //alert("Please enter the Departure City");
                $('#statusDepartCity').siblings('input').addClass('invalid');
                error = true;
            }
            if (!$('#statusDestCity').val().length) {
                //alert("Please enter the Destination City");
                statusValMsg += getLocalizedMessage("Please enter the Destination City.", "statusVal_5");
                $('#statusDestCity').siblings('input').addClass('invalid');
                error = true;
            }
            else if ($('#statusDestCity').val() == $('#statusDepartCity').val()) {
                statusValMsg += getLocalizedMessage("Departure City and Destination City cannot be the same.", "statusVal_6");
                $('#statusDepartCity').siblings('input').addClass('invalid');
                $('#statusDestCity').siblings('input').addClass('invalid');
                error = true;
            }
            if (error) {
                alert(statusValMsg);
                return false;
            }
            $(this).action = relPath + conLan + "/checkFlightStatusHome!checkFlightStatus_Location.action?fromDateFLT=" + date;
            /*$('body').append('<form id="frmStatus" methor="post" action="http://www.flysaa.com/Journeys/checkFlightStatus!checkFlightStatus_Location.action">').hide();$('#frmStatus').append('<input name="departureCity" value="'+$('#statusDepartCity').val()+'" />');$('#frmStatus').append('<input name="destinationCity" value="'+$('#statusDestCity').val()+'" />');$('#frmStatus').append('<input name="fromDateLOC" value="'+date+'" />');$('#frmStatus').submit();*/
        }
    });
    $('#bb-flight-status form .button.reset').click(function () {
        $('#flightNumber').val('').removeClass('invalid');
        $('#statusDepartCity,#statusDestCity').siblings('input').removeClass('invalid');
        $('#statusDepartCity,#statusDestCity').val('');
        updateDates($('#bb-flight-status .calendar'), today);
        //$('#statusDepartCity').siblings('input').val($('statusDepartCityId').html());
        //$('#statusDestCity').siblings('input').val($('statusDestCityId').html());
        return false;
    });

});








/**
 * Update the list of Flight Destinations
 *
 * Fired when the autocomplete field is entered
 *
 * TODO This could be moved directly into th autocomplete function
 *
 * @param value The value of the autocomplete input
 */
function loadFlightDestinations(select, defaultOptionText) {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadChkFltStatusXmlFile.action", function(response)
    {
        select.html('');
        select.append('<option value="">' + defaultOptionText + '</option>');
        $(response).find('point').each(function () {
            select.append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
        });
    }, 'xml', null, {
        localeSeltd: config.language
    });
}