/**
 * Book a flight block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {
    $('#bb-flights form').blockform({
        comboboxInputs: [
            '#departCity',
            '#destCity'
        ],
        selectricInputs: [
            '#bb-flights form select',
        ],
        dateInputs: {
            "#bb-flights .calendar.returning": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();
                    $('#bb .overlay').fadeOut('fast');
                }
            },
            "#bb-flights .calendar.leaving": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();

                    // Update the drop downs of the returning date too!
                    // Makes sure impossible dates don't happen by accident, basically.
                    var $cal = $('#bb-flights .calendar.returning');
                    $cal.datepicker("option", "minDate", date);
                    updateDates($cal, dateObj);

                    // If the return date box is active...
                    if ($('#chkReturn').prop('checked')) {
                        // Show the return date calendar after half a second
                        // The timeout is there to wait for the other calendar to fade out
                        setTimeout("$('#bb-flights .calendar.returning').parent().click()", 500);
                    }

                    $('#bb .overlay').fadeOut('fast');
                }
            }
        },
        beforeInit: function(){
            updateMonthYearSelects('#bb-flights .month-year select');
            updateDaySelects('#bb-flights .day select');
        },
        afterInit: function(){
            updateDates('#bb-flights .calendar', today);

            $('#departCity').siblings('input').on("autocompleteselect", function(event, ui) {
                updateDestinationCities(ui.item.option.value);
                checkReservationConfiguration(ui.item.option.value, $('#destCity').val());
            });
            $('#destCity').siblings('input').on("autocompleteselect", function(event, ui) {
                checkReservationConfiguration($('#departCity').val(), ui.item.option.value);
            });
            $('#chkReturn').click(function () {
                toggleReturnFields($(this).prop('checked'), $(this));
            });
            //show/hide the flexible options fields
            $('#bb-flights .datepicker input[type="hidden"]').on('change', function(){
                checkFlexiOption();
            });

            $('#departDay').change(function () {
                $('#destDay').changeVal($(this).val());
            });
            $('#departMonthYear').change(function () {
                $('#returnMonthYear').changeVal($(this).val());
            });
            /*
            $('#destDay, #returnMonthYear').change(function () {
                validateFlightDates();
            });
            */
            checkFlexiOption();
        }
    });


    $('#bb-flights form').submit(function () {
        var valMsg = "";
        var error = false;
        var departDate = getDepartDate();
        var returnDate = getReturnDate();

        $('#destCity').siblings('input').removeClass('invalid');
        $('#departCity').siblings('input').removeClass('invalid');
        $('#bb-flights .input .calendar.returning').parent().parent().find('select, .linkselect-link').removeClass('invalid');
        $('#bb-flights .input .calendar.leaving').parent().parent().find('select, .linkselect-link').removeClass('invalid');

        if (!$('#departCity').val().length) {
            $('#departCity').siblings('input').addClass('invalid');
            valMsg += getLocalizedMessage("Please select a Destination City.", "bookingBlock_1");
            error = true;
        }

        if (!$('#destCity').val().length) {
            $('#destCity').siblings('input').addClass('invalid');
            valMsg += getLocalizedMessage("Please select a Departure City.", "bookingBlock_2");
            error = true;
        }

        if ($('#departCity').val().length && $('#destCity').val().length && ($('#departCity').val() == $( '#destCity').val())) {
            valMsg += getLocalizedMessage("Departure and Destination can not be the same.","bookingBlock_3");
            error = true;
        }

        if($("#adultCount").val() < $("#infantCount").val()){
            valMsg += getLocalizedMessage("The adult-infant ratio should be 1:1.","bookingBlock_5");
            error = true;
        }

        if (parseInt($("#adultCount").val()) + parseInt($("#infantCount").val()) + parseInt($( "#childCount").val()) > 9) {
            valMsg += getLocalizedMessage("The total number of adults,children and infants must not exceed 9. Please consider splitting your group or complete the <a href='groupBooking.action'> Groups Request Form </a>","bookingBlock_6");
            error = true;
        }



        if ($('#chkReturn').prop('checked')) {
            if (compareDateObjects(departDate, returnDate) == -1) {
                $('.input .calendar.returning').parent().parent().find('select, .linkselect-link').addClass('invalid');
                valMsg += getLocalizedMessage("Please select a valid return date.", "bookingBlock_12");
                error = true;
            }
            //check if it is a valid date
            var returnMonthYear = $('#returnMonthYear').val().split('-');
            returnMonthYear[0] = getMonthIndex(returnMonthYear[0]);
            if (parseInt($('#destDay').val()) > daysInMonth(returnMonthYear[0], returnMonthYear[1])){
                $('.input .calendar.returning').parent().parent().find('select, .linkselect-link').addClass('invalid');
                valMsg += getLocalizedMessage("Please select a valid travel return date.", "bookingBlock_17");
                error = true;
            }
        }

        //check if it is a valid date
        var departMonthYear = $('#departMonthYear').val().split('-');
        departMonthYear[0] = getMonthIndex(departMonthYear[0]);
        if (parseInt($('#departDay').val()) > daysInMonth(departMonthYear[0], departMonthYear[1])){
            $('.input .calendar.leaving').parent().parent().find('select, .linkselect-link').addClass('invalid');
            valMsg += getLocalizedMessage("Please select a valid travel departure date.", "bookingBlock_19");
            error = true;
        }

        if($('input:checkbox[name=chkReturn]').prop('checked')){
            $("#tripType").val('R');
        } else {
            $("#tripType").val('O');
        }
        var tripType = $("#tripType").val();
        if(!$('input[name="flexible"]').val().length){
            error = true;
        }

        var fromDate = getFormatedDateFromObject(departDate);
        var toDate	= getFormatedDateFromObject(returnDate);
        var dateFormt= 'dd-M y';

        if(tripType == 'R' && toDate == ""){
            valMsg += getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_17");
            error = true;
        }
        /*if('R'==tripType && (!$('#returnMonthYear').val().length && !$('#destDay').val().length ) ){
         valMsg += getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_12");
         error = true;
         }*/

        if(compareWithCurrentDate(fromDate, dateFormt) < 0){
            valMsg += getLocalizedMessage("Please take a future departure date for travel.","bookingBlock_10");
            error = true;
        }

        if(!isValidDate(fromDate, dateFormt)){
            valMsg += getLocalizedMessage("Invalid Departure Date","bookingBlock_18");
            error = true;
        }

        if(tripType == 'R' && !isValidDate(toDate, dateFormt)){
            valMsg += getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_17");
            error = true;
        }

        if(tripType == 'R' && (compareWithCurrentDate(toDate, dateFormt) < 0 || compareDateStrings(fromDate, toDate, dateFormt) < 0)){
            valMsg += getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_12");
            error = true;
        }

        if(compareWithCurrentDate(toDate, dateFormt) < 0) {
            valMsg += getLocalizedMessage("Please take a future return date for travel.","bookingBlock_10");
            error = true;
        }

        if (error) {
            alert(valMsg);
            return false;
        }

        //TODO: The below code has an issue in that populateCabinClass() may still be running before it gets a chance to update the variable depCountry
        document.forms.bbFlights.fromDate.value = fromDate;
        document.forms.bbFlights.toDate.value = toDate;
        document.forms.bbFlights.flexible.value = $('input:radio[name=flexible]:checked').val();
        document.forms.bbFlights.selectedLang.value = config.language;
        //this could be legacy as preferredCabinClass already exists
        document.forms.bbFlights.preferredClass.value = $('#flightClass').val();
        //showProcessing();
        var conLanFlightSearch = "/" + depCountry.toLowerCase() + "/" + config.language.toLowerCase();
        document.forms.bbFlights.action = relPath + conLanFlightSearch + '/flexPricerFlightSearch!flexPricerFlightAvailability.action?request_locale=' + config.language.toLowerCase() + "_" + depCountry.toUpperCase();
    });
});





/**
 * Check the Flexible dates and hide/show flexible option
 *
 * Flexi option is shown to the user only if the depart date is atleast 3 days ahead of current date..
 */
function checkFlexiOption() {

    var flexibleGrp = $('input[name="flexible"]');
    if (flexibleGrp.val()) {
        $('input[name="flexible"][value="false"]').prop('checked', true);
    }

    if (isMatrixSearch) {
        $('input[name="flexible"][value="true"]').prop('checked', true);
        return;
    }

    var curntDte = new Date();
    var departDate = getDepartDate();

    departDate.setHours(0);
    departDate.setMinutes(0);
    departDate.setSeconds(0);

    curntDte.setHours(0);
    curntDte.setMinutes(0);
    curntDte.setSeconds(0);

    /*
     this would never perform its function
     try {
     parseDDate = $.datepicker.parseDate('dd-M-yy', departDate);
     // Populate the formatted field
     popFormattedDates(parseDDate, 'depart');
     } catch (e) {
     $('#departDay_link, #departMonthYear_link').addClass('invalid');
     error = true
     }
     try {
     parseRDate = $.datepicker.parseDate('dd-M-yy', curntDte);
     // Populate the formatted field
     popFormattedDates(parseRDate, 'return');
     } catch (e) {
     $('#destDay_link, #returnMonthYear_link').addClass('invalid');
     error = true
     }
     */

    var difference = (departDate.getTime() - curntDte.getTime()) / (1000 * 60 * 60 * 24);
    difference = Math.round(difference);

    if (difference < 3) {
        $('input[name="flexible"][value="false"]').prop('checked', true);
        $('input[name="flexible"][value="true"]').parents('.input').fadeOut();
    } else {
        $('input[name="flexible"][value="true"]').parents('.input').fadeIn();
    }
}


/**
 * Updates the destination cities with the available destinations for that city
 *
 * @param cityCode
 */
function updateDestinationCities(cityCode) {
    //disable it briefly whilst we fetch the destinations
    //$('#destCity').siblings('input').prop('disabled', true);
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadDestinationXmlFile.action", function(response)
    {
        $('#destCity').html('');
        $('#destCity').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_destinationCity') + '</option>');
        $(response).find('destination').each(function () {
            var countryList = $(this).text();
            var countryArray = countryList.split(',');
            var destination = countryArray[0];
            var code = $(this).attr('value');
            var countryDescription = countryArray[1];
            var countryDescriptionSplit = '';
            if (countryDescription != '') {
                countryDescriptionSplit = ",";
            }
            $('#destCity').append('<option value="' + code + '">' + destination + countryDescriptionSplit + countryDescription + '</option>');

            if (!($('#departCity').val() == $('#destCity').val()) && $('#departCity').val() != '' && $('#destCity').val() != '') {
                isMatrixSearch = false;
                if (countryArray[3] == 'MTXSCHD') {
                    //isMatrixSearch	= true;
                    //$('.flexibility .flex').fadeOut();
                    if ($('input[name="flexible"]')) {
                        $('input[name="flexible"][value="false"]').prop('checked', true);
                    }
                } else {
                    checkFlexiOption();
                }
                if ($("#destCity").val().length > 0) {
                    populateSplashCabinClass(countryArray[2]);
                }
            }
        });
        //enable it
        //$('#destCity').siblings('input').prop('disabled', false).focus();
        //$('#destCity').siblings('input').focus();
    }, 'xml', null, {
        localeSeltd: config.language,
        departCitySeltd: cityCode
    });

}



/**
 * Download and update the cabin classes
 *
 * @param isFirstClassEnabled
 */
function populateSplashCabinClass(isFirstClassEnabled) {
    var inputArray = {};
    if (isFirstClassEnabled == 'Y') {
        inputArray["isFirstClassEnabled"] = true;
    } else {
        inputArray["isFirstClassEnabled"] = false;
    }
    inputArray["localeSeltd"] = locale;
    inputArray["country"] = country;
    $.ajaxSetup({
        cache: false
    });
    var url = relPath + "/splashPageAction!populateSplashCabinClassList.action";
    $("#flightClass").parent().html(ajax_load_leisure).load(url, inputArray, function(){
        styleSelect('#flightClass');
    });
}


/**
 * Populate the correct cabin classes for the current booking
 *
 */
function populateCabinClass() {
    loadXmlResponseAndUpdate(relPath + conLan + "/flightSearch!populateHomeCabinClassList.action", function(response)
    {
        var cabinClassArr = [];
        var options = '';
        $(response).find('cabinClass').each(function () {
            var depcountryList = $(this).text();
            var departureCountryArray = depcountryList.split(',');
            depCountry = departureCountryArray[1];

            var tempArray = [];
            tempArray['text'] = departureCountryArray[0];
            tempArray['value'] = $(this).attr('value');
            cabinClassArr.push(tempArray);

            var cabinClass = departureCountryArray[0];
            var cabinCode = $(this).attr('value');

            options += '<option value="' + cabinCode + '">' + cabinClass + '</option>';
        });
        // ipad fix
        updateSelectOptions('#flightClass', options);
        /*
        //refresh non ipad
        if (!iDevice) {
            $("#flightClass").selectric('refresh');
        }
        */
    }, 'xml', 'Could not update Cabin Classes', {
        departureCity: $('#departCity').val(),
        destinationCity: $('#destCity').val()
    });
}



/**
 * Check the reservation configuration
 *
 */
function checkReservationConfiguration(departCity, destCity) {
    if (!(departCity == destCity) && departCity != '' && destCity != '') {
        $.ajaxSetup({
            cache: false,
            async: false
        });
        $.get(
            relPath + conLan + "/flightSearch!checkReservationConfiguration.action",
            {
                departureCity: departCity,
                destinationCity: destCity
            },
            function(){
                checkFlexiOption();
                populateCabinClass();
            },
            'text'
        );
    }
}



/**
 * Load the Depart City XML
 *
 * @param locale
 * @param country
 */
function loadDepartureXML(locale, country) {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadDepartureXmlFile.action", function(response)
    {
        $('#departCity, #destCity').html('');
        country = $("#departCityId").html();
        code = '';
        $('#departCity').append('<option value="' + code + '">' + country + '</option>');
        country = $("#destinCityId").html();
        code = '';
        $('#destCity').append('<option value="' + code + '">' + country + '</option>');
        $(response).find('city').each(function () {
            country = $(this).text();
            code = $(this).attr('value');
            $('#departCity').append('<option value="' + code + '">' + country + '</option>');
        });
    }, 'xml', null, {
        country: country
    });
}


/**
 * Load the Region XML and update the page
 *
 */
function loadDestinationXML(locale, cityCode){

}

function validateFlightDates(){
    var d = getDepartDate();
    var r = getReturnDate();
    if (compareDateObjects(d, r) == -1) {
        alert(propsArray['bookingBlock_17']);
        //updateDates($('#bb-flights .calendar.returning'), $('#bb-flights .calendar.leaving').datepicker('getDate'));
    }
}