/**
 * Check In Block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {




    $('#bb-check-in form').blockform({
        selectricInputs: [
            '#bb-check-in form select',
        ],
        beforeInit: function(){

        },
        afterInit: function(){
            //switch for check-in block
            $('#checkInSAA, #checkInMango').click(function () {
                $('#bb-check-in form .check-in').hide();
                $("#" + $(this).attr('id') + 'Div').show();
            });

            //only alphabets no space
            $('#ISurname').keyup(function () {
                if (this.value.match(/[^a-zA-Z ]/g)) {
                    this.value = this.value.replace(/[^a-zA-Z ]/g, '');
                }
            });
            $('#ISurnameM').keyup(function () {
                if (this.value.match(/[^a-zA-Z ]/g)) {
                    this.value = this.value.replace(/[^a-zA-Z ]/g, '');
                }
            });

            //set maxLength
            if ("PNR" == $('#checkInMethod').val()) {
                $('#IIdentification').val("");
                $('#IIdentification').attr('maxLength', 6);
            }

            $('#checkInMethod').change(function () {
                //hide specific inputs
                var v = $(this).val().toLowerCase();
                $('#bb-check-in form .check-in-method').hide();
                $('#bb-check-in form .check-in-method.'+v).show();
                //set maxLength
                if (v == "pnr") {
                    $('#IIdentification').val("");
                    $('#IIdentification').attr('maxLength', 6);
                } else {
                    $('#IIdentification').attr('maxLength', 100);
                }
            });

            $('#checkInWrap .close').click(function(){
                closeCheckinWrap();
            })


        }
    });




    $('#bb-check-in form').submit(function () {

        var valMsg = "";
        var error = false;

        //check and process mango fields
        if ($("#checkInMangoDiv").css('display') == 'block') {

            var surnameM = $('#ISurnameM');
            var etktNumberM = $('#etktnumberM');

            var boardPoint = $('#IBoardPoint').val();
            var formOfID = "ETKT";
            var group = $('#checkInAddPaxM').prop('checked') ? '&IGroupTravel=on' : '';

            surnameM.removeClass('invalid');
            if (!surnameM.val().length) {
                surnameM.addClass('invalid');
                error = true
            }

            etktNumberM.removeClass('invalid');
            if (!etktNumberM.val().length) {
                etktNumberM.addClass('invalid');
                error = true
            }

            valMsg = getLocalizedMessage("Please fill in the mandatory field(s).", "checkin_1");

            if (error) {
                alert(valMsg);
                return false;
            }


            var url = 'https://checkin.si.amadeus.net/1ASIHSSCWCIJE/sscwje/checkin?' +
                'LANGUAGE=' + config.language.toLowerCase() +
                '&IFormOfIdentification=' + formOfID +
                '&IIdentification=' + etktNumberM.val() +
                '&ISurname=' + surnameM.val() + group +
                '&RedirectedFrom=RSS&ln=EN';
        }
        //check and process saa fields
        else {
            var surname = $('#ISurname');
            var idNumber = $('#IIdentification');

            var boardPoint = $('#IBoardPoint').val();
            var formOfID = $('#checkInMethod').val();
            var group = $('#checkInAddPax').is(':checked') ? '&IGroupTravel=on' : '';

            //alert("surname is : "+surname.value+" formOfID = "+formOfID.value);
            //alert("isurname "+ document.getElementById('ISurname').value+ " ISurnameM "+document.getElementById('ISurnameM').value);

            surname.removeClass('invalid');
            if (!surname.val().length) {
                surname.addClass('invalid');
                error = true
            }

            idNumber.removeClass('invalid');
            if (!idNumber.val().length) {
                idNumber.addClass('invalid');
                valMsg = getLocalizedMessage("Please fill in the mandatory field(s).", "checkin_1");
                error = true;
            }

            if (error) {
                alert(valMsg);
                return false;
            }

            var url = 'https://checkin.si.amadeus.net/1ASIHSSCWCISA/sscwsa/checkin?' +
                'LANGUAGE=' + config.language.toLowerCase() +
                '&IFormOfIdentification=' + formOfID +
                '&IIdentification=' + idNumber.val() +
                '&ISurname=' + surname.val() +
                group + '&RedirectedFrom=RSS&ln=EN';
        }
        iframePopupOpen(url);
        return false;
    });


    //$('#bb-check-in form').submit(function () {

    $('#bb-check-in .button.reset').live('click', function () {
        $('#ISurname,#IIdentification').val('').removeClass('invalid');
        $('#checkInAddPax').prop('checked', false);
        //$('.checkInAddPax.checkStyle.checked').removeClass('checked');

        $('#ISurnameM,#etktnumberM').val('').removeClass('invalid');
        $('#checkInAddPaxM').prop('checked', false);

        //$('.checkInAddPaxM.checkStyle.checked').removeClass('checked');

        $('input[name="checkInMethod"]').changeVal('PNR');
    });


});



//Added for Request 3257468
function changeCheckin(checkins) {
    var checkin = document.getElementsByName(checkins.name);
    document.getElementById('saa_div').style.display = (checkin[0].checked) ? 'block' : 'none';
    document.getElementById('mango_div').style.display = (checkin[1].checked) ? 'block' : 'none';
}


