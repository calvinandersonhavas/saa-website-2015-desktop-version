/**
 * Can Rental Block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {
    $('#bb-car-rental form').blockform({
        comboboxInputs: [
            '#pickupLoc',
            '#dropoffLoc',
            '#carCountry'
        ],
        selectricInputs: [
            '#bb-car-rental form select',
        ],
        dateInputs: {
            "#bb-car-rental .calendar.pick-up": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();
                    // Update the drop downs of the returning date too!
                    // Makes sure impossible dates don't happen by accident, basicaly.
                    var $cal = $('#bb-car-rental .calendar.drop-off');
                    $cal.datepicker("option", "minDate", date);
                    updateDates($cal, dateObj);

                    // Show the return date calendar after half a second
                    // The timeout is there to wait for the other calendar to fade out
                    setTimeout("$('#bb-car-rental .calendar.drop-off').parent().click()", 500);
                    $('#bb .overlay').fadeOut('fast');
                }
            },
            "#bb-car-rental .calendar.drop-off": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();
                    $('#bb .overlay').fadeOut('fast');
                }
            }
        },
        beforeInit: function(){
            updateMonthYearSelects('#bb-car-rental .month-year select');
            updateDaySelects('#bb-car-rental .day select');
        },
        afterInit: function(){
            updateDates('#bb-car-rental .calendar', today);


            $('#pickupLoc').siblings('input').keyup(function (event) {
                var value = $(this).val();
                if (value.length > 2) {
                    loadCarDepartCities(value);
                }
            });

            $('#dropoffLoc').siblings('input').keyup(function (event) {
                var value = $(this).val();
                if (value.length > 2) {
                    loadCarDestCities(value);
                }
            });

            $('#carCountry').siblings('input').keyup(function (event) {
                var value = $(this).val();
                if (value.length > 2) {
                    loadCarCountries(value);
                }
            });


            /*
            //TODO check what this is needed for!!!
            var selectedDropPonit = $('#dropoffLoc').val();
            if (code == selectedDropPonit) {
                $('#txtdrop_loc').val($(this).text());
            }
            */


            /*
             //TODO Check if this is needed
             if (!iDevice) {
             $('#pickDay').linkselect({
             change: function (li, value) {
             updateDropOffDay(value)
             }
             });
             //calling callSetUpdatedDates() to update the hidden variables for carHireDep/DestMonthYear
             $('#pickMonthYear').linkselect({
             change: function (li, value) {
             updateDropOffMonthYear(value), callSetUpdatedDates()
             }
             });
             }

             */
            /*
            $('#pickDay').change(function () {
                $('#dropoffDay').changeVal($(this).val());
            });
            $('#pickMonthYear').change(function () {
                $('#dropoffMonthYear').changeVal($(this).val());
            });
            */
        }
    });




    $('#bb-car-rental form').submit(function () {
        var valMsg = "";
        var error = false;

        //TODO This won't have any affect
        getCityDesc();

        $('#pickupLoc').siblings('input').removeClass('invalid');
        $('#dropoffLoc').siblings('input').removeClass('invalid');
        $('#carCountry').siblings('input').removeClass('invalid');
        $('#driverAge').removeClass('invalid');
        $('#bb-car-rental .input .calendar.pick-up, #bb-car-rental .input .calendar.drop-off').parent().parent().find('select, .linkselect-link').removeClass('invalid');

        if (validateCarDates()) {
            $('#bb-car-rental .input .calendar.pick-up, #bb-car-rental .input .calendar.drop-off').parent().parent().find('.day select, .day .linkselect-link, .month-year select, .month-year .linkselect-link').addClass('invalid');
            valMsg += getLocalizedMessage("Please choose valid dates", "bookingBlock_14");
            error = true;
        }

        if (validateCarTime()) {
            $('#bb-car-rental .input .calendar.pick-up, #bb-car-rental .input .calendar.drop-off').parent().parent().find('.time select, .time .linkselect-link').addClass('invalid');
            valMsg += getLocalizedMessage("Please choose valid time", "bookingBlock_15");
            error = true;
        }
        /*if(!$('#driverAge').val().length||isNaN($('#driverAge').val())||$('#driverAge').val()<18) {
         $('#driverAge').addClass('invalid');
         valMsg=getLocalizedMessage("Please provide a valid driver age.","cars_1");
         error=true; }*/

        if (!$('#pickupLoc').val().length) {
            $('#pickupLoc').siblings('input').addClass('invalid');
            valMsg += getLocalizedMessage("Please provide a pick up location.", "cars_2");
            error = true;
        }

        // if(!$('#dropoffLoc').val().length) { $('#dropoffLoc').siblings('input').addClass('invalid'); valMsg+=getLocalizedMessage("Please provide a drop-off location.","cars_3");error=true; }
        /*
        if (!$('#carCountry').val().length) {
            $('#carCountry').siblings('input').addClass('invalid');
            valMsg += getLocalizedMessage("Please provide the country.", "cars_4");
            error = true;
        }
        */

        if (error) {
            alert(valMsg);
            return false;
        }

        getPosConnectionID();
        return false;
    });

    $('#bb-car-rental form .button.reset').click(function () {
        $("#pickupLoc").val('');
        $("#pickupLoc").siblings('input').val('');

        $("#dropoffLoc").val('');
        $("#dropoffLoc").siblings('input').val('');

        $("#carCountry").val('');
        $("#carCountry").siblings('input').val('');

        $('#driverAge').val('').removeClass('invalid');
        $('#pickupLoc').siblings('input').removeClass('invalid');
        $('#dropoffLoc').siblings('input').removeClass('invalid');
        $('#carCountry').siblings('input').removeClass('invalid');

        updateDates($('#bb-car-rental .calendar'), today);
        $("#dropOffTime,#pickUpTime").changeVal('-1');

        return false;
    });



});




/**
 * Update the list of Car Depart Cities
 *
 * Fired when the autocomplete field is entered
 *
 * TODO This could be moved directly into th autocomplete function
 *
 * @param value The value from the autocomplete field for search
 */
function loadCarDepartCities(value) {
    loadXmlResponseAndUpdate(relPath + conLan + "/crossSellAction!getCarHirePickupDetails.action", function(response)
    {
        $('#pickupLoc').html('').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_pickupHeader') + '</option>');
        $(response).find('airport').each(function () {
            var $xml = $(this);
            $('#pickupLoc').append('<option value="' + $xml.find("cityCode").text() + '">' + $xml.find("cityDesc").text() + '</option>');
        });
    }, 'xml', null, {
        carHireOrg: value
    });
}

/**
 * Load the Car Destination Cities
 *
 * @param value The value from the autocomplete field for search
 */
function loadCarDestCities(value) {

    loadXmlResponseAndUpdate(relPath + conLan + "/crossSellAction!getCarHirePickupDetails.action", function(response)
    {
        $('#dropoffLoc').html('').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_dropoffHeader') + '</option>');
        $(response).find('airport').each(function () {
            var $xml = $(this);
            $('#dropoffLoc').append('<option value="' + $xml.find("cityCode").text() + '">' + $xml.find("cityDesc").text() + '</option>');
        });
    }, 'xml', null, {
        carHireOrg: value
    });
}

/**
 * Load the car countries
 *
 */
function loadCarCountries() {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadLocaleXmlFile.action", function(response)
    {
        $('#carCountry').html('').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_countryResHeader') + '</option>');
        $(response).find('country').each(function () {
            $('#carCountry').append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
        });
    }, 'xml', null, {
        localeSeltd: config.language
    });
}


/**
 * Get the City Descriptor
 *
 */
function getCityDesc() {
    loadCarCityDescriptionXML();
}

/**
 * Load the Car City Description
 *
 */
function loadCarCityDescriptionXML() {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadDepartureXmlFile.action", function(response)
    {
        $(response).find('departure').each(function () {
            var country = $(this).text();
            var code = $(this).attr('code');

            //TODO check if this is the correct way to do this!!!
            if (code == $('#pickupLoc').val()) {
                $('#txtpick_loc').val(country);
            }
            if (code == $('#dropoffLoc').val()) {
                $('#txtdrop_loc').val(country);
            }
        });

    }, 'xml', null, {
        localeSeltd: config.language,
        country: config.country
    });
}






//TODO What is this for???
function getPosConnectionID() {

    loadXmlResponseAndUpdate(relPath + "/splashPageAction!getPosConnectionID.action", function(response)
    {
        if (status == "success") {
            if (response != null) {
                carRentalPosConnId = response;
            }
        } else {
            carRentalPosConnId = null;
        }

        //TODO These don't currently exist!!!
        var pickupMonthndex = getAllMonthIndex($("#carHireDepMonthYear").val());
        var pickUYear = getAllyearValue($("#carHireDepMonthYear").val());
        var dropofMonthIndex = getAllMonthIndex($("#carHireDestMonthYear").val());
        var dropoffear = getAllyearValue($("#carHireDestMonthYear").val());

        if (carRentalPosConnId == null) {
            carRentalPosConnId = "ZAF";
        }

        /*if ( $( '#dropoffLoc').val().length>0 &&($('#pickupLoc').val() != $( '#dropoffLoc').val())) {
         var url = "http://www.flysaa.com/saacui/CarSearchExternal.do?isNewSearchFlow=true&pos=SAACUI-"+carRentalPosConnId+"&lang=en&pickUpLocationName=" + $('#txtpick_loc').val() + "&pickUpLocationCode=" + $('#pickupLoc').val() + "&pickUpDay=" + $('#pickDay').val() + "&pickUpMonth=" + pickupMonthndex + "&pickUpYear=" + pickUYear + "&pickUpTime=" + $('#pickUpTime').val() + "&dropOffDay=" + $('#dropoffDay').val() + "&dropOffMonth=" + dropofMonthIndex + "&dropOffYear=" + dropoffear + "&dropOffTime=" + $('#dropOffTime').val() + "&residentCountry=" + $('#carCountry').val() + "&returningDifferent=true"	+ "&dropOffLocationName=" + $('#txtdrop_loc').val() + "&dropOffLocationCode=" + $('#dropoffLoc').val() + "&driverAge=" + $('#driverAge').val(); }
         else {
         var url = "http://www.flysaa.com/saacui/CarSearchExternal.do?isNewSearchFlow=true&pos=SAACUI-"+carRentalPosConnId+"&lang=en&pickUpLocationName=" + $('#txtpick_loc').val() + "&pickUpLocationCode=" + $('#pickupLoc').val() + "&pickUpDay=" + $('#pickDay').val() + "&pickUpMonth=" + pickupMonthndex + "&pickUpYear=" + pickUYear + "&pickUpTime=" + $('#pickUpTime').val() + "&dropOffDay=" + $('#dropoffDay').val() + "&dropOffMonth=" + dropofMonthIndex + "&dropOffYear=" + dropoffear + "&dropOffTime=" + $('#dropOffTime').val() + "&residentCountry=" + $('#carCountry').val() + "&returningDifferent=false" + "&driverAge=" + $('#driverAge').val(); }
         window.open(url,"_self");*/

        var BDATE = "";
        var EDATE = "";
        var dropLocation = "";
        var BMONTH = pickupMonthndex;
        var EMONTH = dropofMonthIndex;
        if (BMONTH < 10)
            BMONTH = "0" + BMONTH;
        if (EMONTH < 10)
            EMONTH = "0" + EMONTH;
        BDATE = pickUYear + BMONTH + $('#pickDay').val() + $('#pickUpTime').val();
        EDATE = dropoffear + EMONTH + $('#dropoffDay').val() + $('#dropOffTime').val();
        //alert("BDATE->" + BDATE + "EDATE->" + EDATE);
        if ($('#dropoffLoc').val().length > 0 && ($('#pickupLoc').val() != $('#dropoffLoc').val()))
            dropLocation = $('#dropoffLoc').val();
        else
            dropLocation = $('#pickupLoc').val();
        //var url = "https://book.flysaa.com/plnext/flysaa/Override.action?UI_EMBEDDED_TRANSACTION=CarShopperStart&SITE=BAUYBAUY&LANGUAGE=GB&TRIP_FLOW=YES&B_DATE=" + BDATE +"&E_DATE=" + EDATE +"&B_LOCATION=" + $('#pickupLoc').val() + "&E_LOCATION=" + dropLocation + "&CLASS=*&TYPE=*&TRANSMISSION=*&AIR_CONDITIONING=*&EXTERNAL_ID=UICSS:PRODUCTION;PRODUCT:CAR;SA_SOURCE:CAR;COUNTRY:za;LANGUAGE:en;;;;;;;;&SO_SITE_IS_MAIL_CANCEL=TRUE&SO_SITE_IS_MAIL_CONF=TRUE&USE_CAR_SHOPPER=TRUE&SO_SITE_APIV2_SERVER_USER_ID=GUEST&SO_SITE_APIV2_SERVER=194.156.170.78&SO_SITE_APIV2_SERVER_PWD=TAZ&SO_SITE_CORPORATE_ID=OCGPDT&SO_SITE_SI_SAP=1ASIXJCP&SO_SITE_SI_SERVER_PORT=18033&SO_SITE_SI_SERVER_IP=193.23.185.67&SO_SITE_SI_USER=UNSET&SO_SITE_SI_PASSWORD=UNSET&SO_SITE_SI_1AXML_FROM=SEP_JCP&SO_SITE_FQ_INTERFACE_ACTIVE=FALSE&SO_SITE&SO_SITE_EXTERNAL_LOGIN=FALSE&SO_SITE_OFFICE_ID=JNBSA08AA&SO_SITE_HOST_TRACE_ACTIVE=TRUE&SO_SITE_FP_TRACES_ON=FALSE&AUTO_SEARCH=TRUE#results";
        var url = "https://book.flysaa.com/plnext/flysaa/Override.action?UI_EMBEDDED_TRANSACTION=CarShopperStart&SITE=BAUYBAUY&LANGUAGE=GB&TRIP_FLOW=YES&B_DATE=" + BDATE + "&E_DATE=" + EDATE + "&B_LOCATION=" + $('#pickupLoc').val() + "&E_LOCATION=" + dropLocation + "&CLASS=*&TYPE=*&TRANSMISSION=*&AIR_CONDITIONING=*&EXTERNAL_ID=UICSS:PRODUCTION;PRODUCT:CAR;SA_SOURCE:CAR;COUNTRY:za;LANGUAGE:en;;;;;;;;&SO_SITE_IS_MAIL_CANCEL=TRUE&SO_SITE_IS_MAIL_CONF=TRUE&USE_CAR_SHOPPER=TRUE&SO_SITE_EXTERNAL_LOGIN=FALSE&SO_SITE_OFFICE_ID=JNBSA08AA&AUTO_SEARCH=TRUE#results";
        window.open(url, "_self");

    }, 'text', null, {
        countrySel: config.country,
        productSel: config.selectedProductIs
    });
}



//TODO refactor this
function validateCarDates() {
    var flag = false;
    if ($('#pickMonthYear').val() != " ") {
        var departDateArray = $('#pickMonthYear').val().split('-');
        var finalDepartMonth = departDateArray[1].substring(departDateArray[1].length, 2);
        pickupDate = $('#pickDay').val() + "-" + departDateArray[0] + " " + finalDepartMonth;
    }
    if ($('#dropoffMonthYear').val() != " ") {
        var returnDateArray = $('#dropoffMonthYear').val().split('-');
        var finalReturnMonth = returnDateArray[1].substring(returnDateArray[1].length, 2);
        dropoffDate = $('#dropoffDay').val() + "-" + returnDateArray[0] + " " + finalReturnMonth;
    }

    if (!isValidDate(pickupDate, dateFormat)) {
        flag = true;
    }
    if (!isValidDate(dropoffDate, dateFormat)) {
        flag = true;
    }
    if (compareWithCurrentDate(pickupDate, dateFormat) < 0) {
        flag = true;
    }
    if (compareWithCurrentDate(dropoffDate, dateFormat) < 0 || compareDateStrings(pickupDate, dropoffDate, dateFormat) < 0) {
        flag = true;
    }
    return flag;
}


//for cars
function validateCarTime() {
    if ($('#pickMonthYear').val() != " ") {
        var departDateArray = $('#pickMonthYear').val().split('-');
        var finalDepartMonth = departDateArray[1].substring(departDateArray[1].length, 2);
        pickupDate = $('#pickDay').val() + "-" + departDateArray[0] + " " + finalDepartMonth;
    }
    if ($('#dropoffMonthYear').val() != " ") {
        var returnDateArray = $('#dropoffMonthYear').val().split('-');
        var finalReturnMonth = returnDateArray[1].substring(returnDateArray[1].length, 2);
        dropoffDate = $('#dropoffDay').val() + "-" + returnDateArray[0] + " " + finalReturnMonth;
    }
    if ((compareDateStrings(pickupDate, dropoffDate, 'dd-M y') == 0) && (document.getElementById('pickUpTime').value > document.getElementById('dropOffTime').value)) {
        return true;
    }
    return false;
}



