module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                options: {
                    outputStyle: 'formatted'
                },
                files: {
                    'css/style.css': 'src/scss/style.scss'
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            before: {
                src: [
                    'bower_components/modernizr/modernizr.js',
                ],
                dest: 'js/modernizr.js'
            },
            libraries: {
                src: [
                    'bower_components/jquery/jquery.js',
                    'bower_components/jquery-ui/ui/jquery.ui.core.js',
                    'bower_components/jquery-ui/ui/jquery.ui.widget.js',
                    'bower_components/jquery-ui/ui/jquery.ui.datepicker.js',
                    'bower_components/jquery-ui/ui/jquery.ui.tabs.js',
                    'bower_components/jquery-ui/ui/jquery.ui.autocomplete.js',
                    'src/js/lib/jquery.linkselect.js',
                    'src/js/libraries.js',
                ],
                dest: 'js/libraries.js'
            },
            home: {
                src: [
                    'bower_components/flexslider/jquery.flexslider.js',
                    'src/js/init.js',
                    'src/js/common.js',
                    'src/js/header.js',
                    'src/js/footer.js',
                    'src/js/home.js',
                    'src/js/booking-block.js',
                    'src/js/blocks/book.js',
                    'src/js/blocks/check-in.js',
                    'src/js/blocks/log-in.js',
                    'src/js/blocks/flight-status.js',
                    'src/js/blocks/car-rental.js',
                    'src/js/blocks/hotel.js'
                ],
                dest: 'js/home.js'
            }
        },
        uglify: {
            my_target: {
                files: [{
                    expand: true,
                    cwd: 'js',
                    src: '*.js',
                    dest: 'js/min'
                }]
            }
        },
        watch: {
            options: {
                livereload: true
            },
            grunt: {
                files: ['Gruntfile.js']
            },
            sass: {
                files: 'src/scss/**/*.scss',
                tasks: ['sass']
            },
            scripts: {
                files: 'src/js/**/*.js',
                tasks: ['concat', 'uglify']
            }
        }
    });
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('build', ['sass', 'concat', 'uglify']);
    grunt.registerTask('default', ['sass', 'concat', 'uglify', 'watch']);

}