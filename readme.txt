South Africa Airways new Website Design Repository guide
--------------------------------------------------------

Authors: 
	Greg Gunner (greg.gunner@havaswwdigital.co.za)
	
Last Updated: 
	16 March 2015

Project Contacts:
	Havas Lead Developer: Greg Gunner (greg.gunner@havaswwdigital.co.za)
	IBS Lead Developer: Dhaya Thomas (dhaya.thomas@ibsplc.com)
	IBS Implementation Developer: Godfray Thomas (godfray.thomas@ibsplc.com)
	
-----
Folder Structure Guide
-----
- existing:			Pre design files for 2014 website design
- herald:				New website theme folder with the new 2015 design. This is bower and grunt based.
 - bower_components		The javascript/css libraries used in the theme
 - css					An output folder for compiled css (Don't edit these files)
 - images				An output folder for compiled or final images (Don't edit these files)
 - js					An output folder for compiled js (Don't edit these files)
 - node_modules			Node.js compiler modules for compiling the files
 - src					Source files such as scss, js, images (incl sprite source files)
- includes			A series of PHP include files to help easily building localised templates
 - server			A series of local xml output files for testing. These are files that are typically 
					fetched from the SAA server when the page loads, or processes user interactions. 
					It is for local development tests only.
- jsp					JSP templates used by the website. These are this repositories versions of the templates, and NOT the live versions.
 - herald			Website test directory for verifying any changes made to the compiled versions sent back from IBS.
.htaccess			An Apache htaccess config for local testing. This really only handle redirects of requests to the "includes/server" directory.
web.config			An IIS config for local testing. This really only handle redirects of requests to the "includes/server" directory.
*.php / */html		Local test files for processing and testing the css and javascript

-----
General Rules
-----
1) Html, css, js is to be managed by the Havas Team. Changes should be made to the "herald/src" directory only.
2) Changes to the jsp/* files should be managed by IBS. However, if class and id changes are needed, this can go through Havas and IBS and Havas developers responsible for the changes, should be in communication and using the correct source file. These files should always be kept in sync.
3) When managing jsp file changes, ensure to get the latest version from IBS, overwrite the local checked out copy, and very the changes (if any), and commit the file in with comments.
4) Once changes are made to any files, commit changes for the applicable files for that change in one commit.
5) If jsp/herald files have been updated, those changes should be verified and pulled into herald/src files accordingly and recompiled.

-----
General Procedure for Changes
-----
1) Requests received from client
2) Havas to edit and manage the JS/CSS changes and recompile.
3) IBS implement the change as needed, pulling from the Havas SVN "compiled" directories based on Havas instructions.
4) Changes are tested on beta.flysaa.com and revisions done.
5) If all ok, IBS move changes to test.flysaa.com for client review and revisions done.
6) If all ok, IBS move changes to the live site www.flysaa.com.

-----
Havas Rules
-----
1) Havas changes should be commit to the bitbucket/git repository first.
2) Only Greg Gunner is allowed to commit changes from Havas to the SAA SVN repository once all relevant commits in 1) are done.
3) When working on the files, ensure you pull the latest versions.
4) When done with your fixes, push to the bitbucket repository.

-----
CSS Rules
-----
1) Html class names should be lowercase (this-name) and hyphenated.
2) Html ID attributes should be camel case (camelCase) with the first character lowercase.
