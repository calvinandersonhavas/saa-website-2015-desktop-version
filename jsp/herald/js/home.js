/*
 * jQuery FlexSlider v2.2.2
 * Copyright 2012 WooThemes
 * Contributing Author: Tyler Smith
 */
;
(function ($) {

  //FlexSlider: Object Instance
  $.flexslider = function(el, options) {
    var slider = $(el);

    // making variables public
    slider.vars = $.extend({}, $.flexslider.defaults, options);

    var namespace = slider.vars.namespace,
        msGesture = window.navigator && window.navigator.msPointerEnabled && window.MSGesture,
        touch = (( "ontouchstart" in window ) || msGesture || window.DocumentTouch && document instanceof DocumentTouch) && slider.vars.touch,
        // depricating this idea, as devices are being released with both of these events
        //eventType = (touch) ? "touchend" : "click",
        eventType = "click touchend MSPointerUp",
        watchedEvent = "",
        watchedEventClearTimer,
        vertical = slider.vars.direction === "vertical",
        reverse = slider.vars.reverse,
        carousel = (slider.vars.itemWidth > 0),
        fade = slider.vars.animation === "fade",
        asNav = slider.vars.asNavFor !== "",
        methods = {},
        focused = true;

    // Store a reference to the slider object
    $.data(el, "flexslider", slider);

    // Private slider methods
    methods = {
      init: function() {
        slider.animating = false;
        // Get current slide and make sure it is a number
        slider.currentSlide = parseInt( ( slider.vars.startAt ? slider.vars.startAt : 0), 10 );
        if ( isNaN( slider.currentSlide ) ) slider.currentSlide = 0;
        slider.animatingTo = slider.currentSlide;
        slider.atEnd = (slider.currentSlide === 0 || slider.currentSlide === slider.last);
        slider.containerSelector = slider.vars.selector.substr(0,slider.vars.selector.search(' '));
        slider.slides = $(slider.vars.selector, slider);
        slider.container = $(slider.containerSelector, slider);
        slider.count = slider.slides.length;
        // SYNC:
        slider.syncExists = $(slider.vars.sync).length > 0;
        // SLIDE:
        if (slider.vars.animation === "slide") slider.vars.animation = "swing";
        slider.prop = (vertical) ? "top" : "marginLeft";
        slider.args = {};
        // SLIDESHOW:
        slider.manualPause = false;
        slider.stopped = false;
        //PAUSE WHEN INVISIBLE
        slider.started = false;
        slider.startTimeout = null;
        // TOUCH/USECSS:
        slider.transitions = !slider.vars.video && !fade && slider.vars.useCSS && (function() {
          var obj = document.createElement('div'),
              props = ['perspectiveProperty', 'WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
          for (var i in props) {
            if ( obj.style[ props[i] ] !== undefined ) {
              slider.pfx = props[i].replace('Perspective','').toLowerCase();
              slider.prop = "-" + slider.pfx + "-transform";
              return true;
            }
          }
          return false;
        }());
        // CONTROLSCONTAINER:
        if (slider.vars.controlsContainer !== "") slider.controlsContainer = $(slider.vars.controlsContainer).length > 0 && $(slider.vars.controlsContainer);
        // MANUAL:
        if (slider.vars.manualControls !== "") slider.manualControls = $(slider.vars.manualControls).length > 0 && $(slider.vars.manualControls);

        // RANDOMIZE:
        if (slider.vars.randomize) {
          slider.slides.sort(function() { return (Math.round(Math.random())-0.5); });
          slider.container.empty().append(slider.slides);
        }

        slider.doMath();

        // INIT
        slider.setup("init");

        // CONTROLNAV:
        if (slider.vars.controlNav) methods.controlNav.setup();

        // DIRECTIONNAV:
        if (slider.vars.directionNav) methods.directionNav.setup();

        // KEYBOARD:
        if (slider.vars.keyboard && ($(slider.containerSelector).length === 1 || slider.vars.multipleKeyboard)) {
          $(document).bind('keyup', function(event) {
            var keycode = event.keyCode;
            if (!slider.animating && (keycode === 39 || keycode === 37)) {
              var target = (keycode === 39) ? slider.getTarget('next') :
                           (keycode === 37) ? slider.getTarget('prev') : false;
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }
          });
        }
        // MOUSEWHEEL:
        if (slider.vars.mousewheel) {
          slider.bind('mousewheel', function(event, delta, deltaX, deltaY) {
            event.preventDefault();
            var target = (delta < 0) ? slider.getTarget('next') : slider.getTarget('prev');
            slider.flexAnimate(target, slider.vars.pauseOnAction);
          });
        }

        // PAUSEPLAY
        if (slider.vars.pausePlay) methods.pausePlay.setup();

        //PAUSE WHEN INVISIBLE
        if (slider.vars.slideshow && slider.vars.pauseInvisible) methods.pauseInvisible.init();

        // SLIDSESHOW
        if (slider.vars.slideshow) {
          if (slider.vars.pauseOnHover) {
            slider.hover(function() {
              if (!slider.manualPlay && !slider.manualPause) slider.pause();
            }, function() {
              if (!slider.manualPause && !slider.manualPlay && !slider.stopped) slider.play();
            });
          }
          // initialize animation
          //If we're visible, or we don't use PageVisibility API
          if(!slider.vars.pauseInvisible || !methods.pauseInvisible.isHidden()) {
            (slider.vars.initDelay > 0) ? slider.startTimeout = setTimeout(slider.play, slider.vars.initDelay) : slider.play();
          }
        }

        // ASNAV:
        if (asNav) methods.asNav.setup();

        // TOUCH
        if (touch && slider.vars.touch) methods.touch();

        // FADE&&SMOOTHHEIGHT || SLIDE:
        if (!fade || (fade && slider.vars.smoothHeight)) $(window).bind("resize orientationchange focus", methods.resize);

        slider.find("img").attr("draggable", "false");

        // API: start() Callback
        setTimeout(function(){
          slider.vars.start(slider);
        }, 200);
      },
      asNav: {
        setup: function() {
          slider.asNav = true;
          slider.animatingTo = Math.floor(slider.currentSlide/slider.move);
          slider.currentItem = slider.currentSlide;
          slider.slides.removeClass(namespace + "active-slide").eq(slider.currentItem).addClass(namespace + "active-slide");
          if(!msGesture){
              slider.slides.on(eventType, function(e){
                e.preventDefault();
                var $slide = $(this),
                    target = $slide.index();
                var posFromLeft = $slide.offset().left - $(slider).scrollLeft(); // Find position of slide relative to left of slider container
                if( posFromLeft <= 0 && $slide.hasClass( namespace + 'active-slide' ) ) {
                  slider.flexAnimate(slider.getTarget("prev"), true);
                } else if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass(namespace + "active-slide")) {
                  slider.direction = (slider.currentItem < target) ? "next" : "prev";
                  slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                }
              });
          }else{
              el._slider = slider;
              slider.slides.each(function (){
                  var that = this;
                  that._gesture = new MSGesture();
                  that._gesture.target = that;
                  that.addEventListener("MSPointerDown", function (e){
                      e.preventDefault();
                      if(e.currentTarget._gesture)
                          e.currentTarget._gesture.addPointer(e.pointerId);
                  }, false);
                  that.addEventListener("MSGestureTap", function (e){
                      e.preventDefault();
                      var $slide = $(this),
                          target = $slide.index();
                      if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass('active')) {
                          slider.direction = (slider.currentItem < target) ? "next" : "prev";
                          slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                      }
                  });
              });
          }
        }
      },
      controlNav: {
        setup: function() {
          if (!slider.manualControls) {
            methods.controlNav.setupPaging();
          } else { // MANUALCONTROLS:
            methods.controlNav.setupManual();
          }
        },
        setupPaging: function() {
          var type = (slider.vars.controlNav === "thumbnails") ? 'control-thumbs' : 'control-paging',
              j = 1,
              item,
              slide;

          slider.controlNavScaffold = $('<ol class="'+ namespace + 'control-nav ' + namespace + type + '"></ol>');

          if (slider.pagingCount > 1) {
            for (var i = 0; i < slider.pagingCount; i++) {
              slide = slider.slides.eq(i);
              item = (slider.vars.controlNav === "thumbnails") ? '<img src="' + slide.attr( 'data-thumb' ) + '"/>' : '<a>' + j + '</a>';
              if ( 'thumbnails' === slider.vars.controlNav && true === slider.vars.thumbCaptions ) {
                var captn = slide.attr( 'data-thumbcaption' );
                if ( '' != captn && undefined != captn ) item += '<span class="' + namespace + 'caption">' + captn + '</span>';
              }
              slider.controlNavScaffold.append('<li>' + item + '</li>');
              j++;
            }
          }

          // CONTROLSCONTAINER:
          (slider.controlsContainer) ? $(slider.controlsContainer).append(slider.controlNavScaffold) : slider.append(slider.controlNavScaffold);
          methods.controlNav.set();

          methods.controlNav.active();

          slider.controlNavScaffold.delegate('a, img', eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                slider.direction = (target > slider.currentSlide) ? "next" : "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();

          });
        },
        setupManual: function() {
          slider.controlNav = slider.manualControls;
          methods.controlNav.active();

          slider.controlNav.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                (target > slider.currentSlide) ? slider.direction = "next" : slider.direction = "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        set: function() {
          var selector = (slider.vars.controlNav === "thumbnails") ? 'img' : 'a';
          slider.controlNav = $('.' + namespace + 'control-nav li ' + selector, (slider.controlsContainer) ? slider.controlsContainer : slider);
        },
        active: function() {
          slider.controlNav.removeClass(namespace + "active").eq(slider.animatingTo).addClass(namespace + "active");
        },
        update: function(action, pos) {
          if (slider.pagingCount > 1 && action === "add") {
            slider.controlNavScaffold.append($('<li><a>' + slider.count + '</a></li>'));
          } else if (slider.pagingCount === 1) {
            slider.controlNavScaffold.find('li').remove();
          } else {
            slider.controlNav.eq(pos).closest('li').remove();
          }
          methods.controlNav.set();
          (slider.pagingCount > 1 && slider.pagingCount !== slider.controlNav.length) ? slider.update(pos, action) : methods.controlNav.active();
        }
      },
      directionNav: {
        setup: function() {
          var directionNavScaffold = $('<ul class="' + namespace + 'direction-nav"><li><a class="' + namespace + 'prev" href="#">' + slider.vars.prevText + '</a></li><li><a class="' + namespace + 'next" href="#">' + slider.vars.nextText + '</a></li></ul>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            $(slider.controlsContainer).append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider.controlsContainer);
          } else {
            slider.append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider);
          }

          methods.directionNav.update();

          slider.directionNav.bind(eventType, function(event) {
            event.preventDefault();
            var target;

            if (watchedEvent === "" || watchedEvent === event.type) {
              target = ($(this).hasClass(namespace + 'next')) ? slider.getTarget('next') : slider.getTarget('prev');
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function() {
          var disabledClass = namespace + 'disabled';
          if (slider.pagingCount === 1) {
            slider.directionNav.addClass(disabledClass).attr('tabindex', '-1');
          } else if (!slider.vars.animationLoop) {
            if (slider.animatingTo === 0) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "prev").addClass(disabledClass).attr('tabindex', '-1');
            } else if (slider.animatingTo === slider.last) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "next").addClass(disabledClass).attr('tabindex', '-1');
            } else {
              slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
            }
          } else {
            slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
          }
        }
      },
      pausePlay: {
        setup: function() {
          var pausePlayScaffold = $('<div class="' + namespace + 'pauseplay"><a></a></div>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            slider.controlsContainer.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider.controlsContainer);
          } else {
            slider.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider);
          }

          methods.pausePlay.update((slider.vars.slideshow) ? namespace + 'pause' : namespace + 'play');

          slider.pausePlay.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              if ($(this).hasClass(namespace + 'pause')) {
                slider.manualPause = true;
                slider.manualPlay = false;
                slider.pause();
              } else {
                slider.manualPause = false;
                slider.manualPlay = true;
                slider.play();
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function(state) {
          (state === "play") ? slider.pausePlay.removeClass(namespace + 'pause').addClass(namespace + 'play').html(slider.vars.playText) : slider.pausePlay.removeClass(namespace + 'play').addClass(namespace + 'pause').html(slider.vars.pauseText);
        }
      },
      touch: function() {
        var startX,
          startY,
          offset,
          cwidth,
          dx,
          startT,
          scrolling = false,
          localX = 0,
          localY = 0,
          accDx = 0;

        if(!msGesture){
            el.addEventListener('touchstart', onTouchStart, false);

            function onTouchStart(e) {
              if (slider.animating) {
                e.preventDefault();
              } else if ( ( window.navigator.msPointerEnabled ) || e.touches.length === 1 ) {
                slider.pause();
                // CAROUSEL:
                cwidth = (vertical) ? slider.h : slider. w;
                startT = Number(new Date());
                // CAROUSEL:

                // Local vars for X and Y points.
                localX = e.touches[0].pageX;
                localY = e.touches[0].pageY;

                offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                         (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                         (carousel && slider.currentSlide === slider.last) ? slider.limit :
                         (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                         (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                startX = (vertical) ? localY : localX;
                startY = (vertical) ? localX : localY;

                el.addEventListener('touchmove', onTouchMove, false);
                el.addEventListener('touchend', onTouchEnd, false);
              }
            }

            function onTouchMove(e) {
              // Local vars for X and Y points.

              localX = e.touches[0].pageX;
              localY = e.touches[0].pageY;

              dx = (vertical) ? startX - localY : startX - localX;
              scrolling = (vertical) ? (Math.abs(dx) < Math.abs(localX - startY)) : (Math.abs(dx) < Math.abs(localY - startY));

              var fxms = 500;

              if ( ! scrolling || Number( new Date() ) - startT > fxms ) {
                e.preventDefault();
                if (!fade && slider.transitions) {
                  if (!slider.vars.animationLoop) {
                    dx = dx/((slider.currentSlide === 0 && dx < 0 || slider.currentSlide === slider.last && dx > 0) ? (Math.abs(dx)/cwidth+2) : 1);
                  }
                  slider.setProps(offset + dx, "setTouch");
                }
              }
            }

            function onTouchEnd(e) {
              // finish the touch by undoing the touch session
              el.removeEventListener('touchmove', onTouchMove, false);

              if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                var updateDx = (reverse) ? -dx : dx,
                    target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                  slider.flexAnimate(target, slider.vars.pauseOnAction);
                } else {
                  if (!fade) slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                }
              }
              el.removeEventListener('touchend', onTouchEnd, false);

              startX = null;
              startY = null;
              dx = null;
              offset = null;
            }
        }else{
            el.style.msTouchAction = "none";
            el._gesture = new MSGesture();
            el._gesture.target = el;
            el.addEventListener("MSPointerDown", onMSPointerDown, false);
            el._slider = slider;
            el.addEventListener("MSGestureChange", onMSGestureChange, false);
            el.addEventListener("MSGestureEnd", onMSGestureEnd, false);

            function onMSPointerDown(e){
                e.stopPropagation();
                if (slider.animating) {
                    e.preventDefault();
                }else{
                    slider.pause();
                    el._gesture.addPointer(e.pointerId);
                    accDx = 0;
                    cwidth = (vertical) ? slider.h : slider. w;
                    startT = Number(new Date());
                    // CAROUSEL:

                    offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                        (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                            (carousel && slider.currentSlide === slider.last) ? slider.limit :
                                (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                                    (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                }
            }

            function onMSGestureChange(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                var transX = -e.translationX,
                    transY = -e.translationY;

                //Accumulate translations.
                accDx = accDx + ((vertical) ? transY : transX);
                dx = accDx;
                scrolling = (vertical) ? (Math.abs(accDx) < Math.abs(-transX)) : (Math.abs(accDx) < Math.abs(-transY));

                if(e.detail === e.MSGESTURE_FLAG_INERTIA){
                    setImmediate(function (){
                        el._gesture.stop();
                    });

                    return;
                }

                if (!scrolling || Number(new Date()) - startT > 500) {
                    e.preventDefault();
                    if (!fade && slider.transitions) {
                        if (!slider.vars.animationLoop) {
                            dx = accDx / ((slider.currentSlide === 0 && accDx < 0 || slider.currentSlide === slider.last && accDx > 0) ? (Math.abs(accDx) / cwidth + 2) : 1);
                        }
                        slider.setProps(offset + dx, "setTouch");
                    }
                }
            }

            function onMSGestureEnd(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                    var updateDx = (reverse) ? -dx : dx,
                        target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                    if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                        slider.flexAnimate(target, slider.vars.pauseOnAction);
                    } else {
                        if (!fade) slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                    }
                }

                startX = null;
                startY = null;
                dx = null;
                offset = null;
                accDx = 0;
            }
        }
      },
      resize: function() {
        if (!slider.animating && slider.is(':visible')) {
          if (!carousel) slider.doMath();

          if (fade) {
            // SMOOTH HEIGHT:
            methods.smoothHeight();
          } else if (carousel) { //CAROUSEL:
            slider.slides.width(slider.computedW);
            slider.update(slider.pagingCount);
            slider.setProps();
          }
          else if (vertical) { //VERTICAL:
            slider.viewport.height(slider.h);
            slider.setProps(slider.h, "setTotal");
          } else {
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) methods.smoothHeight();
            slider.newSlides.width(slider.computedW);
            slider.setProps(slider.computedW, "setTotal");
          }
        }
      },
      smoothHeight: function(dur) {
        if (!vertical || fade) {
          var $obj = (fade) ? slider : slider.viewport;
          (dur) ? $obj.animate({"height": slider.slides.eq(slider.animatingTo).height()}, dur) : $obj.height(slider.slides.eq(slider.animatingTo).height());
        }
      },
      sync: function(action) {
        var $obj = $(slider.vars.sync).data("flexslider"),
            target = slider.animatingTo;

        switch (action) {
          case "animate": $obj.flexAnimate(target, slider.vars.pauseOnAction, false, true); break;
          case "play": if (!$obj.playing && !$obj.asNav) { $obj.play(); } break;
          case "pause": $obj.pause(); break;
        }
      },
      uniqueID: function($clone) {
        $clone.find( '[id]' ).each(function() {
          var $this = $(this);
          $this.attr( 'id', $this.attr( 'id' ) + '_clone' );
        });
        return $clone;
      },
      pauseInvisible: {
        visProp: null,
        init: function() {
          var prefixes = ['webkit','moz','ms','o'];

          if ('hidden' in document) return 'hidden';
          for (var i = 0; i < prefixes.length; i++) {
            if ((prefixes[i] + 'Hidden') in document)
            methods.pauseInvisible.visProp = prefixes[i] + 'Hidden';
          }
          if (methods.pauseInvisible.visProp) {
            var evtname = methods.pauseInvisible.visProp.replace(/[H|h]idden/,'') + 'visibilitychange';
            document.addEventListener(evtname, function() {
              if (methods.pauseInvisible.isHidden()) {
                if(slider.startTimeout) clearTimeout(slider.startTimeout); //If clock is ticking, stop timer and prevent from starting while invisible
                else slider.pause(); //Or just pause
              }
              else {
                if(slider.started) slider.play(); //Initiated before, just play
                else (slider.vars.initDelay > 0) ? setTimeout(slider.play, slider.vars.initDelay) : slider.play(); //Didn't init before: simply init or wait for it
              }
            });
          }
        },
        isHidden: function() {
          return document[methods.pauseInvisible.visProp] || false;
        }
      },
      setToClearWatchedEvent: function() {
        clearTimeout(watchedEventClearTimer);
        watchedEventClearTimer = setTimeout(function() {
          watchedEvent = "";
        }, 3000);
      }
    };

    // public methods
    slider.flexAnimate = function(target, pause, override, withSync, fromNav) {
      if (!slider.vars.animationLoop && target !== slider.currentSlide) {
        slider.direction = (target > slider.currentSlide) ? "next" : "prev";
      }

      if (asNav && slider.pagingCount === 1) slider.direction = (slider.currentItem < target) ? "next" : "prev";

      if (!slider.animating && (slider.canAdvance(target, fromNav) || override) && slider.is(":visible")) {
        if (asNav && withSync) {
          var master = $(slider.vars.asNavFor).data('flexslider');
          slider.atEnd = target === 0 || target === slider.count - 1;
          master.flexAnimate(target, true, false, true, fromNav);
          slider.direction = (slider.currentItem < target) ? "next" : "prev";
          master.direction = slider.direction;

          if (Math.ceil((target + 1)/slider.visible) - 1 !== slider.currentSlide && target !== 0) {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            target = Math.floor(target/slider.visible);
          } else {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            return false;
          }
        }

        slider.animating = true;
        slider.animatingTo = target;

        // SLIDESHOW:
        if (pause) slider.pause();

        // API: before() animation Callback
        slider.vars.before(slider);

        // SYNC:
        if (slider.syncExists && !fromNav) methods.sync("animate");

        // CONTROLNAV
        if (slider.vars.controlNav) methods.controlNav.active();

        // !CAROUSEL:
        // CANDIDATE: slide active class (for add/remove slide)
        if (!carousel) slider.slides.removeClass(namespace + 'active-slide').eq(target).addClass(namespace + 'active-slide');

        // INFINITE LOOP:
        // CANDIDATE: atEnd
        slider.atEnd = target === 0 || target === slider.last;

        // DIRECTIONNAV:
        if (slider.vars.directionNav) methods.directionNav.update();

        if (target === slider.last) {
          // API: end() of cycle Callback
          slider.vars.end(slider);
          // SLIDESHOW && !INFINITE LOOP:
          if (!slider.vars.animationLoop) slider.pause();
        }

        // SLIDE:
        if (!fade) {
          var dimension = (vertical) ? slider.slides.filter(':first').height() : slider.computedW,
              margin, slideString, calcNext;

          // INFINITE LOOP / REVERSE:
          if (carousel) {
            //margin = (slider.vars.itemWidth > slider.w) ? slider.vars.itemMargin * 2 : slider.vars.itemMargin;
            margin = slider.vars.itemMargin;
            calcNext = ((slider.itemW + margin) * slider.move) * slider.animatingTo;
            slideString = (calcNext > slider.limit && slider.visible !== 1) ? slider.limit : calcNext;
          } else if (slider.currentSlide === 0 && target === slider.count - 1 && slider.vars.animationLoop && slider.direction !== "next") {
            slideString = (reverse) ? (slider.count + slider.cloneOffset) * dimension : 0;
          } else if (slider.currentSlide === slider.last && target === 0 && slider.vars.animationLoop && slider.direction !== "prev") {
            slideString = (reverse) ? 0 : (slider.count + 1) * dimension;
          } else {
            slideString = (reverse) ? ((slider.count - 1) - target + slider.cloneOffset) * dimension : (target + slider.cloneOffset) * dimension;
          }
          slider.setProps(slideString, "", slider.vars.animationSpeed);
          if (slider.transitions) {
            if (!slider.vars.animationLoop || !slider.atEnd) {
              slider.animating = false;
              slider.currentSlide = slider.animatingTo;
            }
            slider.container.unbind("webkitTransitionEnd transitionend");
            slider.container.bind("webkitTransitionEnd transitionend", function() {
              slider.wrapup(dimension);
            });
          } else {
            slider.container.animate(slider.args, slider.vars.animationSpeed, slider.vars.easing, function(){
              slider.wrapup(dimension);
            });
          }
        } else { // FADE:
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeOut(slider.vars.animationSpeed, slider.vars.easing);
            //slider.slides.eq(target).fadeIn(slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);

            slider.slides.eq(slider.currentSlide).css({"zIndex": 1}).animate({"opacity": 0}, slider.vars.animationSpeed, slider.vars.easing);
            slider.slides.eq(target).css({"zIndex": 2}).animate({"opacity": 1}, slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);

          } else {
            slider.slides.eq(slider.currentSlide).css({ "opacity": 0, "zIndex": 1 });
            slider.slides.eq(target).css({ "opacity": 1, "zIndex": 2 });
            slider.wrapup(dimension);
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) methods.smoothHeight(slider.vars.animationSpeed);
      }
    };
    slider.wrapup = function(dimension) {
      // SLIDE:
      if (!fade && !carousel) {
        if (slider.currentSlide === 0 && slider.animatingTo === slider.last && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpEnd");
        } else if (slider.currentSlide === slider.last && slider.animatingTo === 0 && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpStart");
        }
      }
      slider.animating = false;
      slider.currentSlide = slider.animatingTo;
      // API: after() animation Callback
      slider.vars.after(slider);
    };

    // SLIDESHOW:
    slider.animateSlides = function() {
      if (!slider.animating && focused ) slider.flexAnimate(slider.getTarget("next"));
    };
    // SLIDESHOW:
    slider.pause = function() {
      clearInterval(slider.animatedSlides);
      slider.animatedSlides = null;
      slider.playing = false;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) methods.pausePlay.update("play");
      // SYNC:
      if (slider.syncExists) methods.sync("pause");
    };
    // SLIDESHOW:
    slider.play = function() {
      if (slider.playing) clearInterval(slider.animatedSlides);
      slider.animatedSlides = slider.animatedSlides || setInterval(slider.animateSlides, slider.vars.slideshowSpeed);
      slider.started = slider.playing = true;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) methods.pausePlay.update("pause");
      // SYNC:
      if (slider.syncExists) methods.sync("play");
    };
    // STOP:
    slider.stop = function () {
      slider.pause();
      slider.stopped = true;
    };
    slider.canAdvance = function(target, fromNav) {
      // ASNAV:
      var last = (asNav) ? slider.pagingCount - 1 : slider.last;
      return (fromNav) ? true :
             (asNav && slider.currentItem === slider.count - 1 && target === 0 && slider.direction === "prev") ? true :
             (asNav && slider.currentItem === 0 && target === slider.pagingCount - 1 && slider.direction !== "next") ? false :
             (target === slider.currentSlide && !asNav) ? false :
             (slider.vars.animationLoop) ? true :
             (slider.atEnd && slider.currentSlide === 0 && target === last && slider.direction !== "next") ? false :
             (slider.atEnd && slider.currentSlide === last && target === 0 && slider.direction === "next") ? false :
             true;
    };
    slider.getTarget = function(dir) {
      slider.direction = dir;
      if (dir === "next") {
        return (slider.currentSlide === slider.last) ? 0 : slider.currentSlide + 1;
      } else {
        return (slider.currentSlide === 0) ? slider.last : slider.currentSlide - 1;
      }
    };

    // SLIDE:
    slider.setProps = function(pos, special, dur) {
      var target = (function() {
        var posCheck = (pos) ? pos : ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo,
            posCalc = (function() {
              if (carousel) {
                return (special === "setTouch") ? pos :
                       (reverse && slider.animatingTo === slider.last) ? 0 :
                       (reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                       (slider.animatingTo === slider.last) ? slider.limit : posCheck;
              } else {
                switch (special) {
                  case "setTotal": return (reverse) ? ((slider.count - 1) - slider.currentSlide + slider.cloneOffset) * pos : (slider.currentSlide + slider.cloneOffset) * pos;
                  case "setTouch": return (reverse) ? pos : pos;
                  case "jumpEnd": return (reverse) ? pos : slider.count * pos;
                  case "jumpStart": return (reverse) ? slider.count * pos : pos;
                  default: return pos;
                }
              }
            }());

            return (posCalc * -1) + "px";
          }());

      if (slider.transitions) {
        target = (vertical) ? "translate3d(0," + target + ",0)" : "translate3d(" + target + ",0,0)";
        dur = (dur !== undefined) ? (dur/1000) + "s" : "0s";
        slider.container.css("-" + slider.pfx + "-transition-duration", dur);
         slider.container.css("transition-duration", dur);
      }

      slider.args[slider.prop] = target;
      if (slider.transitions || dur === undefined) slider.container.css(slider.args);

      slider.container.css('transform',target);
    };

    slider.setup = function(type) {
      // SLIDE:
      if (!fade) {
        var sliderOffset, arr;

        if (type === "init") {
          slider.viewport = $('<div class="' + namespace + 'viewport"></div>').css({"overflow": "hidden", "position": "relative"}).appendTo(slider).append(slider.container);
          // INFINITE LOOP:
          slider.cloneCount = 0;
          slider.cloneOffset = 0;
          // REVERSE:
          if (reverse) {
            arr = $.makeArray(slider.slides).reverse();
            slider.slides = $(arr);
            slider.container.empty().append(slider.slides);
          }
        }
        // INFINITE LOOP && !CAROUSEL:
        if (slider.vars.animationLoop && !carousel) {
          slider.cloneCount = 2;
          slider.cloneOffset = 1;
          // clear out old clones
          if (type !== "init") slider.container.find('.clone').remove();
          slider.container.append(slider.slides.first().clone().addClass('clone').attr('aria-hidden', 'true')).prepend(slider.slides.last().clone().addClass('clone').attr('aria-hidden', 'true'));
		      methods.uniqueID( slider.slides.first().clone().addClass('clone') ).appendTo( slider.container );
		      methods.uniqueID( slider.slides.last().clone().addClass('clone') ).prependTo( slider.container );
        }
        slider.newSlides = $(slider.vars.selector, slider);

        sliderOffset = (reverse) ? slider.count - 1 - slider.currentSlide + slider.cloneOffset : slider.currentSlide + slider.cloneOffset;
        // VERTICAL:
        if (vertical && !carousel) {
          slider.container.height((slider.count + slider.cloneCount) * 200 + "%").css("position", "absolute").width("100%");
          setTimeout(function(){
            slider.newSlides.css({"display": "block"});
            slider.doMath();
            slider.viewport.height(slider.h);
            slider.setProps(sliderOffset * slider.h, "init");
          }, (type === "init") ? 100 : 0);
        } else {
          slider.container.width((slider.count + slider.cloneCount) * 200 + "%");
          slider.setProps(sliderOffset * slider.computedW, "init");
          setTimeout(function(){
            slider.doMath();
            slider.newSlides.css({"width": slider.computedW, "float": "left", "display": "block"});
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) methods.smoothHeight();
          }, (type === "init") ? 100 : 0);
        }
      } else { // FADE:
        slider.slides.css({"width": "100%", "float": "left", "marginRight": "-100%", "position": "relative"});
        if (type === "init") {
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeIn(slider.vars.animationSpeed, slider.vars.easing);
            slider.slides.css({ "opacity": 0, "display": "block", "zIndex": 1 }).eq(slider.currentSlide).css({"zIndex": 2}).animate({"opacity": 1},slider.vars.animationSpeed,slider.vars.easing);
          } else {
            slider.slides.css({ "opacity": 0, "display": "block", "webkitTransition": "opacity " + slider.vars.animationSpeed / 1000 + "s ease", "zIndex": 1 }).eq(slider.currentSlide).css({ "opacity": 1, "zIndex": 2});
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) methods.smoothHeight();
      }
      // !CAROUSEL:
      // CANDIDATE: active slide
      if (!carousel) slider.slides.removeClass(namespace + "active-slide").eq(slider.currentSlide).addClass(namespace + "active-slide");

      //FlexSlider: init() Callback
      slider.vars.init(slider);
    };

    slider.doMath = function() {
      var slide = slider.slides.first(),
          slideMargin = slider.vars.itemMargin,
          minItems = slider.vars.minItems,
          maxItems = slider.vars.maxItems;

      slider.w = (slider.viewport===undefined) ? slider.width() : slider.viewport.width();
      slider.h = slide.height();
      slider.boxPadding = slide.outerWidth() - slide.width();

      // CAROUSEL:
      if (carousel) {
        slider.itemT = slider.vars.itemWidth + slideMargin;
        slider.minW = (minItems) ? minItems * slider.itemT : slider.w;
        slider.maxW = (maxItems) ? (maxItems * slider.itemT) - slideMargin : slider.w;
        slider.itemW = (slider.minW > slider.w) ? (slider.w - (slideMargin * (minItems - 1)))/minItems :
                       (slider.maxW < slider.w) ? (slider.w - (slideMargin * (maxItems - 1)))/maxItems :
                       (slider.vars.itemWidth > slider.w) ? slider.w : slider.vars.itemWidth;

        slider.visible = Math.floor(slider.w/(slider.itemW));
        slider.move = (slider.vars.move > 0 && slider.vars.move < slider.visible ) ? slider.vars.move : slider.visible;
        slider.pagingCount = Math.ceil(((slider.count - slider.visible)/slider.move) + 1);
        slider.last =  slider.pagingCount - 1;
        slider.limit = (slider.pagingCount === 1) ? 0 :
                       (slider.vars.itemWidth > slider.w) ? (slider.itemW * (slider.count - 1)) + (slideMargin * (slider.count - 1)) : ((slider.itemW + slideMargin) * slider.count) - slider.w - slideMargin;
      } else {
        slider.itemW = slider.w;
        slider.pagingCount = slider.count;
        slider.last = slider.count - 1;
      }
      slider.computedW = slider.itemW - slider.boxPadding;
    };

    slider.update = function(pos, action) {
      slider.doMath();

      // update currentSlide and slider.animatingTo if necessary
      if (!carousel) {
        if (pos < slider.currentSlide) {
          slider.currentSlide += 1;
        } else if (pos <= slider.currentSlide && pos !== 0) {
          slider.currentSlide -= 1;
        }
        slider.animatingTo = slider.currentSlide;
      }

      // update controlNav
      if (slider.vars.controlNav && !slider.manualControls) {
        if ((action === "add" && !carousel) || slider.pagingCount > slider.controlNav.length) {
          methods.controlNav.update("add");
        } else if ((action === "remove" && !carousel) || slider.pagingCount < slider.controlNav.length) {
          if (carousel && slider.currentSlide > slider.last) {
            slider.currentSlide -= 1;
            slider.animatingTo -= 1;
          }
          methods.controlNav.update("remove", slider.last);
        }
      }
      // update directionNav
      if (slider.vars.directionNav) methods.directionNav.update();

    };

    slider.addSlide = function(obj, pos) {
      var $obj = $(obj);

      slider.count += 1;
      slider.last = slider.count - 1;

      // append new slide
      if (vertical && reverse) {
        (pos !== undefined) ? slider.slides.eq(slider.count - pos).after($obj) : slider.container.prepend($obj);
      } else {
        (pos !== undefined) ? slider.slides.eq(pos).before($obj) : slider.container.append($obj);
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.update(pos, "add");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      //FlexSlider: added() Callback
      slider.vars.added(slider);
    };
    slider.removeSlide = function(obj) {
      var pos = (isNaN(obj)) ? slider.slides.index($(obj)) : obj;

      // update count
      slider.count -= 1;
      slider.last = slider.count - 1;

      // remove slide
      if (isNaN(obj)) {
        $(obj, slider.slides).remove();
      } else {
        (vertical && reverse) ? slider.slides.eq(slider.last).remove() : slider.slides.eq(obj).remove();
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.doMath();
      slider.update(pos, "remove");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      // FlexSlider: removed() Callback
      slider.vars.removed(slider);
    };

    //FlexSlider: Initialize
    methods.init();
  };

  // Ensure the slider isn't focussed if the window loses focus.
  $( window ).blur( function ( e ) {
    focused = false;
  }).focus( function ( e ) {
    focused = true;
  });

  //FlexSlider: Default Settings
  $.flexslider.defaults = {
    namespace: "flex-",             //{NEW} String: Prefix string attached to the class of every element generated by the plugin
    selector: ".slides > li",       //{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
    animation: "fade",              //String: Select your animation type, "fade" or "slide"
    easing: "swing",                //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
    direction: "horizontal",        //String: Select the sliding direction, "horizontal" or "vertical"
    reverse: false,                 //{NEW} Boolean: Reverse the animation direction
    animationLoop: true,            //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
    smoothHeight: false,            //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode
    startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
    slideshow: true,                //Boolean: Animate slider automatically
    slideshowSpeed: 7000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 600,            //Integer: Set the speed of animations, in milliseconds
    initDelay: 0,                   //{NEW} Integer: Set an initialization delay, in milliseconds
    randomize: false,               //Boolean: Randomize slide order
    thumbCaptions: false,           //Boolean: Whether or not to put captions on thumbnails when using the "thumbnails" controlNav.

    // Usability features
    pauseOnAction: true,            //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
    pauseOnHover: false,            //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
    pauseInvisible: true,   		//{NEW} Boolean: Pause the slideshow when tab is invisible, resume when visible. Provides better UX, lower CPU usage.
    useCSS: true,                   //{NEW} Boolean: Slider will use CSS3 transitions if available
    touch: true,                    //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
    video: false,                   //{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches

    // Primary Controls
    controlNav: true,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
    directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
    prevText: "Previous",           //String: Set the text for the "previous" directionNav item
    nextText: "Next",               //String: Set the text for the "next" directionNav item

    // Secondary Navigation
    keyboard: true,                 //Boolean: Allow slider navigating via keyboard left/right keys
    multipleKeyboard: false,        //{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
    mousewheel: false,              //{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
    pausePlay: false,               //Boolean: Create pause/play dynamic element
    pauseText: "Pause",             //String: Set the text for the "pause" pausePlay item
    playText: "Play",               //String: Set the text for the "play" pausePlay item

    // Special properties
    controlsContainer: "",          //{UPDATED} jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be $(".flexslider-container"). Property is ignored if given element is not found.
    manualControls: "",             //{UPDATED} jQuery Object/Selector: Declare custom control navigation. Examples would be $(".flex-control-nav li") or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
    sync: "",                       //{NEW} Selector: Mirror the actions performed on this slider with another slider. Use with care.
    asNavFor: "",                   //{NEW} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider

    // Carousel Options
    itemWidth: 0,                   //{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
    itemMargin: 0,                  //{NEW} Integer: Margin between carousel items.
    minItems: 1,                    //{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
    maxItems: 0,                    //{NEW} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
    move: 0,                        //{NEW} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.
    allowOneSlide: true,           //{NEW} Boolean: Whether or not to allow a slider comprised of a single slide

    // Callback API
    start: function(){},            //Callback: function(slider) - Fires when the slider loads the first slide
    before: function(){},           //Callback: function(slider) - Fires asynchronously with each slider animation
    after: function(){},            //Callback: function(slider) - Fires after each slider animation completes
    end: function(){},              //Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
    added: function(){},            //{NEW} Callback: function(slider) - Fires after a slide is added
    removed: function(){},           //{NEW} Callback: function(slider) - Fires after a slide is removed
    init: function() {}             //{NEW} Callback: function(slider) - Fires after the slider is initially setup
  };

  //FlexSlider: Plugin Function
  $.fn.flexslider = function(options) {
    if (options === undefined) options = {};

    if (typeof options === "object") {
      return this.each(function() {
        var $this = $(this),
            selector = (options.selector) ? options.selector : ".slides > li",
            $slides = $this.find(selector);

      if ( ( $slides.length === 1 && options.allowOneSlide === true ) || $slides.length === 0 ) {
          $slides.fadeIn(400);
          if (options.start) options.start($this);
        } else if ($this.data('flexslider') === undefined) {
          new $.flexslider(this, options);
        }
      });
    } else {
      // Helper strings to quickly perform functions on the slider
      var $slider = $(this).data('flexslider');
      switch (options) {
        case "play": $slider.play(); break;
        case "pause": $slider.pause(); break;
        case "stop": $slider.stop(); break;
        case "next": $slider.flexAnimate($slider.getTarget("next"), true); break;
        case "prev":
        case "previous": $slider.flexAnimate($slider.getTarget("prev"), true); break;
        default: if (typeof options === "number") $slider.flexAnimate(options, true);
      }
    }
  };
})(jQuery);
;/**
 * Init Javascript
 *
 * This contains init code for initialising the page js and variables
 *
 * This was built from original website source code and migrated
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/09
 */

/**
 * Relative path
 *
 * @type {String}
 */
var relPath="";

/**
 * ?
 *
 * @type {String}
 */
var conLan="";

/**
 * The site language
 *
 * This should be upper case
 *
 * @type {string}
 */
var language = config.language.toUpperCase();

/**
 * The site country
 *
 * This should be upper case
 *
 * @type {string}
 */
var country = config.country.toUpperCase();


/**
 * A list of regions
 *
 * Key = Region code
 * Value = Region Name
 *
 * This is populated by loadRegionXML
 *
 * @type {Array}
 */
var regions = [];

/**
 * A list of countries
 *
 * @type {Array}
 */
var countries = [];

/**
 * Determine if we are a device
 * This variable is used to do *lots* of checks because we want to use the native <select>s on devices instead of the linkselect plugin.
 *
 * @type {Boolean} True if the device is an iPhone, iPad or Android device, False if not a device
 */
var iDevice = (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Android/i)) ? true : false;

/**
 * All the translated short-months for the datepicker.
 *
 * @type {Array}
 */
// The datepicker translations are set in lib/libraries.js (right at the bottom of the file)
var monthArrays = [];
monthArrays['en'] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
monthArrays['fr'] = ['Jan', 'F\u00E9v', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Ao\u00FB', 'Sep', 'Oct', 'Nov', 'D\u00E9c'];
monthArrays['de'] = ['Jan', 'Feb', 'M\u00E4r', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'];
monthArrays['it'] = ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'];
monthArrays['es'] = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
monthArrays['pt'] = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
monthArrays['zh'] = ['\u4E00\u6708', '\u4E8C\u6708', '\u4E09\u6708', '\u56DB\u6708', '\u4E94\u6708', '\u516D\u6708', '\u4E03\u6708', '\u516B\u6708', '\u4E5D\u6708', '\u5341\u6708', '\u5341\u4E00\u6708', '\u5341\u4E8C\u6708'];
monthArrays['zh_cn'] = ['\u4E00\u6708', '\u4E8C\u6708', '\u4E09\u6708', '\u56DB\u6708', '\u4E94\u6708', '\u516D\u6708', '\u4E03\u6708', '\u516B\u6708', '\u4E5D\u6708', '\u5341\u6708', '\u5341\u4E00\u6708', '\u5341\u4E8C\u6708'];
monthArrays['ja'] = ['1\u6708', '2\u6708', '3\u6708', '4\u6708', '5\u6708', '6\u6708', '7\u6708', '8\u6708', '9\u6708', '10\u6708', '11\u6708', '12\u6708'];
// Populate the 2 arrays used in the Booking & Meta Blocks with the correct (translated) months
// mbMonths	- Meta Block
// months	- Booking Block
//TODO Need to determine if this is a valid way of doing this
var months = monthArrays[config.language.toLowerCase()]; //mbMonths

/**
 * Cookie Enabled
 *
 * ?? Why is this a string value ??
 *
 * @type {string}
 */
var isCookieEnabled = config.isCookieEnabled;


/**
 * Get todays date
 *
 * @type {Date}
 */
var today = new Date();

/**
 * Helper variable to set the day with a leading 0
 *
 * This always spits out a 2 character string
 * If the day is under 10, it just adds a zero to the front of the string (eg: 03 or 16)
 *
 * @type {string}
 */
var leadingZeroDay = ('0' + today.getDate()).slice(-2);


/**
 * Get the formatted version of today
 *
 * @type {string}
 */
var todayFormatted = leadingZeroDay + '-' + monthArrays['en'][today.getMonth()] + '-' + today.getFullYear();


/**
 * Set the Ajax Load Leisure image
 *
 * ?? TODO Is this actually needed ??
 *
 * @type {string}
 */
var ajax_load_leisure = "<img class='loading' src='" + relPath + "/images/ajaxloader/ajax-loader.gif' alt='loading...' />";

/**
 * Messages Array
 *
 * This Array is used For internationalization of validation messages in JS..
 * Required properties are read and pushed to this array such that they can be
 * accessed form js files..
 *
 * @type {Array}
 */
var propsArray = [];


/**
 * The destinations xml for populating the options
 *
 * @type {xml} An xml object
 */
var xmlDestinations = null;


/**
 * The flight status xml
 *
 * @type {xml} An xml object
 */
var xmlFlightStatus = null;


/**
 * The flight status xml
 *
 * @type {xml} An xml object
 */
var xmlChkInStatus = null

/**
 * What is this controlling???
 *
 * @type {boolean}
 */
var isMatrixSearch = false;


/**
 * Maximum allowed guests for hotel bookings
 *
 * @type {number}
 */
var hotelMaxPax = 9;


/**
 * Date Format
 *
 * @type {string}
 */
var dateFormat = 'dd-M y';


/**
 * URL for the global notices
 *
 * @type {string}
 */
var globalNoticeUrl = '';

/**
 * URL for the local notices
 *
 * @type {string}
 */
var localNoticeUrl = '';



//need to investigate these
var tab1 = [], tab2 = [], tab3 = [], tab4 = [], tabVal1 = [], tabVal2 = [], tabVal3 = [], tabVal4 = [], regionXml = [], departXml = [];
var depCountry = '';
var carRentalPosConnId = '';
var posConnId;
// Append the language to the body
//TODO Is this actually needed?
$('body').removeClass('en').addClass(language);


/**
 * Browser properties
 */

/**
 * User Agent
 *
 * @type {string}
 */
var agt = navigator.userAgent.toLowerCase();

/**
 * Browser version
 *
 * @type {string}
 */
var appVer = navigator.appVersion.toLowerCase();

/**
 * Index of IE in the app Version
 *
 * @type {string}
 */
var iePos = appVer.indexOf('msie');

/**
 * The list of cabin classes
 *
 * @type {string}
 */
var cabinClass = '';








/**
 * Load bindings
 */
$(window).load(function(){
    if (iDevice) {
        //TODO This should use language translation!!!
        alert("We've detected you're on a mobile touch browser, please use two fingers to scroll the drop-down options.");
    }
});


/**
 * Document ready bindings
 */
$(document).ready(function(){

    //TODO Check if this can be changed as the wrapping form should be removed
    if (config.mobileUser == true) {

        //TODO this is missing from the page currently! And it should be in propsArray
        if (confirm(document.getElementById('MsgMU').innerHTML)) {
            //TODO Update to redirect without a form submittion
            //document.forms[0].action = relPath + '/splashPageAction!mobileRedirect.action';
            //document.forms[0].submit();
            window.location = relPath + '/splashPageAction!mobileRedirect.action';
        }
    }

    // Determine the browser ie version
    var is_minor, is_major; //TODO Check if these are needed as variables
    if (iePos != -1) {
        is_minor = parseFloat(appVer.substring(iePos + 5, appVer.indexOf(';', iePos)));
        is_major = parseInt(is_minor);
        if (is_major <= 8) {
            alert(propsArray['ieVersion_error']);
        }
    }

    // A neat little trick to open external links in new tabs
    // Just add rel="external" to an <a>
    $('a[rel|=external]').click(function () {
        window.open(this.href);
        return false
    });

    //create an event for the overlay open and close
    $.Event("modal-open");
    $.Event("modal-closed");
    $('body').on('modal-close', function(){
        $('#disabler').fadeOut();
        $('.overlay').fadeOut();
    });

    // Close the modals when you click the body
    $('#disabler, .overlay').click(function () {
        closePopups()
    });
    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            closePopups();
        }
    });
    $('#iframePopup .close').click(function(){
        iframePopupClose();
        return false;
    })



    /**
     * Update the page location and language and get/set the cookie
     */
    // THIS IS NO LONGER NEEDED
    //update the country set to the page
    //if ($.trim($("#sessionCountry").val()) == "") {
    //    $("#sessionCountry").val($("#selectedCountryDiv").html());
    //}
    //$('#selLanguage').val($('.current').attr('rel'));

    if (country.length > 0) {
        conLan = "/" + country.toLowerCase() + "/" + language.toLowerCase();
    }
    if (language != "EN") {
        $('body').removeClass('en');
        $('body').addClass(language.toLowerCase());
    }

    //Update locale and country based on cookie
    config.isCookieEnabled = checkCookie();
    toggleRememberDiv(config.isCookieEnabled);
    updateLocationHtml(language, country);


    // Load the datepicker's translated defaults from lib/libraries.js
    $.datepicker.setDefaults($.datepicker.regional[language.toLowerCase()]);

    // JS Trick to keep Search Engine crawlers out of the Snoopy page
    // The Snoopy page's link can be found in the very bottom left corner of the home page
    // It looks like this: § - click it for lots of information!
    $('#snoopy').click(function () {
        viewSnoopy()
    });



    //TODO This should rather be loaded at server run time perhaps?
    popEssInfo(config.language, config.country);




    //TODO Need to check this again with the new design
    $('.icon-info').hoverIntent(function () {
        var head = $(this).find('span .head').text();
        var copy = $(this).find('span .inner').html();
        $('#bb .info-popup header').html('<span class="icon-info"></span>' + head);
        $('#bb .info-popup div').html(copy);
        $('#bb .info-popup').fadeIn();
    }, function () {
        $('#bb .info-popup').fadeOut();
    });







//Calling the function to populate the Global Emergency Notice
    populateGlobalNotice();
//Calling the function to populate the Global Emergency Notice
    populateLocalNotice();







});

;


// From flysaa_common.js

/**
 * Determine the key that was pressed
 *
 * @param event
 * @returns {integer} The int value of the key pressed
 */
function getKeyCode(event) {
    return  event.keyCode ? event.keyCode :
                event.charCode ? event.charCode :
                    event.which ? event.which : void 0;
}

/**
 * Is the key pressed an integer?
 *
 * @param event
 * @returns {boolean}
 */
function numeric(event) {
    var keyCode = getKeyCode(event)
    if (((keyCode >= 48) && (keyCode <= 57) || (keyCode == 46)) || (keyCode == 8) || keyCode == 9) {
        event.returnValue = true;
        return true;
    } else {
        event.preventDefault;
        return false;
    }
}


/**
 * Override alert function for showing modalwindow based popup for alert mesages
 *
 * @param {string} The message to display
 */
function alert(msg) {
    //TODO update this to refer to the propsArray
    modalShow('scriptAlert', getLocalizedMessageUnformatted('Message', 'msgAlertHeader'), msg);
}

/**
 * Get localised message
 *
 * This method can be used to get the localized messages in javascript.This
 * method takes 2 parameters.. 1. defaultMessage - message to be used if
 * localized message is not available 2. keyIndex - index of the message in
 * propsArray
 *
 * @param {string} The default message to display
 * @param {string} Get the key for finding the translated message
 */
function getLocalizedMessage(defaultMessage, keyIndex) {
    if (propsArray[keyIndex]) {
        //return propsArray[keyIndex]+'\n';
        return propsArray[keyIndex] + '<br />';
    } else {
        //return defaultMessage+'\n';
        return defaultMessage + '<br />';
    }
}

/**
 * Get localised message
 *
 * This method can be used to get the localized messages in javascript.This
 * method takes 2 parameters.. 1. defaultMessage - message to be used if
 * localized message is not available 2. keyIndex - index of the message in
 * propsArray
 *
 * @param {string} The default message to display
 * @param {string} Get the key for finding the translated message
 */
function getLocalizedMessageUnformatted(defaultMessage, keyIndex) {
    if (propsArray[keyIndex]) {
        return propsArray[keyIndex];
    } else {
        return defaultMessage;
    }
}

/**
 * Get localised message using only the keyIndex
 *
 * @param keyIndex
 * @returns {*}
 */
function getLocalizedMessageFromKey(keyIndex) {
    if (propsArray[keyIndex]) {
        return propsArray[keyIndex];
    } else {
        return '';
    }
}


/**
 * Fix to calculate the center of the page
 *
 * Used for center() in modalShow()
 *
 * @returns {jQuery.fn}
 */
jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
    this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
    return this;
};

/**
 * Helper function to Fade elements in or out
 *
 * @param {string} The element id
 * @param {string} type of fade direction: fadeIn, fadeOut
 * @param {mixed} The speed of the fade. @see jQuery.fadeIn or jQuery.fadeOut for supported values
 */
function adjustedFade(id, direction, speed) {
    if ($.browser.msie && ($.browser.version > 6)) {
        if (direction == 'fadeIn') {
            $(id).show(0);
        } else {
            $(id).hide(0);
        }
    } else if ($.browser.msie && ($.browser.version == 6)) {
        if (direction == 'fadeIn') {
            $(id).show();
        } else {
            $(id).hide();
        }
    } else {
        if (direction == 'fadeIn') {
            $(id).fadeIn(speed);
        } else {
            $(id).fadeOut(speed);
        }
    }
}


/**
 * This method is added for the Snoopy Information action
 *
 * TODO Is this actually needed???
 */
function viewSnoopy() {
    var url = relPath + conLan + "/viewSnoopy!snoopy.action";
    window.open(url, 'subWin', 'height=800,width=1000,resizable=yes,scrollbars=yes');
}


/**
 * Checks the cookie for if the remembered Locale and Lang exists
 *
 * This will update the global country and language as well
 *
 * @returns {boolean} True if the cookie was found and valid, false if not
 */
function checkCookie() {
    var rememberLocale = getCookie('rememberLocale');
    var rememberLang = getCookie('rememberLang');
    if (rememberLocale != null && rememberLocale != "" && rememberLang != null && rememberLang != "") {
        language = rememberLang;
        country = rememberLocale;
        return true;
    } else {
        return false;
    }
}

/**
 * Set a cookie key with its value
 *
 * @param c_name The name of the cookie variable
 * @param value The value to set
 * @param expiremonths Expiry date
 * @param path The path
 */
function setCookie(c_name, value, expiremonths, path) {
    var exdate = new Date();
    exdate.setMonth(exdate.getMonth() + expiremonths);
// var pathString = ((path == null) ? "" : ("; path=" + path));
    document.cookie = c_name + "=" + escape(value) + ((path == null) ? "" : ("; path=" + path)) +
    ((expiremonths == null) ? "" : ";expires=" + exdate.toUTCString());
}

/**
 * Get a value from the cookie
 *
 * @param c_name The variable name to fetch the value for
 * @returns {mixed} The value of the cookie if found or an empty string
 */
function getCookie(c_name) {
    var cValue = "", tmpArray;
    if (document.cookie.length > 0) {
        tmpArray = document.cookie.split(";");
        for (var i = 0; i < tmpArray.length; i++) {
            if (tmpArray[i].indexOf(c_name + "=") == 1) {
                cValue = tmpArray[i].split("=");
            }
        }
        if (cValue.length > 0) {
            return cValue[1];
        }
    }
    return "";
}

/**
 * Checks for selected radio button in the button group
 *
 * This method cannot be used for radio values
 *
 * @param buttonGroup
 * @returns {boolean}
 */
function checkRadioSelection(buttonGroup) {
    if (buttonGroup) {
        for (var i = 0; i < buttonGroup.length; i++) {
            if (buttonGroup[i].checked) {
                return true;
            }
        }
    }
    return false;
}


/**
 * A series of date functions
 */

/**
 * Gets the days in the current month
 *
 * Used for Select box-Calendat component
 *
 * @param {int} The month
 * @param {int} The year
 * @returns {number}
 */
function daysInMonth(month, year) {
    return 32 - new Date(year, month, 32).getDate();
}

/**
 * Is the date valid
 *
 * @param {string} The date to check as a string
 * @param {string} The date format, Only supports strings in the format "dd-M y"
 * @returns {boolean} true if valid, false if not a valid date
 */
function isValidDate(dateToCheck, format) {
    if (format == 'dd-M y' && dateToCheck != "") {
        var dateArray = dateToCheck.split('-');
        if (dateArray[0].length == 0 || dateArray[1].length == 0) {
            return false;
        }
        if (dateArray[0] > daysInMonth(getMonthIndex(dateArray[1].split(' ')[0]), '20' + dateArray[1].split(' ')[1])) {
            return false;
        }
    }
    return true;
}

/**
 * Compare date with current date
 *
 *  Note: Only supports strings in the format "dd-M y"
 *
 * @param {string} The date as a string
 * @param {string} The format of the strings supplied, Only supports strings in the format "dd-M y"
 * @returns {number} @see compareDateObjects return, 1 if date2 in future, -1 if date2 is past date, 0 if both the same
 */
function compareWithCurrentDate(dateStr, format) {
    var date1;
    var dateArray;
    if (format == 'dd-M y') {
        if (dateStr != "") {
            dateArray = dateStr.split('-');

            date1 = new Date('20' + dateArray[1].split(' ')[1], getMonthIndex(dateArray[1].split(' ')[0]), dateArray[0]);
        }
    } else {
        alert('Date format not supported by function..');
        return -1;
    }
    return compareDateObjects(new Date(), date1);
}

/**
 * Compares the given String formated dates to determine if greater, lesser, or equal
 *
 * Note: Only supports strings in the format "dd-M y"
 *
 * @param {string} The date as a string
 * @param {string} The date to compare against as a string
 * @param {string} The format of the strings supplied, only supports strings in the format "dd-M y"
 * @returns {number} @see compareDateObjects return, 1 if date2 in future, -1 if date2 is past date, 0 if both the same
 */
function compareDateStrings(date1, date2, format) {
    var dateObj1;
    var dateObj2;
    if (format == 'dd-M y') {
        if (date1 != "") {
            var dateArray = date1.split('-');
            dateObj1 = new Date('20' + dateArray[1].split(' ')[1], getMonthIndex(dateArray[1].split(' ')[0]), dateArray[0]);
        }
        if (date2 != "") {
            dateArray = date2.split('-');
            dateObj2 = new Date('20' + dateArray[1].split(' ')[1], getMonthIndex(dateArray[1].split(' ')[0]), dateArray[0]);
        }
    } else {
        alert('Date format not supported by function..');
        return -1;
    }
    return compareDateObjects(dateObj1, dateObj2);
}

/**
 * Compares 2 date objects with each other to greater or less or if equal
 *
 * @param {object} The date object to check
 * @param {object} The date object to compare against
 * @returns {number} -1 if date1 > date2, 0 if dates the same, 1 if date1 < date2
 */
function compareDateObjects(date1, date2) {
    if (date1 != "" && date2 != undefined) {
        d1 = date1.getDate();
        d2 = date2.getDate();
        m1 = date1.getMonth();
        m2 = date2.getMonth();
        y1 = date1.getFullYear();
        y2 = date2.getFullYear();
        if (y2 < y1) {
            return -1;
        } else if ((y2 == y1) && (m2 < m1)) {
            return -1;
        } else if ((y2 == y1) && (m2 == m1)) {
            if ((d2 < d1)) {
                return -1;
            } else if ((d2 == d1)) {
                return 0;
            }
        }
        return 1;
    }
}


/**
 * Get the Month Index
 *
 * @param {object} A Date object
 * @returns {void|number}
 */
function getMonthIndex(monthVal) {
    switch (monthVal.toUpperCase()) {
        case 'JAN':
            return Number(0);
        case 'FEB':
            return Number(1);
        case 'MAR':
            return Number(2);
        case 'APR':
            return Number(3);
        case 'MAY':
            return Number(4);
        case 'JUN':
            return Number(5);
        case 'JUL':
            return Number(6);
        case 'AUG':
            return Number(7);
        case 'SEP':
            return Number(8);
        case 'OCT':
            return Number(9);
        case 'NOV':
            return Number(10);
        case 'DEC':
            return Number(11);
    }
}

/**
 * Get the day with a # on the front
 *
 * @param {string} The day
 * @returns {string}
 */
function getDayAndMonthval(day) {
    //TODO this variable is not needed
    var strVal = day;
    var subStr = strVal.split('#');
    var dayValue = '#' + subStr[1];
    return dayValue;
}
/**
 * Get the month index
 *
 * @param {string} The month value
 * @returns {integer}
 */
function getAllMonthIndex(monthValue) {
    var valMonthIndex = getMonthIndex(monthValue.split('-')[0]) + 1;
    return valMonthIndex;
}
/**
 * Get the Year index
 *
 * @param {string} the year
 * @returns {string}
 */
function getAllyearValue(yearValue) {
    var yearVal = yearValue.split('-')[1];
    return yearVal;
}















/**
 *
 * @type {object} Object array of Modal buttons
 */
var modalButtons = {Ok: 1, Back: 2, Next: 3, Continue: 4};

/**
 * Show the Modal
 *
 * @param {string} The element id of the Modal to show
 */
function modalShow(id) {
    var heading = arguments[1];
    if (typeof (heading) != 'undefined') {
        var inner = '<div>' + arguments[2] + '</div>';
        var customButton = arguments[3];
        var buttons = '<div class="actions"><a class="button" onclick="modalClose(\'' + id + '\')"><span>{BUTTON}</span></a></div>';
        $('#' + id + ' h4').first().text(heading);
        if (typeof (customButton) != 'undefined') {
            switch (customButton) {
                case modalButtons.Ok:
                    customButton = 'Ok';
                    break;
                case modalButtons.Back:
                    customButton = 'Back';
                    break;
                case modalButtons.Next:
                    customButton = 'Next';
                    break;
                default:
                    customButton = 'Continue';
                    break;
            }
            buttons = buttons.replace('{BUTTON}', customButton);
        } else {
            buttons = buttons.replace('{BUTTON}', getLocalizedMessageUnformatted('Continue', 'msgButtonContinue'));
        }
        $('#' + id + ' h4').text(heading);
        $('#' + id + ' .inner').html(inner + buttons);
    }
    $('#' + id).center();
    adjustedFade('#disabler', 'fadeIn', 200);
    adjustedFade('#' + id, 'fadeIn', 200);
}
/**
 * Close the Modal
 *
 * @param {string} The element id of the Modal to close
 */
function modalClose(id) {
    adjustedFade('#disabler', 'fadeOut', 200);
    adjustedFade('#' + id, 'fadeOut', 200);
    return false;
}




function iframePopupOpen(url){
    $('#iframePopup .inner').html('<span></span><iframe src="' + url + '" frameborder="0" scrolling="auto"></iframe>');
    $('#iframePopup').fadeIn();
    adjustedFade('#disabler', 'fadeIn', 200);
}

function iframePopupClose(){
    $('#iframePopup .inner').html('');
    $('#iframePopup').fadeOut();
    adjustedFade('#disabler', 'fadeOut', 200);
}



/**
 * General Validation Functions
 */

/**
 * Is valid Email
 *
 * @param {element} The email address jquery element to check
 * @returns {boolean}
 */
function isVEM(field) {
    var pattern = /^(.+)@([^\(\);:,<>]+\.[a-zA-Z]{2,4})/;
    if (field.val().length > 0) {
        var result = field.val().match(pattern);
        if (result == null) {
            field.focus();
            return false;
        }
        return true;
    } else {
        return true;
    }
}



function oc(a) {
    var o = {};
    for (var i = 0; i < a.length; i++) {
        o[a[i]] = '';
    }
    return o;
}


/**
 * Close all popups
 *
 * TODO Check if this is infact global or only homepage
 * TODO Check also if any of these have changed or are no longer relevant
 *
 */
function closePopups() {
    //these should be bound to an event that is fired!!!!
    $('.overlay').fadeOut('fast');
    $('.calendar').fadeOut();
    iframePopupClose();
    modalClose('scriptAlert');
    /*
    $('.linkselect-link').each(function () {
        $(this).blur();
    });
    //$('.linkselect-link').siblings('input').linkselect("close");
     */
    //$('#checkInWrap').slideUp(100);
}






/**
 * Change the language on the page
 *
 * TODO This should be updated to use a different form for posting tha page and setting the locale/language
 *
 * @param langCode
 */
function changeLang(language) {
    if ($('#chkRemember').checked) {
        config.isCookieEnabled = true;
        rememberLocation(language, country);
    } else {
        config.isCookieEnabled = false;
        forgetLocation(false);
    }
    //TODO This is a form update, so needs to be checked!!!
    $('#selLanguage').val(language);
    document.forms.bbFlights.action = relPath + "/" + country.toLowerCase() + "/" + language.toLowerCase() + "/home!loadCountryLanguage.action?request_locale=" + language.toLowerCase() + "_" + country.toUpperCase() + "&splashLocale=" + language + "&splashCntry=" + country + "&isCookieEnabled=" + ((isCookieEnabled) ? "true":"false") + "&langChanged=true";
    document.forms.bbFlights.submit();
}


/**
 * Forget the location
 *
 * @param toggle Toggle the display of the remember/forget section
 */
function forgetLocation(toggle) {
    //clearing cookie
    setCookie('rememberLocale', '', -1, '/');
    setCookie('rememberLang', '', -1, '/');
    //toggle remember effects off
    if (typeof toggle != 'undefined' && toggle == true) {
        toggleRememberDiv(false);
    }
}

/**
 * Remember the location and language
 *
 * @param country The country code
 * @param language The language code
 */
function rememberLocation(language, country)
{
    forgetLocation(false);
    //setting new cookie
    setCookie('rememberLang', language.toLowerCase(), 11, '/');
    setCookie('rememberLocale', country.toLowerCase(), 11, '/');
    //update the html
    updateLocationHtml(language, country);
    //toggle remember effects on
    toggleRememberDiv(true);
}

/**
 * Toggles the remember the user location preference
 *
 * @param remember
 */
function toggleRememberDiv(remember){
    if (typeof remember !== 'undefined' && remember == true) {
        $('#rememberDiv').hide();
        $('#savedDiv').show();
        $('#chkRemember').attr('checked', 'checked');
        $('#rememberDiv span.check').removeClass("un");
    } else {
        $('#rememberDiv').show();
        $('#savedDiv').hide();
        $('#chkRemember').removeAttr('checked');
        $('#rememberDiv span.check').addClass("un");
    }
}

/**
 * Updates the text in the header to show the language
 *
 * @param language
 */
function updateLocationHtml(language, country)
{
    $('#currentLoc').attr('class', country.toLowerCase());
    //update the selected language
    $('#dropLangNav .code .text').html(language.toUpperCase());
    $('#dropLangNav li.active').removeClass('selected');
    $('#dropLangNav a.' + language.toUpperCase()).addClass('selected');
}


/**
 * Load an xml response from the server, run the callback if success or display a message if not
 *
 * @param url The url to get the xml data from
 * @param callback The function to run on success
 * @param responseFormat The type of ajax response to expect and interpret
 * @param failMsg The failed message to display. If not set, a default message of "No matching region found."
 * @param params The parameters to send
 */
function loadXmlResponseAndUpdate(url, callback, responseFormat, failMsg, params){

    $.ajaxSetup({
        cache: false
    });

    var defaultParams = {localeSeltd : config.language};

    //set the detaults
    if(typeof responseFormat == 'undefined' || responseFormat == null) {
        responseFormat = 'xml';
    }
    if (typeof failMsg == 'undefined' || failMsg == null) {
        failMsg = "No matching region found.";
    }

    if (typeof params == 'undefined' || params == null) {
        params = defaultParams;
    } else {
        params = $.extend({}, defaultParams, params);
    }

    $.get(url, params, function(response, status){
        if (status == "success") {
            callback(response);
        } else {
            alert(failMsg);
        }
    }, responseFormat);
}



/**
 * Style the selects
 *
 * This also binds the change event of the select to refresh the styled effect
 *
 * @param e The element(s) to style
 */
function styleSelect(e) {
    if (!iDevice) {
        $(e).linkselect();
    }
}

/**
 * Style the element with a combobox
 *
 * @param e The element(s) to style
 */
function styleSelectCombo(e) {
    $(e).combobox();
}


/**
 * Set the date picker with the dates found in the inputs next to the calendar link
 *
 * @param el The Calendar element to make a datepicker from
 * @param ob The options to pass to the date picker on create
 */
function setDatePicker(el, ob)
{
    if (typeof ob == 'undefined') {
        ob = {};
    }
    $(el).datepicker(ob);
    $(el).click(function (e) {
        e.stopPropagation();
    });
    $(el).parent().click(function(e){

        // This checks whether the Return Date block is disabled
        if ($(this).parent().hasClass('disabled')) return false;

        // Don't auto-close this datepicker
        e.stopPropagation();

        // Close all the other modals though
        //closePopups();

        date = new Date();

        if (iDevice) {
            var dayInput = $(el).parents('.input').find('.day select');
            var monthYearInput = $(el).parents('.input').find('.month-year select');
        } else {
            var dayInput = $(el).parents('.input').find('.day input');
            var monthYearInput = $(el).parents('.input').find('.month-year input');
        }

        // Force the string to be sure IE understands what we're doing... *sigh*
        // This spits out something like: "14-Jan-2012"
        var update = $(dayInput).val() + '-' + $(monthYearInput).val();
        //console.log(update);

        // Check if this if the date is a *valid* date string.
        // Fail if it is not (eg: 31 February 2011) - yay for the drop downs.
        try {
            calDate = $.datepicker.parseDate('dd-M-yy', update, {
                //Due to how the date picker works, if it is localised it won't accept a different localised value if
                //passed to it. If the date picker is in French, it would need "15-Mai-2015" to be valid.
                //This line allows us to pass the english value in
                monthNamesShort: $.datepicker.regional[ "en" ].monthNamesShort
            });
        } catch (e) {
            alert(propsArray['bookingBlock_14']);
            // the datepicker doesnt parse when M is localized,eg: Dezember,Marz,Mai in deutsch.
            return false;
        }

        // Here, we know the date is valid! So..
        // Update the datepicker.
        $(this).children('.calendar').datepicker('setDate', calDate);

        $cal = $(this).children('.calendar:not(.disabled)');

        if (typeof ob.onBeforeFadeIn !== 'undefined') {
            ob.onBeforeFadeIn(function(){
                $cal.fadeIn();
            });
        } else {
            // ..and fade it in!
            $cal.fadeIn();
        }

    });
}


/**
 * Update the dates next to the calendar pop up link
 *
 * @param c The Calendar element. Used to find the inputs to update
 * @param d The date object
 */
function updateDates(c, d) {
    var day = ('0' + d.getDate()).slice(-2);
    var monthYear = monthArrays['en'][d.getMonth()] + '-' + d.getFullYear();

    if (iDevice) {
        var dayInput = $(c).parents('.input').find('.day select');
        var monthYearInput = $(c).parents('.input').find('.month-year select');
    } else {
        var dayInput = $(c).parents('.input').find('.day input');
        var monthYearInput = $(c).parents('.input').find('.month-year input');
    }

    $(dayInput).changeVal(day);
    $(monthYearInput).changeVal(monthYear);
}

/**
 * Creates a formatted date from the given values
 *
 * @param day The day
 * @param monthYear Month Year value as "Jan-2010"
 * @param format The format wanted. Defaults to the global variable dateFormat
 * @returns {String} The date formatted
 */
function getFormatedDate(day, monthYear, format)
{
    if (typeof format == 'undefined') {
        format = dateFormat;
    }
    return $.datepicker.formatDate(format, new Date(day + ' ' + monthYear), {
        monthNamesShort: $.datepicker.regional[ "en" ].monthNamesShort
    });
}

/**
 * Creates a formatted date from the given values
 *
 * @param date The date object
 * @returns {String} The date formatted
 */
function getFormatedDateFromObject(date)
{
    if (typeof format == 'undefined') {
        format = dateFormat;
    }
    return $.datepicker.formatDate(dateFormat, date, {
        monthNamesShort: $.datepicker.regional[ "en" ].monthNamesShort
    });
}



/**
 * Update the Month Year selects to have the translated months
 *
 * @param e The element to the selects to update
 */
function updateMonthYearSelects(e) {
    var dates = [], values = [], now = new Date(), option = new String();
    // February bug
    now.setDate(28);
    //clear them
    $(e).html('');
    // Loop through 13 months. (eg: December 2011 => December 2012)
    for (var i = 0; i < 13; i++) {
        values[i] = monthArrays['en'][now.getMonth()] + '-' + now.getFullYear();
        dates[i] = months[now.getMonth()] + ' ' + now.getFullYear();
        option = '<option value="' + values[i] + '">' + dates[i] + '</option>';
        $(e).append(option);
        now.setMonth(now.getMonth() + 1)
    }
}

/**
 * Update the Month Year selects to have the translated months
 *
 * @param e The element to the selects to update
 */
function updateDaySelects(e) {
    var option = new String();
    //clear them
    $(e).html('');
    // Loop through 13 months. (eg: December 2011 => December 2012)
    for (var i = 1; i < 32; i++) {
        var d = '0' + i.toString();
        option = '<option value="' + d.substr(-2) + '">' + i + '</option>';
        $(e).append(option);
    }
}


/**
 * Updates select input options with a new list
 *
 * @param elm The select element
 * @param elms {object|string} The list of options to update with
 * @param first_text The text to display in the first option
 */
function updateSelectOptions(elm, elms, first_text) {
    $(elm).html('');
    //TODO This may need to be updated to support the ability to customise adding this or not
    if (typeof first_text !== 'undefined') {
        $(elm).append('<option value="">' + first_text + '</option>');
    }
    if (typeof elms == 'string') {
        $(elm).append(elms);
    } else {
        $(elms).each(function () {
            $(elm).append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
        });
    }
}


/**
 * Redirect to the Hotels URL
 */
function goToHotelsURL() {
    var url = "http://clk.tradedoubler.com/click?p=250485&a=2414975&g=18488210&url=http://za.hotels.com/?rffrid=AFF.HCOM.ZA.001.000.2414975.18488210.SAA";
    window.open(url);
    return false;
}


/**
 * Toggle the return files
 *
 * @param enabled
 */
function toggleReturnFields(enabled, el) {
    $container = $(el).parents('.datepicker').find('> div');
    if (iDevice) {
        if (enabled) {
            $($container).show();
        } else {
            $($container).hide();
        }
    } else {
        if (enabled) {
            $($container).find('input').linkselect('disable', false);
            $($container).find('.calendar').datepicker('enable').removeClass('disabled');
            $($container).fadeTo(100, 1).css('cursor', 'pointer').removeClass('disabled');
        }
        else {
            $($container).find('input').linkselect('disable', true);
            $($container).find('.calendar').datepicker('disable').addClass('disabled');
            $($container).fadeTo(100, 0.5).css('cursor', 'default').addClass('disabled');
        }
    }
}








/**
 * Deprecated Functions
 */

/**
 * Forget the Locale and Language for the user
 *
 * TODO Rename this function
 *
 * @deprecated  This is no longer used, use forgetLocation
 */
function forgetMyLoc() {
    forgetLocation();
}



/**
 * Update given selects
 *
 * @param e The elements to loop through
 * @param x The elements to make a combobox
 * @param y The elements exclude
 */
function styleSelects(e, x, y) {
    return;


    $(e).each(function () {
        id = '#' + $(this).attr('id');
        if (!(id in oc(y))) {
            if ($.browser.msie && $.browser.version < 7) {
                $(this).linkselect()
            } else {
                if (id in oc(x)) {
                    $(this).combobox();
                } else {
                    //$(this).linkselect();
                    $(this).selectric();
                }
            }
        }
    });
}



//TODO Check and refactor this
function popEssInfo(locale, country) {
    var essential = new String();
    if (country == 'default') {
        essential = relPath + "/cms/ZA/routeSpecific/IncludeCMS_Content.jsp?template=ESSENTIALINFO&locale=en&country=ZA";
        tryAgain = false;
    } else {
        essential = relPath + "/cms/ZA/routeSpecific/IncludeCMS_Content.jsp?template=ESSENTIALINFO&locale=" + locale + "&country=" + country;
        tryAgain = true;
    }
    $.ajax({
        url: essential, success: function (data) {
            loadEssInfo(data)
        }, error: function (data) {
            errorEssInfo(data, tryAgain)
        }
    });
}
function loadEssInfo(data) {
    $('#essentialInfoLinks').html('');
    $(data).find('a').each(function () {
        val = $(this).attr('href')
        name = $(this).text();
        $('#essentialInfoLinks').append('<li><a href="' + val + '">' + name + '</a></li>');
    });
}
function errorEssInfo(data, tryAgain) {
    if (tryAgain) {
        setTimeout("popEssInfo('','default')", 1000);
    } else {
        $('#essentialInfoLinks').html('');
        //TODO this should use language translation
        $('#menu nav .essential-info').html('<a href="#">Essential Info</a>');
    }
}






/*
 * This method is used to load the Global Emergency notice.
 * This should be called on page load
 *
 * TODO Don't have the styling for this currently
 */
function populateGlobalNotice() {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadGlobalEmergencyXmlFile.action", function(response)
    {
        $(response).find('notice').each(function () {
            $("#globalNoticeDesc").html($(this).text());
            globalNoticeUrl = $(this).attr('value');
            $("#globalnoticediv").show();
        });

    }, 'xml', null, {
        localeSeltd: config.language
    });
}


/*
 * This method will be called when the user clicks the 'more info'
 * tab for getting the details of the Global Emergency Notice.
 */
function cmsPageForGlobalNotice() {
    var cmsUrlForGlobalNotice = globalNoticeUrl;
    /*
     * modifying the code assuming that the user will enter the correct url after rewriting for the news page in the below format say,
     * For pages inside ZA folder: http://www.flysaa.com/$country/$language/emergency/test.html (rewriten url)
     * For pages outside ZA folder: http://test.flysaa.com/cms/common/test.html(exact url)
     */
    if (cmsUrlForGlobalNotice != null && $.trim(cmsUrlForGlobalNotice).length > 0) {
        if (cmsUrlForGlobalNotice.indexOf("$") >= 0) {
            cmsUrlForGlobalNotice = cmsUrlForGlobalNotice.replace("$country", config.country.toLowerCase());
            cmsUrlForGlobalNotice = cmsUrlForGlobalNotice.replace("$language", config.language);
        }
        window.open(cmsUrlForGlobalNotice, "_self");
    } else {
        window.location.href = "#";
    }
}



/*
 * This method is used to load the Local Emergency notice.
 * This should be called on page load
 *
 * TODO Don't have the styling for this currently
 */
function populateLocalNotice() {

    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadLocalEmergencyXmlFile.action", function(response)
    {
        $(response).find('notice').each(function () {
            $("#globalNoticeDesc").html($(this).text());
            globalNoticeUrl = $(this).attr('value');
            $("#globalnoticediv").show();
        });


        $(response).find('notice').each(function () {
            $("#localNoticeDesc").html($(this).text());
            //$("#globalNoticeUrl").html($(this).attr('value'));
            localNoticeUrl = $(this).attr('value');
            $("#localnoticediv").show();
        });

    }, 'xml', null, {
        localeSeltd: config.language,
        localNoticeCntry: config.country.toUpperCase()
    });
}

/*
 * This method will be called when the user clicks the 'more info'
 * tab for getting the details of the Local Emergency Notice.
 */
function cmsPageForLocalNotice() {
    var cmsUrlForLocalNotice = localNoticeUrl;

    /*
     * modifying the code assuming that the user will enter the correct url after rewriting for the news page in the below format say,
     * For pages inside ZA folder: http://www.flysaa.com/$country/$language/emergency/test.html (rewriten url)
     * For pages outside ZA folder: http://test.flysaa.com/cms/common/test.html(exact url)
     */
    if (cmsUrlForLocalNotice != null && $.trim(cmsUrlForLocalNotice).length > 0) {

        if (cmsUrlForLocalNotice.indexOf("$") >= 0) {
            cmsUrlForLocalNotice = cmsUrlForLocalNotice.replace("$country", config.country.toLowerCase());
            cmsUrlForLocalNotice = cmsUrlForLocalNotice.replace("$language", locale);
        }
        window.open(cmsUrlForLocalNotice, "_self");
    } else {
        window.location.href = "#";
    }
}



;/**
 * Header
 *
 * This contains all relevant code for handling the header javascript of the SAA Website
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/09
 */
$(document).ready(function(){

    //bind to the language dropdown
    $('#dropLang a').click(function(){
        changeLang(this.hash.substring(1).toUpperCase());
    });
    //bind to the remember and forget elements
    $('#regionNav .forget a').click(function(){
        forgetLocation(true);
    })
    $('#chkRemember').change(function () {
        if ($(this).prop('checked')) {
            rememberLocation(language, country);
        } else {
            forgetLocation(true);
        }
    });

    //bind to the search field
    $('#searchInput').on('keyup', function(event){
        if (event.keyCode == 13) {
            searchFlysaa();
        }
    });
    $('#searchButton').click(function(){
        searchFlysaa();
    });
    /*
    $('#searchInput').click(function(){
        $(this).value('');
    });
    $('#searchInput').onblur(function(){
        if ($(this.value() == '')) {
            $(this).value($(this).data('value'));
        }
    });
    */

    //TODO This should rather run all server side and be sent with the html on page download
    loadHomeLangXML();
    //update the region menu
    populateMegaMenu();

    //setup the region tabs
    $('#regionNav .tabs').tabs({
        fx: {
            opacity: 'toggle',
            duration: 'fast'
        },
        select: function () {
            // This fixes a bug where the adspace images sometimes don't show
            //TODO need to check why this happens
            //$('.adSpace').hide().show();
        }
    });














});

















/**
 * Search Bar
 *
 * When a user enters in a value in the Search Bar, redirect the user to the search url with what they entered
 */
function searchFlysaa() {
    var i = $('#searchInput').val();
    var e = encodeURIComponent(i);
    window.location.href = relPath + "/cms/commons/flysaa_search.html?searchQuery=" + e;
}



/**
 * Loads the list of languages to display in the dropdown for the current language
 */
/*
//this are replaced by the function call below
function loadHomeLangXML() {
    $.ajaxSetup({
        cache: false
    });
    var params = {
        localeSeltd : config.language
    };
    var url = relPath + "/splashPageAction!loadLangXmlFile.action";
    $.get(url, params, getHomeLangXmlValues, 'xml');
}
function getHomeLangXmlValues(XMLResponse, status) {
    if (status == "success") {
        parseHomeLangXMLResponse(XMLResponse);
    } else {
        alert("No matching region found.");
    }
}
function parseHomeLangXMLResponse(xml) {
    var langCode = '', langDesc = '';
    var selLang = config.language;
    $('#dropLang').html('');
    $(xml).find('language').each(
        function () {
            langDesc = $(this).text();
            langCode = $(this).attr('value');
            if (selLang != langCode.toLowerCase()) {
                $('#dropLang').append('<li><a  onclick="changeLang(' + "'" + langCode + "'" + ');" href="#' + langCode.toLowerCase() + '">' + langDesc + '</a></li>');
            }
        }
    );
}
*/
function loadHomeLangXML(){
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadLangXmlFile.action", function(xml) {
        var langCode = '', langDesc = '';
        $('#dropLang').html('');
        $(xml).find('language').each(
            function () {
                langDesc = $(this).text();
                langCode = $(this).attr('value');
                if (config.language != langCode.toLowerCase()) {
                    $('#dropLang').append('<li><a  onclick="changeLang(' + "'" + langCode + "'" + ');" href="#' + langCode.toLowerCase() + '">' + langDesc + '</a></li>');
                }
            }
        );
    })
}





/**
 * The Region Drop Down and related functions
 *
 * @depends ???
 */


/**
 * Changes Region Mega Menu at Home page
 */
function populateMegaMenu(){
    //TODO refactor where these two variables are collected from
    /*
    var selectedCountry = config.country;
    var selectedLang = config.language;
    if(selectedCountry == null || selectedCountry == "")
    {
        selectedCountry = "ZA";
    }
    if(selectedLang == null || selectedLang == "")
    {
        selectedLang = "EN";
    }
    //loadXMLforCDesc(selectedLang);*/
    //loadPageLabels();
    loadRegionXML();

}

/**
 * Load the Region XML and update the page
 *
 */
function loadRegionXML(){
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadRegionXmlFile.action", function(response)
    {
        //NOTE This could be made into a single data set and avoid the split
        xml =  (response).split('CACHEDELIMITER');

        // for populating car countries
        //TODO Move this to its own section
        //$('#carCountry').html('');
        //country = $("#countryResHeaderId").html();
        //code = '';
        //$('#carCountry').append('<option value="' + code + '">' + country + '</option>');
        //TODO move to its own location and reference the list of countries
        //$('#carCountry').append('<option value="'+code+'">'+country+'</option>');

        //get the countries
        xmlDoc = $.parseXML(xml[1]);
        $countryXml = $(xmlDoc);
        $($countryXml).find('country').each(function(){
            countries[$(this).attr('value')] = $(this).text();
        });

        if (countries[config.country]) {
            //$('#currentLoc').html(countries[config.country]);
            config.countryLong = countries[config.country];
            $('#savedLocale').html(config.countryLong);
        }

        //Get the regions and update the countries for each region on the page
        xmlDoc = $.parseXML(xml[0]);
        $newRegionXml = $(xmlDoc);
        var regionTabs = [];
        $($newRegionXml).find('region').each(function(){
            var regionCode = $(this).attr('value');
            var regionName = $(this).find('region_name').text();

            $('#regionNav .tabs li.' + regionCode + ' a').html(regionName);
            //add to global regions
            regions[regionCode] = regionName;
            //update countries for this region
            regionTabs[regionCode] = [];
            $(this).find("country").each(function(){
                regionTabs[regionCode][$(this).attr('value')] = $(this).text();
            });
        });

        //update all the regions with their countries
        for (var regionCode in regionTabs) {
            var $region = $('#region-' + regionCode.toLowerCase() + ' ul');
            $region.html('');
            for (var countryCode in regionTabs[regionCode]) {
                $region.append('<li><a href="#" onclick="changeCountry(' + "'" + countryCode + "'" + ')">' + regionTabs[regionCode][countryCode] + '</a></li>');
            }
        }
    }, 'html');
}


/**
 * Change the country the user is in.
 *
 * Used with the header and when users click on a new country
 *
 * @param countryCode
 */
function changeCountry(countryCode) {
    updateCookieAndRedirect(config.language, countryCode);
    return false;
}


/**
 * Update the cookie if remember is set and redirect the user to the new URL
 *
 * @param newCountryCode The new country code
 * @param newLanguageCode The new language code
 */
function updateCookieAndRedirect(newLanguageCode, newCountryCode)
{
    rememberLocation(newLanguageCode, newCountryCode);
    if ($('#chkRemember').prop('checked')) {
        isCookieEnabled = "true";
        rememberLocation(newLanguageCode, newCountryCode);
    } else {
        isCookieEnabled = "false";
        forgetLocation(false);
    }
    //TODO this should use a correct form name to avoid any confusions. This should also be changed to a universal function if this function can't be it
    var conLanForHome = "/" + newCountryCode.toLowerCase() + "/" + newLanguageCode.toLowerCase();
    document.forms.bbFlights.elements.country.value = newCountryCode;
    document.forms.bbFlights.action = relPath + conLanForHome + "/home!loadCountryLanguage.action?request_locale=" + newLanguageCode.toLowerCase() + "_" + newCountryCode.toUpperCase() + "&splashLocale=" + newLanguageCode + "&splashCntry=" + newCountryCode + "&isCookieEnabled=" + isCookieEnabled;
    document.forms.bbFlights.submit();
}

;

$(document).ready(function(){

    //determine if the footer is above the bottom of the window and if so, absolute it to the bottom
    $( window ).resize(function() {
        adjustFooter();
    });

    //adjustFooter();

})

function adjustFooter(){
    //position: absolute; width: 100%; bottom: 0;
    var h = $('#footer').offset();
    if ((h.top + $('#footer').height()) < $('body').height()) {
        $('#footer').css({
            position: 'absolute',
            bottom: '0'
        })
    } else {
        $('#footer').css({
            position: 'relative',
            bottom: 'auto'
        })
    }
};

$(document).ready(function(){
    //Load the Slider
    loadSliderImages();
    //Load the Bottom content
    loadFlightDeals();
    loadSpecialOffer();
    loadLeftBlocks();

    //loadAdserverBanners();
    //TODO Element does not exist
    //loadAdSpaceImage();
    //TODO Element does not exist
    //loadGlobalImage();
    //TODO Element does not exist
    //Load the links
    //loadHomeLinks();

});



/**
 *Load OpenCMS image for adspace content using Jquery
 *
 */
function loadAdSpaceImage() {
    $("#adSpaceImage").load(relPath + "/cms/ZA/leisure/AdSpaceImage.html");
}

function loadGlobalImage() {
    $("#specials").load(relPath + "/cms/ZA/leisure/AdGlobalHighlights.html");
}

function loadHomeLinks() {
    $("#homeLinks").load(relPath + "/cms/US/leisure/flysaa_homeLinks.html");
}
function loadAdserverBanners() {
    $("#banners").load(relPath + "/cms/" + country + "/leisure/AdServerImage.html", function (response, status, xhr) {
        if (status == "error") {
            $("#banners").load(relPath + "/cms/ZA/leisure/AdServerImage.html");
        }
    });
}

function loadFlightDeals(){
    $("#flightDeals").load(relPath+"/cms/ZA/HomePageDetails/flysaa_FlightDealsData.html");
}

function loadSpecialOffer(){
    $("#specialOffers").load(relPath+"/cms/ZA/HomePageDetails/flysaa_SpecialOfferData.jsp");
}

function loadSliderImages(){
    $("#slider").load(relPath+"/cms/ZA/HomePageDetails/flysaa_SliderImages.jsp", function(){

        $(".flexslider").flexslider({
            animation: "slide",
            start: function() {
                $("body").removeClass("loading");
            }
        })

    });
}

function loadLeftBlocks(){
    $("#featuredArticles").load(relPath+"/cms/ZA/HomePageDetails/flysaa_LeftBlocks.jsp");
};/**
 * Booking Block
 *
 * This contains all relevant code for handling the booking block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function(){

    // Instantiate the Meta Block & List of Locations' tabs
    $('#bb').tabs({
        fx: {
            opacity: 'toggle',
            duration: 'fast'
        }
    });

    //update the destinations, flight status cities, status cities
    loadHomePageXmls(config.language, config.country);

});


function getDepartDate() {
    var departMonthYear = $('#departMonthYear').val().split('-');
    return new Date(departMonthYear[1], getMonthIndex(departMonthYear[0]), $('#departDay').val());
}

function getReturnDate() {
    var returnMonthYear = $('#returnMonthYear').val().split('-');
    return new Date(returnMonthYear[1], getMonthIndex(returnMonthYear[0]), $('#destDay').val());
}

/**
 * Updates several booking block selects with applicable data
 *
 * NOTE: This should be broken up into several separate calls/functions to allow the blocks to be loaded dynamically
 *
 * @param locale
 * @param country
 */
function loadHomePageXmls(locale, country) {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadHomePageXmlFiles.action", function(response) {
        xml = (response).split('CACHEDELIMITER');

        // to populate check flight status
        xmlFlightStatus = $.parseXML(xml[1]);
        updateFlightStatus(xmlFlightStatus);

        // Check in status cities
        xmlChkInStatus = $.parseXML(xml[2]);
        updateChkInStatus(xml);

        // to populate departure cities
        //TODO This is currently returning null
        xmlDestinations = $.parseXML(xml[3]);
        updateDepartDestCities(xml[3]);

    }, 'html', null, {
        localeSeltd: locale,
        country: country
    });
}

/**
 * Update the Checkin Status Selects
 *
 * @param xml
 */
function updateChkInStatus(xml) {
    var elms = $(xml).find('checkinpoint');
    updateSelectOptions("#IBoardPoint", elms, getLocalizedMessageFromKey('bookingBlock_boardPoint'));
}

/**
 * UPdate the Flight Status Selects
 *
 * @param xml
 */
function updateFlightStatus(xml) {
    var elms = $(xml).find('point');
    updateSelectOptions("#statusDepartCity", elms, getLocalizedMessageFromKey('bookingBlock_departCity'));
    updateSelectOptions("#statusDestCity", elms, getLocalizedMessageFromKey('bookingBlock_destinationCity'));
}

/**
 * Update the Destination and Depart City Selects
 *
 * @param xml
 */
function updateDepartDestCities(xml){
    var elms = $(xml).find('city');
    updateSelectOptions("#departCity", elms, getLocalizedMessageFromKey('bookingBlock_departCity'));
    updateSelectOptions("#destCity", elms, getLocalizedMessageFromKey('bookingBlock_destinationCity'));
}


/**
 * Plugin to auto style the required fields in a form
 *
 */
(function($) {
    // Plugin definition.
    $.fn.blockform = function(options) {

        /**
         * The state of the object
         *
         * @type {null}
         */
        var state = null;

        /**
         * The detaults
         *
         * @type {Object}
         */
        var defaults = {
            comboboxInputs: [],
            selectricInputs: [],
            autocompleteInputs: {},
            dateInputs: {},
            dateDefaults: {
                onBeforeFadeIn: function(callback){
                    $('#bb .overlay').fadeIn('fast', callback);
                },
                onAfterFadeOut: function(){
                    $('#bb .overlay').fadeOut('fast');
                }
            },
            afterInit: function(){}
        };
        /**
         * The settings
         *
         * @type {Object}
         */
        var settings = $.extend( {}, defaults, options );

        var comboboxInputs = [];

        initComboBoxInputs = function(){
            var elms = settings.comboboxInputs;
            for (var i in elms) {
                styleSelectCombo(elms[i]);
                comboboxInputs.push($(elms[i]).attr('id'));
            }
        }

        initSelectricInputs = function(){
            var elms = settings.selectricInputs;
            for (var i in elms) {
                $(elms[i]).each(function(){
                    if ($.inArray($(this).attr('id'), comboboxInputs) == -1) {
                        styleSelect(this);
                    }
                });
            }
        }

        initAutocompleteInputs = function() {
            var elms = settings.autocompleteInputs;
            for (var i in elms) {
                if (elms.hasOwnProperty(i)) {
                    $(i).autocomplete(elms[i]);
                } else {
                    $(elms[i]).autocomplete();
                }
            }
        }

        initDateInputs = function() {
            var elms = settings.dateInputs;
            for (var i in elms) {
                var params = settings.dateDefaults;
                if (elms.hasOwnProperty(i)) {
                    $.extend(params, elms[i]);
                    setDatePicker(i, params);
                } else {
                    setDatePicker(elms[i], params);
                }
            }
        }

        this.state = 'loaded';

        /**
         * Initialise the block and run the standard init calls
         *
         */
        init = function() {
            if (settings.comboboxInputs.length > 0)
                initComboBoxInputs();
            if (settings.selectricInputs.length > 0)
                initSelectricInputs();
            if (!$.isEmptyObject(settings.autocompleteInputs))
                initAutocompleteInputs();
            if (!$.isEmptyObject(settings.dateInputs))
                initDateInputs();
        }

        //$('#bb .overlay').addClass('loading').fadeIn('fast');
        settings.beforeInit();
        init();
        settings.afterInit();
        $('#bb .overlay').fadeOut('fast', function(){
            $(this).removeClass('loading');
        });
    };
// End of closure.
})(jQuery);








/**
 * This method is the callback fucntion configured in the $.get() method. This
 * will be called by the Ajax framework, if the ajax call returned a success. By
 * default two values are passed into this fucntion : The response data and
 * status.
 *
 * @deprecated
 */
function filterFlexiSearchOption(matrixOrTdp, status) {
    //NOTE This conditional would never have been met!!!
    matrixOrTdp = 'TDP';
    if (matrixOrTdp == 'Matrix') {
        //isMatrixSearch	= true;
        //$('.flexibility .flex').fadeOut();
        $('input[name="flexible"][value="true"]').prop('checked', true);
        var flexibleGrp = $('input[name="flexible"]');
        if (flexibleGrp) {
            flexibleGrp[0].checked = true;
        }
    } else {
        checkFlexiOption();
    }
    if ($.trim($("#destCity").val()).length > 0) {
        populateCabinClass();
    }
}
;/**
 * Book a flight block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {
    $('#bb-flights form').blockform({
        comboboxInputs: [
            '#departCity',
            '#destCity'
        ],
        selectricInputs: [
            '#bb-flights form select',
        ],
        dateInputs: {
            "#bb-flights .calendar.returning": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();
                    $('#bb .overlay').fadeOut('fast');
                }
            },
            "#bb-flights .calendar.leaving": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();

                    // Update the drop downs of the returning date too!
                    // Makes sure impossible dates don't happen by accident, basically.
                    var $cal = $('#bb-flights .calendar.returning');
                    $cal.datepicker("option", "minDate", date);
                    updateDates($cal, dateObj);

                    // If the return date box is active...
                    if ($('#chkReturn').prop('checked')) {
                        // Show the return date calendar after half a second
                        // The timeout is there to wait for the other calendar to fade out
                        setTimeout("$('#bb-flights .calendar.returning').parent().click()", 500);
                    }

                    $('#bb .overlay').fadeOut('fast');
                }
            }
        },
        beforeInit: function(){
            updateMonthYearSelects('#bb-flights .month-year select');
            updateDaySelects('#bb-flights .day select');
        },
        afterInit: function(){
            updateDates('#bb-flights .calendar', today);

            $('#departCity').siblings('input').on("autocompleteselect", function(event, ui) {
                updateDestinationCities(ui.item.option.value);
                checkReservationConfiguration(ui.item.option.value, $('#destCity').val());
            });
            $('#destCity').siblings('input').on("autocompleteselect", function(event, ui) {
                checkReservationConfiguration($('#departCity').val(), ui.item.option.value);
            });
            $('#chkReturn').click(function () {
                toggleReturnFields($(this).prop('checked'), $(this));
            });
            //show/hide the flexible options fields
            $('#bb-flights .datepicker input[type="hidden"]').on('change', function(){
                checkFlexiOption();
            });

            $('#departDay').change(function () {
                $('#destDay').changeVal($(this).val());
            });
            $('#departMonthYear').change(function () {
                $('#returnMonthYear').changeVal($(this).val());
            });
            /*
            $('#destDay, #returnMonthYear').change(function () {
                validateFlightDates();
            });
            */
            checkFlexiOption();
        }
    });


    $('#bb-flights form').submit(function () {
        var valMsg = "";
        var error = false;
        var departDate = getDepartDate();
        var returnDate = getReturnDate();

        $('#destCity').siblings('input').removeClass('invalid');
        $('#departCity').siblings('input').removeClass('invalid');
        $('#bb-flights .input .calendar.returning').parent().parent().find('select, .linkselect-link').removeClass('invalid');
        $('#bb-flights .input .calendar.leaving').parent().parent().find('select, .linkselect-link').removeClass('invalid');

        if (!$('#departCity').val().length) {
            $('#departCity').siblings('input').addClass('invalid');
            valMsg += getLocalizedMessage("Please select a Destination City.", "bookingBlock_1");
            error = true;
        }

        if (!$('#destCity').val().length) {
            $('#destCity').siblings('input').addClass('invalid');
            valMsg += getLocalizedMessage("Please select a Departure City.", "bookingBlock_2");
            error = true;
        }

        if ($('#departCity').val().length && $('#destCity').val().length && ($('#departCity').val() == $( '#destCity').val())) {
            valMsg += getLocalizedMessage("Departure and Destination can not be the same.","bookingBlock_3");
            error = true;
        }

        if($("#adultCount").val() < $("#infantCount").val()){
            valMsg += getLocalizedMessage("The adult-infant ratio should be 1:1.","bookingBlock_5");
            error = true;
        }

        if (parseInt($("#adultCount").val()) + parseInt($("#infantCount").val()) + parseInt($( "#childCount").val()) > 9) {
            valMsg += getLocalizedMessage("The total number of adults,children and infants must not exceed 9. Please consider splitting your group or complete the <a href='groupBooking.action'> Groups Request Form </a>","bookingBlock_6");
            error = true;
        }



        if ($('#chkReturn').prop('checked')) {
            if (compareDateObjects(departDate, returnDate) == -1) {
                $('.input .calendar.returning').parent().parent().find('select, .linkselect-link').addClass('invalid');
                valMsg += getLocalizedMessage("Please select a valid return date.", "bookingBlock_12");
                error = true;
            }
            //check if it is a valid date
            var returnMonthYear = $('#returnMonthYear').val().split('-');
            returnMonthYear[0] = getMonthIndex(returnMonthYear[0]);
            if (parseInt($('#destDay').val()) > daysInMonth(returnMonthYear[0], returnMonthYear[1])){
                $('.input .calendar.returning').parent().parent().find('select, .linkselect-link').addClass('invalid');
                valMsg += getLocalizedMessage("Please select a valid travel return date.", "bookingBlock_17");
                error = true;
            }
        }

        //check if it is a valid date
        var departMonthYear = $('#departMonthYear').val().split('-');
        departMonthYear[0] = getMonthIndex(departMonthYear[0]);
        if (parseInt($('#departDay').val()) > daysInMonth(departMonthYear[0], departMonthYear[1])){
            $('.input .calendar.leaving').parent().parent().find('select, .linkselect-link').addClass('invalid');
            valMsg += getLocalizedMessage("Please select a valid travel departure date.", "bookingBlock_19");
            error = true;
        }

        if($('input:checkbox[name=chkReturn]').prop('checked')){
            $("#tripType").val('R');
        } else {
            $("#tripType").val('O');
        }
        var tripType = $("#tripType").val();
        if(!$('input[name="flexible"]').val().length){
            error = true;
        }

        var fromDate = getFormatedDateFromObject(departDate);
        var toDate	= getFormatedDateFromObject(returnDate);
        var dateFormt= 'dd-M y';

        if(tripType == 'R' && toDate == ""){
            valMsg += getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_17");
            error = true;
        }
        /*if('R'==tripType && (!$('#returnMonthYear').val().length && !$('#destDay').val().length ) ){
         valMsg += getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_12");
         error = true;
         }*/

        if(compareWithCurrentDate(fromDate, dateFormt) < 0){
            valMsg += getLocalizedMessage("Please take a future departure date for travel.","bookingBlock_10");
            error = true;
        }

        if(!isValidDate(fromDate, dateFormt)){
            valMsg += getLocalizedMessage("Invalid Departure Date","bookingBlock_18");
            error = true;
        }

        if(tripType == 'R' && !isValidDate(toDate, dateFormt)){
            valMsg += getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_17");
            error = true;
        }

        if(tripType == 'R' && (compareWithCurrentDate(toDate, dateFormt) < 0 || compareDateStrings(fromDate, toDate, dateFormt) < 0)){
            valMsg += getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_12");
            error = true;
        }

        if(compareWithCurrentDate(toDate, dateFormt) < 0) {
            valMsg += getLocalizedMessage("Please take a future return date for travel.","bookingBlock_10");
            error = true;
        }

        if (error) {
            alert(valMsg);
            return false;
        }

        //TODO: The below code has an issue in that populateCabinClass() may still be running before it gets a chance to update the variable depCountry
        document.forms.bbFlights.fromDate.value = fromDate;
        document.forms.bbFlights.toDate.value = toDate;
        document.forms.bbFlights.flexible.value = $('input:radio[name=flexible]:checked').val();
        document.forms.bbFlights.selectedLang.value = config.language;
        //this could be legacy as preferredCabinClass already exists
        document.forms.bbFlights.preferredClass.value = $('#flightClass').val();
        //showProcessing();
        var conLanFlightSearch = "/" + depCountry.toLowerCase() + "/" + config.language.toLowerCase();
        document.forms.bbFlights.action = relPath + conLanFlightSearch + '/flexPricerFlightSearch!flexPricerFlightAvailability.action?request_locale=' + config.language.toLowerCase() + "_" + depCountry.toUpperCase();
    });
});





/**
 * Check the Flexible dates and hide/show flexible option
 *
 * Flexi option is shown to the user only if the depart date is atleast 3 days ahead of current date..
 */
function checkFlexiOption() {

    var flexibleGrp = $('input[name="flexible"]');
    if (flexibleGrp.val()) {
        $('input[name="flexible"][value="false"]').prop('checked', true);
    }

    if (isMatrixSearch) {
        $('input[name="flexible"][value="true"]').prop('checked', true);
        return;
    }

    var curntDte = new Date();
    var departDate = getDepartDate();

    departDate.setHours(0);
    departDate.setMinutes(0);
    departDate.setSeconds(0);

    curntDte.setHours(0);
    curntDte.setMinutes(0);
    curntDte.setSeconds(0);

    /*
     this would never perform its function
     try {
     parseDDate = $.datepicker.parseDate('dd-M-yy', departDate);
     // Populate the formatted field
     popFormattedDates(parseDDate, 'depart');
     } catch (e) {
     $('#departDay_link, #departMonthYear_link').addClass('invalid');
     error = true
     }
     try {
     parseRDate = $.datepicker.parseDate('dd-M-yy', curntDte);
     // Populate the formatted field
     popFormattedDates(parseRDate, 'return');
     } catch (e) {
     $('#destDay_link, #returnMonthYear_link').addClass('invalid');
     error = true
     }
     */

    var difference = (departDate.getTime() - curntDte.getTime()) / (1000 * 60 * 60 * 24);
    difference = Math.round(difference);

    if (difference < 3) {
        $('input[name="flexible"][value="false"]').prop('checked', true);
        $('input[name="flexible"][value="true"]').parents('.input').fadeOut();
    } else {
        $('input[name="flexible"][value="true"]').parents('.input').fadeIn();
    }
}


/**
 * Updates the destination cities with the available destinations for that city
 *
 * @param cityCode
 */
function updateDestinationCities(cityCode) {
    //disable it briefly whilst we fetch the destinations
    //$('#destCity').siblings('input').prop('disabled', true);
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadDestinationXmlFile.action", function(response)
    {
        $('#destCity').html('');
        $('#destCity').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_destinationCity') + '</option>');
        $(response).find('destination').each(function () {
            var countryList = $(this).text();
            var countryArray = countryList.split(',');
            var destination = countryArray[0];
            var code = $(this).attr('value');
            var countryDescription = countryArray[1];
            var countryDescriptionSplit = '';
            if (countryDescription != '') {
                countryDescriptionSplit = ",";
            }
            $('#destCity').append('<option value="' + code + '">' + destination + countryDescriptionSplit + countryDescription + '</option>');

            if (!($('#departCity').val() == $('#destCity').val()) && $('#departCity').val() != '' && $('#destCity').val() != '') {
                isMatrixSearch = false;
                if (countryArray[3] == 'MTXSCHD') {
                    //isMatrixSearch	= true;
                    //$('.flexibility .flex').fadeOut();
                    if ($('input[name="flexible"]')) {
                        $('input[name="flexible"][value="false"]').prop('checked', true);
                    }
                } else {
                    checkFlexiOption();
                }
                if ($("#destCity").val().length > 0) {
                    populateSplashCabinClass(countryArray[2]);
                }
            }
        });
        //enable it
        //$('#destCity').siblings('input').prop('disabled', false).focus();
        //$('#destCity').siblings('input').focus();
    }, 'xml', null, {
        localeSeltd: config.language,
        departCitySeltd: cityCode
    });

}



/**
 * Download and update the cabin classes
 *
 * @param isFirstClassEnabled
 */
function populateSplashCabinClass(isFirstClassEnabled) {
    var inputArray = {};
    if (isFirstClassEnabled == 'Y') {
        inputArray["isFirstClassEnabled"] = true;
    } else {
        inputArray["isFirstClassEnabled"] = false;
    }
    inputArray["localeSeltd"] = locale;
    inputArray["country"] = country;
    $.ajaxSetup({
        cache: false
    });
    var url = relPath + "/splashPageAction!populateSplashCabinClassList.action";
    $("#flightClass").parent().html(ajax_load_leisure).load(url, inputArray, function(){
        styleSelect('#flightClass');
    });
}


/**
 * Populate the correct cabin classes for the current booking
 *
 */
function populateCabinClass() {
    loadXmlResponseAndUpdate(relPath + conLan + "/flightSearch!populateHomeCabinClassList.action", function(response)
    {
        var cabinClassArr = [];
        var options = '';
        $(response).find('cabinClass').each(function () {
            var depcountryList = $(this).text();
            var departureCountryArray = depcountryList.split(',');
            depCountry = departureCountryArray[1];

            var tempArray = [];
            tempArray['text'] = departureCountryArray[0];
            tempArray['value'] = $(this).attr('value');
            cabinClassArr.push(tempArray);

            var cabinClass = departureCountryArray[0];
            var cabinCode = $(this).attr('value');

            options += '<option value="' + cabinCode + '">' + cabinClass + '</option>';
        });
        // ipad fix
        updateSelectOptions('#flightClass', options);
        /*
        //refresh non ipad
        if (!iDevice) {
            $("#flightClass").selectric('refresh');
        }
        */
    }, 'xml', 'Could not update Cabin Classes', {
        departureCity: $('#departCity').val(),
        destinationCity: $('#destCity').val()
    });
}



/**
 * Check the reservation configuration
 *
 */
function checkReservationConfiguration(departCity, destCity) {
    if (!(departCity == destCity) && departCity != '' && destCity != '') {
        $.ajaxSetup({
            cache: false,
            async: false
        });
        $.get(
            relPath + conLan + "/flightSearch!checkReservationConfiguration.action",
            {
                departureCity: departCity,
                destinationCity: destCity
            },
            function(){
                checkFlexiOption();
                populateCabinClass();
            },
            'text'
        );
    }
}



/**
 * Load the Depart City XML
 *
 * @param locale
 * @param country
 */
function loadDepartureXML(locale, country) {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadDepartureXmlFile.action", function(response)
    {
        $('#departCity, #destCity').html('');
        country = $("#departCityId").html();
        code = '';
        $('#departCity').append('<option value="' + code + '">' + country + '</option>');
        country = $("#destinCityId").html();
        code = '';
        $('#destCity').append('<option value="' + code + '">' + country + '</option>');
        $(response).find('city').each(function () {
            country = $(this).text();
            code = $(this).attr('value');
            $('#departCity').append('<option value="' + code + '">' + country + '</option>');
        });
    }, 'xml', null, {
        country: country
    });
}


/**
 * Load the Region XML and update the page
 *
 */
function loadDestinationXML(locale, cityCode){

}

function validateFlightDates(){
    var d = getDepartDate();
    var r = getReturnDate();
    if (compareDateObjects(d, r) == -1) {
        alert(propsArray['bookingBlock_17']);
        //updateDates($('#bb-flights .calendar.returning'), $('#bb-flights .calendar.leaving').datepicker('getDate'));
    }
};/**
 * Check In Block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {




    $('#bb-check-in form').blockform({
        selectricInputs: [
            '#bb-check-in form select',
        ],
        beforeInit: function(){

        },
        afterInit: function(){
            //switch for check-in block
            $('#checkInSAA, #checkInMango').click(function () {
                $('#bb-check-in form .check-in').hide();
                $("#" + $(this).attr('id') + 'Div').show();
            });

            //only alphabets no space
            $('#ISurname').keyup(function () {
                if (this.value.match(/[^a-zA-Z ]/g)) {
                    this.value = this.value.replace(/[^a-zA-Z ]/g, '');
                }
            });
            $('#ISurnameM').keyup(function () {
                if (this.value.match(/[^a-zA-Z ]/g)) {
                    this.value = this.value.replace(/[^a-zA-Z ]/g, '');
                }
            });

            //set maxLength
            if ("PNR" == $('#checkInMethod').val()) {
                $('#IIdentification').val("");
                $('#IIdentification').attr('maxLength', 6);
            }

            $('#checkInMethod').change(function () {
                //hide specific inputs
                var v = $(this).val().toLowerCase();
                $('#bb-check-in form .check-in-method').hide();
                $('#bb-check-in form .check-in-method.'+v).show();
                //set maxLength
                if (v == "pnr") {
                    $('#IIdentification').val("");
                    $('#IIdentification').attr('maxLength', 6);
                } else {
                    $('#IIdentification').attr('maxLength', 100);
                }
            });

            $('#checkInWrap .close').click(function(){
                closeCheckinWrap();
            })


        }
    });




    $('#bb-check-in form').submit(function () {

        var valMsg = "";
        var error = false;

        //check and process mango fields
        if ($("#checkInMangoDiv").css('display') == 'block') {

            var surnameM = $('#ISurnameM');
            var etktNumberM = $('#etktnumberM');

            var boardPoint = $('#IBoardPoint').val();
            var formOfID = "ETKT";
            var group = $('#checkInAddPaxM').prop('checked') ? '&IGroupTravel=on' : '';

            surnameM.removeClass('invalid');
            if (!surnameM.val().length) {
                surnameM.addClass('invalid');
                error = true
            }

            etktNumberM.removeClass('invalid');
            if (!etktNumberM.val().length) {
                etktNumberM.addClass('invalid');
                error = true
            }

            valMsg = getLocalizedMessage("Please fill in the mandatory field(s).", "checkin_1");

            if (error) {
                alert(valMsg);
                return false;
            }


            var url = 'https://checkin.si.amadeus.net/1ASIHSSCWCIJE/sscwje/checkin?' +
                'LANGUAGE=' + config.language.toLowerCase() +
                '&IFormOfIdentification=' + formOfID +
                '&IIdentification=' + etktNumberM.val() +
                '&ISurname=' + surnameM.val() + group +
                '&RedirectedFrom=RSS&ln=EN';
        }
        //check and process saa fields
        else {
            var surname = $('#ISurname');
            var idNumber = $('#IIdentification');

            var boardPoint = $('#IBoardPoint').val();
            var formOfID = $('#checkInMethod').val();
            var group = $('#checkInAddPax').is(':checked') ? '&IGroupTravel=on' : '';

            //alert("surname is : "+surname.value+" formOfID = "+formOfID.value);
            //alert("isurname "+ document.getElementById('ISurname').value+ " ISurnameM "+document.getElementById('ISurnameM').value);

            surname.removeClass('invalid');
            if (!surname.val().length) {
                surname.addClass('invalid');
                error = true
            }

            idNumber.removeClass('invalid');
            if (!idNumber.val().length) {
                idNumber.addClass('invalid');
                valMsg = getLocalizedMessage("Please fill in the mandatory field(s).", "checkin_1");
                error = true;
            }

            if (error) {
                alert(valMsg);
                return false;
            }

            var url = 'https://checkin.si.amadeus.net/1ASIHSSCWCISA/sscwsa/checkin?' +
                'LANGUAGE=' + config.language.toLowerCase() +
                '&IFormOfIdentification=' + formOfID +
                '&IIdentification=' + idNumber.val() +
                '&ISurname=' + surname.val() +
                group + '&RedirectedFrom=RSS&ln=EN';
        }
        iframePopupOpen(url);
        return false;
    });


    //$('#bb-check-in form').submit(function () {

    $('#bb-check-in .button.reset').live('click', function () {
        $('#ISurname,#IIdentification').val('').removeClass('invalid');
        $('#checkInAddPax').prop('checked', false);
        //$('.checkInAddPax.checkStyle.checked').removeClass('checked');

        $('#ISurnameM,#etktnumberM').val('').removeClass('invalid');
        $('#checkInAddPaxM').prop('checked', false);

        //$('.checkInAddPaxM.checkStyle.checked').removeClass('checked');

        $('input[name="checkInMethod"]').changeVal('PNR');
    });


});



//Added for Request 3257468
function changeCheckin(checkins) {
    var checkin = document.getElementsByName(checkins.name);
    document.getElementById('saa_div').style.display = (checkin[0].checked) ? 'block' : 'none';
    document.getElementById('mango_div').style.display = (checkin[1].checked) ? 'block' : 'none';
}


;/**
 * Check In Block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {

    $('#voyager .loadingIndicator').fadeOut();

    loadVoyagerInfo("voyagerInfo");

    if($('#isHomeVoyLogin').val()=="Y"){
        var name=$('#nameInitial').val() +' ' + $('#name').val();
        $('#voyager').addClass('loggedin');
        $('#memName span').html(name);
        $('#memNum').html('<span></span>'+ $('#membrshpNum').val());
        $('#memMiles').html('<span></span>'+$('#miles').val());
        $('#memStatus').html('<span></span>'+$('#currenttierid').val());
        $('#memStatus').addClass($('#status').val());
        $('#voyager .in').css('display','block');
        $('#voyager .out').css('display','none');

    } else {

        $('#bb-log-in a.forgot').click(function(){
            $('#bb-log-in form[name="voyagerLogin"]').slideUp();
            $('#bb-log-in form[name="voyagerForgot"]').fadeIn();
            return false;
        });

        $('#bb-log-in .back').click(function(){
            $('#bb-log-in form[name="voyagerLogin"]').slideDown();
            $('#bb-log-in form[name="voyagerForgot"]').fadeOut();
            return false;
        });

        $('#bb-log-in form[name="voyagerLogin"]').submit(function(){

// Remove all the styling from the inputs pre-validation.
            $('#pin, #voyagerId').removeClass('invalid');

            if ($('#voyagerId').val().length==0){
                alert(getLocalizedMessage("The voyager number field is empty.","voylogin_usename"));
                $('#voyagerId').addClass('invalid');
                return false;
            }

            if(($('#voyagerId').val().length < 6) || ($('#voyagerId').val().length > 15)){
                alert(getLocalizedMessage("The voyager number is not valid.","voyLogin_usename3"));
                $('#voyagerId').addClass('invalid');
                return false;
            }

// Voyager number must be a number
            if(isNaN($('#voyagerId').val())){
                alert(getLocalizedMessage("The voyager number should be a Number.","voylogin_usename2"));
                $('#voyagerId').addClass('invalid');
                return false;
            }
            if ($('#pin').val().length==0){
                alert(getLocalizedMessage("The PIN field is empty.","voylogin_pin"));
                $('#pin').addClass('invalid');
                return false;
            }
// Voyager PIN must be at least 4 characters long
            if($('#pin').val().length<4){
                alert(getLocalizedMessage("The PIN number may not be less than 4 characters.","voylogin_pin2"));
                $('#pin').addClass('invalid');
                return false;
            }

// Voyager PIN must be a number
            if(isNaN($('#pin').val())){
                alert(getLocalizedMessage("The PIN should be a Number.","voylogin_pin3"));
                $('#pin').addClass('invalid');
                return false;
            }
            checkVoyagerLoginTab();
            return false;
        });}
});


function checkVoyagerLoginTab(){

    document.forms['voyagerLogin'].action = conLanPath+"/voyagerloginaction!voyagerloginaction.action";
    document.forms['voyagerLogin'].target = "_parent";
    document.forms['voyagerLogin'].submit();

}
function checkJoinVoyager() {

    document.forms['voyagerLogin'].action = conLanPath+"/voyager!registration.action";
    document.forms['voyagerLogin'].target = "_parent";
    document.forms['voyagerLogin'].submit();

}
function goToVoyHelpDesk()
{
    document.forms['voyagerLogin'].action = "/splashPageAction!goToVoyHelpDesk.action";
    document.forms['voyagerLogin'].target = "_parent";
    document.forms['voyagerLogin'].submit();
}

/**
 *Load OpenCMS Voyager Info content using Jquery
 *
 */
function loadVoyagerInfo(id){
    var ajax_load = "<img class='loading' src='/images/processing-loading.gif' alt='loading...' />";
    var selectedCnt = config.country;
    var langSelected = config.language.toUpperCase();
    var template		= "common";
    var cmsParam={};
    cmsParam["country"] = selectedCnt;
    cmsParam["locale"] = langSelected;
    cmsParam["template"] = template;
    cmsParam["id"] = id;
    $("#"+id).html(ajax_load).load("/cms/ZA/routeSpecific/IncludeCMS_Content.jsp",cmsParam);
};/**
 * Flight Status Block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {
    $('#bb-flight-status form').blockform({
        comboboxInputs: [
            '#statusDepartCity',
            '#statusDestCity'
        ],
        selectricInputs: [
            '#bb-flight-status form select',
        ],
        dateInputs: {
            /*
            "#bb-flight-status .calendar": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();
                    $('#bb .overlay').fadeOut('fast');
                }
            }
            */
        },
        beforeInit: function(){
            updateMonthYearSelects('#bb-flight-status .month-year select');
            updateDaySelects('#bb-flight-status .day select');
        },
        afterInit: function(){
            updateDates('#bb-flight-status .calendar', today);

            $('#statusDepartCity').siblings('input').keyup(function (event) {
                if ($(this).val().length > 2) {
                    loadFlightDestinations($('#statusDepartCity'), getLocalizedMessageFromKey('statusDepartCityId'));
                }
            });
            $('#statusDestCity').siblings('input').keyup(function (event) {
                if ($(this).val().length > 2) {
                    loadFlightDestinations($('#statusDestCity'), getLocalizedMessageFromKey('statusDestCityId'));
                }
            });

            //switch the flight status
            $('#flightChecked').change(function () {
                if ($(this).prop('checked')) {
                    $('#FlightNo').show();
                    $('#NoFlight').hide();
                } else {
                    $('#NoFlight').show();
                    $('#FlightNo').hide();
                }
            });

            //override the exist autocomplete function
            $('#statusDepartCity').siblings('input').autocomplete('option', 'source', function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response($("#statusDepartCity option").map(function () {
                    var text = $(this).text();
                    var code = $(this).val();
                    if ((matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text) && ($.trim(code).length > 0)))
                        return {
                            label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "$1"),
                            value: text,
                            option: this
                        };
                }));
            })

            $('#statusDestCity').siblings('input').autocomplete('option', 'source', function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response($("#statusDestCity option").map(function () {
                    var text = $(this).text();
                    var code = $(this).val();
                    if ((matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text) && ($.trim(code).length > 0)))
                        return {
                            label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "$1"),
                            value: text,
                            option: this
                        };
                }));
            });
        }
    });


    //only numeric( no space)
    $('#flightNumber').keyup(function () {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });


    $('#bb-flight-status footer .button.route').click(function () {
        var url = 'http://timetables.oag.com/saaroutemapper/"';
        iframePopupOpen(url);
    });


    $('#bb-flight-status form').submit(function () {
        var valMsg = "", statusValMsg = "";
        var error = false;
        var flightNum = $('#FlightChecked').prop('checked');

        $('#carrierCode').removeClass('invalid');
        $('#flightNumber').removeClass('invalid');

        $('#statusDepartCity').siblings('input').removeClass('invalid');
        $('#statusDestCity').siblings('input').removeClass('invalid');

        var d = new Date();
        d.setDate(d.getDate() + parseInt($('#fromDateFLT').linkselect('val')));
        var date = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();

        if (flightNum) {
            if (!$('#flightNumber').val().length) {
                valMsg += getLocalizedMessage("Please enter the Flight number.", "statusVal_2");
                //alert("Please enter the Flight number.");
                $('#flightNumber').addClass('invalid');
                error = true;
            }
            else if (isNaN($('#flightNumber').val())) {
                valMsg += getLocalizedMessage("Flight Number should be a number.", "statusVal_3");
                //alert("Flight Number should be a number.");
                $('#flightNumber').addClass('invalid');
                error = true;
            }
            if (error) {
                alert(valMsg);
                return false;
            }

            $(this).action = relPath + conLan + "/checkFlightStatusHome!checkFlightStatus_FltNum.action?request_locale=" + locale + "&fromDateFLT=" + date;
            /*$('body').append('<form id="frmStatus" methor="post" action="http://www.flysaa.com/Journeys/checkFlightStatus!checkFlightStatus_FltNum.action">').hide();$('#frmStatus').append('<input name="carrierCode" value="'+$('#carrierCode').val()+'" />');$('#frmStatus').append('<input name="flightNumber" value="'+$('#flightNumber').val()+'" />');$('#frmStatus').append('<input name="fromDateFLT" value="'+date+'" />');$('#frmStatus').submit();*/
        }
        else {
            if (!$('#statusDepartCity').val().length) {
                statusValMsg = getLocalizedMessage("Please enter the Departure City.", "statusVal_4");
                //alert("Please enter the Departure City");
                $('#statusDepartCity').siblings('input').addClass('invalid');
                error = true;
            }
            if (!$('#statusDestCity').val().length) {
                //alert("Please enter the Destination City");
                statusValMsg += getLocalizedMessage("Please enter the Destination City.", "statusVal_5");
                $('#statusDestCity').siblings('input').addClass('invalid');
                error = true;
            }
            else if ($('#statusDestCity').val() == $('#statusDepartCity').val()) {
                statusValMsg += getLocalizedMessage("Departure City and Destination City cannot be the same.", "statusVal_6");
                $('#statusDepartCity').siblings('input').addClass('invalid');
                $('#statusDestCity').siblings('input').addClass('invalid');
                error = true;
            }
            if (error) {
                alert(statusValMsg);
                return false;
            }
            $(this).action = relPath + conLan + "/checkFlightStatusHome!checkFlightStatus_Location.action?fromDateFLT=" + date;
            /*$('body').append('<form id="frmStatus" methor="post" action="http://www.flysaa.com/Journeys/checkFlightStatus!checkFlightStatus_Location.action">').hide();$('#frmStatus').append('<input name="departureCity" value="'+$('#statusDepartCity').val()+'" />');$('#frmStatus').append('<input name="destinationCity" value="'+$('#statusDestCity').val()+'" />');$('#frmStatus').append('<input name="fromDateLOC" value="'+date+'" />');$('#frmStatus').submit();*/
        }
    });
    $('#bb-flight-status form .button.reset').click(function () {
        $('#flightNumber').val('').removeClass('invalid');
        $('#statusDepartCity,#statusDestCity').siblings('input').removeClass('invalid');
        $('#statusDepartCity,#statusDestCity').val('');
        updateDates($('#bb-flight-status .calendar'), today);
        //$('#statusDepartCity').siblings('input').val($('statusDepartCityId').html());
        //$('#statusDestCity').siblings('input').val($('statusDestCityId').html());
        return false;
    });

});








/**
 * Update the list of Flight Destinations
 *
 * Fired when the autocomplete field is entered
 *
 * TODO This could be moved directly into th autocomplete function
 *
 * @param value The value of the autocomplete input
 */
function loadFlightDestinations(select, defaultOptionText) {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadChkFltStatusXmlFile.action", function(response)
    {
        select.html('');
        select.append('<option value="">' + defaultOptionText + '</option>');
        $(response).find('point').each(function () {
            select.append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
        });
    }, 'xml', null, {
        localeSeltd: config.language
    });
};/**
 * Can Rental Block
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {
    $('#bb-car-rental form').blockform({
        comboboxInputs: [
            '#pickupLoc',
            '#dropoffLoc',
            '#carCountry'
        ],
        selectricInputs: [
            '#bb-car-rental form select',
        ],
        dateInputs: {
            "#bb-car-rental .calendar.pick-up": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();
                    // Update the drop downs of the returning date too!
                    // Makes sure impossible dates don't happen by accident, basicaly.
                    var $cal = $('#bb-car-rental .calendar.drop-off');
                    $cal.datepicker("option", "minDate", date);
                    updateDates($cal, dateObj);

                    // Show the return date calendar after half a second
                    // The timeout is there to wait for the other calendar to fade out
                    setTimeout("$('#bb-car-rental .calendar.drop-off').parent().click()", 500);
                    $('#bb .overlay').fadeOut('fast');
                }
            },
            "#bb-car-rental .calendar.drop-off": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);
                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();
                    $('#bb .overlay').fadeOut('fast');
                }
            }
        },
        beforeInit: function(){
            updateMonthYearSelects('#bb-car-rental .month-year select');
            updateDaySelects('#bb-car-rental .day select');
        },
        afterInit: function(){
            updateDates('#bb-car-rental .calendar', today);


            $('#pickupLoc').siblings('input').keyup(function (event) {
                var value = $(this).val();
                if (value.length > 2) {
                    loadCarDepartCities(value);
                }
            });

            $('#dropoffLoc').siblings('input').keyup(function (event) {
                var value = $(this).val();
                if (value.length > 2) {
                    loadCarDestCities(value);
                }
            });

            $('#carCountry').siblings('input').keyup(function (event) {
                var value = $(this).val();
                if (value.length > 2) {
                    loadCarCountries(value);
                }
            });


            /*
            //TODO check what this is needed for!!!
            var selectedDropPonit = $('#dropoffLoc').val();
            if (code == selectedDropPonit) {
                $('#txtdrop_loc').val($(this).text());
            }
            */


            /*
             //TODO Check if this is needed
             if (!iDevice) {
             $('#pickDay').linkselect({
             change: function (li, value) {
             updateDropOffDay(value)
             }
             });
             //calling callSetUpdatedDates() to update the hidden variables for carHireDep/DestMonthYear
             $('#pickMonthYear').linkselect({
             change: function (li, value) {
             updateDropOffMonthYear(value), callSetUpdatedDates()
             }
             });
             }

             */
            /*
            $('#pickDay').change(function () {
                $('#dropoffDay').changeVal($(this).val());
            });
            $('#pickMonthYear').change(function () {
                $('#dropoffMonthYear').changeVal($(this).val());
            });
            */
        }
    });




    $('#bb-car-rental form').submit(function () {
        var valMsg = "";
        var error = false;

        //TODO This won't have any affect
        getCityDesc();

        $('#pickupLoc').siblings('input').removeClass('invalid');
        $('#dropoffLoc').siblings('input').removeClass('invalid');
        $('#carCountry').siblings('input').removeClass('invalid');
        $('#driverAge').removeClass('invalid');
        $('#bb-car-rental .input .calendar.pick-up, #bb-car-rental .input .calendar.drop-off').parent().parent().find('select, .linkselect-link').removeClass('invalid');

        if (validateCarDates()) {
            $('#bb-car-rental .input .calendar.pick-up, #bb-car-rental .input .calendar.drop-off').parent().parent().find('.day select, .day .linkselect-link, .month-year select, .month-year .linkselect-link').addClass('invalid');
            valMsg += getLocalizedMessage("Please choose valid dates", "bookingBlock_14");
            error = true;
        }

        if (validateCarTime()) {
            $('#bb-car-rental .input .calendar.pick-up, #bb-car-rental .input .calendar.drop-off').parent().parent().find('.time select, .time .linkselect-link').addClass('invalid');
            valMsg += getLocalizedMessage("Please choose valid time", "bookingBlock_15");
            error = true;
        }
        /*if(!$('#driverAge').val().length||isNaN($('#driverAge').val())||$('#driverAge').val()<18) {
         $('#driverAge').addClass('invalid');
         valMsg=getLocalizedMessage("Please provide a valid driver age.","cars_1");
         error=true; }*/

        if (!$('#pickupLoc').val().length) {
            $('#pickupLoc').siblings('input').addClass('invalid');
            valMsg += getLocalizedMessage("Please provide a pick up location.", "cars_2");
            error = true;
        }

        // if(!$('#dropoffLoc').val().length) { $('#dropoffLoc').siblings('input').addClass('invalid'); valMsg+=getLocalizedMessage("Please provide a drop-off location.","cars_3");error=true; }
        /*
        if (!$('#carCountry').val().length) {
            $('#carCountry').siblings('input').addClass('invalid');
            valMsg += getLocalizedMessage("Please provide the country.", "cars_4");
            error = true;
        }
        */

        if (error) {
            alert(valMsg);
            return false;
        }

        getPosConnectionID();
        return false;
    });

    $('#bb-car-rental form .button.reset').click(function () {
        $("#pickupLoc").val('');
        $("#pickupLoc").siblings('input').val('');

        $("#dropoffLoc").val('');
        $("#dropoffLoc").siblings('input').val('');

        $("#carCountry").val('');
        $("#carCountry").siblings('input').val('');

        $('#driverAge').val('').removeClass('invalid');
        $('#pickupLoc').siblings('input').removeClass('invalid');
        $('#dropoffLoc').siblings('input').removeClass('invalid');
        $('#carCountry').siblings('input').removeClass('invalid');

        updateDates($('#bb-car-rental .calendar'), today);
        $("#dropOffTime,#pickUpTime").changeVal('-1');

        return false;
    });



});




/**
 * Update the list of Car Depart Cities
 *
 * Fired when the autocomplete field is entered
 *
 * TODO This could be moved directly into th autocomplete function
 *
 * @param value The value from the autocomplete field for search
 */
function loadCarDepartCities(value) {
    loadXmlResponseAndUpdate(relPath + conLan + "/crossSellAction!getCarHirePickupDetails.action", function(response)
    {
        $('#pickupLoc').html('').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_pickupHeader') + '</option>');
        $(response).find('airport').each(function () {
            var $xml = $(this);
            $('#pickupLoc').append('<option value="' + $xml.find("cityCode").text() + '">' + $xml.find("cityDesc").text() + '</option>');
        });
    }, 'xml', null, {
        carHireOrg: value
    });
}

/**
 * Load the Car Destination Cities
 *
 * @param value The value from the autocomplete field for search
 */
function loadCarDestCities(value) {

    loadXmlResponseAndUpdate(relPath + conLan + "/crossSellAction!getCarHirePickupDetails.action", function(response)
    {
        $('#dropoffLoc').html('').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_dropoffHeader') + '</option>');
        $(response).find('airport').each(function () {
            var $xml = $(this);
            $('#dropoffLoc').append('<option value="' + $xml.find("cityCode").text() + '">' + $xml.find("cityDesc").text() + '</option>');
        });
    }, 'xml', null, {
        carHireOrg: value
    });
}

/**
 * Load the car countries
 *
 */
function loadCarCountries() {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadLocaleXmlFile.action", function(response)
    {
        $('#carCountry').html('').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_countryResHeader') + '</option>');
        $(response).find('country').each(function () {
            $('#carCountry').append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
        });
    }, 'xml', null, {
        localeSeltd: config.language
    });
}


/**
 * Get the City Descriptor
 *
 */
function getCityDesc() {
    loadCarCityDescriptionXML();
}

/**
 * Load the Car City Description
 *
 */
function loadCarCityDescriptionXML() {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadDepartureXmlFile.action", function(response)
    {
        $(response).find('departure').each(function () {
            var country = $(this).text();
            var code = $(this).attr('code');

            //TODO check if this is the correct way to do this!!!
            if (code == $('#pickupLoc').val()) {
                $('#txtpick_loc').val(country);
            }
            if (code == $('#dropoffLoc').val()) {
                $('#txtdrop_loc').val(country);
            }
        });

    }, 'xml', null, {
        localeSeltd: config.language,
        country: config.country
    });
}






//TODO What is this for???
function getPosConnectionID() {

    loadXmlResponseAndUpdate(relPath + "/splashPageAction!getPosConnectionID.action", function(response)
    {
        if (status == "success") {
            if (response != null) {
                carRentalPosConnId = response;
            }
        } else {
            carRentalPosConnId = null;
        }

        //TODO These don't currently exist!!!
        var pickupMonthndex = getAllMonthIndex($("#carHireDepMonthYear").val());
        var pickUYear = getAllyearValue($("#carHireDepMonthYear").val());
        var dropofMonthIndex = getAllMonthIndex($("#carHireDestMonthYear").val());
        var dropoffear = getAllyearValue($("#carHireDestMonthYear").val());

        if (carRentalPosConnId == null) {
            carRentalPosConnId = "ZAF";
        }

        /*if ( $( '#dropoffLoc').val().length>0 &&($('#pickupLoc').val() != $( '#dropoffLoc').val())) {
         var url = "http://www.flysaa.com/saacui/CarSearchExternal.do?isNewSearchFlow=true&pos=SAACUI-"+carRentalPosConnId+"&lang=en&pickUpLocationName=" + $('#txtpick_loc').val() + "&pickUpLocationCode=" + $('#pickupLoc').val() + "&pickUpDay=" + $('#pickDay').val() + "&pickUpMonth=" + pickupMonthndex + "&pickUpYear=" + pickUYear + "&pickUpTime=" + $('#pickUpTime').val() + "&dropOffDay=" + $('#dropoffDay').val() + "&dropOffMonth=" + dropofMonthIndex + "&dropOffYear=" + dropoffear + "&dropOffTime=" + $('#dropOffTime').val() + "&residentCountry=" + $('#carCountry').val() + "&returningDifferent=true"	+ "&dropOffLocationName=" + $('#txtdrop_loc').val() + "&dropOffLocationCode=" + $('#dropoffLoc').val() + "&driverAge=" + $('#driverAge').val(); }
         else {
         var url = "http://www.flysaa.com/saacui/CarSearchExternal.do?isNewSearchFlow=true&pos=SAACUI-"+carRentalPosConnId+"&lang=en&pickUpLocationName=" + $('#txtpick_loc').val() + "&pickUpLocationCode=" + $('#pickupLoc').val() + "&pickUpDay=" + $('#pickDay').val() + "&pickUpMonth=" + pickupMonthndex + "&pickUpYear=" + pickUYear + "&pickUpTime=" + $('#pickUpTime').val() + "&dropOffDay=" + $('#dropoffDay').val() + "&dropOffMonth=" + dropofMonthIndex + "&dropOffYear=" + dropoffear + "&dropOffTime=" + $('#dropOffTime').val() + "&residentCountry=" + $('#carCountry').val() + "&returningDifferent=false" + "&driverAge=" + $('#driverAge').val(); }
         window.open(url,"_self");*/

        var BDATE = "";
        var EDATE = "";
        var dropLocation = "";
        var BMONTH = pickupMonthndex;
        var EMONTH = dropofMonthIndex;
        if (BMONTH < 10)
            BMONTH = "0" + BMONTH;
        if (EMONTH < 10)
            EMONTH = "0" + EMONTH;
        BDATE = pickUYear + BMONTH + $('#pickDay').val() + $('#pickUpTime').val();
        EDATE = dropoffear + EMONTH + $('#dropoffDay').val() + $('#dropOffTime').val();
        //alert("BDATE->" + BDATE + "EDATE->" + EDATE);
        if ($('#dropoffLoc').val().length > 0 && ($('#pickupLoc').val() != $('#dropoffLoc').val()))
            dropLocation = $('#dropoffLoc').val();
        else
            dropLocation = $('#pickupLoc').val();
        //var url = "https://book.flysaa.com/plnext/flysaa/Override.action?UI_EMBEDDED_TRANSACTION=CarShopperStart&SITE=BAUYBAUY&LANGUAGE=GB&TRIP_FLOW=YES&B_DATE=" + BDATE +"&E_DATE=" + EDATE +"&B_LOCATION=" + $('#pickupLoc').val() + "&E_LOCATION=" + dropLocation + "&CLASS=*&TYPE=*&TRANSMISSION=*&AIR_CONDITIONING=*&EXTERNAL_ID=UICSS:PRODUCTION;PRODUCT:CAR;SA_SOURCE:CAR;COUNTRY:za;LANGUAGE:en;;;;;;;;&SO_SITE_IS_MAIL_CANCEL=TRUE&SO_SITE_IS_MAIL_CONF=TRUE&USE_CAR_SHOPPER=TRUE&SO_SITE_APIV2_SERVER_USER_ID=GUEST&SO_SITE_APIV2_SERVER=194.156.170.78&SO_SITE_APIV2_SERVER_PWD=TAZ&SO_SITE_CORPORATE_ID=OCGPDT&SO_SITE_SI_SAP=1ASIXJCP&SO_SITE_SI_SERVER_PORT=18033&SO_SITE_SI_SERVER_IP=193.23.185.67&SO_SITE_SI_USER=UNSET&SO_SITE_SI_PASSWORD=UNSET&SO_SITE_SI_1AXML_FROM=SEP_JCP&SO_SITE_FQ_INTERFACE_ACTIVE=FALSE&SO_SITE&SO_SITE_EXTERNAL_LOGIN=FALSE&SO_SITE_OFFICE_ID=JNBSA08AA&SO_SITE_HOST_TRACE_ACTIVE=TRUE&SO_SITE_FP_TRACES_ON=FALSE&AUTO_SEARCH=TRUE#results";
        var url = "https://book.flysaa.com/plnext/flysaa/Override.action?UI_EMBEDDED_TRANSACTION=CarShopperStart&SITE=BAUYBAUY&LANGUAGE=GB&TRIP_FLOW=YES&B_DATE=" + BDATE + "&E_DATE=" + EDATE + "&B_LOCATION=" + $('#pickupLoc').val() + "&E_LOCATION=" + dropLocation + "&CLASS=*&TYPE=*&TRANSMISSION=*&AIR_CONDITIONING=*&EXTERNAL_ID=UICSS:PRODUCTION;PRODUCT:CAR;SA_SOURCE:CAR;COUNTRY:za;LANGUAGE:en;;;;;;;;&SO_SITE_IS_MAIL_CANCEL=TRUE&SO_SITE_IS_MAIL_CONF=TRUE&USE_CAR_SHOPPER=TRUE&SO_SITE_EXTERNAL_LOGIN=FALSE&SO_SITE_OFFICE_ID=JNBSA08AA&AUTO_SEARCH=TRUE#results";
        window.open(url, "_self");

    }, 'text', null, {
        countrySel: config.country,
        productSel: config.selectedProductIs
    });
}



//TODO refactor this
function validateCarDates() {
    var flag = false;
    if ($('#pickMonthYear').val() != " ") {
        var departDateArray = $('#pickMonthYear').val().split('-');
        var finalDepartMonth = departDateArray[1].substring(departDateArray[1].length, 2);
        pickupDate = $('#pickDay').val() + "-" + departDateArray[0] + " " + finalDepartMonth;
    }
    if ($('#dropoffMonthYear').val() != " ") {
        var returnDateArray = $('#dropoffMonthYear').val().split('-');
        var finalReturnMonth = returnDateArray[1].substring(returnDateArray[1].length, 2);
        dropoffDate = $('#dropoffDay').val() + "-" + returnDateArray[0] + " " + finalReturnMonth;
    }

    if (!isValidDate(pickupDate, dateFormat)) {
        flag = true;
    }
    if (!isValidDate(dropoffDate, dateFormat)) {
        flag = true;
    }
    if (compareWithCurrentDate(pickupDate, dateFormat) < 0) {
        flag = true;
    }
    if (compareWithCurrentDate(dropoffDate, dateFormat) < 0 || compareDateStrings(pickupDate, dropoffDate, dateFormat) < 0) {
        flag = true;
    }
    return flag;
}


//for cars
function validateCarTime() {
    if ($('#pickMonthYear').val() != " ") {
        var departDateArray = $('#pickMonthYear').val().split('-');
        var finalDepartMonth = departDateArray[1].substring(departDateArray[1].length, 2);
        pickupDate = $('#pickDay').val() + "-" + departDateArray[0] + " " + finalDepartMonth;
    }
    if ($('#dropoffMonthYear').val() != " ") {
        var returnDateArray = $('#dropoffMonthYear').val().split('-');
        var finalReturnMonth = returnDateArray[1].substring(returnDateArray[1].length, 2);
        dropoffDate = $('#dropoffDay').val() + "-" + returnDateArray[0] + " " + finalReturnMonth;
    }
    if ((compareDateStrings(pickupDate, dropoffDate, 'dd-M y') == 0) && (document.getElementById('pickUpTime').value > document.getElementById('dropOffTime').value)) {
        return true;
    }
    return false;
}



;/**
 * Book a hotel room
 *
 * @author      Greg Gunner <greg.gunner@havaswwdigital.co.za>
 * @created     2015/01/16
 */
$(document).ready(function() {

    $('#bb-hotels form').blockform({
        comboboxInputs: [
            '#hotelDestination'
        ],
        selectricInputs: [
            '#bb-hotels form select',
        ],
        dateInputs: {
            "#bb-hotels .calendar": {
                firstDay: 1,
                minDate: 0,
                maxDate: '+1y',
                showOtherMonths: true,
                selectOtherMonths:true,
                dateFormat: 'dd-M-yy',
                onSelect: function (date) {
                    var dateObj = $.datepicker.parseDate('dd-M-yy', date);
                    // Upon selecting a date, update the relative drop downs
                    updateDates($(this), dateObj);

                    // Hide the datepicker that the user just clicked on
                    $(this).fadeOut();

                    // If the date selected was the checkin date.
                    if ($(this).hasClass('check-in')) {
                        // Update the drop downs of the returning date too!
                        // Makes sure impossible dates don't happen by accident, basicaly.
                        var $cal = $('#bb-hotels .calendar.check-out');
                        $cal.datepicker("option", "minDate", date);
                        updateDates($cal, dateObj);

                        // Show the leave date calendar after half a second
                        // The timeout is there to wait for the other calendar to fade out
                        setTimeout("$('#bb-hotels .calendar.check-out').parent().click()", 500);
                    }

                    $('#bb .overlay').fadeOut('fast');
                }
            }
        },
        beforeInit: function(){
            updateMonthYearSelects('#bb-hotels .month-year select');
            updateDaySelects('#bb-hotels .day select');
        },
        afterInit: function(){
            updateDates('#bb-hotels .calendar', today);
            updateGuests();

            $('#bb-hotels .hotel-guests').on('change', function(){
                updateGuests();
            });

            $('#checkinDay').on('change', function(){
                $('#checkoutDay').changeVal($(this).val());
            });
            $('#checkinMonthYear').on('change', function(){
                $('#checkoutMonthYear').changeVal($(this).val());
            });

            $('#hotelDestination').siblings('input').keyup(function (event) {
                loadHotelDestination($(this).val());
            });
        }
    });

    $('#bb-hotels form').submit(function () {
        var valMsg = '';
        var error = false;
        var chkInDate = getCheckInDate();
        var chkOutDate = getCheckOutDate();

        //TODO This is a callback function which may never even get to update the values on the page before submission!!!!
        //updateDestinationCityDesc();

        $('#hotelDestination').siblings('input').removeClass('invalid');

        //validate the destination is ok
        if ($('#hotelDestination').val().length == 0) {
            $('#hotelDestination').siblings('input').addClass('invalid');
            valMsg = getLocalizedMessage("Please provide the hotel destination.", "hotelDest_1");
            error = true;
        }


        //validate the dates selected
        if (compareDateObjects(chkInDate, chkOutDate) == -1) {
            valMsg += getLocalizedMessage("Please choose valid dates.", "bookingBlock_14");
            error = true;
        }

        //show error if present
        if (error) {
            alert(valMsg);
            return false;
        }

        var numberOfNights = Math.ceil((chkOutDate.getTime() - chkInDate.getTime()) / (1000 * 60 * 60 * 24));

        var url = "http://www.flysaa.com/saacui/HotelSearchExternal.do?isNewSearchFlow=true&pos=SAACUI-ZAF&lang=en&hotelSearchDestinationType=IAN_HOTELS&hotelSearchDestination=" + $('#txtHotelDes_loc').val() +
            "&hotelSearchDestinationCode=" + $('#hotelDestination').val() +
            "&numberOfNights=" + numberOfNights +
            "&hotelSearchNumberOfRooms=" + $('#numberoffrooms').val() +
            "&guestTypeBeans[1].guestTypes[0].type=10.AQC&guestTypeBeans[1].guestTypes[0].amount=" + $('#hotelAdults').val() +
            "&guestTypeBeans[1].guestTypes[1].type=8.AQC&guestTypeBeans[1].guestTypes[1].amount=" + $('#hotelChildren').val() +
            "&guestTypeBeans[1].guestTypes[2].type=7.AQC&guestTypeBeans[1].guestTypes[2].amount=" + $('#hotelInfants').val() +
                //TODO Check that the date format of this is correct!!!
            "&checkinDate=" + $.datepicker.formatDate(chkInDate, 'd/m/y') + //$('#checkinDay').val() + "/" + chkInMonthIndex + "/" + chkInYear +
            "&checkoutDate=" + $.datepicker.formatDate(chkOutDate, 'd/m/y'); //$('#checkoutDay').val() + "/" + chkOutMonthIndex + "/" + chkOutYear;

        window.open(url, "_self");
        return false;
    });

    $('#bb-hotels form .button.reset').click(function () {
        $('#hotelDestination').siblings('input').removeClass('invalid');
        $("#hotelDestination").val('');
        //$("#hotelDestination").siblings('input').val(getLocalizedMessageFromKey('bookingBlock_hotelHeaderTxt'));
        updateDates($('#bb-hotels .calendar'), today);
        $("#hotelAdults,#hotelChildren,#hotelInfants,#numberoffrooms").changeVal('-1');
        return false;
    });

});




/**
 * Update the options for the hotel guest fields to eliminate invalid quantity of guests selectable
 *
 * Refers to global maxPax variable for the maximum
 *
 */
function updateGuests() {

    var adults = parseInt($('#hotelAdults').val());
    var children = parseInt($('#hotelChildren').val());
    var infants = parseInt($('#hotelInfants').val());
    var remainder = hotelMaxPax - adults - children - infants;

    if (!iDevice) {
        //TODO Update to be universal and work on iDevice as well
        $('#hotelAdults_list li').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + adults;
            if (i > selectable)$(this).addClass('hidden');
        });
        $('#hotelChildren_list li').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + children;
            if (i > selectable)$(this).addClass('hidden');
        });
        $('#hotelInfants_list li').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + infants;
            if (i > selectable)$(this).addClass('hidden');
        });
    } else {
        $('#hotelAdults option').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + adults;
            if (i > selectable)$(this).addClass('hidden');
        });
        $('#hotelChildren option').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + children;
            if (i > selectable)$(this).addClass('hidden');
        });
        $('#hotelInfants option').each(function (i) {
            $(this).removeClass('hidden');
            selectable = remainder + infants;
            if (i > selectable)$(this).addClass('hidden');
        });
    }
}

/**
 * Get the checkin date
 *
 * @returns {Date}
 */
function getCheckInDate()
{
    return new Date($('#checkinDay').val() + '-' + $('#checkinMonthYear').val())
}

/**
 * Get the checkout date
 *
 * @returns {Date}
 */
function getCheckOutDate()
{
    return new Date($('#checkoutDay').val() + '-' + $('#checkoutMonthYear').val())
}


/**
 * Update the list of Hotel Destinations
 *
 * Fired when the autocomplete field is entered
 *
 * TODO This could be moved directly into th autocomplete function
 *
 * @param value The value of the autocomplete input
 */
function loadHotelDestination(value) {
    if (value.length <= 2) {
        return;
    }
    loadXmlResponseAndUpdate(relPath + conLan + "/crossSellAction!getAccomodationDetails.action", function(response)
    {
        $('#hotelDestination').html('');
        $('#hotelDestination').append('<option value="">' + getLocalizedMessageFromKey('bookingBlock_destinationHeader') + '</option>');
        $(response).find('airport').each(function () {
            var $xml = $(this);
            $('#hotelDestination').append('<option value="' + $xml.find("cityCode").text() + '">' + $xml.find("cityDesc").text() + '</option>');
        });
    }, 'xml', null, {
        accommodationDest: value
    });
}


/**
 * Update the Destination City Description
 *
 */
function updateDestinationCityDesc() {
    loadXmlResponseAndUpdate(relPath + "/splashPageAction!loadDepartureXmlFile.action", function(response)
    {
        var selectedHotDes = $('#hotelDestination').val();
        $(XMLResponse).find('departure').each(function () {
            code = $(this).attr('code');
            if (code == selectedHotDes) {
                $('#txtHotelDes_loc').val($(this).text());
            }
        });
    }, 'xml', null, {
        localeSeltd: config.language,
        country: country
    });


}
