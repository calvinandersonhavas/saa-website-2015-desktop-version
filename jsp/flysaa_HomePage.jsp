<!doctype html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!--[if lt IE 7 ]>
<html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="content-language" content="en-za">
    <meta http-equiv="X-UA-Compatible" content="IE=10">

    <!-- NEW DEV NOTE: Replace with correct info -->
    <title>Flights to South Africa & Beyond | South African Airways </title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <meta name="author" content="South African Airways">
    <meta name="viewport" content="width=1050, initial-scale=1.0, maximum-scale=1.4">
    <link type="image/x-icon" rel="shortcut icon" href="<s:url value='/favicon.ico'/>">
    <link type="image/x-icon" rel="apple-touch-icon" href="<s:url value='/images/apple-touch-icon.png'/>">
    <link href="<s:url value='/jsp/herald/css/style.css'/>" rel="stylesheet" type="text/css">

	<script src="<s:url value='/jsp/herald/js/modernizr.js'/>"> </script> 
	<!--<script src="<s:url value='/scripts/lib/libraries-splash.js'/>"></script>-->

    </head>

<body class="home en">

    <%
	String country=(String)request.getSession().getAttribute("SELECTED_COUNTRY");
	String language=(String)request.getSession().getAttribute("SELECTED_LOCALE");
	String conLan="/"+country.toLowerCase()+"/"+language.toLowerCase();

	String locale_nojs = "en";
    if(language != null){
        locale_nojs = language.toLowerCase();
    }
	String country_nojs = "za";
    if(country != null){
        country_nojs = country.toLowerCase();
    }
	%>

    <s:set var="pickupHeaderTxt" id="pickupHeaderTxt"><s:text name="label.carhire.pickuppoint"/></s:set>
    <s:set var="dropoffHeaderTxt" id="dropoffHeaderTxt"><s:text name="label.carhire.dropoffpoint"/></s:set>
    <s:set var="departCity"><s:text name="label.departureCity"/></s:set>
    <s:set var="destinCity"><s:text name="label.destinationCity"/></s:set>


    <!--
    <script src='//www.flysaa.com/cms/eretail/scripts/Amadeus2.js'></script>
    <!-- Google Tag Manager -->
    <!--
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-T8DLCS"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T8DLCS');</script>
    <!-- End Google Tag Manager -->


    <!--IE Notice-->
    <!-- NEW DEV NOTE: This should be implemented with JS perhaps? It will impact how SE's read the page -->
    <!-- was class="ieNotice" -->
    <div id="ieNotice">
        <p>To fully experience the functionality of our site, you will need to upgrade your browser.</p>
        <p class="grey">We recommend:
            <a href="http://www.google.com/chrome" rel="external" class="browsers chrome">Chrome</a>
            <a href="http://www.microsoft.com/ie" rel="external" class="browsers ie">Internet Explorer 9</a>
            <a href="http://www.mozilla.com/firefox" rel="external" class="browsers firefox">Firefox</a>
            <a href="http://www.apple.com/safari" rel="external" class="browsers safari">Safari</a>
        </p>
    </div>
    <!--/IE Notice-->
    
    <!-- Start of Site Header -->
    <header id="header">
        <div class="top">
            <div class="wrap">
                <div class="left">
                
                    <!-- Site Links -->
                    <nav class="siteLinks">
                        <ul>
                           <s:if test="#session.PRODUCT_LIST">			
			          		<s:iterator value="#session.PRODUCT_LIST">   
			          			<s:if test='productCode!="SKINS" && productCode!="CHARTER"'>								
								 <s:if test='productCode=="CARG"'>
								  <li>
									 <a href="<%=conLan%>/home!loadCargoHome.action"  >
										<s:property value="productDescription"/>
								     </a>
								  </li>
								 </s:if>
								 <s:elseif test='productCode=="TECH"'>
								 <li>
									 <a href="<%=conLan%>/home!loadTechnicalHome.action" >
										<s:property value="productDescription"/>
									</a>
								</li>
								</s:elseif>
								<s:elseif test='productCode=="FTS" || productCode=="SKINS"'>
								 <li class="selected">
 									<a href="<%=conLan%>/home.action?request_locale=<%=request.getSession().getAttribute("SELECTED_LOCALE").toString().toLowerCase()%>_<%=request.getSession().getAttribute("SELECTED_COUNTRY").toString().toUpperCase()%>" >
										<s:property value="productDescription"/>
									</a>
								</li>
								</s:elseif>
								<s:elseif test='productCode=="ONBIZ"'>
								 <li>
									 <a href="<%=conLan%>/OnbizLogin!login.secured" >
										<s:property value="productDescription"/>
									</a>
								</li>
								</s:elseif>
								<s:elseif test='productCode=="VOY"'>
								 <li>
									<a href="<%=conLan%>/voyagerLogin.secured" >
										<s:property value="productDescription"/>
									</a>
								</li>
								</s:elseif>
								<s:else>
								 <li>
  			          				<a href="<s:property value="productUrl"/>" >
			          					<s:property value="productDescription"/>
			          				</a>
			          			</li>
								</s:else>
			          		</s:if>				
						</s:iterator>	 
			          </s:if>
                     </ul>
                    </nav>
                    <!--/ Site Links -->
                    
                </div>
                <div class="right">

                    <!-- Search -->
                    <div class="search">
                        <input id="searchInput" class="input" type="text" title="Search" placeholder="Search...">
                        <input id="searchButton" value="Search" class="button" type="button">
                    </div>
                    <!--/ Search -->

                    <!-- Region Selection -->
                    <div class="region nojs">

                        <a id="currentLoc" class="za"></a>
                        <nav id="regionNav">
                            <div>
                                <div class="regions tabs">
                                    <ul>
                                    	<li class="americas am"><a href="#region-am"><s:text name="label.splash.americas"/></a></li>
										<li class="africa af"><a href="#region-af"><s:text name="label.splash.africa"/></a></li>
										<li class="europe eu"><a href="#region-eu"><s:text name="label.splash.europe"/></a></li>
										<li class="asiapacific as"><a href="#region-as"><s:text name="label.splash.asiapacific"/></a></li>
										<li class="middleeast me"><a href="#region-me"><s:text name="label.splash.middleeast"/></a></li>
                                    </ul>
                                    <div>
                                        <div id="region-am">
                                            <ul>
                                                <li><a href="/us/<%=locale_nojs%>/home.action">USA</a></li>
												<li><a href="/ca/<%=locale_nojs%>/home.action">Canada</a></li>
												<li><a href="/ar/<%=locale_nojs%>/home.action">Argentina</a></li>
												<li><a href="/bo/<%=locale_nojs%>/home.action">Bolivia</a> </li>
												<li><a href="/br/<%=locale_nojs%>/home.action">Brazil</a></li>
												<li><a href="/cl/<%=locale_nojs%>/home.action">Chile</a></li>
												<li><a href="/co/<%=locale_nojs%>/home.action">Colombia</a></li>
												<li><a href="/mx/<%=locale_nojs%>/home.action">Mexico</a></li>
												<li><a href="/py/<%=locale_nojs%>/home.action">Paraguay</a></li>
												<li><a href="/pe/<%=locale_nojs%>/home.action">Peru</a></li>
												<li><a href="/uy/<%=locale_nojs%>/home.action">Uruguay</a></li>
												<li><a href="/ve/<%=locale_nojs%>/home.action">Venezuela</a></li>
                                            </ul>
                                        </div>
                                        <div id="region-af">
                                            <ul>
                                                <li><a href="/za/<%=session.getAttribute("SELECTED_LOCALE").toString().toLowerCase()%>/home.action">South Africa</a></li>
												<li><a href="/za/<%=locale_nojs%>/home.action">South Africa</a></li>
												<li><a href="/ao/<%=locale_nojs%>/home.action">Angola</a></li>
												<li><a href="/bw/<%=locale_nojs%>/home.action">Botswana</a></li>
												<li><a href="/cm/<%=locale_nojs%>/home.action">Cameroon</a></li>
												<li><a href="/cg/<%=locale_nojs%>/home.action">Congo</a></li>
												<li><a href="/cd/<%=locale_nojs%>/home.action">D.R. of Congo</a></li>
												<li><a href="/et/<%=locale_nojs%>/home.action">Ethiopia</a></li>
												<li><a href="/ga/<%=locale_nojs%>/home.action">Gabon</a></li>
												<li><a href="/gh/<%=locale_nojs%>/home.action">Ghana</a></li>
												<li><a href="/ci/<%=locale_nojs%>/home.action">Ivory Coast</a></li>
												<li><a href="/ke/<%=locale_nojs%>/home.action">Kenya</a></li>
												<li><a href="/ls/<%=locale_nojs%>/home.action">Lesotho</a></li>
												<li><a href="/mg/<%=locale_nojs%>/home.action">Madagascar</a></li>
												<li><a href="/mw/<%=locale_nojs%>/home.action">Malawi</a></li>
												<li><a href="/mu/<%=locale_nojs%>/home.action">Mauritius</a></li>
												<li><a href="/mz/<%=locale_nojs%>/home.action">Mozambique</a></li>
												<li><a href="/na/<%=locale_nojs%>/home.action">Namibia</a></li>
												<li><a href="/ng/<%=locale_nojs%>/home.action">Nigeria</a></li>
												<li><a href="/sn/<%=locale_nojs%>/home.action">Senegal</a></li>
												<li><a href="/sz/<%=locale_nojs%>/home.action">Swaziland</a></li>
												<li><a href="/tz/<%=locale_nojs%>/home.action">Tanzania</a></li>
												<li><a href="/ug/<%=locale_nojs%>/home.action">Uganda</a></li>
												<li><a href="/zm/<%=locale_nojs%>/home.action">Zambia</a></li>
												<li><a href="/zw/<%=locale_nojs%>/home.action">Zimbabwe</a></li>
                                            </ul>
                                        </div>
                                        <div id="region-eu">
                                            <ul>
                                                <li><a href="/uk/<%=locale_nojs%>/home.action">United Kingdom</a></li>
												<li><a href="/ie/<%=locale_nojs%>/home.action">Ireland</a></li>
												<li><a href="/fr/<%=locale_nojs%>/home.action">France</a></li>
												<li><a href="/de/<%=locale_nojs%>/home.action">Germany</a></li>
												<li><a href="/at/<%=locale_nojs%>/home.action">Austria</a></li>
												<li><a href="/by/<%=locale_nojs%>/home.action">Belarus</a></li>
												<li><a href="/be/<%=locale_nojs%>/home.action">Belgium</a></li>
												<li><a href="/bg/<%=locale_nojs%>/home.action">Bulgaria</a></li>
												<li><a href="/hr/<%=locale_nojs%>/home.action">Croatia</a></li>
												<li><a href="/cz/<%=locale_nojs%>/home.action">Czech Republic</a></li>
												<li><a href="/dk/<%=locale_nojs%>/home.action">Denmark</a></li>
												<li><a href="/fi/<%=locale_nojs%>/home.action">Finland</a></li>
												<li><a href="/hu/<%=locale_nojs%>/home.action">Hungary</a></li>
												<li><a href="/it/<%=locale_nojs%>/home.action">Italy</a></li>
												<li><a href="/lv/<%=locale_nojs%>/home.action">Latvia</a></li>
												<li><a href="/lt/<%=locale_nojs%>/home.action">Lithuania</a></li>
												<li><a href="/lu/<%=locale_nojs%>/home.action">Luxembourg</a></li>
												<li><a href="/nl/<%=locale_nojs%>/home.action">Netherlands</a></li>
												<li><a href="/no/<%=locale_nojs%>/home.action">Norway</a></li>
												<li><a href="/pl/<%=locale_nojs%>/home.action">Poland</a></li>
												<li><a href="/pt/<%=locale_nojs%>/home.action">Portugal</a></li>
												<li><a href="/ru/<%=locale_nojs%>/home.action">Russia</a></li>
												<li><a href="/sk/<%=locale_nojs%>/home.action">Slovakia</a></li>
												<li><a href="/si/<%=locale_nojs%>/home.action">Slovenia</a></li>
												<li><a href="/es/<%=locale_nojs%>/home.action">Spain</a></li>
												<li><a href="/se/<%=locale_nojs%>/home.action">Sweden</a></li>
												<li><a href="/ch/<%=locale_nojs%>/home.action">Switzerland</a></li>
												<li><a href="/ua/<%=locale_nojs%>/home.action">Ukraine</a></li>
                                            </ul>
                                        </div>
                                        <div id="region-as">
                                            <ul>
                                                <li><a href="/au/<%=locale_nojs%>/home.action">Australia</a></li>
												<li><a href="/hk/<%=locale_nojs%>/home.action">Hong Kong</a></li>
												<li><a href="/cn/<%=locale_nojs%>/home.action">China</a></li>
												<li><a href="/in/<%=locale_nojs%>/home.action">India</a></li>
												<li><a href="/ir/<%=locale_nojs%>/home.action">Iran</a></li>
												<li><a href="/jp/<%=locale_nojs%>/home.action">Japan</a></li>
												<li><a href="/kr/<%=locale_nojs%>/home.action">Korea</a></li>
												<li><a href="/my/<%=locale_nojs%>/home.action">Malaysia</a></li>
												<li><a href="/nz/<%=locale_nojs%>/home.action">New Zealand</a></li>
												<li><a href="/ph/<%=locale_nojs%>/home.action">Philippines</a></li>
												<li><a href="/sg/<%=locale_nojs%>/home.action">Singapore</a></li>
												<li><a href="/tw/<%=locale_nojs%>/home.action">Taiwan</a></li>
												<li><a href="/th/<%=locale_nojs%>/home.action">Thailand</a></li>
                                            </ul>
                                        </div>
                                        <div id="region-me">
                                            <ul>
                                                <li><a href="/bh/<%=locale_nojs%>/home.action">Bahrain</a></li>
										 		<li><a href="/eg/<%=locale_nojs%>/home.action">Egypt</a></li>
										 		<li><a href="/il/<%=locale_nojs%>/home.action">Israel</a></li>
										 		<li><a href="/jo/<%=locale_nojs%>/home.action">Jordan</a></li>
										 		<li><a href="/kw/<%=locale_nojs%>/home.action">Kuwait</a></li>
												<li><a href="/lb/<%=locale_nojs%>/home.action">Lebanon</a></li>
												<li><a href="/om/<%=locale_nojs%>/home.action">Oman</a></li>
												<li><a href="/qa/<%=locale_nojs%>/home.action">Qatar</a></li>
												<li><a href="/sa/<%=locale_nojs%>/home.action">Saudi Arabia</a></li>
												<li><a href="/ae/<%=locale_nojs%>/home.action">United Arab Emirates</a></li>
												<li><a href="/ye/<%=locale_nojs%>/home.action">Yemen</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="preference">
                                    <div id="rememberDiv" class="remember">
                                        <label for="chkRemember">
                                            <span class="un check"><input id="chkRemember" type="checkbox"></span>
                                            <span class="bg"><s:text name="label.availability.location.remember"/></span>
                                        </label>
                                    </div>
                                    <s:set var="sLang">label.selected.language_<%=session.getAttribute("SELECTED_LOCALE")%></s:set>
                                    <div id="savedDiv" class="saved">
                                        <p class="pref"><s:text name="label.availability.preference"/>
                                            <span id="savedLocale"></span>
                                            <span id="savedLanguage">(<s:text name="%{sLang}"/>)</span>
                                        </p>
                                        <p class="forget">
                                            <a href="#"><s:text name="label.availability.location.forget"/> <span class="icon-pin"></span></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <!--/Region Selection -->

                    <!-- Language Selection -->
                    <nav id="dropLangNav" class="language regionLang">
                        <span class="code"><span class="text">EN</span> <span class="icon-downarrow"></span></span>                        
                        <ul id="dropLang"> 
							<li><a href="/<%=country_nojs%>/zh/home.action">&#32321;&#39636;&#20013;&#25991;</a></li> 
							<li><a href="/<%=country_nojs%>/de/home.action">Deutsch</a></li> 
							<li><a href="/<%=country_nojs%>/es/home.action">Espa�ol</a></li> 
							<li><a href="/<%=country_nojs%>/en/home.action">English</a></li> 
							<li><a href="/<%=country_nojs%>/fr/home.action">Fran�ais</a></li> 
							<li><a href="/<%=country_nojs%>/it/home.action">Italiano</a></li> 
							<li><a href="/<%=country_nojs%>/pt/home.action">Portugu�s</a></li>
							<li><a href="/<%=country_nojs%>/zh_cn/home.action">&#31616;&#20307;&#20013;&#25991;</a></li>
							<li><a href="/<%=country_nojs%>/ja/home.action">&#26085;&#26412;&#35486;</a></li>
						</ul>
                    </nav>
                    <!--/Language Selection -->

                    <!-- Context Links -->
                    <nav class="contextLinks">
                        <ul>
                            <li><a href="<%=conLan%>/myProfileLogin.action"><span>My Profile</span></a></li>
                            <li><a href="<%=request.getContextPath()%>/cms/commons/flysaa_registerNewsLetter.html"><span><s:text name="label.header.newsletter"/></span></a></li>
                        </ul>
                    </nav>
                    <!--/Context Links -->
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="masthead">
                <h1 class="logo"><a href="<s:url value='/home.action?request_locale=<%=request.getSession().getAttribute("SELECTED_LOCALE").toString().toLowerCase()%>_<%=request.getSession().getAttribute("SELECTED_COUNTRY").toString().toUpperCase()%>'/>">South African Airways</a></h1>
                <div id="adSpaceImage" class="leaderboard"><a href="#"><img src="/jsp/herald/images/content-img/leaderboard.jpg" width="728" height="90"></a></div>
            </div>
        </div>
    </header>
    <!-- End of Site Header -->


    <!-- Start of Warnings -->
    <!-- TODO Styling of this has not been set -->
    <div id="warnings" class="hidden">
        <div class="wrap">
            <div class="noticeFix">
                <div id="globalnoticediv">
                    <!-- TODO Move onclick to jquery bindings -->
                    <a class="ntfWrap" onclick="cmsPageForGlobalNotice()">
                        <span class="ntfIcon"></span>
                        <span class="ntfArea" id="globalNoticeDesc"><span></span></span>
                        <!-- NEW DEV NOTE: Could this be set to the href of the link above??? -->
                        <input type="hidden" name="globalNoticeUrl" value="" id="za_en_home_globalNoticeUrl"/>
                        <span class="ntfMore">more info<span></span></span>
                    </a>
                </div>
                <div class="clr"></div>
                <div id="localnoticediv">
                    <!-- TODO Move onclick to jquery bindings -->
                    <a class="ntfWrap" onclick="cmsPageForLocalNotice()">
                        <span class="ntfIcon"></span>
                        <span class="ntfArea" id="localNoticeDesc"><span></span></span>
                        <!-- NEW DEV NOTE: Could this be set to the href of the link above??? -->
                        <input type="hidden" name="localNoticeUrl" value="" id="za_en_home_localNoticeUrl"/>
                        <span class="ntfMore">more info<span></span></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--/#warnings-->


    <!-- Previous id="midMenu" -->
    <section id="menu">
        <nav>
            <ul>
                <li class="plan-and-book"><a href="<%=conLan%>/planMyTrip.action"><s:text name="key.menu.plan.book"/></a>
                    <ul>
                        <li class="promotions">
                            <h2><span class="icon-menu-speaker"></span><s:text name="key.submenu.promotions"/></h2>
                              <ul>
                                <li><a href="<%=conLan%>/specials/saa-specials.html"><s:text name="key.submenu.saa.specials"/></a></li>
                                <li><a href="<%=conLan%>/best_fares/best_fares.html"><s:text name="key.submenu.best.fare.guarantee"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/promotions/carHireAirportTransfers.html"><s:text name="key.submenu.car.rental"/></a></li>                        
                                <li><a href="<%=conLan%>/planmytrip/promotions/carHireAirportTransfers.html#point"><s:text name="key.submenu.airport.transfers"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/promotions/SearchAccommodation.html"><s:text name="key.submenu.hotels"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/insurance/travel_insurance.html"><s:text name="key.submenu.insurance"/></a></li>
                                <li><a href="http://flysaa.brandnamemarketing.co.za/" target='_blank'><s:text name="key.submenu.saa.online.shop"/></a></li>
								<li><a href="http://www.executivecarport.co.za/Booking4SAA.aspx" target='_blank'><s:text name="key.submenu.executive.carport"/></a></li>								      	                                		 
								<li><a href="<%=conLan%>/planmytrip/promotions/SAA-Holidays.html"><s:text name="key.submenu.saa.holidays"/></a></li>	     	                                		 
								<li><a href="<%=conLan%>/planmytrip/promotions/goldentickets.html"><s:text name="key.submenu.golden.tickets"/></a></li>									      	                                		 
								<li><a href="<%=conLan%>/planmytrip/promotions/vodacom-b4igo.html"><s:text name="key.submenu.vodacom.b4igo"/></a></li>					
                            </ul>
                        </li>
                        <li class="book-flights">
                            <h2><span class="icon-menu-plane"></span><s:text name="key.submenu.book.flights"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/planmytrip/schedulesroutes/flight_schedules.html"><s:text name="key.submenu.flight.schedules"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/bookFlights/BookBusiness.html"><s:text name="key.submenu.book.business.trip"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/bookFlights/SearchAccommodation.html"><s:text name="key.submenu.book.holiday"/></a></li>
                            </ul>
                        </li>
                        <li class="destinations">
                            <h2><span class="icon-menu-pin"></span><s:text name="key.submenu.destinations"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/planmytrip/destinations/destinationGuides.html"><s:text name="key.submenu.destination.guides"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/schedulesroutes/flight_schedules.html"><s:text name="key.submenu.route.maps"/></a></li>
                                <li><a href="<%=conLan%>/newroutes/new-routes.html"><s:text name="key.submenu.new.routes"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/destinations/codeshares.html"><s:text name="key.submenu.codeshares"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/destinations/cityGuides.html"><s:text name="key.submenu.city.guides"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/destinations/discoverSouthAfrica.html"><s:text name="key.submenu.discover.southafrica"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/destinations/events.html"><s:text name="key.submenu.eventst"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/destinations/airportInformation.html"><s:text name="key.submenu.airports"/></a></li>
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span><s:text name="key.submenu.travel.advisory"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/planmytrip/TravelAdvisoryNews/YellowFever.html"><s:text name="key.submenu.yellow.fever.warning"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/travelAdvisory/Visa.html"><s:text name="key.submenu.visa.requirements"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/travelAdvisory/health.html"><s:text name="key.submenu.health"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/TravelAdvisoryFood/FoodAdvisory.html"><s:text name="key.submenu.food.drink"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/travelAdvisory/CabinDisinsection.html"><s:text name="key.submenu.cabin.disinfection"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/travelAdvisory/sa-immigration-under-18.html"><s:text name="key.submenu.immigration.changes.children"/></a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- Submenu -->
                </li>
                <li class="manage-my-booking"><a href="<%=conLan%>/reservation.action"><s:text name="key.menu.manage.booking"/></a>
                    <ul>
                        <li class="promotions">
                            <h2><span class="icon-menu-speaker"></span><s:text name="key.submenu.before.flight"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/manageMyTrip/onlineCheckin/aboutOnlineCheckIn.html#MobileCheckIn"><s:text name="key.submenu.mobile.checkin"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/onlineCheckin/onlineCheckIn.html"><s:text name="key.submenu.online.checkin"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/onlineCheckin/social-check-in.html"><s:text name="key.submenu.social.checkin"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.meal.request"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.select.seats"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/specialneeds/SpecialTravelNeeds.html"><s:text name="key.submenu.special.travel.needs"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.health.tips"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/baggage/checkedBaggage.html"><s:text name="key.submenu.baggage.policies"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/Visas/visa_and_passport_info.html"><s:text name="key.submenu.visa.requirements"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/loungeinformation/airportLounges.html"><s:text name="key.submenu.lounge.information"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="book-flights">
                            <h2><span class="icon-menu-plane"></span><s:text name="key.submenu.onmyflight"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/manageMyTrip/onboardHealth/OnboardExercise.html"><s:text name="key.submenu.inflight.fitness"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/onboardShopping/onboardShoppingCatalogue.html"><s:text name="key.submenu.preview.onboard.shopping"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/SAAFleet/aircraftFeatures.html"><s:text name="key.submenu.view.aircraft.information"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/inflightEntertainment/InFlightEntertainment.html"><s:text name="key.submenu.view.inflight.entertainment.information"/></a></li>
                            </ul>
                        </li>
                        <li class="destinations">
                            <h2><span class="icon-menu-pin"></span><s:text name="key.submenu.atmydestination"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/manageMyTrip/AtMyDestination/SearchAccommodation.html"><s:text name="key.submenu.hotels"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/AtMyDestination/carHireAirportTransfers.html"><s:text name="key.submenu.car.rental"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/destinations/destinationGuides.html"><s:text name="key.submenu.destination.information"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span><s:text name="key.submenu.aftermyflight"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/manageMyTrip/baggage/delayedDamagedBaggage.html"><s:text name="key.submenu.baggage.tracing"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.miles.balance"/></a></li>
                                <li><a href="<%=conLan%>/voyagerLogin.action"><s:text name="key.submenu.signup.voyager.ff"/></a></li>
                                <li><a href="<%=conLan%>/viewPastTrips!showScreen.action"><s:text name="key.submenu.view.past.reservation"/></a></li>
                                <li><a href="<%=conLan%>/taxInvoiceOnline.action"><s:text name="key.submenu.request.tax.invoice"/></a></li>
                                <li><a href="<%=conLan%>/manageRequestCreditNote.action"><s:text name="key.submenu.request.credit.note"/></a></li>
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span><s:text name="key.submenu.mydetails"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.update.contact"/></a></li>
                                <li><a href="<%=conLan%>/voyagerLogin.action"><s:text name="key.submenu.signup.voyager.ff"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.add.frequent.flyer.number"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.add.fremec.number"/></a></li>
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span><s:text name="key.submenu.mybooking"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.pay.reservation"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.change.booking"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/manageinfo/ManageMyBookingInfo.html"><s:text name="key.submenu.cancel.booking.refund"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/MyBooking/AmadeusInfo.html"><s:text name="key.submenu.view.itinerary"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/paymentAndTicketing/TicketCollectionOptions.html"><s:text name="key.submenu.payment.ticketing"/></a></li>
                                <li><a href="<%=conLan%>/manageMyTrip/insurance/travel_insurance.html"><s:text name="key.submenu.travel.insurance"/></a></li>
                                <li><a href="<%=conLan%>/taxInvoiceOnline.action"><s:text name="key.submenu.request.tax.invoice"/></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a target="_blank" href="http://destinations.flysaa.com"><s:text name="key.menu.destinations"/></a></li>
                <li><a target="_blank" href="<%=conLan%>/home!loadSAAHolidays.action?redirect=/cms/ZA/leisure/saaHolidays.html"><s:text name="key.menu.holidays"/></a></li>
                <li class="saa-experience"><a href="<%=conLan%>/flyingSAA.action"  onclick="setProductValue()"><s:text name="key.menu.saa.experience"/></a>
                    <ul>
                        <li class="promotions">
                            <h2><span class="icon-menu-speaker"></span><s:text name="key.submenu.saa.experience"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/flyingSAA/loungeinformation/airportLounges.html"><span><s:text name="key.submenu.airport.lounges"/></span></a>                                </li>
                                <li><a href="<%=conLan%>/flyingSAA/SAAFleet/aircraftFeatures.html"><span><s:text name="key.submenu.saa.fleet"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/inflightEntertainment/InFlightEntertainment.html"><span><s:text name="key.submenu.flight.entertainment"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/FoodAndWine/FoodWine.html"><span><s:text name="key.submenu.food.wine"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/onboardShopping/onboardShoppingCatalogue.html"><span><s:text name="key.submenu.onboard.shopping"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/Sawubona/Sawubona.html"><span><s:text name="key.submenu.sawubona.magazine"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/voyager/Voyager.html"><span><s:text name="key.submenu.vff"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/specialneeds/SpecialTravelNeeds.html"><span><s:text name="key.submenu.special.assistance"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/onboardHealth/OnboardExercise.html"><span><s:text name="key.submenu.onboard.health"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/aboutUs/SAA-Environment.html"><span><s:text name="key.submenu.environmental"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/mobileaccess/mobileaccess.html"><span><s:text name="key.submenu.mobile.access"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/News/NewsList.html"><span><s:text name="key.submenu.latest.news"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/NewsLetters/newsletterArchive.html"><span><s:text name="key.submenu.newsletter.archive"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/healthyTravel/SAANetcare.html"><span><s:text name="key.submenu.travel.clinic"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/paymentAndTicketing/PaymentOptions.html"><span><s:text name="key.submenu.payment.ticketing"/></span></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/FAQs/FAQs.html"><span><s:text name="key.submenu.frequently.asked"/></span></a></li>
                            </ul>
                        </li>
                        <li class="book-flights">
                            <h2><span class="icon-menu-plane"></span><s:text name="key.submenu.flight"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/flyingSAA/schedulesroutes/flight_schedules.html"><s:text name="key.submenu.route.maps.timetable"/></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/aboutUs/starAlliance.html"><s:text name="key.submenu.star.alliance"/></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/CustomerServices/immigration-fast-track-service.html"><s:text name="key.submenu.immigration.track.service"/></a></li>
                            </ul>
                        </li>
                        <li class="destinations">
                            <h2><span class="icon-menu-pin"></span><s:text name="key.submenu.fare.classes"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/flyingSAA/fareClasses/International_Fare_Families.html"><s:text name="key.submenu.international"/></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/fareClasses/Domestic_Fare_Families.html"><s:text name="key.submenu.domestic"/></a></li>                                
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span><s:text name="key.submenu.baggage"/></h2>
                            <ul>
                                <li><a href="<%=conLan%>/flyingSAA/baggage/checkedBaggage.html"><s:text name="key.submenu.checked.baggage"/></a></li>                                	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/baggage_tips.html"><s:text name="key.submenu.baggage.tips"/></a></li>						      	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/handBaggage.html"><s:text name="key.submenu.hand.baggage"/></a></li>						     	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/sportingEquipment.html"><s:text name="key.submenu.sporting.equipment"/></a></li>						      	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/excessBaggage.html"><s:text name="key.submenu.excess.baggage"/></a></li>						       	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/travellingWithPets.html"><s:text name="key.submenu.travelling.pets"/></a></li>						      	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/firearms.html"><s:text name="key.submenu.firearms.ammunition"/></a></li>						      	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/unaccompaniedBaggage.html"><s:text name="key.submenu.unaccompanied.baggage"/></a></li>						       	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/baggageRestrictions.html"><s:text name="key.submenu.baggage.restrictions"/></a></li>						       	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/musicalInstruments.html"><s:text name="key.submenu.musical.instruments"/></a></li>						     	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/delayedDamagedBaggage.html"><s:text name="key.submenu.delayed.damaged.baggage"/></a></li>						       	  
								<li><a href="<%=conLan%>/flyingSAA/baggage/baggageLiabilityAndConditions.html"><s:text name="key.submenu.baggage.conditions"/></a></li>					       	  
								<li><a href="<%=conLan%>/flyingSAA/Fees/fees.html"><s:text name="key.submenu.baggage.optional.services.usa"/></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a class="subscribe" href="<%=conLan%>/customercharter/CustomerServiceCharter.html"><s:text name="key.menu.customer.service"/></a></li>
                <li class="last essential-info"><a href="#"><s:text name="key.menu.essential.info"/></a>
                    <ul>
                        <li class="promotions">
                            <h2><span class="icon-menu-speaker"></span><s:text name="key.menu.essential.info"/></h2>
                            <ul id="essentialInfoLinks">
                                <li><a href="<%=conLan%>/manageMyTrip/Visas/visa_and_passport_info.html"><s:text name="key.submenu.visa.passport.info"/></a></li>
                                <li><a href="<%=conLan%>/planmytrip/travelAdvisory/sa-immigration-under-18.html"><s:text name="key.submenu.immigration.changes.children"/></a></li>
                                <li><a href="<%=conLan%>/essentialInfo/essentialinfo/SAA-and-Mango-codeshare.html"><s:text name="key.submenu.saa.mango.codeshare"/></a></li>
                                <li><a href="<%=conLan%>/essentialInfo/essentialinfo/checkinInfo.html"><s:text name="key.submenu.essential.checkin"/></a></li>
                                <li><a href="<%=conLan%>/essentialInfo/essentialinfo/closureTimes.html"><s:text name="key.submenu.flight.boarding.gate.closure"/></a></li>
                                <li><a href="<%=conLan%>/essentialInfo/essentialinfo/delayedDamagedBaggage.html"><s:text name="key.submenu.baggage.tracking"/></a></li>
                                <li><a href="<%=conLan%>/essentialInfo/essentialinfo/YellowFever.html"><s:text name="key.submenu.yellow.fever.advisory"/></a></li>
                                <li><a href="<%=conLan%>/essentialInfo/essentialinfo/SAA-Environment.html"><s:text name="key.submenu.saa.environment"/></a></li>
                                <li><a href="<%=conLan%>/flyingSAA/News/SAA_launches_new_mobile_applications.html"><s:text name="key.submenu.saa.launches.mobile.applications"/></a></li>
                                <li><a href="<%=conLan%>/essentialInfo/essentialinfo/PolioVaccination.html"><s:text name="key.submenu.polio.vaccination"/></a></li>
                                <li><a href="<%=conLan%>/essentialInfo/essentialinfo/brazil_cuurrency_restricions.html"><s:text name="key.submenu.brazil.currency.restrictions"/></a></li>
                                <li><a href="<%=conLan%>/essentialInfo/essentialinfo/Ebola.html"><s:text name="key.submenu.ebola.virus.disease"/></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <a href="#" class="best-fare-guarantee">&nbsp;</a>
        </nav>
    </section>
    <!-- Mid Menu/Nav -->


    <section class="content">

        <div class="wrap">
            <div class="left">
                <div id="bb" class="tabs">
                    <ul>
                        <li class="flights"><a href="#bb-flights"><span class="icon-bb-flight"></span><span class="text"><s:text name="tab.label.flights" /></span></a></li>
                        <li class="check-in"><a href="#bb-check-in"><span class="icon-bb-checkin"></span><span class="text"><s:text name="label.checkin.checkin" /></span></a></li>
                        <li class="log-in"><a href="#bb-log-in"><span class="icon-bb-user"></span><span class="text"><s:text name="label.navi.Voyager"/></span></a></li>
                        <li class="flight-status"><a href="#bb-flight-status"><span class="icon-bb-timer"></span><span class="text"><s:text name="label.checkFltSts.fltStatus" /></span></a></li>
                        <li class="car-rental"><a href="#bb-car-rental"><span class="icon-bb-car"></span><span class="text"><s:text name="tab.label.carHire"/></span></a></li>
                        <li class="hotels"><a onclick="goToHotelsURL();"><span class="icon-bb-hotel"></span><span class="text"><s:text name="tab.label.accommodation"/></span></a></li>
                        <!--<li class="hotels"><a href="#bb-hotels"><span class="icon-bb-hotel"></span><span class="text"><s:text name="tab.label.accommodation"/></span></a></li>-->
                    </ul>
                    <div class="overlay loading"></div>
                    <div class="info-popup"><header></header><div></div></div>
                    <div class="blocks">
                        <div id="bb-flights">
                            <form name="bbFlights" method="post">
                            	<s:hidden name="selectedLang"></s:hidden>		
                            	<s:hidden name="selLanguage" id="selLanguage"/>						
								<s:hidden name="countrySeltd" id="countrySeltd"/>
        						<s:hidden name="country" id="sessionCountry"/>        						
        						<s:hidden name="fromDate" id="fromDate"/>
        						<s:hidden name="toDate" id="toDate"/>
        						<s:hidden name="preferredClass" id="preferredClass"></s:hidden>
        						<s:hidden name="calendarSearchFlag" value="false"/>
                                <h2>Book A Flight</h2>
                                <!--
                                <div class="input radios">
                                    <label class="return"><input type="radio" name="tripType" id="tpTypeR" value="R">Return</label>
                                    <label class="one-way"><input type="radio" name="tripType" id="tpTypeO" value="O">One Way</label>
                                    <label class="multi-city"><input type="radio" name="tripType" id="tpTypeM" value="M" onclick="goToMulticityFlightSearch(this.value);">Multi-city</label>
                                </div>
                                -->
                                <div class="input select combobox">
                                    <s:select list="%{''}" id="departCity" name="departCity" headerKey="" title="%{#departCity}"></s:select>
                                </div>
                                <div class="input row text">
                                    <div class="column small-8 select combobox">
                                        <s:select list="%{''}" id="destCity" name="destCity" headerKey="" title="%{#destinCity}"></s:select>
                                    </div>
                                    <div class="column small-4">
                                        <a href="<s:url value='/multiCityFlightSearch!loadMultiCityPage.action'/>" class="button small no-margin multi-city"><span><s:text name="label.multiCity"></s:text></span></a>
                                    </div>
                                </div>
                                <div class="input datepicker">
                                    <label><s:text name="label.leaving"></s:text></label>
                                    <div>
                                        <div class="input day">
                                            <select id="departDay" name="departDay">
                                                <option></option>
                                            </select>                                            
                                        </div>                                        
                                        
                                        <div class="input month-year">
                                            <select id="departMonthYear" name="departMonthYear">
                                                <option></option>
                                            </select>
                                        </div>
                                        
                                        <div class="icon-calendar">
                                            <div class="calendar leaving"><span class="title"><s:text name="label.leaving"></s:text></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="input datepicker">
                                    <label><s:text name="label.returning"></s:text>
                                    <input type="checkbox" id="chkReturn" name="chkReturn" checked="checked">
                                    </label>
                                    <s:hidden id="tripType" name="tripType"/>
                                    <div>
                                        <div class="input day">
                                            <select id="destDay" name="destDay">
                                                <option></option>
                                            </select>                                            
                                        </div>
                                        
                                        <div class="input month-year">
                                            <select id="returnMonthYear" name="returnMonthYear">
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="icon-calendar">
                                            <div class="calendar returning"><span class="title"><s:text name="label.returning"></s:text></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flexibility">
                                    <div class="row">
                                        <div class="column small-6">
                                            <div class="input radio">
                                                <label for="radExact"><input id="radExact" type="radio" name="flexible" value="false"><s:text name="label.exact.dates"></s:text></label>
                                            </div>
                                        </div>
                                        <div class="column small-6 flex">
                                            <div class="input radio">
                                                <label for="radFlexible"><input id="radFlexible" type="radio" name="flexible" value="true"><s:text name="label.three.days"></s:text></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row input">
                                    <div class="column small-4 input select">
                                        <select id="adultCount" name="adultCount">
											<option value="1">1 <s:text name="label.adult"/></option>
										    <s:iterator begin="2" end="9" step="1" var="adtSize">	
											<option value="<s:property value='#adtSize'/>"><s:property value='#adtSize'/> <s:text name="label.adults"/></option>
										    </s:iterator>
										</select>
                                        <!--<p class="tip">+12 years</p>-->
                                    </div>
                                    <div class="column small-4 input select">
                                        <select id="childCount" name="childCount">
                                            <option value="0"><s:text name="label.children"/></option>
                                            <option value="1">1 <s:text name="label.paxdetails.payment.enter.paxdetails.child"/></option>
                                            <s:iterator begin="2" end="8" step="1" var="cnnSize">
                                            <option value="<s:property value='#cnnSize'/>"><s:property value='#cnnSize'/> <s:text name="label.children"/></option>
                                            </s:iterator>
                                        </select>
                                        <p class="tip"><s:text name="label.children.ageInfo" /></p>
                                    </div>
                                    
                                    <div class="column small-4 input select">
                                        <select id="infantCount" name="infantCount">
                                            <option value="0"><s:text name="label.infants"/></option>
                                            <option value="1">1 <s:text name="label.paxdetails.payment.enter.paxdetails.infant"/></option>
                                            <s:iterator begin="2" end="4" step="1" var="infSize">
                                            <option value="<s:property value='#infSize'/>"><s:property value='#infSize'/> <s:text name="label.infants"/></option>
                                            </s:iterator>
                                        </select>
                                        <p class="tip"><s:text name="label.infants.ageInfo" /></p>
                                    </div>
                                </div>
                                <div class="row input">
                                    <div class="column small-7">
                                        <label class="inline"><s:text name="label.cabinClass"/><a href="" class="icon-info">
                                            <span>
                                                <span class="head"><s:text name="tooltip.leisure.travelclass.header" /></span>
                                                <span class="inner">
                                                    <s:text name="tooltip.leisure.travelclass.content" />
                                                </span>
                                            </span>
                                        </a></label>
                                    </div>
                                    <div class="column small-5">
                                    <s:if test="cabinClassList">
										<s:select id="flightClass" name="preferredCabinClass" list="cabinClassList" listKey="cabinValue" listValue="cabinDescription"/>
									</s:if>
									<s:else>
										<s:select id="flightClass" name="preferredCabinClass" list="#session.SESSION_CABIN_CLASS_LIST" listKey="cabinValue" listValue="cabinDescription" />
									</s:else>
                                    </div>
                                </div>
                                <div class="row input">
                                    <div class="column small-7">
                                        <label class="inline"><s:text name="label.promotionCode"/><a href="" class="icon-info">
                                            <span>
                                                <span class="head"><s:text name="tooltip.leisure.promocode.header" /></span>
                                                <span class="inner"><s:text name="tooltip.leisure.promocode.content" /></span>
                                            </span>
                                        </a></label>
                                    </div>
                                    <div class="column small-5">
                                        <input type="text" id="promoCode" name="promoCode">
                                    </div>
                                </div>
                                <p class="note"><small>
                                <s:if test="#sessCnt == 'US' || #sessCnt == 'CA'">
								<a href="<s:url value='http://flysaausa.com/groups'/>" target="_blank" class="morePaxLink"><s:text name="label.morethan.nine.people" /></a>
								</s:if>
								<s:else>
								<a href="<s:url value='/groupBooking.action'/>" class="morePaxLink"><s:text name="label.morethan.nine.people" /></a>
								</s:else>
                                </small></p>
                                <div class="input submit text-right">
                                    <button type="submit" class="button green submit"><span><s:text name="button.label.book"/></span></button>
                                </div>
                            </form>
                        </div>
                        <div id="bb-check-in">
                            <form action="" method="post">
                                <h2><s:text name="label.checkin.checkin" /></h2>
                                <header>
                                    <div class="input radios">
                                        <label><input type="radio" name="fcheckin" id="checkInSAA" value="SaaCheckin" checked="checked"><s:text name="label.saa.checkin"/></label>
                                        <label><input type="radio" name="fcheckin" id="checkInMango" value="MangoCheckin"><s:text name="label.mango.checkin"/></label>
                                    </div>
                                </header>
                                <hr />
                                <p class="text-center no-margin"><s:text name="label.checkin.mobile.info"/></p>
                                <hr class="dashed" />

                                <div id="checkInSAADiv" class="check-in">
                                    <div class="input text">
                                        <label><s:text name="label.checkin.sur.name"/></label>
                                        <input type="text" id="ISurname" name="ISurname">
                                    </div>
                                    <div class="input">
                                        <label class="label"><s:text name="label.form.identification"/><a class="icon-info">
                                        <span>
                                            <span class="head"><s:text name="label.form.identification"/></span>
                                            <span class="inner"><s:text name="label.iconinfo.form.identification"/><br>
                                                <strong>- <s:text name="label.booking.refernce"/></strong>,<br>
                                                <strong>- <s:text name="label.electronic.ticket.number"/></strong><br>
                                                <strong>- <s:text name="label.southAfrican.frequent.flyer.number"/></strong>.
                                            </span>
                                        </span>
                                        </a>
                                        </label>
                                        <div class="input">
                                            <select id="checkInMethod" name="checkInMethod">
                                                <option value="PNR"><s:text name="label.booking.refernce"/></option>
                                                <option value="FFSA"><s:text name="label.southAfrican.frequent.flyer.number"/></option>
                                                <option value="ETKT"><s:text name="label.electronic.ticket.number"/></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input">
                                        <label class="check-in-method pnr"><s:text name="label.booking.refernce"/>
                                            <a class="icon-info">
                                            <span>
                                                <span class="head"><s:text name="label.booking.refernce"/></span>
                                                <span class="inner"><s:text name="label.iconinfo.booking.refernce"/></span>
                                            </span>
                                            </a>
                                        </label>
                                        <label class="check-in-method ffsa"><s:text name="label.southAfrican.frequent.flyer.number"/>
                                            <a class="icon-info">
                                            <span>
                                                <span class="head"><s:text name="label.southAfrican.frequent.flyer.number"/></span>
                                                <span class="inner"><s:text name="label.iconinfo.southAfrican.frequent.flyer.number"/></span>
                                            </span>
                                            </a>
                                        </label>
                                        <label class="check-in-method etkt"><s:text name="label.electronic.ticket.number"/>
                                            <a class="icon-info">
                                            <span>
                                                <span class="head"><s:text name="label.electronic.ticket.number"/></span>
                                                <span class="inner"><s:text name="label.iconinfo.electronic.ticket.number"/></span>
                                            </span>
                                            </a>
                                        </label>
                                        <input id="IIdentification" />
                                    </div>
                                    <div class="input">
                                        <label><input id="checkInAddPax" type="checkbox" /> <s:text name="label.checkin.additional.passengers"/></label>
                                    </div>
                                </div>

                                <div id="checkInMangoDiv" class="check-in hidden">
                                    <div class="input text">
                                        <label><s:text name="label.checkin.sur.name"/></label>
                                        <input type="text" id="ISurnameM" name="ISurnameM">
                                    </div>
                                    <div class="input">
                                        <label class="label"><s:text name="label.form.identification"/>
                                            <a class="icon-info">
                                                <span>
                                                    <span class="head"><s:text name="label.form.identification"/></span>
                                                    <span class="inner"><s:text	name="label.iconinfo.electronic.ticket.number"/>
                                                    </span>
                                                </span>
                                            </a>
                                        </label>
                                        <label class="label"><s:text name="label.electronic.ticket.number"/></label>
                                        <input type="text" id="etktnumberM" name="etktnumberM" />
                                    </div>
                                    <div class="input">
                                        <label><input id="checkInAddPaxM" name="checkInAddPaxM" type="checkbox" /> <s:text name="label.checkin.additional.passengers"/></label>
                                    </div>
                                </div>
                                <div class="input submit">
                                    <div class="left">
                                        <button type="reset" class="button orange reset"><span><s:text name="label.booking.common.button.reset"/></span></button>
                                    </div>
                                    <div class="right">
                                        <button type="submit" class="button green submit"><span><s:text name="label.availability.continue"/></span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="bb-log-in">
                            <form name="voyagerLogin" action="" method="post">
                                <div class="voyager-status-<s:property value='isHomeVoyLogin'/>">
                                    <div class="out">
                                        <h2>SAA Voyager Login</h2>
                                        <div class="input">
                                            <label for="voyagerId"><s:text name="label.voyager.number"/></label>
                                            <input id="voyagerId" name="voyagerId" maxlength="15">
                                        </div>
                                        <div class="input">
                                            <label for="pin"><s:text name="label.pin"/></label>
                                            <input id="pin" name="pin" type="password" maxlength="4">
                                        </div>
                                        <div>
                                            <div class="left">
                                                <p class="no-margin"><small>
                                                    <a href="#" class="forgot"><span class="icon-arrow-right"></span><s:text name="label.forget.details"/></a>
                                                    <br />
                                                    <a href="<%=conLan%>/voyager/AboutVoyager/flysaa_VoyagerContacts.html"><span class="icon-arrow-right"></span><s:text name="label.helpdesk"/></a>
                                                </small>
                                                </p>
                                            </div>
                                            <div class="text-right">
                                                <button type="submit" class="button green submit no-margin"><span><s:text name="label.login"/></span></button>
                                                <a href="#" onclick="checkJoinVoyager()" class="button join"><span><s:text name="label.join.voyager"/></span></a>
                                            </div>
                                        </div>
                                        <!--
                                        <div id="voyagerInfo">
                                            <h2>SAA Voyager Credit Card</h2>
                                            <p>Get the best Voyager Mile earn rate with the SAA Voyager credit card. </p>
                                            <p class="text-right"><a target="_blank" href="https://www.nedbank.co.za/website/content/forms/form.asp?FormsId=621&amp;Location=&amp;FormLocation=flysaa" class="button"><span>Apply online now</span></a></p>
                                        </div>
                                        -->
                                    </div>
                                    <div class="in">
                                        <h2>SAA Voyager Login</h2>
                                        <hr />
                                        <h3><s:text name="label.welcome"></s:text>, <span><s:property value="#session.loginVO.firstname"/></span></h3>
                                        <p class="half-margin"><s:text name="Membership No"></s:text>:<br />
                                            <span class="bold blue"><s:property value="#session.loginVO.voyagerId"/></span></p>
                                        <p class="half-margin"><s:text name="label.miles"></s:text>:<br />
                                            <span class="bold blue"><s:property value="#session.loginVO.balancemiles"/></span></p>
                                        <p class="half-margin"><s:text name="label.voy.status"></s:text>:<br />
                                            <span class="bold blue"><s:property value="#session.loginVO.status"/></span></p>
                                        <hr class="dashed" />
                                        <nav>
                                            <p>
                                                <a href="<s:url value='/voyager!fetchUserDetails.action'/>" class="link-arrow"><s:text name="label.mydetails"></s:text></a><br />
                                                <a href="<s:url value='/redeemMiles!redeemMiles.action'/>" class="link-arrow"><s:text name="label.awards"></s:text></a><br />
                                                <a href="<s:url value='/mileagesummary!voyagermileagesummary.action'/>" class="link-arrow"><s:text name="label.mileage.summary"></s:text></a><br />
                                                <a href="<s:url value='/voyagermileagecaliculator!voyagermileagecaliculator.action'/>" class="link-arrow"><s:text name="label.miles.calculator"></s:text></a><br />
                                                <a href="<s:url value='/voyagerNews.action'/>" class="link-arrow"><s:text name="label.voyager.news"></s:text></a><br />
                                                <a href="<s:url value='/voyagerHelpDesk.action'/>" class="link-arrow"><s:text name="label.helpdesk"></s:text></a>
                                            </p>
                                        </nav>
                                        <br />
                                        <p class="text-right">
                                            <a href="<s:url value='/voyagerLogin.action'/>" class="button"><span>My Voyager</span></a>
                                        </p>
                                    </div>
                                </div>

                            </form>

                            <form name="voyagerForgot" method="post">
                                <div class="clearfix">
                                    <h2><s:text name="label.voy.forgotPwd"/></h2>

                                    <div class="input">
                                        <label for="voyagerNumber"><s:text name="label.voyager.number"/></label>
                                        <input name="voyagerNumber" id="voyagerNumber">
                                    </div>

                                    <div class="input">
                                        <label for="email"><s:text name="label.voyager.pinreset.email"/></label>
                                        <input name="email" id="email">
                                    </div>

                                    <div>
                                        <div class="left">
                                            <a class="button orange back"><span>Cancel</span></a>
                                        </div>
                                        <div class="text-right">
                                            <button id="forgotbutton" type="button" class="button green submit"><span><s:text name="label.voy.send"/></span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div id="bb-flight-status">
                            <form name="flightstatus" method="post">
                                <h2><s:text name="label.checkFltSts.fltStatus"/></h2>
                                <div class="input checkbox">
                                    <label><input id="flightChecked" name="flightChecked" type="checkbox"><s:text name="label.checkFltSts.know.fltNum"/></label>
                                </div>

                                <div id="FlightNo" class="input row hidden">
                                    <div class="column small-6">
                                        <label for="carrierCode"><s:text name="label.checkFltSts.carrierCode"/></label>
                                        <input id="carrierCode" name="carrierCode" maxlength="2" value="SA" disabled="disabled">
                                    </div>
                                    <div class="column small-6">
                                        <label for="flightNumber"><s:text name="label.checkFltSts.flightNUmber"/></label>
                                        <input id="flightNumber" name="flightNumber" maxlength="5">
                                    </div>
                                </div>

                                <div id="NoFlight">
                                    <div class="input select combobox">
                                        <s:select list="%{''}"  id="statusDepartCity" title="%{#departCity}" name="departureCity"></s:select>
                                    </div>

                                    <div class="input select combobox">
                                        <s:select list="%{''}"  id="statusDestCity"  title="%{#destinCity}" name="destinationCity"></s:select>
                                    </div>

                                    <div class="input datepicker">
                                        <label><s:text name="label.checkFltSts.depDate"/></label>
                                        <div class="select">
                                            <select class="DepDate" id="fromDateFLT" name="fromDateFLT">
                                                <option value="-1"><s:text name="label.checkFlt.yesterday"/></option>
                                                <option value="0"><s:text name="label.checkFlt.today"/></option>
                                                <option value="1"><s:text name="label.checkFlt.tomorrow"/></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <button type="submit" class="button green right submit no-margin"><span><s:text name="label.availability.continue"/></span></button>
                                    <button type="reset" class="button orange left reset no-margin"><span><s:text name="label.booking.common.button.reset"/></span></button>
                                    <a class="button orange left" href="<%=conLan%>/flightStatus/flight_status_other_airlines.html"><span><s:text name="label.checkFltSts.airlines"/></span></a>
                                </div>

                                <footer>
                                    <h3><s:text name="label.checkFltSts.flightSchedule"/></h3>
                                    <p><s:text name="label.checkFltSts.flightSchedule.track"/></p>
                                    <div class="text-center">
                                        <a href="<%=conLan%>/Documents/flightschedules/Timetable.pdf" rel="external" class="button orange download"><span><s:text name="label.checkFltSts.pdf"/></span></a>
                                        <a class="button route"><span><s:text name="label.checkFltSts.online"/></span></a>
                                    </div>
                                </footer>
                            </form>
                        </div>
                        <div id="bb-car-rental">
                            <form name="carRental" method="post">
                                <h2><s:text name="tab.label.carHire"/></h2>
                                <div class="input combobox">
                                    <s:select id="pickupLoc" list="%{''}" title="%{#pickupHeaderTxt}"   name="pickupLoc"/> 
                                    <input id="txtpick_loc" name="txtpick_loc" type="hidden" />
                                </div>

                                <div class="input combobox">
                                    <s:select id="dropoffLoc"  list="%{''}" title="%{#dropoffHeaderTxt}"  name="dropoffLoc"/>
                                    <input id="txtdrop_loc" name="txtdrop_loc" type="hidden" />
                                </div>

                                <div id="carDateUp" class="input datetimepicker">
                                    <label><s:text name="label.carhire.pickupdate"/></label>
                                    <div>
                                        <div class="input day">
                                            <select id="pickDay" name="pickDay">
                                            </select>
                                        </div>
                                        <div class="input month-year">
                                            <select id="pickMonthYear" name="pickMonthYear">
                                            </select>
                                        </div>
                                        <div class="icon-calendar">
                                            <div class="calendar pick-up"><span class="title"><s:text name="label.carhire.pickupdate"/></span></div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <label class="time"><s:text name="label.carhire.time"/></label>
                                        <div class="input time">
                                            <select id="pickUpTime" name="pickUpTime">
                                                <option value="0000">00:00</option>
                                                <option value="0030">00:30</option>
                                                <option value="0100">01:00</option>
                                                <option value="0130">01:30</option>
                                                <option value="0200">02:00</option>
                                                <option value="0230">02:30</option>
                                                <option value="0300">03:00</option>
                                                <option value="0330">03:30</option>
                                                <option value="0400">04:00</option>
                                                <option value="0430">04:30</option>
                                                <option value="0500">05:00</option>
                                                <option value="0530">05:30</option>
                                                <option value="0600">06:00</option>
                                                <option value="0630">06:30</option>
                                                <option value="0700">07:00</option>
                                                <option value="0730">07:30</option>
                                                <option value="0800">08:00</option>
                                                <option value="0830">08:30</option>
                                                <option value="0900" selected="selected">09:00</option>
                                                <option value="0930">09:30</option>
                                                <option value="1000">10:00</option>
                                                <option value="1030">10:30</option>
                                                <option value="1100">11:00</option>
                                                <option value="1130">11:30</option>
                                                <option value="1200">12:00</option>
                                                <option value="1230">12:30</option>
                                                <option value="1300">13:00</option>
                                                <option value="1330">13:30</option>
                                                <option value="1400">14:00</option>
                                                <option value="1430">14:30</option>
                                                <option value="1500">15:00</option>
                                                <option value="1530">15:30</option>
                                                <option value="1600">16:00</option>
                                                <option value="1630">16:30</option>
                                                <option value="1700">17:00</option>
                                                <option value="1730">17:30</option>
                                                <option value="1800">18:00</option>
                                                <option value="1830">18:30</option>
                                                <option value="1900">19:00</option>
                                                <option value="1930">19:30</option>
                                                <option value="2000">20:00</option>
                                                <option value="2030">20:30</option>
                                                <option value="2100">21:00</option>
                                                <option value="2130">21:30</option>
                                                <option value="2200">22:00</option>
                                                <option value="2230">22:30</option>
                                                <option value="2300">23:00</option>
                                                <option value="2330">23:30</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div id="carDateOff" class="input datetimepicker">
                                    <label><s:text name="label.carhire.dropoffdate"/></label>
                                    <div>
                                        <div class="input day">
                                            <select id="dropoffDay" name="dropoffDay">
                                            </select>
                                        </div>
                                        <div class="input month-year">
                                            <select id="dropoffMonthYear" name="dropoffMonthYear" class="month-year">
                                            </select>
                                        </div>
                                        <div class="icon-calendar">
                                            <div class="calendar drop-off"><span class="title"><s:text name="label.carhire.dropoffdate"/></span></div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <label class="time"><s:text name="label.carhire.time"/></label>
                                        <div class="input time">
                                            <select id="dropOffTime" name="dropOffTime">
                                                <option value="0000">00:00</option>
                                                <option value="0030">00:30</option>
                                                <option value="0100">01:00</option>
                                                <option value="0130">01:30</option>
                                                <option value="0200">02:00</option>
                                                <option value="0230">02:30</option>
                                                <option value="0300">03:00</option>
                                                <option value="0330">03:30</option>
                                                <option value="0400">04:00</option>
                                                <option value="0430">04:30</option>
                                                <option value="0500">05:00</option>
                                                <option value="0530">05:30</option>
                                                <option value="0600">06:00</option>
                                                <option value="0630">06:30</option>
                                                <option value="0700">07:00</option>
                                                <option value="0730">07:30</option>
                                                <option value="0800">08:00</option>
                                                <option value="0830">08:30</option>
                                                <option value="0900" selected="selected">09:00</option>
                                                <option value="0930">09:30</option>
                                                <option value="1000">10:00</option>
                                                <option value="1030">10:30</option>
                                                <option value="1100">11:00</option>
                                                <option value="1130">11:30</option>
                                                <option value="1200">12:00</option>
                                                <option value="1230">12:30</option>
                                                <option value="1300">13:00</option>
                                                <option value="1330">13:30</option>
                                                <option value="1400">14:00</option>
                                                <option value="1430">14:30</option>
                                                <option value="1500">15:00</option>
                                                <option value="1530">15:30</option>
                                                <option value="1600">16:00</option>
                                                <option value="1630">16:30</option>
                                                <option value="1700">17:00</option>
                                                <option value="1730">17:30</option>
                                                <option value="1800">18:00</option>
                                                <option value="1830">18:30</option>
                                                <option value="1900">19:00</option>
                                                <option value="1930">19:30</option>
                                                <option value="2000">20:00</option>
                                                <option value="2030">20:30</option>
                                                <option value="2100">21:00</option>
                                                <option value="2130">21:30</option>
                                                <option value="2200">22:00</option>
                                                <option value="2230">22:30</option>
                                                <option value="2300">23:00</option>
                                                <option value="2330">23:30</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <!--<div class="input combobox">
                                    <s:select id="carCountry" list="%{''}" title="%{#countryResHeaderTxt}" name="carCountry"/>
                                    <input id="txtcountry" name="txtcountry" type="hidden" />
                                </div>

                                <div class="input text">
                                    <div class="carDriverAge">
                                        <input id="driverAge" maxlength="3" placeholder="Driver Age" />
                                    </div>
                                </div>
                                -->

                                <div class="actions">
                                    <button type="button" class="button green right submit no-margin"><span><s:text name="label.availability.booknow"/></span></button>
                                    <button type="reset" class="button orange left reset no-margin"><span><s:text name="label.booking.common.button.reset"/></span></button>
                                </div>
                            </form>
                            <a class="cars adSpace" target="_blank" href="http://www.flysaa.com<%=conLan%>/manageMyTrip/insurance/travel_insurance.html"></a>
                        </div>
                        <div id="bb-hotels" style="display: none">
                            <form action="" method="post">
                                <h2><s:text name="tab.label.accommodation"/></h2>
                                <div class="input select combobox">
                                    <s:select id="hotelDestination"  list="%{''}" title="%{#hotelHeaderTxt}" />
                                    <input id="txtHotelDes_loc" name="txtHotelDes_loc"  type="hidden" />
                                </div>

                                <div class="input datepicker">
                                    <label><s:text name="label.booking.accomodation.checkin.date"/></label>
                                    <div>
                                        <div class="input day">
                                            <select id="checkinDay" name="checkinDay">
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="input month-year">
                                            <select id="checkinMonthYear" name="checkinMonthYear">
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="icon-calendar">
                                            <div class="calendar check-in"><span class="title"><s:text name="label.booking.accomodation.checkin.date"/></span></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="input datepicker">
                                    <label><s:text name="label.booking.accomodation.checkout.date"/></label>
                                    <div>
                                        <div class="input day">
                                            <select id="checkoutDay" name="checkoutDay">
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="input month-year">
                                            <select id="checkoutMonthYear" name="checkoutMonthYear">
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="icon-calendar">
                                            <div class="calendar check-out"><span class="title"><s:text name="label.booking.accomodation.checkout.date"/></span></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row input hotel-guests">
                                    <div class="column small-4 input select">
                                        <select id="hotelAdults" name="hotelAdults">
											<option value="1">1 <s:text name="label.adult"/></option>
					    					<s:iterator begin="2" end="9" step="1" var="adtSize">	
											<option value="<s:property value='#adtSize'/>"><s:property value='#adtSize'/> <s:text name="label.adults"/>	</option>
					    					</s:iterator>	
					    				</select>
                                        <p class="tip">+12 years</p>
                                    </div>
                                    <div class="column small-4 input select">
                                        <select id="hotelChildren" name="hotelChildren">
											<option value="0"><s:text name="label.children"/>	</option>
											<option value="1">1 <s:text name="label.paxdetails.payment.enter.paxdetails.child"/>	</option>
										    <s:iterator begin="2" end="8" step="1" var="cnnSize">	
											<option value="<s:property value='#cnnSize'/>"><s:property value='#cnnSize'/> <s:text name="label.children"/>	</option>
										    </s:iterator>		
									    </select>
                                        <p class="tip">2-11 years</p>
                                    </div>
                                    <div class="column small-4 input select">
                                        <select id="hotelInfants" name="hotelInfants">
											<option value="0"><s:text name="label.infants"/>	</option>
											<option value="1">1 <s:text name="label.paxdetails.payment.enter.paxdetails.infant"/>	</option>
										    <s:iterator begin="2" end="4" step="1" var="infSize">	
											<option value="<s:property value='#infSize'/>"><s:property value='#infSize'/> <s:text name="label.infants"/>	</option>
										    </s:iterator>		
										</select>
                                        <p class="tip">0-23 months</p>
                                    </div>
                                </div>
                                <div class="row input">
                                    <div class="column small-6">
                                        <label class="inline"><s:text name="label.booking.accomodation.number.rooms"/></label>
                                    </div>
                                    <div class="column small-6">
                                        <select id="numberoffrooms" name="numberoffrooms">						
										    <s:iterator begin="1" end="4" step="1" var="numSize">	
											<option value="<s:property value='#numSize'/>"><s:property value='#numSize'/></option>
										    </s:iterator>		
										</select>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <button type="submit" class="button green right submit no-margin"><span><s:text name="label.availability.booknow"/></span></button>
                                    <button type="reset" class="button orange left reset no-margin"><span><s:text name="label.booking.common.button.reset"/></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right">
                <div id="slider"> </div>
            </div>
        </div>

        <div class="wrap">
            <div id="featuredArticles" class="left"> </div>
            <div class="right">
            	<div id="flightDeals"> </div>
            	<div id="specialOffers"> </div>
            </div>
        </div>

    </section>

    <footer id="footer">
        <div class="wrap">
            <div class="copyright">&copy; 2014 South African Airways</div>
            <nav>
                <ul>
                    <li><a href="<%=conLan%>/about_us.action">About Us</a></li>
                    <li><a href="<%=conLan%>/contact_us.action">Contact Us</a></li>
                    <li><a href="<%=conLan%>/careers.action"> Careers</a></li>
                    <li><a href="<%=conLan%>/policies_and_Disclaimers.action">Policies</a></li>
                    <li><a href="<%=conLan%>/conditions_of_Contract.action">Conditions of Contract</a></li>
                    <li><a href="<%=conLan%>/browser_Requirements.action">Browser Requirements</a></li>
                    <li><a href="<%=conLan%>/accessibility.action">Accessibility</a></li>
                    <li><a href="<%=conLan%>/imprint.action">Legal</a></li>
                    <li><a href="<%=conLan%>/sitemap/sitemap.html">Site Map</a></li>
                    <li><a href="<%=conLan%>/planmytrip/destinations/codeshares.html">Codeshares</a></li>
                </ul>
            </nav>
            <aside class="alliance" id="allianceLogo"><a target="_blank" class="satourism" href="http://www.southafrica.net">South African Tourism</a></aside>
            <aside class="social">
                <a href="http://www.facebook.com/FlySAA" class="facebook">Facebook</a>
                <a href="http://twitter.com/@flysaa" class="twitter">Twitter</a>
                <a href="http://www.flysaa.com/dynamicAd/GetAdvert/en_ZA_specials" class="rss" rel="external">RSS</a>
                <a href="http://plus.google.com/+southafricanairways" class="googleplus">Google +</a>
                <a href="https://itunes.apple.com/us/app/south-african-airways/id619821296?mt=8" class="applestore">Apple Store</a>
                <a href="https://play.google.com/store/apps/details?id=com.flysaa.mobile.android" class="googleplay">Google Play Store</a>
                <a href="http://apps.microsoft.com/windows/app/flysaa/8add1a17-1300-4045-9d24-bbde76994206" class="windowstore">Window Store</a>
                <a href="http://pinterest.com/flysaa" class="pinterest">Pinterest</a>
            </aside>
            <!--<aside id="snoopy">Â§</aside>
            
        --></div>
    </footer>



    <form name="submitter" action="" method="POST">
        <!--
        <input type="hidden" name="selLanguage" value="EN" />
        <input type="hidden" name="selectedLang" value="" id="za_en_home_selectedLang"/>
        <input type="hidden" name="preferredClass" value="" id="preferredClass"/>
        <input type="hidden" name="calendarSearchFlag" value="false" id="za_en_home_calendarSearchFlag"/>
        -->
    </form>


    <!-- Helpers -->
    <!-- Start of Alert-->

    <div id="scriptAlert" class="alertmodal">
        <a href="javascript:;" class="close" onclick="modalClose('scriptAlert')">Close</a>
        <h4>label.modal.modalBox</h4>
        <div class="inner">
        </div>
    </div>


    <!-- End of Alert Section-->

    <div id="modal"></div>

    <!-- Help Div for manage res retrieve and pay -->
    <div id="helpModalPopUp" class="modal">
        <a href="javascript:;" class="close" onclick="modalClose('helpModalPopUp')">Close</a>
        <h4>Need Help ?</h4>
        <div class="inner scrollable">
            <div id="helpModalPopUpDetails" class="scr">
            </div>
        </div>
        <div class="bottom"></div>
    </div>

    <div id="disabler"></div>

    <a href="#" onclick="viewSnoopy()" style="position: absolute;left: 0;bottom: 0;z-index: 1000;"><font color="#4676a7">S</font> </a>

    <!--iFrame Wrappers-->
    <div id="iframePopup"><a href="#" class="close">Close</a><div class="inner"></div></div>
    <!--/iFrame Wrappers-->

    <!--/Helpers -->



    <script type="text/javascript">
    //These were pulled out of the top of the body and should be in the propsArray and not as html tags
    var config = {
        //<div class="hidden" id="display_language"><%=language%></div>
        language: '<%=language%>',
        //<div class="hidden" id="user_location"><%=country%></div>
        country: '<%=country%>',
        //<div id="long_location" style="display: none"></div>
        countryLong: '',
        //<div id='locale_str' style='display: none;'><%=language%></div>
        locale_str: '<%=language%>',

        mobileUser: false,

        //moved from region diw
        //There is no need for these, they are already set above
        //<div id="selectedCountryDiv" class="hide">ZA</div>
        //<div id="selectedLocaleDiv" class="hide">EN</div>
        //<div id="selectedLocaleVal" style="display:none">EN</div>

        //<div id="isCookieEnabledDiv" class="hide">false</div>  //There is no need for this, it is already set above
        isCookieEnabled: false,

        //<s:hidden id="selectedProductIs" name="selectedProductIs" value="%{#session.SELECTED_PRODUCT}"></s:hidden>
        selectedProductIs: '<%=session.getAttribute("SELECTED_PRODUCT")%>',

        //To set country and language from the Menu in home page
        //There is no need for these, it is already set above
        //          <input type="hidden" name="country" value="ZA" id="sessionCountry" />
        //          <input type="hidden" name="selLanguage" value="EN" id="selLanguage" />

        //Voyager Settings
        isHomeVoyLogin: '<s:property value="isHomeVoyLogin"/>'
        //<s:set name="loginVOforVoy" value="#session.loginVO"></s:set>

    };
 
    var	voyagerUser = {
            nameInitial:    '<s:property value="#session.loginVO.firstname"/>',
            name:           '<s:property value="#session.loginVO.firstname"/>',
            membrshpNum:    '<s:property value="#session.loginVO.voyagerId"/>',
            miles:          '<s:property value="#session.loginVO.balancemiles"/>',
            currenttierid:  '<s:property value="#session.loginVO.currenttierid"/>',
            status:         '<s:property value="#session.loginVO.status"/>'       
    };

    </script>
    <script type="text/javascript" src="<s:url value='/jsp/herald/js/libraries.js'/>"></script>
    <script type="text/javascript" src="<s:url value='/jsp/herald/js/home.js'/>"></script>
    <script>
        /**
         * Lanugage Translations
         */
        propsArray['bookingBlock_0']	= "Trip type is required.";
        propsArray['bookingBlock_1']	= "Please select a departure city.";
        propsArray['bookingBlock_2']	= "Please select a destination city.";
        propsArray['bookingBlock_3']	= "The Origin and Destination are the same, please select a different origin or destination.";
        propsArray['bookingBlock_4']	= "Minimum of 1 adult traveler required in the booking.";
        propsArray['bookingBlock_5']	= "For safety reasons on-board, each infant under 2 years must be accompanied by a separate adult";
        propsArray['bookingBlock_6']	= "The total number of adults,children and infants must not exceed 9. Please consider splitting your group or complete the <a href=\"groupBooking.action\">Groups Request Form</a>";
        propsArray['bookingBlock_7']	= "Please select class of travel.";
        propsArray['bookingBlock_8']	= "From Date is required.";
        propsArray['bookingBlock_9']	= "To Date is required.";
        propsArray['bookingBlock_10']	= "Sorry you cannot go beyond the designated range!";
        propsArray['bookingBlock_11']	= "Sorry! you may not go beyond the designated date.";
        propsArray['bookingBlock_12']	= "The return date you have selected is before the departure date. Please review the dates selected.";
        propsArray['bookingBlock_13']	= "Please select a proper location";
        propsArray['bookingBlock_14']	= "Please select valid dates";
        propsArray['bookingBlock_15']	= "Please choose valid time";
        propsArray['bookingBlock_16']	= "Please provide Driver age";
        propsArray['bookingBlock_17']	= "Please enter a valid travel return date.";
        propsArray['bookingBlock_18']   = "Invalid Departure Date";
        propsArray['bookingBlock_19']	= "Please enter a valid travel departure date.";
        propsArray['checkin_1']			= "Please fill in the mandatory field(s).";
        propsArray['statusVal_1']		= "Please enter the Carrier Code.";
        propsArray['statusVal_2']		= "Please enter the Flight number.";
        propsArray['statusVal_3']		= "Flight Number should be a number.";
        propsArray['statusVal_4']		= "Please enter the Departure City.";
        propsArray['statusVal_5']		= "Please enter the Destination City.";
        propsArray['statusVal_6']		= "Departure City and  Destination City cannot be the same.";
        propsArray['voylogin_usename']	= "The voyager number field is empty";
        propsArray['voylogin_usename2']	= "The voyager number should be a Number";
        propsArray['login_usename1']	= "Voyager number  may not be less than 6 characters.";
        propsArray['login_usename2']	= "Voyager number  exceeds 15 characters..";
        propsArray['voylogin_pin']		= "The PIN field is empty";
        propsArray['voylogin_pin2']		= "The PIN number may not be less than 4 characters.";
        propsArray['voylogin_pin3']		= "The PIN should be a Number.";
        propsArray['hotelDest_1']		= "Please provide the hotel destination";
        propsArray['cars_1']			= "Please provide a valid driver age.";
        propsArray['cars_2']			= "Please provide a pick up location.";
        propsArray['cars_3']			= "Please provide a drop-off location.";
        propsArray['cars_4']			= "Please provide the country.";
        propsArray['voyLogin_usename3']	= "The voyager number is not valid.";
        propsArray['ieVersion_error']	= "You are using a version of Internet Explorer which is no longer supported. If you are using Windows XP or older, please switch to Firefox or Chrome as Microsoft no longer supports these systems. If you are using a system newer than Windows XP, you may upgrade Internet Explorer by clicking <a href=http://windows.microsoft.com/en-ZA/internet-explorer/download-ie>here</a>";

        //These were pulled out of the top of the body and should be in the propsArray and not as html tags
//      <div id="pickupHeaderId" class="hidden">Type your pick-up point here</div>
        propsArray['bookingBlock_pickupHeader']	        = '<s:property value="#pickupHeaderTxt"/>';
//      <div id="dropoffHeaderId" class="hidden">Type your drop-off point here</div>
        propsArray['bookingBlock_dropoffHeader']	    = '<s:property value="#dropoffHeaderTxt"/>';
//      <div id="hotelHeaderTxtId" class="hidden">Type your destination here</div>
        propsArray['bookingBlock_hotelHeaderTxt']	    = "Type your drop-off point here.";
//      <div id="destinationHeaderId" class="hidden">Destination City</div>
        propsArray['bookingBlock_destinationHeader']	= "Destination City";
//      <div id="countryResHeaderId" class="hidden">Country of Residence</div>
        propsArray['bookingBlock_countryResHeader']	    = "Country of Residence";
//      <div id="voyagerNumErrorId" class="hidden">Voyager Number is mandatory.</div>
        propsArray['bookingBlock_voyagerNumError']	    = "Voyager Number is mandatory.";
//      <div id="voyagerNumErrorId1" class="hidden">voyager number must be between 6 and 15 characters. may not be less than 6 characters.</div>
        propsArray['bookingBlock_voyagerNumError']	    = "Voyager number must be between 6 and 15 characters. may not be less than 6 characters.";
//      <div id="voyagerNumErrorId2" class="hidden">voyager number must be between 6 and 15 characters. exceeds 15 characters.</div>
        propsArray['bookingBlock_voyagerNumError2']	    = "Yoyager number must be between 6 and 15 characters. exceeds 15 characters.";
//      <div id="voyagerNumErrorId3" class="hidden">error.voyager.forgotpassword.voyagerNumber3.required</div>
        propsArray['bookingBlock_voyagerNumError3']	    = "error.voyager.forgotpassword.voyagerNumber3.required";
//      <div id="voyagerEmailErrorId" class="hidden">Either the email or mobile number is mandatory.</div>
        propsArray['bookingBlock_voyagerEmailErrorId']	= "Either the email or mobile number is mandatory.";
//      <div id="voyagerEmailErrorId1" class="hidden">Invalid Email Id: Enter a valid Email Id, e.g. admin@saa.com.</div>
        propsArray['bookingBlock_voyagerEmailErrorId1']	= "Invalid Email Id: Enter a valid Email Id, e.g. admin@saa.com.";

        //<div id="msgAlertDiv" class="hidden">Message</div>
        propsArray['msgAlertHeader'] = "Message";
        //<div id="continueDiv" class="hidden">Continue</div>
        propsArray['msgButtonContinue'] = "Continue";

        //<div id="departCityId" class="hidden">Departure City</div>
        propsArray['bookingBlock_departCity'] =  'Departure City';

        //<div id="destinCityId" class="hidden">Destination City</div>
        propsArray['bookingBlock_destinationCity'] = 'Destination City';
        //<div id="IBoardPoint"></div>
        propsArray['bookingBlock_boardPoint'] = '';

        //<div id="statusDepartCityId" class="hidden"></div>
        propsArray['statusDepartCityId'] = '';
        //<div id="statusDestCityId" class="hidden"></div>
        propsArray['statusDestCityId'] = '';



    </script>








</body>
</html>
