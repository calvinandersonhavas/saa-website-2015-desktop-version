<?php
ob_start();

include('includes/header.php');
include('includes/content-no-sidebar.php');
include('includes/footer.php');

$contents = ob_get_clean();
file_put_contents('content-no-sidebar.html', $contents);

echo $contents;