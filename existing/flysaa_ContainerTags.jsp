
<%-- 
/***********************************************************************
* Project	 		: FLYSAA
* Module Code & Name: NA
* File Name			: flysaa_ContainerTags.jsp
* Date				: 30-AUG-2013
* Author(s)			: RAJASEKHAR REDDY D
*************************************************************************/
 --%>

<%
String noTag= (String)request.getSession().getAttribute("NOTAG");

if (noTag == null)
	noTag = "FALSE";
	
if (noTag.equalsIgnoreCase("FALSE"))
{
%>
<script src='//www.flysaa.com/cms/eretail/scripts/Amadeus2.js'></script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T8DLCS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T8DLCS');</script>
<!-- End Google Tag Manager -->
<%
}
%>