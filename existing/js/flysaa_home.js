// HOME
// Returns true if the device is an iPhone, iPad or Android device
// This variable is used to do *lots* of checks because we want to use the native <select>s on devices instead of the linkselect plugin.
var iDevice = (navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/Android/i)) ? true : false;

var tab1 = [], tab2 = [], tab3 = [], tab4 = [], tabVal1=[],tabVal2=[],tabVal3=[],tabVal4=[],regionXml=[],departXml=[];
var depCountry;
var country=$.trim($('#user_location').text().toUpperCase());
var locale=$.trim($('#display_language').text().toLowerCase());
var relPath="";
var conLan;
var carRentalPosConnId;
var posConnId;
 // All the translated short-months for the datepicker.
	// The datepicker translations are set in lib/libraries.js (right at the bottom of the file)
	monthArrays = [],// The array-of-arrays
	months_en	= ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
	months_fr	= ['Jan','F\u00E9v','Mar','Avr','Mai','Jun','Jul','Ao\u00FB','Sep','Oct','Nov','D\u00E9c'],
	months_de	= ['Jan','Feb','M\u00E4r','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
	months_it	= ['Gen','Feb','Mar','Apr','Mag','Giu','Lug','Ago','Set','Ott','Nov','Dic'],
	months_es	= ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
	months_pt	= ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	months_zh	= ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708','\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
	months_zh_cn	= ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708','\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'];
	months_ja	= ['1\u6708','2\u6708','3\u6708','4\u6708','5\u6708','6\u6708','7\u6708','8\u6708','9\u6708','10\u6708','11\u6708','12\u6708'];
	monthArrays['en'] = months_en;
	monthArrays['fr'] = months_fr;
	monthArrays['de'] = months_de;
	monthArrays['it'] = months_it;
	monthArrays['es'] = months_es;
	monthArrays['pt'] = months_pt;
	monthArrays['zh'] = months_zh;
	monthArrays['zh_cn'] = months_zh_cn;
	monthArrays['ja'] = months_ja;
// Populate the 2 arrays used in the Booking & Meta Blocks with the correct (translated) months
// mbMonths	- Meta Block
// months	- Booking Block
mbMonths = months = monthArrays[locale];

	var isCookieEnabled="false";

	iDevice = (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Android/i)) ? true : false;
$(window).load(function(){
if (iDevice) { $('.calWrap').hide(); alert("We've detected you're on a mobile touch browser, please use two fingers to scroll the drop-down options."); }
});


var ajax_load_leisure="<img class='loading' src='"+relPath+"/images/ajaxloader/ajax-loader.gif' alt='loading...' />";


// Append the locale to the body
$('body').removeClass('en').addClass(locale);

// Get today's date!
today = new Date();

// This always spits out a 2 character string
// If the day is under 10, it just adds a zero to the front of the string (eg: 03 or 16)
leadingZeroDay = ('0' + today.getDate()).slice(-2);

$(document).ready(function(){

	var agt=navigator.userAgent.toLowerCase();
    var appVer = navigator.appVersion.toLowerCase();

	var iePos  = appVer.indexOf('msie');
	var is_minor,is_major;
	if (iePos !=-1) {
		is_minor = parseFloat(appVer.substring(iePos+5,appVer.indexOf(';',iePos)));
		is_major = parseInt(is_minor);
		if(is_major<=8){
			alert(propsArray['ieVersion_error']);
		}
	}

	// A neat little trick to open external links in new tabs
	// Just add rel="external" to an <a>
	$('a[rel|=external]').click(function(){
		window.open(this.href);
		return false
	});

// Close the modals when you click the body
	$('body').click(function(){closePopups()});
	if($.trim($("#sessionCountry").val())==""){
		$("#sessionCountry").val($("#selectedCountryDiv").html());
	}
	$('#selLanguage').val($('.current').attr('rel'));
	var selectedCnt = $('#selectedCountryDiv').html();
	var langSelected = $('#selectedLocaleDiv').text().toUpperCase();
	if(langSelected != "EN"){
		$('body').removeClass('en');
		$('body').addClass(langSelected.toLowerCase());
	}
	//need to check if above code is top-banner specific
	if(!checkCookie()){
		//$('#currentLoc').html($('#selectedCountryDiv'));
		$('#dropLang').val($('#selectedLocaleDiv').text().toUpperCase());
		}
		else{

			//clearing cookie to begin new set for the data into cookie
			setCookie('rememberLocale','',-1,'/');
			setCookie('rememberLang','',-1,'/');
			//setting new cookie
			setCookie('rememberLocale',selectedCnt,11,'/');
			setCookie('rememberLang',langSelected,11,'/');

			//currentLocation = getCookie('rememberLocale');
			currentLanguage = getCookie('rememberLang');
			//$('#currentLoc').html(currentLocation);
			$('#dropLang').val(currentLanguage);
			//setting new cookie
	 		//setCookie('rememberLocale',currentLocation,11,'/');
			setCookie('rememberLang',currentLanguage,11,'/');

			document.getElementById('rememberDiv').style.display='none';
			$('#savedID').css('display','block');
			document.getElementById('forgetDiv').style.display='block';
			//below code is to handle if the homepage URL is directly entered, which is rare
			$('#chkRemember').attr('checked','checked');
			$('.remember span').removeClass("un");

		}

	if($('#isCookieEnabledDiv').html()=="true"){
		document.getElementById('rememberDiv').style.display='none';
		$('#savedID').css('display','block');
	document.getElementById('forgetDiv').style.display='block';
		$('#chkRemember').attr('checked','checked');
		$('.remember span').removeClass("un");
	}else{
	$('#savedID').css('display','none');
	}

	if($('#selcountry').length > 0){
		conLan = "/"+document.getElementById('selcountry').innerHTML.toLowerCase()+"/"+document.getElementById('selectedLanguage').innerHTML.toLowerCase();
	}
	/*if(locale == "zh" || locale == "zh_cn"){
		$.datepicker.setDefaults($.datepicker.regional['en']);
		}else{
			$.datepicker.setDefaults($.datepicker.regional[locale]);
		}*/
// Load the datepicker's translated defaults from lib/libraries.js
$.datepicker.setDefaults($.datepicker.regional[locale]);
	// JS Trick to keep Search Engine crawlers out of the Snoopy page
	// The Snoopy page's link can be found in the very bottom left corner of the home page
	// It looks like this: § - click it for lots of information!

	$('#snoopy').click(function(){viewSnoopy()});

	var mobileUser = $("#mobileUser").val();
	if(mobileUser == "true"){
		if(confirm(document.getElementById('MsgMU').innerHTML)){
			document.forms[0].action=relPath+'/splashPageAction!mobileRedirect.action';
			document.forms[0].submit();
		}
	}
	populateMegaMenu(locale);
	loadHomeLangXML();
	// To display selected language on change - by BA
		$('.regionLang ul a').each(function(){

			var lang = $('#selectedLocaleDiv').text().toLowerCase();
			var length = $(this).attr('href').length;

				noHash = $(this).attr('href').substr(4,(length-16));
				$(this).parents('li').removeClass('selected');
			if(noHash==lang){
				$(this).parents('li').addClass('selected');
				$('#dropLangNav p').html($(this).html());
				document.forms[0].selLanguage.value=lang.toUpperCase();

			}
		});

	// Instantiate the Meta Block & List of Locations' tabs
	$('#mb, #LoL').tabs({
		fx: {
			opacity:'toggle',
			duration:'fast'
			},
		select: function(){
			// This fixes a bug:
			// => The drop downs stay open, even when browsing to a different tab
			$('#mb .linkselectLink').blur();

			// This fixes a bug where the adspace images sometimes don't show
			$('.adSpace').hide().show();
		}
	});

mb_popMonths();
home_comboExclude=['#hotelDestination','#pickupLoc','#dropoffLoc','#carCountry'];
if (!iDevice) {
	home_linkselectExclude=['#hotelAdults','#hotelChildren','#hotelInfants','#checkinDay','#checkinMonthYear','#pickDay','#pickMonthYear'];
	home_styleSelects(home_comboExclude,home_linkselectExclude);
}else{
	home_linkselectExclude=['#hotelAdults','#hotelChildren','#hotelInfants','#checkinDay','#checkinMonthYear','#checkoutDay','#checkoutMonthYear','#pickDay','#pickMonthYear','#dropoffDay','#dropoffMonthYear','#pickUpTime','#dropOffTime'];
	home_styleSelects(home_comboExclude,home_linkselectExclude);
}

if(!iDevice){

$('#hotelAdults').linkselect({change:function(li,value){updateGuests('adults',value)}});
$('#hotelChildren').linkselect({change:function(li,value){updateGuests('children',value)}});
$('#hotelInfants').linkselect({change:function(li,value){updateGuests('infants',value)}});
$('#checkinDay').linkselect({change:function(li,value){updateCheckOutDay(value)}});
$('#checkinMonthYear').linkselect({change:function(li,value){updateCheckoutMonthYear(value)}});
$('#pickDay').linkselect({change:function(li,value){updateDropOffDay(value)}});
	//calling callSetUpdatedDates() to update the hidden variables for carHireDep/DestMonthYear
	$('#pickMonthYear').linkselect({change:function(li,value){updateDropOffMonthYear(value),callSetUpdatedDates()}});

updateGuests();
}

   var dropOffDayNew = parseInt(leadingZeroDay) + 2;
   var dropOffDayVal = '0' + dropOffDayNew;
   if(dropOffDayVal.length>2) {
		dropOffDayVal = dropOffDayVal.substring(1);
   }

!iDevice &&
	$('#checkinDay').linkselect('val',leadingZeroDay);

!iDevice &&
	$('#checkoutDay').linkselect('val',leadingZeroDay);
!iDevice &&
	$('#pickDay').linkselect('val',leadingZeroDay);
!iDevice &&
	$('#dropoffDay').linkselect('val',dropOffDayVal);
	$('#mb .calendar').datepicker({
		firstDay:1,
		minDate:0,
		maxDate:'+1y',
	showOtherMonths:true,
	selectOtherMonths:true,
	onSelect:function(date){
		updateDates($(this),date);
		$(this).fadeOut()
	}
});
	$('#mb .ui-datepicker').click(function(e){
		e.stopPropagation()
		});

	$('#mb .calendar').click(function(e){
		e.stopPropagation();
		$(this).fadeOut()
		});

	$('#mb .calWrap').click(function(e){
		e.stopPropagation();
		closePopups();
		date=new Date();
		box=$('#'+$(this).parents('.blueBox').attr('id'));
		dayLinkSelect=box.find('.inputDay').children('a').attr('href');
		monthYearLinkSelect=box.find('.inputMonthYear').children('a').attr('href');
		if($.browser.msie){
			d=dayLinkSelect.split('#');
			m=monthYearLinkSelect.split('#');
			dayLinkSelect='#'+d[1];
			monthYearLinkSelect='#'+m[1]
		}
		update=$(dayLinkSelect).linkselect('val').toString()+'-'+$(monthYearLinkSelect).linkselect('val').toString();
		try {
		calDate = $.datepicker.parseDate('dd-M-yy', update);
		} catch (e){
			//alert(propsArray['invalidCalDate']);	  -- using an existing val-msg
			alert(propsArray['bookingBlock_14']);

		return false
		}
		$(this).children('.calendar').datepicker('setDate',calDate);
		$(this).children('.calendar:not(.disabled)').fadeIn()
		});

	$('#LoL *').click(function(e){
		e.stopPropagation();
		$('#bb .linkselectLink, #mb .linkselectLink').blur()
	});

$('#LoL .tabs').addClass('js');
$('.regionCountry').removeClass('nojs');

	$('#mb .linkselectLink').click(function(e){
		e.stopPropagation();
		if (!$(this).hasClass('.linkselectLinkOpen')){
			$($(this).attr('href')).linkselect('open')
		}
	});
	$('#LoL article li a').click(function(e){
		e.stopPropagation();
$('.dropLoc span').html($(this).html());
	});

	$('.remember label input').click(function(){
		isChecked = $(this).is(':checked');
$(this).parents('span').toggleClass('un',!isChecked);
	});
	popEssInfo(locale,country);

  //$('#midMenu .last').html('<a class="button essentialInfo"><span>Essential Info</span></a>');
	$('.essentialInfo').live('click',function(e){
		e.stopPropagation();
		l=20+$(this).offset().left+($(this).width()*0.5)-($('.essInfoWrap').width()*0.5);
		t=$(this).offset().top+26;
$('.essInfoWrap').slideToggle(100).offset({top:t,left:l});
	});
	$('.dropLoc').click(function(e){
		e.stopPropagation();
		!$.browser.msie &&
		$('#LoL').slideToggle(100);
		$.browser.msie &&
		$('#LoL').toggle();
		$(this).toggleClass('selected');
		$('.essInfoWrap').hide();
		$('.linkselectLink').blur()
	});
	$('#mb .linkselectLink').click(function(e){
		e.stopPropagation()
	});

$('#hotels .button.submit').click(function(){
		var valMsg = '';
	getDestinationCityDesc();
	$('#hotelDestination').siblings('input').removeClass('invalid');

	error=false;
	if(!$('#hotelDestination').val().length) {
		$('#hotelDestination').siblings('input').addClass('invalid');
		valMsg=getLocalizedMessage("Please provide the hotel destination.","hotelDest_1");
			error = true
		}
	if(validateDates('hotels')){
		valMsg += getLocalizedMessage("Please choose valid dates","bookingBlock_14");
		error=true;
	}
		if (error){
			alert(valMsg);
			return false
		}
	chkInDate = new Date($('#checkinDay').val()+' '+$('#checkinMonthYear').val());
	chkOutDate = new Date($('#checkoutDay').val()+' '+$('#checkoutMonthYear').val());
	//var numofDay=Math.abs(Math.round(((chkOutDate.getTime()) - (chkInDate.getTime()))/oneDay));
	var chkInMonthIndex=getAllMonthIndex($("#checkinMonthYear").val());
	var chkOutMonthIndex=getAllMonthIndex($("#checkoutMonthYear").val());
	var chkInYear=getAllyearValue($("#checkinMonthYear").val());
	var chkOutYear=getAllyearValue($("#checkoutMonthYear").val());
// for calculating No:of nights - not changing above chkOutDate & chkInDate since they're used elsewhere ; need to check later
	var monthYear = $('#checkinMonthYear').val().split('-');
	var checkinDate = new Date($("#checkinDay").val() + ' ' + monthYear[0] + ' 20' + monthYear[1]);
		monthYear= $('#checkoutMonthYear').val().split('-');
	var checkoutDate = new Date($('#checkoutDay').val() + ' ' + monthYear[0] + ' 20' + monthYear[1]);
	var numberOfNights = Math.ceil((checkoutDate.getTime()-checkinDate.getTime())/(1000*60*60*24));

	var url = "http://www.flysaa.com/saacui/HotelSearchExternal.do?isNewSearchFlow=true&pos=SAACUI-ZAF&lang=en&hotelSearchDestinationType=IAN_HOTELS&hotelSearchDestination=" + $('#txtHotelDes_loc').val() + "&hotelSearchDestinationCode=" + $('#hotelDestination').val() + "&numberOfNights=" + numberOfNights + "&hotelSearchNumberOfRooms=" + $('#numberoffrooms').val() + "&guestTypeBeans[1].guestTypes[0].type=10.AQC&guestTypeBeans[1].guestTypes[0].amount=" + $('#hotelAdults').val() + "&guestTypeBeans[1].guestTypes[1].type=8.AQC&guestTypeBeans[1].guestTypes[1].amount=" + $('#hotelChildren').val() + "&guestTypeBeans[1].guestTypes[2].type=7.AQC&guestTypeBeans[1].guestTypes[2].amount=" + $('#hotelInfants').val() + "&checkinDate=" + $('#checkinDay').val() + "/" + chkInMonthIndex + "/" + chkInYear + "&checkoutDate=" + $('#checkoutDay').val() + "/" + chkOutMonthIndex + "/" + chkOutYear; window.open(url,"_self"); });$('#hotels .button.reset').click(function(){ $('#hotelDestination').siblings('input').removeClass('invalid'); $("#hotelDestination").val(''); $("#hotelDestination").siblings('input').val($("#hotelHeaderTxtId").html());today = new Date();$('#checkinDay').linkselect('val',today.getDate()); $('#checkoutDay').linkselect('val',today.getDate()); $("#hotelAdults,#hotelChildren,#hotelInfants,#numberoffrooms,#checkinMonthYear,#checkoutMonthYear").linkselect("val", '-1');});



$('#pickupLoc').siblings('input').keyup(function(event){
	 var value = $(this).val();


	loadCarDepartCities(value);



});

$('#dropoffLoc').siblings('input').keyup(function(event){

	 var value = $(this).val();

	 $('#dropoffLoc').html('');

	loadCarDestCities(value);
});

$('#hotelDestination').siblings('input').keyup(function(event){

	 var value = $(this).val();

	 $('#hotelDestination').html('');

	 loadHotelDestination(value);
});
$('#pickupLoc').siblings('input').autocomplete({
	select:function(event,ui){
	select=$('#pickupLoc').siblings('select');
	if ( !ui.item ) {
		var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ), valid = false;
		select.children( "option" ).each(function() {
			if ( $( this ).text().match( matcher ) ) {
				this.selected = valid = true;
				return false; }
			});
		if ( !valid ) {
					if (Modernizr.input.placeholder){
						$("#pickupLoc").val('');
						$("#pickupLoc").siblings('input').val($("#pickupHeaderId").html())
			} else {
				$("#pickupLoc").val('');
				$("#pickupLoc").siblings('input').val($("#pickupHeaderId").html());
			}
			$(this).data( "autocomplete" ).term = "";
			return false;
			}
		}
	$('#pickupLoc').val(ui.item.option.value);
	$('#pickupLoc').siblings('input') .val($(this).val());
	$('#dropoffLoc').val($('#pickupLoc').val());
	$('#dropoffLoc').siblings('input') .val($(this).val());
	}  });
$('#dropoffLoc').siblings('input').autocomplete({
	select:function(event,ui){
	select=$('#dropoffLoc').siblings('select');
	if(!ui.item){
		var matcher=new RegExp("^"+$.ui.autocomplete.escapeRegex($(this).val())+"$","i"),valid=false;
		select.children("option").each(function(){
			if($(this).text().match(matcher)){
				this.selected=valid=true;
				return false;
			}
		});
		if(!valid){
			if(Modernizr.input.placeholder){
				$(this).val($('#pickupLoc').siblings('input').val());
				select.val($('#pickupLoc').siblings('input').val());
	}else{
		if($('#pickupLoc').val()){
			$(this).val($('#pickupLoc').siblings('input').val());
			select.val($('#pickupLoc').siblings('input').val());
		}else{
			$(this).val('');
			$("#dropoffLoc").siblings('input').val($("#dropoffHeaderId").html());
		}
	}

$(this).data("autocomplete").term="";return false;}}
	$('#dropoffLoc').val(ui.item.option.value);
	$('#dropoffLoc').siblings('input') .val($(this).val());
}});

$('#hotelDestination').siblings('input').autocomplete({
	select:function(event, ui){
	select=$('#hotelDestination').siblings('select');
	if ( !ui.item ) {
		var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
			valid = false;
		select.children( "option" ).each(function() {
			if ( $( this ).text().match( matcher ) ) {
				this.selected = valid = true;
				return false;
			}
		});
		if ( !valid ) {
			// remove invalid value, as it didn't match anything
			if(Modernizr.input.placeholder) {
				$("#hotelDestination").val('');$("#hotelDestination").siblings('input').val($("#hotelHeaderTxtId").html());
			}
			else {
				$("#hotelDestination").val('');$("#hotelDestination").siblings('input').val($("#hotelHeaderTxtId").html());
			}
			$(this).data( "autocomplete" ).term = "";
			return false;
		}
	}
	$('#hotelDestination').val(ui.item.option.value);
	$('#hotelDestination').siblings('input') .val($(this).val());
}
});

function loadAdserverBanners(){

    $("#banners").load(relPath+"/cms/" + country + "/leisure/AdServerImage.html", function( response, status, xhr ) {;
    	  if ( status == "error" ) {
    		  $("#banners").load(relPath+"/cms/ZA/leisure/AdServerImage.html");
    	  }
    });

}

loadAdserverBanners();
loadAdSpaceImage();
loadGlobalImage();
loadHomeLinks();
//loadCarCountriesXML(locale);

/*$.ajax({url:"xml/locales_"+locale+".xml",dataType:"xml",asunc:'true',success:function(xml){
	$('#carCountry').html('');country=$("#countryResHeaderId").html();code='';
	$('#carCountry').append('<option value="' + code + '">' + country + '</option>');
	$(xml).find('country').each(function(){
		country=$(this).text();code=$(this).attr('value');
	$('#carCountry').append('<option value="'+code+'">'+country+'</option>');});}});*/
//calling the Global Emergency Notice

});

/**
*Load OpenCMS image for adspace content using Jquery
*
*/
function loadAdSpaceImage(){
$("#adSpaceImage").load(relPath+"/cms/ZA/leisure/AdSpaceImage.html");
}

function loadGlobalImage(){
$("#specials").load(relPath+"/cms/ZA/leisure/AdGlobalHighlights.html");
}

function loadHomeLinks(){
	$("#homeLinks").load(relPath+"/cms/US/leisure/flysaa_homeLinks.html");
}

function validateDates(id){

	var flag = false;
	var dateFormt = 'dd-M y';


	//for hotels
// commented the below code to avoide script error - USVD 2984612
/*	if($('#checkinMonthYear').val()!=" "){
		var departDateArray = $('#checkinMonthYear').val().split('-');
		var finalDepartMonth = departDateArray[1].substring(departDateArray[1].length, 2);
		chkInDate=$('#checkinDay').val()+"-"+departDateArray[0]+" " +finalDepartMonth;
		}
		if($('#checkoutMonthYear').val()!=" "){
		var returnDateArray = $('#checkoutMonthYear').val().split('-');
		var finalReturnMonth = returnDateArray[1].substring(returnDateArray[1].length, 2);
		chkOutDate=$('#checkoutDay').val()+"-"+returnDateArray[0]+" " +finalReturnMonth;
		}*/

	//for cars

		if($('#pickMonthYear').val()!=" "){
			var departDateArray = $('#pickMonthYear').val().split('-');
			var finalDepartMonth = departDateArray[1].substring(departDateArray[1].length, 2);
			pickupDate=$('#pickDay').val()+"-"+departDateArray[0]+" " +finalDepartMonth;
			}
			if($('#dropoffMonthYear').val()!=" "){
			var returnDateArray = $('#dropoffMonthYear').val().split('-');
			var finalReturnMonth = returnDateArray[1].substring(returnDateArray[1].length, 2);
			dropoffDate=$('#dropoffDay').val()+"-"+returnDateArray[0]+" " +finalReturnMonth;
			}



	if(id =='cars'){
	if(!isValidDate(pickupDate,dateFormt)){
		flag=true;
	}
	if(!isValidDate(dropoffDate,dateFormt)){
		flag=true;
	}

	if(compareWithCurrentDate(pickupDate,dateFormt)<0){
		flag=true;
	}

		if(compareWithCurrentDate(dropoffDate,dateFormt)<0 || compareDateStrings(pickupDate,dropoffDate,dateFormt)<0){
			flag=true;
		}

	}
	else if(id == 'hotels'){

	if(!isValidDate(chkInDate,dateFormt)){
		flag=true;
	}
	if(!isValidDate(chkOutDate,dateFormt)){
		flag=true;
	}

	if(compareWithCurrentDate(chkInDate,dateFormt)<0){
		flag=true;
	}

		if(compareWithCurrentDate(chkOutDate,dateFormt)<0 || compareDateStrings(chkInDate,chkOutDate,dateFormt)<0){
			flag=true;
		}
	}
	return flag;
}


function timeValidate(){
	//for cars

	if($('#pickMonthYear').val()!=" "){
		var departDateArray = $('#pickMonthYear').val().split('-');
		var finalDepartMonth = departDateArray[1].substring(departDateArray[1].length, 2);
		pickupDate=$('#pickDay').val()+"-"+departDateArray[0]+" " +finalDepartMonth;
		}
		if($('#dropoffMonthYear').val()!=" "){
		var returnDateArray = $('#dropoffMonthYear').val().split('-');
		var finalReturnMonth = returnDateArray[1].substring(returnDateArray[1].length, 2);
		dropoffDate=$('#dropoffDay').val()+"-"+returnDateArray[0]+" " +finalReturnMonth;
		}
	if((compareDateStrings(pickupDate,dropoffDate,'dd-M y')==0) && (document.getElementById('pickUpTime').value > document.getElementById('dropOffTime').value)){
		return true;
		}
	return false;
}

function loadHotelDestination(value){

	var crossSellCarHire={};
	crossSellCarHire["accommodationDest"] = value;
	$.ajaxSetup ({
		cache: false
	});

	var url = relPath+conLan+"/crossSellAction!getAccomodationDetails.action";
	//$.get(url,argument,callbackFunction,returnType) is the Syntax.
	if(value.length > 2){

	$.get(url,crossSellCarHire,getHotelDestinationXmlValues,'xml');

}

}

function getHotelDestinationXmlValues(XMLResponse,status)
{

	if(status=="success") {

		$('#hotelDestination').html('');
		country=$("#destinationHeaderId").html();
		code='';
		$('#hotelDestination').append('<option value="' + code + '">' + country + '</option>');

		$(XMLResponse).find('airport').each(function(){
			 var $xml = $(this);
			 country = 	$xml.find( "cityDesc" ).text();
			 code =  $xml.find( "cityCode" ).text();
			$('#hotelDestination').append('<option value="' + code + '">' + country + '</option>'); });




	}else {
		 alert("No matching region found.");
	}
}

function loadCarDepartCities(value){

	var crossSellCarHire={};
	crossSellCarHire["carHireOrg"] = value;
	$.ajaxSetup ({
		cache: false
	});

	var url = relPath+conLan+"/crossSellAction!getCarHirePickupDetails.action";
	//$.get(url,argument,callbackFunction,returnType) is the Syntax.
	if(value.length > 2){

	$.get(url,crossSellCarHire,loadCarDepartCitiesXml,'xml');

}

}
function loadCarDepartCitiesXml(XMLResponse,status)
{

	if(status=="success") {


		$('#pickupLoc').html('');
		country=$("#pickupHeaderId").html();
		code=''; $('#pickupLoc').append('<option value="' + code + '">' + country + '</option>');

		$(XMLResponse).find('airport').each(function(){
			 var $xml = $(this);
			 country = 	$xml.find( "cityDesc" ).text();
			 code =  $xml.find( "cityCode" ).text();
		$('#pickupLoc').append('<option value="' + code + '">' + country + '</option>'); });


	}else {
		 alert("No matching region found.");
	}
}
function loadCarDestCities(value){

	var crossSellCarHire={};
	crossSellCarHire["carHireOrg"] = value;
	$.ajaxSetup ({
		cache: false
	});

	var url = relPath+conLan+"/crossSellAction!getCarHirePickupDetails.action";
	//$.get(url,argument,callbackFunction,returnType) is the Syntax.
	if(value.length > 2){

	$.get(url,crossSellCarHire,loadCarDestCitiesXml,'xml');

}

}
function loadCarDestCitiesXml(XMLResponse,status)
{

	if(status=="success") {


		$('#dropoffLoc').html('');
		country=$("#dropoffHeaderId").html();
		code='';
		$('#dropoffLoc').append('<option value="' + code + '">' + country + '</option>');
		$(XMLResponse).find('airport').each(function(){
			 var $xml = $(this);
			 country = 	$xml.find( "cityDesc" ).text();
			 code =  $xml.find( "cityCode" ).text();
		$('#dropoffLoc').append('<option value="' + code + '">' + country + '</option>'); });
		 var selectedDropPonit = $('#dropoffLoc').val();
			if(code==selectedDropPonit){
				$('#txtdrop_loc').val($(this).text());}


	}else {
		 alert("No matching region found.");
	}
}

function loadCarCountriesXML(locale){

	$.ajaxSetup ({
		cache: false
	});
	var localeArray={};
	localeArray["localeSeltd"] = locale;
	var url = relPath+"/splashPageAction!loadLocaleXmlFile.action";

	$.get(url,localeArray,getCarCountriesXmlValues,'xml');

}
function getCarCountriesXmlValues(XMLResponse,status)
{

	if(status=="success") {

		$('#carCountry').html('');
		country=$("#countryResHeaderId").html();
		code='';
		$('#carCountry').append('<option value="' + code + '">' + country + '</option>');
		$(XMLResponse).find('country').each(function(){
			country=$(this).text();
			code=$(this).attr('value');
		$('#carCountry').append('<option value="'+code+'">'+country+'</option>');});

	}else {
		 alert("No matching region found.");
	}
}

$('#cars .button.submit').click(function(){ var valMsg="";
	getCityDesc();
    $('#pickupLoc').siblings('input').removeClass('invalid');
    $('#dropoffLoc').siblings('input').removeClass('invalid');
    $('#carCountry').siblings('input').removeClass('invalid');
    $('#driverAge').removeClass('invalid');
    var pickupMonthndex=getAllMonthIndex($("#carHireDepMonthYear").val());
    var pickUYear=getAllyearValue($("#carHireDepMonthYear").val());
    var dropofMonthIndex=getAllMonthIndex($("#carHireDestMonthYear").val());
    var dropoffear=getAllyearValue($("#carHireDestMonthYear").val());

    error=false;
    if(validateDates('cars')){
		valMsg += getLocalizedMessage("Please choose valid dates","bookingBlock_14");
		error=true;
	}
    if(timeValidate()){
    	valMsg += getLocalizedMessage("Please choose valid time","bookingBlock_15");
    	error=true;
	}
    /*if(!$('#driverAge').val().length||isNaN($('#driverAge').val())||$('#driverAge').val()<18) {
    	$('#driverAge').addClass('invalid');
    	valMsg=getLocalizedMessage("Please provide a valid driver age.","cars_1");
    	error=true; }*/
    if(!$('#pickupLoc').val().length) { $('#pickupLoc').siblings('input').addClass('invalid'); valMsg+=getLocalizedMessage("Please provide a pick up location.","cars_2");error=true; }
   // if(!$('#dropoffLoc').val().length) { $('#dropoffLoc').siblings('input').addClass('invalid'); valMsg+=getLocalizedMessage("Please provide a drop-off location.","cars_3");error=true; }
   if(!$('#carCountry').val().length) { $('#carCountry').siblings('input').addClass('invalid');
   valMsg+=getLocalizedMessage("Please provide the country.","cars_4");error=true; }
   if(error){ alert(valMsg);return false;}
   getPosConnectionID();

 });

$('#cars .button.reset').click(function(){ $("#pickupLoc").val('');$("#pickupLoc").siblings('input').val($("#pickupHeaderId").html());$("#dropoffLoc").val('');	$("#dropoffLoc").siblings('input').val($("#dropoffHeaderId").html());$("#carCountry").val(''); $("#carCountry").siblings('input').val($("#countryResHeaderId").html());	today = new Date();$('#driverAge').val('').removeClass('invalid');				 $('#pickupLoc').siblings('input').removeClass('invalid');		 $('#dropoffLoc').siblings('input').removeClass('invalid'); $('#carCountry').siblings('input').removeClass('invalid');	 $('#pickDay').linkselect('val',today.getDate()); $('#dropoffDay').linkselect('val',today.getDate()); $("#pickMonthYear,#dropoffMonthYear,#checkoutMonthYear,#dropOffTime,#pickUpTime").linkselect("val", '-1'); });
$(window).load(function(){$('#mb .loadingIndicator,#voyager .loadingIndicator').fadeOut();!Modernizr.input.placeholder&&$('input').each(function(){a=$(this).attr('title');if (!(a == undefined) && (a.length > 0)) {$(this).css('color','darkGrey');$(this).val(a);$(this).focus(function(){$(this).css('color','#444');if($(this).val().length==0||$(this).val()==$(this).attr('title')){$(this).val('')}});$(this).blur(function(){if($(this).val().length==0){$(this).css('color','darkGrey');$(this).val($(this).attr('title'))}});}});});$(document).keyup(function(e){if(e.keyCode==27){closePopups()}});$(document.body).click(function(){closePopups();});function closePopups(){$('.calendar').fadeOut();$('.essInfoWrap').slideUp(100);$('#LoL').slideUp(100);$('.dropLoc').removeClass('selected');$('#dropLang').blur();$('.linkselectLink').each(function(){$(this).blur();});}


  function  getPosConnectionID()  {
	  var countrySel=$.trim($('#user_location').text().toUpperCase());
	  var product =$("#selectedProductIs").val();
	  $.ajaxSetup ({
			cache: false
		});
	  var posConfigArray={};
	  posConfigArray["countrySel"]= countrySel;
	  posConfigArray["productSel"]= product;
	  var url = relPath+"/splashPageAction!getPosConnectionID.action";
      $.get(url,posConfigArray,returnPOSConnID,'text');

  }

  function returnPOSConnID(posConnId, status){
	     if(status=="success"){
		  if(posConnId != null){
			  carRentalPosConnId =  posConnId;
		   }
	    }else{
		      carRentalPosConnId =  null;
	     }
	     var pickupMonthndex=getAllMonthIndex($("#carHireDepMonthYear").val());
	     var pickUYear=getAllyearValue($("#carHireDepMonthYear").val());
	     var dropofMonthIndex=getAllMonthIndex($("#carHireDestMonthYear").val());
	     var dropoffear=getAllyearValue($("#carHireDestMonthYear").val());

	    if(carRentalPosConnId == null){
	    	carRentalPosConnId ="ZAF";
	    }

	     /*if ( $( '#dropoffLoc').val().length>0 &&($('#pickupLoc').val() != $( '#dropoffLoc').val())) {
	   	  var url = "http://www.flysaa.com/saacui/CarSearchExternal.do?isNewSearchFlow=true&pos=SAACUI-"+carRentalPosConnId+"&lang=en&pickUpLocationName=" + $('#txtpick_loc').val() + "&pickUpLocationCode=" + $('#pickupLoc').val() + "&pickUpDay=" + $('#pickDay').val() + "&pickUpMonth=" + pickupMonthndex + "&pickUpYear=" + pickUYear + "&pickUpTime=" + $('#pickUpTime').val() + "&dropOffDay=" + $('#dropoffDay').val() + "&dropOffMonth=" + dropofMonthIndex + "&dropOffYear=" + dropoffear + "&dropOffTime=" + $('#dropOffTime').val() + "&residentCountry=" + $('#carCountry').val() + "&returningDifferent=true"	+ "&dropOffLocationName=" + $('#txtdrop_loc').val() + "&dropOffLocationCode=" + $('#dropoffLoc').val() + "&driverAge=" + $('#driverAge').val(); }
	      else {
	      var url = "http://www.flysaa.com/saacui/CarSearchExternal.do?isNewSearchFlow=true&pos=SAACUI-"+carRentalPosConnId+"&lang=en&pickUpLocationName=" + $('#txtpick_loc').val() + "&pickUpLocationCode=" + $('#pickupLoc').val() + "&pickUpDay=" + $('#pickDay').val() + "&pickUpMonth=" + pickupMonthndex + "&pickUpYear=" + pickUYear + "&pickUpTime=" + $('#pickUpTime').val() + "&dropOffDay=" + $('#dropoffDay').val() + "&dropOffMonth=" + dropofMonthIndex + "&dropOffYear=" + dropoffear + "&dropOffTime=" + $('#dropOffTime').val() + "&residentCountry=" + $('#carCountry').val() + "&returningDifferent=false" + "&driverAge=" + $('#driverAge').val(); }
	      window.open(url,"_self");*/

	    var BDATE = "";
	    var EDATE = "";
	    var dropLocation = "";
	    var BMONTH = pickupMonthndex;
	    var EMONTH = dropofMonthIndex;
	    if(BMONTH < 10)
	    	BMONTH = "0"+BMONTH;
	    if(EMONTH < 10)
	    	EMONTH = "0"+EMONTH;
	    BDATE = pickUYear + BMONTH + $('#pickDay').val() + $('#pickUpTime').val();
	    EDATE = dropoffear + EMONTH + $('#dropoffDay').val() + $('#dropOffTime').val();
	    //alert("BDATE->" + BDATE + "EDATE->" + EDATE);
	    if($('#dropoffLoc').val().length>0 &&($('#pickupLoc').val() != $('#dropoffLoc').val()))
	    	dropLocation = $('#dropoffLoc').val();
	    else
	    	dropLocation = $('#pickupLoc').val();
	  //var url = "https://book.flysaa.com/plnext/flysaa/Override.action?UI_EMBEDDED_TRANSACTION=CarShopperStart&SITE=BAUYBAUY&LANGUAGE=GB&TRIP_FLOW=YES&B_DATE=" + BDATE +"&E_DATE=" + EDATE +"&B_LOCATION=" + $('#pickupLoc').val() + "&E_LOCATION=" + dropLocation + "&CLASS=*&TYPE=*&TRANSMISSION=*&AIR_CONDITIONING=*&EXTERNAL_ID=UICSS:PRODUCTION;PRODUCT:CAR;SA_SOURCE:CAR;COUNTRY:za;LANGUAGE:en;;;;;;;;&SO_SITE_IS_MAIL_CANCEL=TRUE&SO_SITE_IS_MAIL_CONF=TRUE&USE_CAR_SHOPPER=TRUE&SO_SITE_APIV2_SERVER_USER_ID=GUEST&SO_SITE_APIV2_SERVER=194.156.170.78&SO_SITE_APIV2_SERVER_PWD=TAZ&SO_SITE_CORPORATE_ID=OCGPDT&SO_SITE_SI_SAP=1ASIXJCP&SO_SITE_SI_SERVER_PORT=18033&SO_SITE_SI_SERVER_IP=193.23.185.67&SO_SITE_SI_USER=UNSET&SO_SITE_SI_PASSWORD=UNSET&SO_SITE_SI_1AXML_FROM=SEP_JCP&SO_SITE_FQ_INTERFACE_ACTIVE=FALSE&SO_SITE&SO_SITE_EXTERNAL_LOGIN=FALSE&SO_SITE_OFFICE_ID=JNBSA08AA&SO_SITE_HOST_TRACE_ACTIVE=TRUE&SO_SITE_FP_TRACES_ON=FALSE&AUTO_SEARCH=TRUE#results";
	    var url = "https://book.flysaa.com/plnext/flysaa/Override.action?UI_EMBEDDED_TRANSACTION=CarShopperStart&SITE=BAUYBAUY&LANGUAGE=GB&TRIP_FLOW=YES&B_DATE=" + BDATE +"&E_DATE=" + EDATE +"&B_LOCATION=" + $('#pickupLoc').val() + "&E_LOCATION=" + dropLocation + "&CLASS=*&TYPE=*&TRANSMISSION=*&AIR_CONDITIONING=*&EXTERNAL_ID=UICSS:PRODUCTION;PRODUCT:CAR;SA_SOURCE:CAR;COUNTRY:za;LANGUAGE:en;;;;;;;;&SO_SITE_IS_MAIL_CANCEL=TRUE&SO_SITE_IS_MAIL_CONF=TRUE&USE_CAR_SHOPPER=TRUE&SO_SITE_EXTERNAL_LOGIN=FALSE&SO_SITE_OFFICE_ID=JNBSA08AA&AUTO_SEARCH=TRUE#results";
	    window.open(url,"_self");
 }

  function oc(a){var o={};for(var i=0;i<a.length;i++){o[a[i]]=''}return o}

function home_styleSelects(x,y){
	$('#mb select').each(function(){
		id='#'+$(this).attr('id');
		if(!(id in oc(y))){
			if($.browser.msie&&$.browser.version<7){
				$(this).linkselect();
         }else{
				if(id in oc(x)){
					$(this).combobox();
}else{
					$(this).linkselect();
				}
			}
		}
	});
}

//for specials block
var conLan;
if($('#selcountry').length > 0){
	conLan = "/"+document.getElementById('selcountry').innerHTML.toLowerCase()+"/"+document.getElementById('selectedLanguage').innerHTML.toLowerCase();
}
var specialsUrl= conLan+"/specials/saa-specials.html";
function popEssInfo(locale,country){var essential=new String();if(country=='default'){essential=relPath+"/cms/ZA/routeSpecific/IncludeCMS_Content.jsp?template=ESSENTIALINFO&locale=en&country=ZA";tryAgain=false;}else{essential=relPath+"/cms/ZA/routeSpecific/IncludeCMS_Content.jsp?template=ESSENTIALINFO&locale="+locale+"&country="+country;tryAgain=true;}
$.ajax({url:essential,success:function(data){loadEssInfo(data)},error:function(data){errorEssInfo(data,tryAgain)}});
}
function loadEssInfo(data){$('.essInfoWrap').html('').prepend('<ul class="essInfo">');$('.essInfo').prepend('<li class="head"></li>');$(data).find('a').each(function(){val=$(this).attr('href')
name=$(this).text();$('.essInfo').append('<li><a href="'+val+'">'+name+'</a></li>');});}
function errorEssInfo(data,tryAgain){if(tryAgain){setTimeout("popEssInfo('','default')",1000);}else{$('.essInfoWrap').html('');$('#midMenu nav li.last').html('<a href="" class="button blue"><span>Essential Info</span></a>');}}

// This function is used in the Booking Block also, concatenation just makes the file smaller. Yay!
function updateDates(c,d){
//var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
	// Updates the link select drop downs with the calendar selected dates

	// c = object
	// d = date "mm/dd/yy
	//months and day get interchanged, not sure how this happens, has the dateformat: mm/dd got to do anything with this ?
	//WL: This has been fixed. It was a formatting error in libraries.js

	var newMonthYear=new String();
date=new Date(d);
box=$('#'+c.parents('.blueBox').attr('id'));
day=box.find('.inputDay').children('a').attr('href');
monthYear=box.find('.inputMonthYear').children('a').attr('href');

newMonthYear='"'+months[date.getMonth()]+'-'+date.getFullYear()+'"';

// Splitting the href string fixes a bug in IE
d=day.split('#');
m=monthYear.split('#');

day='#'+d[1];
monthYear='#'+m[1];
//dd = (date.getDate() < 10) ? '0' + date.getDate() : date.getDate();
	dd=('0'+date.getDate()).slice(-2);

$(day).linkselect('val',dd);
//$(day).linkselect('val',date.getDate());
$(monthYear).linkselect('val',newMonthYear);
}

function updateGuests(element,value){

	if(iDevice) return false;

	maxPax=9;var total=0;adults=parseInt($('#hotelAdults').linkselect('val'));children=parseInt($('#hotelChildren').linkselect('val'));infants=parseInt($('#hotelInfants').linkselect('val'));switch(element){case'adults':adults=parseInt(value);break;case'children':children=parseInt(value);break;case'infants':infants=parseInt(value);break;default:break;}
passengers=[parseInt(adults),parseInt(children),parseInt(infants)];total=passengers[0]+passengers[1]+passengers[2];remainder=maxPax-total;$('#hotelAdults_list li').each(function(i){$(this).removeClass('hidePassenger');selectable=remainder+adults;if(i>selectable)$(this).addClass('hidePassenger');});$('#hotelChildren_list li').each(function(i){$(this).removeClass('hidePassenger');selectable=remainder+children;if(i>selectable)$(this).addClass('hidePassenger');});$('#hotelInfants_list li').each(function(i){$(this).removeClass('hidePassenger');selectable=remainder+infants;if(i>selectable)$(this).addClass('hidePassenger');});}
function mb_popMonths(){
	var dates=[],
	values=[],
	now=new Date(),
	ob=new String();
	// February bug
	now.setDate(28);
  // Loop through 13 months. (eg: December 2011 => December 2012)
	for(i=0;i<13;i++){
	values[i]=mbMonths[now.getMonth()]+'-'+now.getFullYear();
	dates[i]=mbMonths[now.getMonth()]+' '+now.getFullYear();
	ob='<option value="'+values[i]+'">'+dates[i]+'</option>';
	$('#pickMonthYear').append(ob);
	$('#dropoffMonthYear').append(ob);
	$('#checkinMonthYear').append(ob);
	$('#checkoutMonthYear').append(ob);
	now.setMonth(now.getMonth()+1)
	}
	};



function getCityDesc(){

	var selectedPickPoint = $('#pickupLoc').val();
	var selectedDropPonit = $('#dropoffLoc').val();


	loadCarCityDescriptionXML(locale);


}
function loadCarCityDescriptionXML(locale){

	$.ajaxSetup ({
		cache: false
	});
	var inputArray={};
	inputArray["localeSeltd"] = locale;
	inputArray["country"] = country;
	var url = relPath+"/splashPageAction!loadDepartureXmlFile.action";


	$.get(url,inputArray,getCarDescriptionXMLValues,'xml');

}
function getCarDescriptionXMLValues(XMLResponse,status)
{

	if(status=="success") {
		var selectedPickPoint = $('#pickupLoc').val();
		var selectedDropPonit = $('#dropoffLoc').val();
		$(XMLResponse).find('departure').each(function(){
		country = $(this).text();
		code = $(this).attr('code');
		if (code == selectedPickPoint) {
			$('#txtpick_loc').val($(this).text());}
		if(code==selectedDropPonit){
			$('#txtdrop_loc').val($(this).text());} } );
	}else {
		 alert("No matching region found.");
	}
}

function getMonthIndex(monthVal) { switch(monthVal.toUpperCase()) { case 'JAN': return Number(0); case 'FEB': return Number(1); case 'MAR': return Number(2); case 'APR': return Number(3); case 'MAY': return Number(4); case 'JUN': return Number(5); case 'JUL': return Number(6); case 'AUG': return Number(7); case 'SEP': return Number(8); case 'OCT': return Number(9); case 'NOV': return Number(10); case 'DEC': return Number(11); } }
function getDayAndMonthval(day){ var strVal=day; var subStr=strVal.split('#');	 var dayValue='#'+subStr[1];return dayValue; }
function getAllMonthIndex(monthValue){;var valMonthIndex = getMonthIndex(monthValue.split('-')[0])+1;return valMonthIndex;	}
function getAllyearValue(yearValue){var yearVal = yearValue.split('-')[1];	 return yearVal; }
function getDestinationCityDesc(){
	var selectedHotDes = $('#hotelDestination').val();
	loadHotelCityDescrptnXML(locale);


}
function loadHotelCityDescrptnXML(locale){

	$.ajaxSetup ({
		cache: false
	});
	var inputArray={};
	inputArray["localeSeltd"] = locale;
	inputArray["country"] = country;
	var url = relPath+"/splashPageAction!loadDepartureXmlFile.action";
	$.get(url,inputArray,getHotelCityDescrptnXMLValues,'xml');

}
function getHotelCityDescrptnXMLValues(XMLResponse,status)
{

	if(status=="success") {

		$(XMLResponse).find('departure').each(function(){
			code = $(this).attr('code');
		if(code==selectedHotDes){
			$('#txtHotelDes_loc').val($(this).text());
			} } );
	}else {
		 alert("No matching region found.");
	}
}


function isVEM(field) { var eMailPattern=/^(.+)@([^\(\);:,<>]+\.[a-zA-Z]{2,4})/;  if(field.val().length > 0) { var matchResult=field.val().match(eMailPattern); if(matchResult == null) {field.focus(); return false; } return true; } else { return true; } }
var modalButtons = { Ok : 1, Back : 2, Next : 3, Continue : 4 };
function modalShow(id) { var heading = arguments[1]; if (typeof (heading) != 'undefined') { var inner = arguments[2]; var customButton = arguments[3]; var buttons = '<div class="buttons"><a href="javascript:;" class="arrowBlue right" onclick="modalClose(\'' + id + '\')">{BUTTON}</a></div>'; $('#' + id + ' h4').first().text(heading); if (typeof (customButton) != 'undefined') { switch (customButton) { case modalButtons.Ok: customButton = 'Ok'; break; case modalButtons.Back: customButton = 'Back'; break; case modalButtons.Next: customButton = 'Next'; break; default: customButton = 'Continue'; break; } buttons = buttons.replace('{BUTTON}', customButton); } else {buttons = buttons.replace('{BUTTON}', document.getElementById('continueDiv').innerHTML);} $('#' + id + ' h4').text(heading); $('#' + id + ' .inner').html(inner + buttons); }	 $('#' + id).center();	 $('#disabler').css( { 'height' : $(document).height(), 'width' : '100%', 'left' : '0', 'top' : '0' }); if ((!$.browser.msie) || ($.browser.msie) && ($.browser.version > 6)) { $('.modal').css( { 'position' : 'fixed' }); } adjustedFade('#disabler', 'fadeIn', 200); adjustedFade('#' + id, 'fadeIn', 200); }
//for center()in modalShow()
jQuery.fn.center = function () { this.css("position","absolute");  this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");   this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");   return this; } ;
function modalClose(id) { adjustedFade('#disabler', 'fadeOut', 200); adjustedFade('#' + id, 'fadeOut', 200); }
function adjustedFade(id, direction, speed) { if ($.browser.msie && ($.browser.version > 6)) { if (direction == 'fadeIn') { $(id).show(0); } else { $(id).hide(0); } } else if ($.browser.msie && ($.browser.version == 6)) { if (direction == 'fadeIn') { $(id).show(); } else { $(id).hide(); } } else { if (direction == 'fadeIn') { $(id).fadeIn(speed); } else { $(id).fadeOut(speed); } } }




// BB
	   function populateDestinationcities(cityCode){
	loadDestinationXML(locale,cityCode);
}

  function loadHomePageXmls(locale,country){

			$.ajaxSetup ({
				cache: false
			});
			var inputArray={};
			inputArray["localeSeltd"] = locale;
			inputArray["country"] = country;
			var url = relPath+"/splashPageAction!loadHomePageXmlFiles.action";

			$.get(url,inputArray,getHomePageXmlXmlValues,'html');
		}

  function getHomePageXmlXmlValues(XMLResponse,status)
	{

		if(status=="success") {
			// to populate departure cities

			departXml =  (XMLResponse).split('CACHEDELIMITER');

			var newDepartXml = departXml[3];
		    departXmlObj = $.parseXML( newDepartXml );
		    $newDepartXml = $( departXmlObj );

			$('#departCity, #destCity').html('');
			country = $("#departCityId").html();
			code = '';
			$('#departCity').append( '<option value="' + code + '">' + country + '</option>');
			country = $("#destinCityId").html(); code = '';
			$('#destCity').append( '<option value="' + code + '">' + country + '</option>');


			$($newDepartXml).find('city').each(function(){

				country=$(this).text();
				code=$(this).attr('value');
				$('#departCity').append('<option value="'+code+'">'+country+'</option>');

			});





			// to populate check flight status cities

			var newFltStatusXml = departXml[1];
			fltStatusXmlObj = $.parseXML( newFltStatusXml );
		    $newFltStatusXml = $( fltStatusXmlObj );

			$('#statusDepartCity,#statusDestCity').html('');
			country = $("#statusDepartCityId").html(); code = '';
		    $('#statusDepartCity').append( '<option value="' + code + '">' + country + '</option>');
		    country = $("#statusDestCityId").html(); code = '';
		    $('#statusDestCity').append( '<option value="' + code + '">' + country + '</option>');
			$($newFltStatusXml).find('point').each(function(){

				country = $(this).text();
				code = $(this).attr('value');

				$('#statusDepartCity,#statusDestCity').append('<option value="' + code + '">' + country + '</option>');


			});

			// Check in status cities

			var newChkInStatusXml = departXml[2];
			chkInStatusXmlObj = $.parseXML( newChkInStatusXml );
		    $newChkInStatusXml = $( chkInStatusXmlObj );


			$('#IBoardPoint').html('');
			country = $("#IBoardPoint").html(); code = '';
		    $('#IBoardPoint').append( '<option value="' + code + '">' + country + '</option>');
		    country = $("#IBoardPoint").html(); code = '';
		    $('#IBoardPoint').append( '<option value="' + code + '">' + country + '</option>');
			$($newChkInStatusXml).find('checkinpoint').each(function(){

				country = $(this).text();
				code = $(this).attr('value');

				$('#IBoardPoint').append('<option value="' + code + '">' + country + '</option>');

			});
		}else {
			 alert("No matching region found.");
		}
	}



  function loadDepartureXML(locale,country){

		$.ajaxSetup ({
			cache: false
		});
		var inputArray={};
		inputArray["localeSeltd"] = locale;
		inputArray["country"] = country;
		var url = relPath+"/splashPageAction!loadDepartureXmlFile.action";

		$.get(url,inputArray,getDepartureXmlValues,'xml');

	}

	function getDepartureXmlValues(XMLResponse,status)
	{

		if(status=="success") {
			parseDepartureXMLResponse(XMLResponse);

		}else {
			 alert("No matching region found.");
		}
	}

	function parseDepartureXMLResponse(xml){

		$('#departCity, #destCity').html('');
		country = $("#departCityId").html();
		code = '';
		$('#departCity').append( '<option value="' + code + '">' + country + '</option>');
		country = $("#destinCityId").html(); code = '';
		$('#destCity').append( '<option value="' + code + '">' + country + '</option>');
		$(xml).find('city').each(function(){

			country=$(this).text();
			code=$(this).attr('value');
			$('#departCity').append('<option value="'+code+'">'+country+'</option>');

		});


	}
	 function loadDestinationXML(locale,cityCode){

			$.ajaxSetup ({
				cache: false
			});
			var inputArray={};
			inputArray["localeSeltd"] = locale;
			inputArray["departCitySeltd"] = cityCode;
			var url = relPath+"/splashPageAction!loadDestinationXmlFile.action";

			$.get(url,inputArray,getDestinationXmlValues,'xml');

		}

		function getDestinationXmlValues(XMLResponse,status)
		{

			if(status=="success") {
				var countryArray=[];
				$('#destCity').html('');
				country = $("#destinCityId").html(); code = '';
			    $('#destCity').append( '<option value="' + code + '">' + country + '</option>');

				$(XMLResponse).find('destination').each(function(){
					countryList = $(this).text();
					countryArray= countryList.split(',');
					destination = countryArray[0] ;
					code = $(this).attr('value');
					var countryDescription = countryArray[1];
					var countryDescriptionSplit = '';
					if(countryDescription != ''){
						countryDescriptionSplit = ",";
					}

					$(' #destCity').append('<option value="' + code + '">' + destination + countryDescriptionSplit + countryDescription + '</option>');


					if(!($('#departCity').val()==$('#destCity').val()) && $('#departCity').val()!='' && $('#destCity').val()!= '') {
						isMatrixSearch	= false;

						if(countryArray[3]=='MTXSCHD'){

							//isMatrixSearch	= true;
							//$('.flexibility .flex').fadeOut();
							$('#FlexiOption').click();
							var flexibleGrp			= $('input[name="flexible"]');
							if(flexibleGrp){
								flexibleGrp[0].checked=true;
							}
						}else{

							checkFlexiOption();
						}
						if($.trim($("#destCity").val()).length > 0){

							populateSplashCabinClass(countryArray[2]);
						}


					}



				});


			}else {
				 alert("No matching region found.");
			}

}



		  function loadFlightStatusXML(locale){

				$.ajaxSetup ({
					cache: false
				});
				var inputArray={};
				inputArray["localeSeltd"] = locale;

				var url = relPath+"/splashPageAction!loadChkFltStatusXmlFile.action";

				$.get(url,inputArray,getChkFltStatusXmlValues,'xml');

			}

			function getChkFltStatusXmlValues(XMLResponse,status)
			{

				if(status=="success") {

					$('#statusDepartCity,#statusDestCity').html('');
					country = $("#statusDepartCityId").html(); code = '';
				    $('#statusDepartCity').append( '<option value="' + code + '">' + country + '</option>');
				    country = $("#statusDestCityId").html(); code = '';
				    $('#statusDestCity').append( '<option value="' + code + '">' + country + '</option>');
					$(XMLResponse).find('point').each(function(){

						country = $(this).text();
						code = $(this).attr('value');

						$('#statusDepartCity,#statusDestCity').append('<option value="' + code + '">' + country + '</option>');

				});


			}else {
				 alert("No matching region found.");
			}

}

//var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
$(document).ready(function(){

	if($('#selcountry').length > 0){
		conLan = "/"+document.getElementById('selcountry').innerHTML.toLowerCase()+"/"+document.getElementById('selectedLanguage').innerHTML.toLowerCase();
	}
	var flexibleGrp			= $('input[name="flexible"]');
	/*
	 * flexible to Exact
	 */
	if(flexibleGrp.val() && !checkRadioSelection(flexibleGrp)){
		flexibleGrp[0].checked=true;
	}

	var valueInCombo = $('#checkInMethod').val();
	if("PNR"==valueInCombo){
		$('#IIdentification').val("");
		document.getElementById('IIdentification').maxLength=6;
	}
	//$(":radio[name='flexible'][id='radExact']").attr('checked', 'true');
$('#bb').tabs({fx:{opacity:'toggle',duration:'fast'},select:function(){$('#bb .linkselectLink').blur()}});
popMonths();
if (!iDevice){
    comboExclude=['#departCity','#destCity','#IBoardPoint'];

	linkselectExclude=['#adultPop','#childPop','#infantPop','#checkInMethod','#departDay','#departMonthYear','#destDay','#returnMonthYear','#statusDepartCity','#statusDestCity'];
	styleSelects(comboExclude,linkselectExclude);


}else {
comboExclude=['#departCity','#destCity'];
	linkselectExclude=['#adultPop','#childPop','#infantPop','#checkInMethod','#departDay','#departMonthYear','#destDay','#returnMonthYear','#flightClass','#statusDepartCity','#statusDestCity'];
	styleSelects(comboExclude,linkselectExclude);
}

$('#checkInMethod').linkselect({change:function(li,value){id='#id_'+value.toLowerCase();$('#checkin .idOpt').hide()
$(id).show();

if("PNR"==value){
	$('#IIdentification').val("");
	document.getElementById('IIdentification').maxLength=6;
} else {
	document.getElementById('IIdentification').maxLength=100;
}
}});
!iDevice &&
$('#adultPop').linkselect({change:function(li,value){updatePassengers('adults',value)}});
!iDevice &&
$('#childPop').linkselect({change:function(li,value){updatePassengers('children',value);}});
!iDevice &&
$('#infantPop').linkselect({change:function(li,value){updatePassengers('infants',value);}});
!iDevice &&
updatePassengers();

$('#departCity').siblings('input').autocomplete({

	 source: function( request, response ) {
		var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i" );
		response( $( "#departCity option" ).map(function() {
		var text = $( this ).text();
		var code = $( this ).val();
		if ( (matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text)  && ($.trim(code).length > 0)))
		return {
		label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) +")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
		 value: text,
		 option: this
		 };
		 }) );
		 },

	 change:function(event, ui){
	select=$('#departCity').siblings('select');
	if ( !ui.item ) {
		var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
			valid = false;
		select.children( "option" ).each(function() {
			if ( $( this ).text().match( matcher ) ) {
				this.selected = valid = true;
				return false;
			}
		});
		if ( !valid ) {
			// remove invalid value, as it didn't match anything
			if(Modernizr.input.placeholder) {
				$("#departCity").val('');$("#departCity").siblings('input').val($("#departCityId").html());
			}
			else {
				$("#departCity").val('');$("#departCity").siblings('input').val($("#departCityId").html());
			}
			$(this).data( "autocomplete" ).term = "";
			return false;
		}
	}

	//checkReservationConfiguration();
	populateDestinationcities(document.getElementsByName('departCity')[0].value);

}  });

$('#destCity').siblings('input').autocomplete({

	source: function( request, response ) {
		var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i" );
		response( $( "#destCity option" ).map(function() {
		var text = $( this ).text();
		var code = $( this ).val();
		if ( (matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text)  && ($.trim(code).length > 0)))
		return {
		label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) +")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
		 value: text,
		 option: this
		 };
		 }) );
		 },

	select:function(event, ui){
	select=$('#destCity').siblings('select');
	if ( !ui.item ) {
		var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
			valid = false;
		select.children( "option" ).each(function() {
			if ( $( this ).text().match( matcher ) ) {
				this.selected = valid = true;
				return false;
			}
		});
		if ( !valid ) {
			// remove invalid value, as it didn't match anything
			if(Modernizr.input.placeholder) {
				$("#destCity").val('');$("#destCity").siblings('input').val($("#departCityId").html());
			}
			else {
				$("#destCity").val('');$("#destCity").siblings('input').val($("#destinCityId").html());
			}
			$(this).data( "autocomplete" ).term = "";
			return false;
		}
	}
	$("#destCity").val(ui.item.option.value);


	checkReservationConfiguration();
}
});

$('#IBoardPoint').siblings('input').autocomplete({

	source: function( request, response ) {
var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i" );
response( $( "#IBoardPoint option" ).map(function() {
var text = $( this ).text();
var code = $( this ).val();
if ( (matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text)  && ($.trim(code).length > 0)))
return {
label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) +")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
 value: text,
 option: this
 };
 }) );
 }
});

$('#statusDepartCity').siblings('input').autocomplete({

	source: function( request, response ) {
var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i" );
response( $( "#statusDepartCity option" ).map(function() {
var text = $( this ).text();
var code = $( this ).val();
if ( (matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text)  && ($.trim(code).length > 0)))
return {
label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) +")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
 value: text,
 option: this
 };
 }) );
 }
});

$('#statusDestCity').siblings('input').autocomplete({

	source: function( request, response ) {
var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i" );
response( $( "#statusDestCity option" ).map(function() {
var text = $( this ).text();
var code = $( this ).val();
if ( (matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text)  && ($.trim(code).length > 0)))
return {
label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) +")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
 value: text,
 option: this
 };
 }) );
 }
});


//loadFlightStatusXML(locale);


 today=new Date();
 leadingZeroDay = ('0' + today.getDate()).slice(-2);
  if(!iDevice){
		$('#departDay').linkselect({change:function(li,value){checkFlexiDateConf();updateReturnDay(value);callSetUpdatedDates();}});
		$('#departMonthYear').linkselect({change:function(li,value){checkFlexiDateConf();updateReturnMonthYear(value);callSetUpdatedDates();}});
		$('#destDay').linkselect({change:function(li,value){checkFlexiDateConf();callSetUpdatedDates();}});
		$('#returnMonthYear').linkselect({change:function(li,value){checkFlexiDateConf();callSetUpdatedDates();}});
	$('#departDay').linkselect('val',leadingZeroDay);
	$('#destDay').linkselect('val',leadingZeroDay);

}else{
		$('#departDay').change(function(){callSetUpdatedDates();checkFlexiDateConf();});
		$('#departMonthYear').change(function(li,value){callSetUpdatedDates();checkFlexiDateConf();});
		$('#destDay').change(function(li,value){callSetUpdatedDates();checkFlexiDateConf();});
		$('#returnMonthYear').change(function(li,value){callSetUpdatedDates();checkFlexiDateConf();});
	$('#departDay').val(today.getDate());
	$('#destDay').val(today.getDate());
}


	// Instantiate the Booking Block's datepicker objects
$('#bb .calendar').datepicker({
	firstDay:1,
	minDate:0,
	maxDate:'+1y',
		showOtherMonths:true,
		selectOtherMonths:true,
	onSelect:function(date){

			// Upon selecting a date, update the relative drop downs
	updateDates($(this),date);

			// If the date selected was the departure date..
	if ($(this).parent().attr('id')=='leaveCal'){

				// Update the drop downs of the returning date too!
				// Makes sure impossible dates don't happen by accident, basicaly.
			updateDates($('#returnCal .calendar'),date);
		}

			// Hide the datepicker that the user just clicked on
		$(this).fadeOut();

			// If the return date box is active...
	if ($(this).parent().attr('id') == 'leaveCal' && $('#chkReturn').is(':checked')) {

				// Show the return date calendar after half a second
				// The timeout is there to wait for the other calendar to fade out
			setTimeout("$('#returnCal').click()", 500);
		}
	}
});

$('#bb .ui-datepicker').click(function(e){
	e.stopPropagation();});
$('#bb .calendar').click(function(e){
	e.stopPropagation();
	$(this).fadeOut();
	});
//$('#bb .calWrap').click(function(e){e.stopPropagation();closePopups();date=new Date();box=$('#'+$(this).parents('.blueBox').attr('id'));dayLinkSelect=box.find('.inputDay').children('a').attr('href');monthYearLinkSelect=box.find('.inputMonthYear').children('a').attr('href');d=new Date($(dayLinkSelect).linkselect('val')+' '+$(monthYearLinkSelect).linkselect('val'));$(this).children('.calendar').datepicker('setDate',d);$(this).children('.calendar:not(.disabled)').fadeIn();});

	// Some complicated logic & relative selection in here.
	// Note: this is basically a duplication of $('#mb .calWrap').click();
$('#bb .calWrap').click(function(e){

		// This checks whether the Return Date block is disabled
		if ($(this).hasClass('disabled')) return false;

		// Don't auto-close this datepicker
	e.stopPropagation();

		// Close all the other modals though
	closePopups();
	date=new Date();
	box=$('#'+$(this).parents('.blueBox').attr('id'));
	dayLinkSelect=box.find('.inputDay').children('a').attr('href');
	monthYearLinkSelect=box.find('.inputMonthYear').children('a').attr('href');

		// This fixes a bug where IE wouldn't find the correct value for the
		// 'href' attribute (above)
	if($.browser.msie){
		d=dayLinkSelect.split('#');
		m=monthYearLinkSelect.split('#');
		dayLinkSelect='#'+d[1];
			monthYearLinkSelect='#'+m[1];

		}

		// Force the string to be sure IE understands what we're doing... *sigh*
		// This spits out something like: "14-Jan-2012"
	update=$(dayLinkSelect).linkselect('val').toString()+'-'+$(monthYearLinkSelect).linkselect('val').toString();

		// Check if this if the date is a *valid* date string.
		// Fail if it is not (eg: 31 February 2011) - yay for the drop downs.
		try {
	calDate = $.datepicker.parseDate('dd-M-yy', update);
	 } catch (e){
	  alert(propsArray['bookingBlock_14']);
			// the datepicker doesnt parse when M is localized,eg: Dezember,Marz,Mai in deutsch.
			return false;
		}

		// Here, we know the date is valid! So..
		// Update the datepicker.
	$(this).children('.calendar').datepicker('setDate',calDate);

		// ..and fade it in!
		$(this).children('.calendar:not(.disabled)').fadeIn()

	});

setUpdatedDates();
$('#bb .linkselectLink').click(function(e){e.stopPropagation();if(!$(this).hasClass('.linkselectLinkOpen')){$($(this).attr('href'),true).linkselect('open')}});
$('#FlightChecked').iphoneStyle();
// Apply the jQuery UI button
	$('#FlightChecked').button();
(!$.browser.msie)&&$('#chkReturn, #checkInAddPax').each(function(){isChecked=$(this).is(':checked')?' checked':'';$(this).wrap('<label for="'+$(this).attr('id')+'" class="checkStyle'+isChecked+'">').hide();});$('.checkStyle input').click(function(){isChecked=$(this).is(':checked');$(this).parents('label').toggleClass('checked',isChecked);});$('.chkContainer input').change(function(){isChecked=$(this).is(':checked');if(isChecked){first=$('#NoFlight');last=$('#FlightNo');}else{first=$('#FlightNo');last=$('#NoFlight');}
first.fadeOut(100,function(){last.fadeIn(100)})});$('#chkReturn').click(function(){toggleReturnFields($(this).is(':checked'));
});

$('#bb .depCity').css({width:270-$('a.mulCity').width()-30+'px'});$('#bb .depCity input').css({width:$('.depCity').width()-6+'px'});$('.iconInfo').hoverIntent(function(){head=$(this).children('.head').text();copy=$(this).children('.inner').html();$('#bb .infoPopup header').html('<span class="iconInfo"></span><h1>'+head+'</h1>');$('#bb .infoPopup div').html(copy);$('#bb .infoPopup').fadeIn();},function(){$('#bb .infoPopup').fadeOut();});
//loadDepartureXML(locale,country);
loadHomePageXmls(locale,country);

//Calling the function to populate the Global Emergency Notice
populateGlobalNotice();
//Calling the function to populate the Global Emergency Notice
populateLocalNotice();

$('#flights .button.submit').click(function(){
var cnty=$('#user_location').text().toUpperCase();	//added for passing request_locale
	var valMessage="";
	$('#departCity').siblings('input').removeClass('invalid');
	$('#destCity').siblings('input').removeClass('invalid');
	error=false;
	if (!$('#departCity').val().length) {
		$('#departCity').siblings('input').addClass('invalid');
		valMessage=getLocalizedMessage("Departure city is required.","bookingBlock_1");
		error = true;
	} if (!$('#destCity').val().length) {
		$('#destCity').siblings('input').addClass('invalid');
		valMessage+=getLocalizedMessage("Destination city is required.","bookingBlock_2");
		error = true;
	} if ($('#departCity').val().length && $('#destCity').val().length && ($('#departCity').val() == $( '#destCity').val())) {
		valMessage+=getLocalizedMessage("Departure and Destination can not be the same.","bookingBlock_3");
		error = true;
	} if($("#adultPop").val()<$("#infantPop").val()){
		valMessage+=getLocalizedMessage("The adult-infant ratio should be 1:1.","bookingBlock_5");
		error = true;
	}	if (parseInt($("#adultPop").val()) + parseInt($("#infantPop").val()) + parseInt($( "#childPop").val()) > 9) {
		valMessage+=getLocalizedMessage("The total number of adults,children and infants must not exceed 9. Please consider splitting your group or complete the <a href='groupBooking.action'> Groups Request Form </a>","bookingBlock_6");
		error = true;
	}
	$("#adultCount").val($("#adultPop").val());
	$("#childCount").val($("#childPop").val());
	$("#infantCount").val($("#infantPop").val());
	var dep	=$('#departCity').val();
	var dest= $('#destCity').val();
	if($('input:checkbox[name=chkReturn]').attr('checked')){
		$("#tripType").val('R'); }
	else{		 $("#tripType").val('O'); }
	var tripType			= $("#tripType").val();
	if(!$('input[name="flexible"]').val().length){
		error = true;
	}
	//flexibleGrp=$('input[name="flexible"]').val();
	flexibleGrp = $('input:radio[name=flexible]:checked').val();
	document.forms[0].flexible.value = flexibleGrp ;

	setUpdatedDates();
	var fromDate	= $("#dateDepart").val();
	var toDate	= $("#dateDest").val();
	var dateFormt= 'dd-M y';
	if('R'==tripType && toDate==""){
		valMessage+=getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_17");
		error = true;
	}
	/*if('R'==tripType && (!$('#returnMonthYear').val().length && !$('#destDay').val().length ) ){
		valMessage+=getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_12");
		error = true;
	}*/

	if(compareWithCurrentDate(fromDate,dateFormt)<0){
		valMessage+=getLocalizedMessage("Please take a future departure date for travel.","bookingBlock_10");
		error = true; }
	if(!isValidDate(fromDate,dateFormt)){
		valMessage+=getLocalizedMessage("Invalid Departure Date","bookingBlock_18");
		error = true;
	}
	if('R'==tripType && !isValidDate(toDate,dateFormt)){
		valMessage+=getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_17");
		error = true;
	}

	if('R'==tripType){
		if(compareWithCurrentDate(toDate,dateFormt)<0 || compareDateStrings(fromDate,toDate,dateFormt)<0){
			valMessage+=getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_12");
		error = true;
		}
	}
    if(compareWithCurrentDate(toDate,dateFormt)<0){
    	valMessage+=getLocalizedMessage("Please take a future return date for travel.","bookingBlock_10");
    error = true; }
    if(error){
    	alert(valMessage);
    return false;
}
    // the language selected by the user is set to the field 'selectedLang'
    document.forms[0].selectedLang.value = document.getElementById('selectedLocaleDiv').innerHTML;
    document.forms[0].preferredClass.value = $('#flightClass').val();
    showProcessing();
	var conLanFlightSearch = "/"+depCountry.toLowerCase()+"/"+locale.toLowerCase();
	cnty = depCountry.toUpperCase();
	$("#sessionCountry").val(depCountry);

	if($('#profile').val()=='profile'){

		if(this.id=='link_bookByMiles')
		document.forms[0].action=relPath+conLanFlightSearch+'/flexPricerFlightSearch!milesBooking.action';
		else if(this.id=='link_bookByCash')
			document.forms[0].action=relPath+conLanFlightSearch+'/flexPricerFlightSearch!getProfileAvailability.action';
	document.forms[0].submit();

		}
	else{
		document.forms[0].action=relPath+conLanFlightSearch+'/flexPricerFlightSearch!flexPricerFlightAvailability.action?request_locale='+locale+"_"+cnty;
		document.forms[0].submit();

	}
});
//only alphabets no space
$('#ISurname').keyup(function() {
	 if (this.value.match( /[^a-zA-Z ]/g)) {
		 this.value = this.value.replace(/[^a-zA-Z ]/g, '');
		 }
	 });


$('#ISurnameM').keyup(function() {
	 if (this.value.match( /[^a-zA-Z ]/g)) {
		 this.value = this.value.replace(/[^a-zA-Z ]/g, '');
		 }
	 });


$('#checkin .button.submit').click(function(){
		var valMsg="";
if($("#mango_div").css('display')=='block'){
surnameM=$('#ISurnameM');boardPoint=$('#IBoardPoint').val();
etktnumberM=$('#etktnumberM');
var checkinlanguage = $('#display_language').text().toLowerCase();
language=$('#display_language').text().toLowerCase();
group=$('#checkInAddPaxM').is(':checked')?'&IGroupTravel=on':'';
error=false;
surnameM.removeClass('invalid');
etktnumberM.removeClass('invalid');
if(!surnameM.val().length){
	surnameM.addClass('invalid');
	error=true
	}

if(!etktnumberM.val().length){
	etktnumberM.addClass('invalid');
	error=true
	}
valMsg=getLocalizedMessage("Please fill in the mandatory field(s).","checkin_1");

if(error){
	alert(valMsg);
	return false;
	}
surnameM.removeClass('invalid');
etktnumberM.removeClass('invalid');
formOfID="ETKT";
var url='https://checkin.si.amadeus.net/1ASIHSSCWCIJE/sscwje/checkin?'+'LANGUAGE='+checkinlanguage+'&IFormOfIdentification='+formOfID+'&IIdentification='+etktnumberM.val()+'&ISurname='+surnameM.val()+group+'&RedirectedFrom=RSS&ln=EN';

$('#schedWrap').slideUp(function(){$(this).html('')
$('#checkInWrap').html('<span></span><iframe src="'+url+'" frameborder="0" scrolling="auto"></iframe>').slideDown();$('html, body').animate({scrollTop:$('#checkInWrap').offset().top-15},1500,function(){$('#checkInWrap span').fadeIn()});});
}
else{
	surname=$('#ISurname');ID=$('#IIdentification');boardPoint=$('#IBoardPoint').val();formOfID=$('#checkInMethod').val();
	//alert("surname is : "+surname.value+" formOfID = "+formOfID.value);
	//alert("isurname "+ document.getElementById('ISurname').value+ " ISurnameM "+document.getElementById('ISurnameM').value);
	var checkinlanguage = $('#display_language').text().toLowerCase();
	language=$('#display_language').text().toLowerCase();group=$('#checkInAddPax').is(':checked')?'&IGroupTravel=on':'';error=false;surname.removeClass('invalid');ID.removeClass('invalid');if(!surname.val().length){surname.addClass('invalid');error=true}
	if(!ID.val().length){ID.addClass('invalid');valMsg=getLocalizedMessage("Please fill in the mandatory field(s).","checkin_1");error=true;}
	if(error){alert(valMsg);return false;}surname.removeClass('invalid');ID.removeClass('invalid');var url='https://checkin.si.amadeus.net/1ASIHSSCWCISA/sscwsa/checkin?'+'LANGUAGE='+checkinlanguage+'&IFormOfIdentification='+formOfID+'&IIdentification='+ID.val()+'&ISurname='+surname.val()+
	group+'&RedirectedFrom=RSS&ln=EN';$('#schedWrap').slideUp(function(){$(this).html('')
	$('#checkInWrap').html('<span></span><iframe src="'+url+'" frameborder="0" scrolling="auto"></iframe>').slideDown();$('html, body').animate({scrollTop:$('#checkInWrap').offset().top-15},1500,function(){$('#checkInWrap span').fadeIn()});});
}

});

$('#checkin .button.reset, #checkInWrap span').live('click',function(){$('#checkInWrap span').hide();$('#checkInWrap').slideUp(function(){$(this).html('')});$('#ISurname,#IIdentification').val('').removeClass('invalid');$('#checkInAddPax').removeAttr('checked');$('.checkInAddPax.checkStyle.checked').removeClass('checked');
$('#ISurnameM,#etktnumberM').val('').removeClass('invalid');
$('#checkInAddPaxM').removeAttr('checked');
$('.checkInAddPaxM.checkStyle.checked').removeClass('checked');
$("input:[name='checkInMethod']").each(function(idx, element) {
	if ($(element).val() != " ") {
		$(element).linkselect("val", "PNR");
	}
});});


//only numeric( no space)
$('#flightNumber').keyup(function() {
		 if (this.value.match( /[^0-9]/g)) {
			 this.value = this.value.replace(/[^0-9]/g, '');
			 }
		 });




$('#status .button.submit').click(function(){var valMsg = "",statusValMsg = "";
flightNum=$('#FlightChecked').is(':checked');
$('#carrierCode').removeClass('invalid');$('#flightNumber').removeClass('invalid');
$('#statusDepartCity').siblings('input').removeClass('invalid');$('#statusDestCity').siblings('input').removeClass('invalid');
d=new Date();
d.setDate(d.getDate()+parseInt($('#fromDateFLT').linkselect('val')));
date=d.getDate()+'-'+(d.getMonth()+1)+'-'+d.getFullYear();
error=false;
if(flightNum){
 if(!$('#flightNumber').val().length){
	valMsg+=getLocalizedMessage("Please enter the Flight number.","statusVal_2");
	//alert("Please enter the Flight number.");
	$('#flightNumber').addClass('invalid');
	error=true;}
else if(isNaN($('#flightNumber').val())){
	valMsg+=getLocalizedMessage("Flight Number should be a number.","statusVal_3");
	//alert("Flight Number should be a number.");
	$('#flightNumber').addClass('invalid');
	error=true;}
if(error){
	alert(valMsg);
	return false;
}
document.forms[0].action=relPath+conLan+"/checkFlightStatusHome!checkFlightStatus_FltNum.action?request_locale="+locale+"&fromDateFLT="+date;
document.forms[0].submit();/*$('body').append('<form id="frmStatus" methor="post" action="http://www.flysaa.com/Journeys/checkFlightStatus!checkFlightStatus_FltNum.action">').hide();$('#frmStatus').append('<input name="carrierCode" value="'+$('#carrierCode').val()+'" />');$('#frmStatus').append('<input name="flightNumber" value="'+$('#flightNumber').val()+'" />');$('#frmStatus').append('<input name="fromDateFLT" value="'+date+'" />');$('#frmStatus').submit();*/}
else{ if(!$('#statusDepartCity').val().length){
	statusValMsg=getLocalizedMessage("Please enter the Departure City.","statusVal_4");
	//alert("Please enter the Departure City");
	$('#statusDepartCity').siblings('input').addClass('invalid');
	error=true;}
 if(!$('#statusDestCity').val().length){
	//alert("Please enter the Destination City");
	statusValMsg+=getLocalizedMessage("Please enter the Destination City.","statusVal_5");
	$('#statusDestCity').siblings('input').addClass('invalid');
	error=true;}
else if($('#statusDestCity').val()==$('#statusDepartCity').val()){
//	alert("Departure City and  Destination City cannot be the same");
	statusValMsg+=getLocalizedMessage("Departure City and  Destination City cannot be the same.","statusVal_6");
	$('#statusDepartCity').siblings('input').addClass('invalid');
	$('#statusDestCity').siblings('input').addClass('invalid');
	error=true;}
if(error){
	alert(statusValMsg);
	return false;
}
document.forms[0].action=relPath+conLan+"/checkFlightStatusHome!checkFlightStatus_Location.action?fromDateFLT="+date;
document.forms[0].submit();/*$('body').append('<form id="frmStatus" methor="post" action="http://www.flysaa.com/Journeys/checkFlightStatus!checkFlightStatus_Location.action">').hide();$('#frmStatus').append('<input name="departureCity" value="'+$('#statusDepartCity').val()+'" />');$('#frmStatus').append('<input name="destinationCity" value="'+$('#statusDestCity').val()+'" />');$('#frmStatus').append('<input name="fromDateLOC" value="'+date+'" />');$('#frmStatus').submit();*/}});
$('#status .button.reset').click(function(){
	$('#carrierCode, #flightNumber').val('').removeClass('invalid');
$('#statusDepartCity,#statusDestCity').val('');
$('#statusDepartCity').siblings('input').val($('statusDepartCityId').html());
$('#statusDestCity').siblings('input').val($('statusDestCityId').html());
$('#statusDepartCity,#statusDestCity').siblings('input').removeClass('invalid');});

$('.flightSchedule .button.route').click(function(){var url='http://timetables.oag.com/saaroutemapper/"';$('#checkInWrap').slideUp(function(){$(this).html('<span>')

$('#schedWrap').html('<span></span><iframe src="'+url+'" frameborder="0" id="worldTracer" scrolling="auto"></iframe>').slideDown();$('#schedWrap span').fadeIn();$('html, body').animate({scrollTop:$('#schedWrap').offset().top-15},1500);});});$('#schedWrap span').live('click',function(){$('#schedWrap').slideUp(function(){$(this).html('')})})});$(window).load(function(){$('#bb .loadingIndicator').fadeOut();});function oc(a){var o={};for(var i=0;i<a.length;i++){o[a[i]]='';}return o}
function styleSelects(x,y){$('#bb select').each(function(){id='#'+$(this).attr('id');if(!(id in oc(y))){if($.browser.msie&&$.browser.version<7){$(this).linkselect()}else{if(id in oc(x)){$(this).combobox();}else{$(this).linkselect();}}}});$('#statusDepartCity,#statusDestCity').combobox()}
function toggleReturnFields(enabled){
	if(iDevice){
if(enabled){
			$('#destDay, #returnMonthYear').show();
		}else{
			$('#destDay, #returnMonthYear').hide();
		}
	}else{
		if(enabled){
			$('#destDay, #returnMonthYear').linkselect('disable',false);
$('#returnCal .calendar').datepicker('enable').removeClass('disabled');
$('#returnCal, #returnDate .returnD').fadeTo(100,1).css('cursor','pointer').removeClass('disabled');}
else{
$('#destDay, #returnMonthYear').linkselect('disable',true);
$('#returnCal .calendar').datepicker('disable').addClass('disabled');
$('#returnCal, #returnDate .returnD').fadeTo(100,0.5).css('cursor','default').addClass('disabled');
}}	}

//Added for Request 3257468
function changeCheckin(checkins){
	var checkin=document.getElementsByName(checkins.name);
	document.getElementById('saa_div').style.display=(checkin[0].checked)?'block':'none';
	document.getElementById('mango_div').style.display=(checkin[1].checked)?'block':'none';
	}

function goToHotelsURL(){
	var url = "http://clk.tradedoubler.com/click?p=250485&a=2414975&g=18488210&url=http://za.hotels.com/?rffrid=AFF.HCOM.ZA.001.000.2414975.18488210.SAA";
	window.open(url);
}
/*function updateDates(c,d){
	date=new Date(d);
	box=$('#'+c.parents('.blueBox').attr('id'));
	day=box.find('.inputDay').children('a').attr('href');
	monthYear=box.find('.inputMonthYear').children('a').attr('href');
	newDay='"'+date.getDate()+'"';
	newMonthYear='"'+months[date.getMonth()]+'-'+date.getFullYear()+'"';
	$(day).linkselect('val',newDay);
	$(monthYear).linkselect('val',newMonthYear);
	}*/
/*
function checkFlex(element,value){dd=$('#departDay').linkselect('val');dmy=$('#departMonthYear').linkselect('val');rd=$('#destDay').linkselect('val');rmy=$('#returnMonthYear').linkselect('val');switch(element){case'dd':dd=value;break;case'dmy':dmy=value;break;case'rd':rd=value;break;case'rmy':rmy=value;break;default:break;}
dDay=new Date(dd+' '+dmy);rDay=new Date(rd+' '+rmy);flex=new Date(dDay);flex.setDate(flex.getDate()+2);if(rDay<=flex){$('.flexibility .flex').fadeOut();$('#radExact').click();}else{$('.flexibility .flex').fadeIn();}}

*/
//function checkFlex(element,value){
function checkFlex(){
	if (!iDevice){
		dd=$('#departDay').linkselect('val');
		dmy=$('#departMonthYear').linkselect('val');
		rd=$('#destDay').linkselect('val');
		rmy=$('#returnMonthYear').linkselect('val');
		} else {
			dd=$('#departDay').val();
			dmy=$('#departMonthYear').val();
			rd=$('#destDay').val();
			rmy=$('#returnMonthYear').val();
			}
	/*switch(element){
	case'dd':dd=value;
	break;
	case'dmy':dmy=value;
	break;
	case'rd':rd=value;
	break;
	case'rmy':rmy=value;
	break;
		default:
		break;
	}	 */
	 error = false;

	try {
	parseDDate = $.datepicker.parseDate('dd-M-yy', dd+'-'+dmy);

		// Populate the formatted field
		popFormattedDates(parseDDate,'depart');

	} catch (e){
		$('#departDay_link, #departMonthYear_link').addClass('invalid');
		error = true
	}

	try {
	parseRDate = $.datepicker.parseDate('dd-M-yy', rd+'-'+rmy);

		// Populate the formatted field
		popFormattedDates(parseRDate,'return');

	} catch (e){
		$('#destDay_link, #returnMonthYear_link').addClass('invalid');
		error = true
	}

	dDay=new Date(parseDDate);
	rDay=new Date(parseRDate);

	if (rDay<dDay){
		$('#destDay_link, #returnMonthYear_link').addClass('invalid');
		error = true
	}

	if (error) return false;

	flex=new Date(dDay);
	flex.setDate(flex.getDate()+2);



	if(rDay<=flex){
		$('.flexibility .flex').fadeOut();
		$('#radExact').click();
		} else {
			$('.flexibility .flex').fadeIn();
			} }
function updatePassengers(element,value){
// This function will only work on linkselect elements,
	// so restrict mobile browsers.
	if(iDevice) return false;
maxPax=9;
adults=parseInt($('#adultPop').linkselect('val'));
children=parseInt($('#childPop').linkselect('val'));
infants=parseInt($('#infantPop').linkselect('val'));
switch(element){
case'adults':adults=parseInt(value);
break;
case'children':children=parseInt(value);
break;
case'infants':infants=parseInt(value);
break;
default:break;
}
passengers=[parseInt(adults),parseInt(children),parseInt(infants)];
total=passengers[0]+passengers[1]+passengers[2];
remainder=maxPax-total;
$('#adultPop_list li').each(function(i){$(this).removeClass('hidePassenger');
selectable=remainder+adults;
if(i>selectable)$(this).addClass('hidePassenger');
}
);
$('#childPop_list li').each(function(i){
$(this).removeClass('hidePassenger');
selectable=remainder+children;
if(i>selectable)$(this).addClass('hidePassenger');
});
$('#infantPop_list li').each(function(i){
$(this).removeClass('hidePassenger');
selectable=remainder+infants;
if(i>selectable)$(this).addClass('hidePassenger');
});
if(total==9){
// Highlight the link!
$('#flights .morePaxLink').addClass('highlight')}
else{
   // un-Highlight the link!
$('#flights .morePaxLink').removeClass('highlight')
}}
//function popMonths(){var dates=[],values=[],now=new Date(),ob=new String(); for(i=1;i<13;i++){values[i]=months[now.getMonth()]+'-'+now.getFullYear();dates[i]=months[now.getMonth()]+' '+now.getFullYear();ob='<option value="'+values[i]+'">'+dates[i]+'</option>';$('#departMonthYear').append(ob);$('#returnMonthYear').append(ob);if(now.getDate()==31)now.setDate(28);now.setMonth(now.getMonth()+1);}}
function popMonths(){
	var dates=[],
	values=[],
	now=new Date(),
	ob=new String();

	// This fixes a bug
	// => If the current day was over 28, February would be skipped, and would disappear from the drop downs!
	//    Meaning, people couldn't book flights in February
	now.setDate(28);

	// Loop through 13 months. (eg: December 2011 => December 2012)
	for(i=0;i<13;i++){
	values[i]=months[now.getMonth()]+'-'+now.getFullYear();
	dates[i]=months[now.getMonth()]+' '+now.getFullYear();
	ob='<option value="'+values[i]+'">'+dates[i]+'</option>';
	$('#departMonthYear').append(ob);
	$('#returnMonthYear').append(ob);
	now.setMonth(now.getMonth()+1)
	}
	};
function numeric(event){var keyCode=getKeyCode(event)
if(((keyCode>=48)&&(keyCode<=57)||(keyCode==46))||(keyCode==8)||keyCode==9){event.returnValue=true;return true;}else{event.preventDefault;return false;}};
;
function showProcessing(){
	$("#blocks,#warnings, #midMenu nav, .regionCountry, .regionLang").css("display","none");
	setTimeout(function(){$("#processing").css("display","block");},50); //modified as per BA's request for animating the processing image on flysaa.com in IE

}



// changes for Region Mega Menu at Home page

function populateMegaMenu(locale){
	var selectedCountry = $('#selectedCountryDiv').html();
	var selectedLang = $('#selectedLocaleDiv').html();
	if(selectedCountry == null || selectedCountry == "")
	{
		selectedCountry = "ZA";
	}
	if(selectedLang == null || selectedLang == "")
	{
		selectedLang = "EN";
		}
	//loadXMLforCDesc(selectedLang);
	//loadPageLabels(locale);
	loadRegionXML(locale);

}
function loadPageLabels(locale){

	$.ajaxSetup ({
		cache: false
	});
	var localeArray={};
	localeArray["localeSeltd"] = locale;
	var url = relPath+"/splashPageAction!loadPageLabelXmlFile.action";

	$.get(url,localeArray,getPagelabelXmlValues,'xml');

}

function getPagelabelXmlValues(XMLResponse,status)
{

	if(status=="success") {
		parsePagelabelXML(XMLResponse);

	}else {
		 alert("No matching region found.");
	}
}

function parsePagelabelXML(xml){


	txt_tab1 = $(xml).find('tab1').text();
	txt_tab2 = $(xml).find('tab2').text();
	txt_tab3 = $(xml).find('tab3').text();
	txt_tab4 = $(xml).find('tab4').text();


	$('li.americas a').html(txt_tab1);
	$('li.africa a').html(txt_tab2);
	$('li.europe a').html(txt_tab3);
	$('li.asiapacific a').html(txt_tab4);
}

function loadRegionXML(locale){

	$.ajaxSetup ({
		cache: false
	});
	var localeArray={};
	localeArray["localeSeltd"] = locale;
	var url = relPath+"/splashPageAction!loadRegionXmlFile.action";

	$.get(url,localeArray,getRegionXmlValues,'html');

}

function getRegionXmlValues(XMLResponse,status)
{

	if(status=="success") {
		parseRegionXMLResponse(XMLResponse);

	}else {
		 alert("No matching region found.");
	}
}


function parseRegionXMLResponse(xml)
{
	regionXml =  (xml).split('CACHEDELIMITER');

	// for populating car countries
	$('#carCountry').html('');
	country=$("#countryResHeaderId").html();
	code='';
	$('#carCountry').append('<option value="' + code + '">' + country + '</option>');

	var selectedCountry;// = $('#selectedCountryDiv').html();
		if(!checkCookie()){
			selectedCountry = $('#selectedCountryDiv').html();
		}else{
			selectedCountry = getCookie('rememberLocale');
			//setting new cookie
	 		setCookie('rememberLocale',selectedCountry,11,'/');
			//setCookie('rememberLang',currentLanguage,11,'/');
	}

	var newRegionXml = regionXml[1];
    xmlDoc = $.parseXML( newRegionXml );
    $newRegionXml = $( xmlDoc );

	$($newRegionXml).find('country').each(function(){
			val = $(this).attr('value');
			name = $(this).text();
			if (val == selectedCountry) {
				$('#currentLoc').html($(this).text());
				$('#long_location').html($(this).text());
				$('#saved_locale').html($(this).text());
				}

			// for populating car countries
			country=$(this).text();
			code=$(this).attr('value');
		$('#carCountry').append('<option value="'+code+'">'+country+'</option>');
			});



	var newCntryXml = regionXml[0];
    cntryXmlDoc = $.parseXML( newCntryXml );
    $newCntryXml = $( cntryXmlDoc );


	tab1.length = tab2.length = tab3.length = tab4.length = 0;
	noLocation="Type Here";
	$($newCntryXml).find('region').each(function(){
		      var $region = $(this);
		      if($region.attr('value')=="AM"  ){
		  		descrption1 = $region.find('region_name').text();
		  		 var saCountries = $($newCntryXml).find('region[value='+"AM"+']').get();
		  		 $(saCountries).find("country").each(function(){
		  			//saTab.push($(this).text());
		  			tab1.push($(this).text());
		  			tabVal1.push($(this).attr('value'));
		  		 });
		  	 }
		      if($region.attr('value')=="AF"  ){
			  		descrption2 = $region.find('region_name').text();
			  		 var saCountries = $($newCntryXml).find('region[value='+"AF"+']').get();
			  		 $(saCountries).find("country").each(function(){
			  			 tab2.push($(this).text());
			  			tabVal2.push($(this).attr('value'));
			  			//tab1.push(naTab);
			  		 });
			  	 }
		      if($region.attr('value')=="EU"  ){
			  		descrption3 = $region.find('region_name').text();
			  		 var saCountries = $($newCntryXml).find('region[value='+"EU"+']').get();
			  		 $(saCountries).find("country").each(function(){
			  			 tab3.push($(this).text());
			  			tabVal3.push($(this).attr('value'));
			  			//tab2.push(afTab);
			  		 });
			  	 }
		      if($region.attr('value')=="AS"  ){
			  		descrption4 = $region.find('region_name').text();
			  		 var saCountries = $($newCntryXml).find('region[value='+"AS"+']').get();
			  		 $(saCountries).find("country").each(function(){
			  			tab4.push($(this).text());
			  			tabVal4.push($(this).attr('value'));
			  			//tab2.push(zaTab);
			  		 });
			  	 }
		     /* if($region.attr('value')=="EU"  ){
			  		descrption5 = $region.find('region_name').text();
			  		 var saCountries = $($newCntryXml).find('region[value='+"EU"+']').get();
			  		 $(saCountries).find("country").each(function(){
			  			tab3.push($(this).text());
			  			tabVal3.push($(this).attr('value'));
			  			//tab3.push(euTab);
			  		 });
			  	 }
		      if($region.attr('value')=="ME"  ){
			  		descrption6 = $region.find('region_name').text();
			  		 var saCountries = $($newCntryXml).find('region[value='+"ME"+']').get();
			  		 $(saCountries).find("country").each(function(){
			  			tab4.push($(this).text());
			  			tabVal4.push($(this).attr('value'));
			  			//tab3.push(meTab);
			  		 });
			  	 }
		      if($region.attr('value')=="AU"  ){
			  		descrption7 = $region.find('region_name').text();
			  		 var saCountries = $($newCntryXml).find('region[value='+"AU"+']').get();
			  		 $(saCountries).find("country").each(function(){
			  			tab4.push($(this).text());
			  			tabVal4.push($(this).attr('value'));
			  			//tab4.push(auTab);
			  		 });
			  	 }
		      if($region.attr('value')=="AS"  ){
			  		descrption8 = $region.find('region_name').text();
			  		 var saCountries = $($newCntryXml).find('region[value='+"AS"+']').get();
			  		 $(saCountries).find("country").each(function(){
			  			tab4.push($(this).text());
			  			tabVal4.push($(this).attr('value'));
			  			//tab4.push(asTab);
			  		 });
			  	 } */

});




	$('.articleWrap article').each(function(){ $(this).html('<ul class="clearfix"></ul>'); });

	for (i in tab1){
		$('#list1 ul').append('<li><a href="#" onclick="populateCountryFromMenu(' + "'"+tabVal1[i]+"'" + ')">' + tab1[i] + '</a></li>');
	}

	for (i in tab2){
			$('#list2 ul').append('<li><a href="#" onclick="populateCountryFromMenu(' + "'"+tabVal2[i]+"'" + ')">' + tab2[i] + '</a></li>');
	}

	for (i in tab3){
			$('#list3 ul').append('<li><a href="#" onclick="populateCountryFromMenu('+ "'"+tabVal3[i]+"'" +')">' + tab3[i] + '</a></li>');
	}

	for (i in tab4){
			$('#list4 ul').append('<li><a href="#" onclick="populateCountryFromMenu(' + "'"+tabVal4[i]+"'" + ')">' + tab4[i] + '</a></li>');
	}

	/*$(regionXml[2]).find('product').each(function(){


		// for populating car countries
		product=$(this).text();
		code=$(this).attr('value');
	$('#carCountry').append('<option value="'+code+'">'+product+'</option>');
		});*/


}


//functions for xml loading

function loadSplashXMLToLink(locale,cCode){
	$.ajaxSetup ({
		cache: false
	});
	var localeArray={};
	localeArray["localeSeltd"] = locale;
	localeArray["countrySeltd"] = cCode;
	document.forms[0].countrySeltd.value = cCode;
	var url = relPath+"/splashPageAction!loadRegionXmlFile.action";
	$.get(url,localeArray,getXmlValuesToLink,'html');
}
function getXmlValuesToLink(XMLResponse,status){
	if(status=="success") {
		parseXMLResponseToLink(XMLResponse);


	}else {
		 alert("No matching region found.");
	}
}
function parseXMLResponseToLink(xml){

	regionXml =  (xml).split('CACHEDELIMITER');
	var cCode = document.forms[0].countrySeltd.value;
	var selectedLang = $('#selectedLocaleDiv').html();
	var conLanForHome = "/"+cCode.toLowerCase()+"/"+selectedLang.toLowerCase();
	var newCntryXmlToLink = regionXml[0];
    cntryXmlDocToLink = $.parseXML( newCntryXmlToLink );
    $newCntryXmlToLink = $( cntryXmlDocToLink );

	$($newCntryXmlToLink).find('region').each(function(){
	      var $region = $(this);
	      if($region.attr('value')=="AM"  ){
	  		descrption1 = $region.find('region_name').text();
	  		 var saCountries = $($newCntryXmlToLink).find('region[value='+"AM"+']').get();
	  		 $(saCountries).find("country").each(function(){
	  			val = $(this).attr('value');
	  			if (val == cCode) {
	  			        document.forms[0].country.value = $(this).attr('value');
	  				if(document.getElementById('chkRemember').checked){
	  					isCookieEnabled = "true";
	  					//clearing cookie
	  					setCookie('rememberLocale','',-1,'/');
	  					setCookie('rememberLang','',-1,'/');
	  					//setting new cookie
	  					setCookie('rememberLocale',cCode,11,'/');
	  					setCookie('rememberLang',selectedLang,11,'/');
	  				}//comment the below code, to use previous cookie value, if Remember me is not checked in homepage
	  				else{
	  					setCookie('rememberLocale','',-1,'/');
	  					setCookie('rememberLang','',-1,'/');
	  				}
	  				document.forms[0].action = relPath+conLanForHome+"/home!loadCountryLanguage.action?request_locale="+selectedLang+"_"+cCode+"&splashLocale="+selectedLang+"&splashCntry="+cCode+"&isCookieEnabled="+isCookieEnabled;
	  				document.forms[0].submit();
	  				}


	  		 });
	  	 }
	      if($region.attr('value')=="AF"  ){
		  		descrption2 = $region.find('region_name').text();
		  		 var saCountries = $($newCntryXmlToLink).find('region[value='+"AF"+']').get();
		  		 $(saCountries).find("country").each(function(){
		  			val = $(this).attr('value');
		  			if (val == cCode) {
		  			        document.forms[0].country.value = $(this).attr('value');
		  				if(document.getElementById('chkRemember').checked){
		  					isCookieEnabled = "true";
		  					//clearing cookie
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  					//setting new cookie
		  					setCookie('rememberLocale',cCode,11,'/');
		  					setCookie('rememberLang',selectedLang,11,'/');
		  				}//commented the below code, to use previous cookie value, if Remember me is not checked in homepage
		  				else{
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  				}
		  				document.forms[0].action = relPath+conLanForHome+"/home!loadCountryLanguage.action?request_locale="+selectedLang+"_"+cCode+"&splashLocale="+selectedLang+"&splashCntry="+cCode+"&isCookieEnabled="+isCookieEnabled;
		  				document.forms[0].submit();
		  				}


		  		 });
		  	 }
	      if($region.attr('value')=="EU"  ){
		  		descrption3 = $region.find('region_name').text();
		  		 var saCountries = $($newCntryXmlToLink).find('region[value='+"EU"+']').get();
		  		 $(saCountries).find("country").each(function(){
		  			val = $(this).attr('value');
		  			if (val == cCode) {
		  			        document.forms[0].country.value = $(this).attr('value');
		  				if(document.getElementById('chkRemember').checked){
		  					isCookieEnabled = "true";
		  					//clearing cookie
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  					//setting new cookie
		  					setCookie('rememberLocale',cCode,11,'/');
		  					setCookie('rememberLang',selectedLang,11,'/');
		  				}//commented the below code, to use previous cookie value, if Remember me is not checked in homepage
		  				else{
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  				}
		  				document.forms[0].action = relPath+conLanForHome+"/home!loadCountryLanguage.action?request_locale="+selectedLang+"_"+cCode+"&splashLocale="+selectedLang+"&splashCntry="+cCode+"&isCookieEnabled="+isCookieEnabled;
		  				document.forms[0].submit();
		  				}


		  		 });
		  	 }
	      if($region.attr('value')=="AS"  ){
		  		descrption4 = $region.find('region_name').text();
		  		 var saCountries = $($newCntryXmlToLink).find('region[value='+"AS"+']').get();
		  		 $(saCountries).find("country").each(function(){

		  			val = $(this).attr('value');
		  			if (val == cCode) {
		  			        document.forms[0].country.value = $(this).attr('value');
		  				if(document.getElementById('chkRemember').checked){
		  					isCookieEnabled = "true";
		  					//clearing cookie
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  					//setting new cookie
		  					setCookie('rememberLocale',cCode,11,'/');
		  					setCookie('rememberLang',selectedLang,11,'/');
		  				}//commented the below code, to use previous cookie value, if Remember me is not checked in homepage
		  				else{
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  				}
		  				document.forms[0].action = relPath+conLanForHome+"/home!loadCountryLanguage.action?request_locale="+selectedLang+"_"+cCode+"&splashLocale="+selectedLang+"&splashCntry="+cCode+"&isCookieEnabled="+isCookieEnabled;
		  				document.forms[0].submit();
		  				}

		  		 });
		  	 }
/*	      if($region.attr('value')=="EU"  ){
		  		descrption5 = $region.find('region_name').text();
		  		 var saCountries = $($newCntryXmlToLink).find('region[value='+"EU"+']').get();
		  		 $(saCountries).find("country").each(function(){
		  			val = $(this).attr('value');
		  			if (val == cCode) {
		  			        document.forms[0].country.value = $(this).attr('value');
		  				if(document.getElementById('chkRemember').checked){
		  					isCookieEnabled = "true";
		  					//clearing cookie
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  					//setting new cookie
		  					setCookie('rememberLocale',cCode,1,'/');
		  					setCookie('rememberLang',selectedLang,1,'/');
		  				}//commented the below code, to use previous cookie value, if Remember me is not checked in homepage
		  				else{
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  				}
		  				document.forms[0].action = relPath+conLanForHome+"/home!loadCountryLanguage.action?request_locale="+selectedLang+"&splashLocale="+selectedLang+"&splashCntry="+cCode+"&isCookieEnabled="+isCookieEnabled;
		  				document.forms[0].submit();
		  				}
		  		 });
		  	 }
	      if($region.attr('value')=="ME"  ){
		  		descrption6 = $region.find('region_name').text();
		  		 var saCountries = $($newCntryXmlToLink).find('region[value='+"ME"+']').get();
		  		 $(saCountries).find("country").each(function(){

		  			val = $(this).attr('value');
		  			if (val == cCode) {
		  			        document.forms[0].country.value = $(this).attr('value');
		  				if(document.getElementById('chkRemember').checked){
		  					isCookieEnabled = "true";
		  					//clearing cookie
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  					//setting new cookie
		  					setCookie('rememberLocale',cCode,1,'/');
		  					setCookie('rememberLang',selectedLang,1,'/');
		  				}//commented the below code, to use previous cookie value, if Remember me is not checked in homepage
		  				else{
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  				}
		  				document.forms[0].action = relPath+conLanForHome+"/home!loadCountryLanguage.action?request_locale="+selectedLang+"&splashLocale="+selectedLang+"&splashCntry="+cCode+"&isCookieEnabled="+isCookieEnabled;
		  				document.forms[0].submit();
		  				}

		  		 });
		  	 }
	      if($region.attr('value')=="AU"  ){
		  		descrption7 = $region.find('region_name').text();
		  		 var saCountries = $($newCntryXmlToLink).find('region[value='+"AU"+']').get();
		  		 $(saCountries).find("country").each(function(){
		  			val = $(this).attr('value');
		  			if (val == cCode) {
		  			        document.forms[0].country.value = $(this).attr('value');
		  				if(document.getElementById('chkRemember').checked){
		  					isCookieEnabled = "true";
		  					//clearing cookie
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  					//setting new cookie
		  					setCookie('rememberLocale',cCode,1,'/');
		  					setCookie('rememberLang',selectedLang,1,'/');
		  				}//commented the below code, to use previous cookie value, if Remember me is not checked in homepage
		  				else{
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  				}
		  				document.forms[0].action = relPath+conLanForHome+"/home!loadCountryLanguage.action?request_locale="+selectedLang+"&splashLocale="+selectedLang+"&splashCntry="+cCode+"&isCookieEnabled="+isCookieEnabled;
		  				document.forms[0].submit();
		  				}

		  		 });
		  	 }
	      if($region.attr('value')=="AS"  ){
		  		descrption8 = $region.find('region_name').text();
		  		 var saCountries = $($newCntryXmlToLink).find('region[value='+"AS"+']').get();
		  		 $(saCountries).find("country").each(function(){
		  			val = $(this).attr('value');
		  			if (val == cCode) {
		  			        document.forms[0].country.value = $(this).attr('value');
		  				if(document.getElementById('chkRemember').checked){
		  					isCookieEnabled = "true";
		  					//clearing cookie
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  					//setting new cookie
		  					setCookie('rememberLocale',cCode,1,'/');
		  					setCookie('rememberLang',selectedLang,1,'/');
		  				}//commented the below code, to use previous cookie value, if Remember me is not checked in homepage
		  				else{
		  					setCookie('rememberLocale','',-1,'/');
		  					setCookie('rememberLang','',-1,'/');
		  				}
		  				document.forms[0].action = relPath+conLanForHome+"/home!loadCountryLanguage.action?request_locale="+selectedLang+"&splashLocale="+selectedLang+"&splashCntry="+cCode+"&isCookieEnabled="+isCookieEnabled;
		  				document.forms[0].submit();
		  				}

		  		 });
		  	 } */

});

}

function populateCountryFromMenu(cCode){

	var selectedLocale = $('#selectedCountryDiv').html();
	var selectedLang = $('#selectedLocaleDiv').html();
	loadSplashXMLToLink(selectedLang,cCode);

	}


/*// work around done for voyager login. Need to modify the method
function checkVoyagerLogin(voyagerId,pin){


	$.ajaxSetup ({
		cache: false
	});
	var detailsArray={};
	detailsArray["voyagerId"] = voyagerId;
	detailsArray["pin"] = pin;
	var url = relPath+"/splashPageAction!voyagerLogin.action";

	$.get(url,detailsArray,getVoyagerData,'text');


}

function getVoyagerData(Response,respStatus)
{
	if(Response != "" && respStatus=="success") {
	var voyDetails=Response.split(',');

	$('#voyager').addClass('loggedin');
	$('#memName span').html(voyDetails[1]+voyDetails[2]);
	$('#memNum').html('<span></span>'+voyDetails[3]);
	$('#memMiles').html('<span></span>'+voyDetails[4]);
	$('#memStatus').html('<span></span>'+voyDetails[5]);
	$('#memStatus').addClass(voyDetails[5].toLowerCase());
	$('#voyager .in').css('display','block');
	$('#voyager .out').css('display','none');

	}else {
		alert("No records found");
		 $('#mb #voyagerId, #mb #pin').addClass('invalid');
	}
			$('#mb .loadingIndicator').fadeOut();

}*/

function setUpdatedDates(){

// Departure date
	dep				= [];
	dep['day']		= $('#departDay').val();
	dep['month']	= $('#departMonthYear').val().split('-')[0];
	dep['year']		= $('#departMonthYear').val().split('-')[1];

	log(dep);

	// Get the final two digits of the year
	dep['year']		= dep['year'].substr(-2);

	// Get the 'en' month
	i = $.inArray(dep['month'],eval('months_'+locale));
	dep['month']	= months_en[i];

	//
	// Set the correct value of the departure date
	try {
		$('#dateDepart').val( dep['day'] + "-" + dep['month'] + " " + dep['year'] );
	} catch (e) {
		$('#dateDepart').val('');
		$('#dateDest').val('');
	}


// Return date
	ret				= [];
	ret['day']		= $('#destDay').val();
	ret['month']	= $('#returnMonthYear').val().split('-')[0];
	ret['year']		= $('#returnMonthYear').val().split('-')[1];

	log(ret);

	// Get the final two digits of the year
	ret['year']		= ret['year'].substr(-2);

	// Get the 'en' month
	i = $.inArray(ret['month'],eval('months_'+locale));
	ret['month']	= months_en[i];

	//
	// Set the correct value of the return date
	try {
		$('#dateDest').val( ret['day'] + "-" + ret['month'] + " " + ret['year'] );
	} catch (e) {
		$('#dateDest').val('');
	}



	// to update the hidden variables for carHireDep/DestMonthYear

	carDep				= [];

	carDep['month']	= $('#pickMonthYear').val().split('-')[0];
	carDep['year']		= $('#pickMonthYear').val().split('-')[1];


	// Get the 'en' month
	i = $.inArray(carDep['month'],eval('months_'+locale));
	carDep['month']	= months_en[i];

	try {
		$('#carHireDepMonthYear').val( carDep['month'] + "-" + carDep['year'] );
	} catch (e) {
		$('#carHireDepMonthYear').val('');
		$('#carHireDestMonthYear').val('');
	}

	carRet				= [];

	carRet['month']	= $('#dropoffMonthYear').val().split('-')[0];
	carRet['year']		= $('#dropoffMonthYear').val().split('-')[1];

	log(carRet);


	// Get the 'en' month
	i = $.inArray(carRet['month'],eval('months_'+locale));
	carRet['month']	= months_en[i];


	try {
		$('#carHireDestMonthYear').val( carRet['month'] + "-" + carRet['year'] );
	} catch (e) {
		$('#carHireDestMonthYear').val('');
	}


}
/*
 * For Select box-Calendat component
 */
function daysInMonth(iMonth, iYear){
	return 32 - new Date(iYear, iMonth, 32).getDate();
}

function isValidDate(dateToCheck,format){
	if(format=='dd-M y' && dateToCheck!=""){
		var dateArray 	= dateToCheck.split('-');
		if(dateArray[0].length==0 || dateArray[1].length==0){
			return false;
		}
		if(dateArray[0] > daysInMonth(getMonthIndex(dateArray[1].split(' ')[0]),'20'+dateArray[1].split(' ')[1])){
			return false;
		}
	}
	return true;
}
function compareWithCurrentDate(dateStr,format){
	var date1;
	var dateArray;
	if(format=='dd-M y'){
		if(dateStr!=""){
		dateArray 	= dateStr.split('-');

		date1		= new Date('20'+dateArray[1].split(' ')[1],getMonthIndex(dateArray[1].split(' ')[0]),dateArray[0]);
		}
	}else{
		alert('Date format not supported by function..');return -1;
	}
	return compareDateObjects(new Date(),date1);
}
/*
 * compareDateStrings - Compares the given String formated date to current date
 * Supported Formats - dd-M y
 * parameters
 * -----
 * param1- String date
 * param2- format of String date
 * returns
 * -------
 * 1		-if date in future date
 * -1 	-if date is past date
 */
function compareDateStrings(date1,date2,format){
	var dateObj1;
	var dateObj2;
	if(format=='dd-M y'){
		if(date1!=""){
		var dateArray 	= date1.split('-');
		dateObj1		= new Date('20'+dateArray[1].split(' ')[1],getMonthIndex(dateArray[1].split(' ')[0]),dateArray[0]);
		}
		if(date2!=""){
		dateArray 		= date2.split('-');
		dateObj2		= new Date('20'+dateArray[1].split(' ')[1],getMonthIndex(dateArray[1].split(' ')[0]),dateArray[0]);
		}
	}else{
		alert('Date format not supported by function..');return -1;
	}
	return compareDateObjects(dateObj1,dateObj2);
}
function compareDateObjects(date1,date2){
	if(date1 !="" && date2!= undefined ){
	d1=	date1.getDate();
	d2= date2.getDate();
	m1= date1.getMonth();
	m2= date2.getMonth();
	y1= date1.getFullYear();
	y2= date2.getFullYear();
	if(y2<y1){
		return -1;
	}else if((y2==y1) && (m2<m1)){
		return -1;
	}else if((y2==y1) && (m2==m1)){
		if((d2<d1)){
			return -1;
		}else if((d2==d1)){
			return 0;
		}
	}
	return 1;
}
}
/*
 * Checks for selected radio button in the button group..This method cannot be
 * used for radio values - true/false
 */
function checkRadioSelection(buttonGroup){
	if(buttonGroup){
		for(var i=0;i<buttonGroup.length;i++){
			if(buttonGroup[i].checked){
				return true;
			}
		}
	}
	return false;
}

function checkFlexiDateConf()
{
	if(!($('#departCity').val()==$('#destCity').val()) && $('#departCity').val()!='' && $('#destCity').val()!= '') {
		isMatrixSearch	= false;
		var reservationConfigArray={};
		reservationConfigArray["departureCity"] =$('#departCity').val();
		reservationConfigArray["destinationCity"]= $('#destCity').val();
		$.ajaxSetup ({
			cache: false

		});

		var url = relPath+conLan+"/flightSearch!checkReservationConfiguration.action";
		// $.get(url,argument,callbackFunction,returnType) is the Syntax.
		$.get(url,reservationConfigArray,checkFlexiSearchOption,'text');
	}
}
/**
 * This method is the callback fucntion configured in the $.get() method. This
 * will be called by the Ajax framework, if the ajax call returned a success. By
 * default two values are passed into this fucntion : The response data and
 * status.
 */
function checkFlexiSearchOption(matrixOrTdp,status){
	matrixOrTdp='TDP';
	if(matrixOrTdp=='Matrix'){
		//isMatrixSearch	= true;
		//$('.flexibility .flex').fadeOut();
		$('#FlexiOption').click();
		var flexibleGrp			= $('input[name="flexible"]');
		if(flexibleGrp){
			flexibleGrp[0].checked=true;
		}
	}else{
		checkFlexiOption();
	}


}
function checkReservationConfiguration()
{
	if(!($('#departCity').val()==$('#destCity').val()) && $('#departCity').val()!='' && $('#destCity').val()!= '') {
		isMatrixSearch	= false;
		var reservationConfigArray={};
		reservationConfigArray["departureCity"] =$('#departCity').val();
		reservationConfigArray["destinationCity"]= $('#destCity').val();
		$.ajaxSetup ({
			cache: false

		});

		var url = relPath+conLan+"/flightSearch!checkReservationConfiguration.action";
		// $.get(url,argument,callbackFunction,returnType) is the Syntax.
		$.get(url,reservationConfigArray,filterFlexiSearchOption,'text');
	}
}
/**
 * This method is the callback fucntion configured in the $.get() method. This
 * will be called by the Ajax framework, if the ajax call returned a success. By
 * default two values are passed into this fucntion : The response data and
 * status.
 */
function filterFlexiSearchOption(matrixOrTdp,status){
	matrixOrTdp='TDP';
	if(matrixOrTdp=='Matrix'){
		//isMatrixSearch	= true;
		//$('.flexibility .flex').fadeOut();
		$('#FlexiOption').click();
		var flexibleGrp			= $('input[name="flexible"]');
		if(flexibleGrp){
			flexibleGrp[0].checked=true;
		}
	}else{
		checkFlexiOption();
	}
	if($.trim($("#destCity").val()).length > 0){
		populateCabinClass();

	}


}
/*
 * Flexi option is shown to the user only if the depart date is atleast 3 days
 * ahead of current date..
 */
function checkFlexiOption() {
	var flexibleGrp			= $('input[name="flexible"]');
	if(flexibleGrp.val()){
		flexibleGrp[0].checked=true;
	}
	if(isMatrixSearch){
	//	$('.flexibility .flex').fadeOut();
		$('#FlexiOption').click();
		return;
	}
	var curntDte	= new Date();
	//var dateArray 	= $('#dateDepart').val().split('-');
	departDate=new Date($('#departDay').val()+' '+$('#departMonthYear').val().split('-')[0]+ ' '+$('#departMonthYear').val().split('-')[1]);
	//var departDate = new Date('20'+dateArray[1].split(' ')[1],getMonthIndex(dateArray[1].split(' ')[0]),dateArray[0]);
	departDate.setHours(0);
	departDate.setMinutes(0);
	departDate.setSeconds(0);
	curntDte.setHours(0);
	curntDte.setMinutes(0);
	curntDte.setSeconds(0);
     try {
		parseDDate=$.datepicker.parseDate('dd-M-yy', departDate);

		// Populate the formatted field
		popFormattedDates(parseDDate,'depart');

	} catch (e){
		$('#departDay_link, #departMonthYear_link').addClass('invalid');
		error = true
	}

	try {
		parseRDate=$.datepicker.parseDate('dd-M-yy', curntDte);

		// Populate the formatted field
		popFormattedDates(parseRDate,'return');

	} catch (e){
		$('#destDay_link, #returnMonthYear_link').addClass('invalid');
		error = true
	}
	var difference = (departDate.getTime() - curntDte.getTime())/(1000*60*60*24);
	difference	= Math.round(difference);
	if ( difference < 3 ) {
		$('.flexibility .flex').fadeOut();
		$('#FlexiOption').click();
	}else{
		$('.flexibility .flex').fadeIn();
	}


}

function populateSplashCabinClass(isFirstClassEnabled){
	 var inputArray={};
	 if(isFirstClassEnabled =='Y'){
	 inputArray["isFirstClassEnabled"] = true;
	 }else{
	 inputArray["isFirstClassEnabled"] = false;
	 }
	 inputArray["localeSeltd"] = locale;
	 inputArray["country"] = country;

	 $.ajaxSetup ({
			cache: false
		});

	 var url = relPath+"/splashPageAction!populateSplashCabinClassList.action";
	 $("#cabinClassLinkSelect").html(ajax_load_leisure).load(url,parameterArray,toLinkSelect);
}


function populateCabinClass(){
	 var parameterArray={};
	 parameterArray["departureCity"] = document.forms[0].departCity.value;
	 parameterArray["destinationCity"]= document.forms[0].destCity.value;
	 $.ajaxSetup ({
			cache: false
		});
	 var url = relPath+conLan+"/flightSearch!populateHomeCabinClassList.action";
	 //$("#cabinClassLinkSelect").html(ajax_load_leisure).load(url,parameterArray,toLinkSelect);
	 $.get(url,parameterArray,createCabinFromXML,'xml');
}


function createCabinFromXML(xmlData,status)
{

	var cabinClassArr = new Array();
	var tempArray = new Array();
	if(status=="success"){

		$('#flightClass').html('');

		$(xmlData).find('cabinClass').each(function(){

			var departureCountryArray = [];

			depcountryList = $(this).text();
					departureCountryArray= depcountryList.split(',');

			depCountry = departureCountryArray[1];

			var tempArray = new Array();
			tempArray['text'] = departureCountryArray[0];
			tempArray['value'] = $(this).attr('value');
			cabinClassArr.push(tempArray);
            cabinClass=departureCountryArray[0];
			cabinCode=$(this).attr('value');
			// ipad fix
			if (iDevice){

			    $('#flightClass').append('<option value="'+cabinCode+'">'+cabinClass+'</option>');
			} else {
		$("#flightClass").linkselect("replaceOptions", cabinClassArr);
		    }

		});


	}

}
 function toLinkSelect(data,status){
	 if(status=="success"){
	 comboExclude=['#departCity','#destCity',"#IBoardPoint","#statusDepartCity","#statusDestCity"];
	 linkselectExclude=['#adultPop','#childPop','#infantPop','#checkInMethod','#departDay','#departMonthYear','#destDay','#returnMonthYear','#flightClass'];
	 styleSelects(comboExclude,linkselectExclude);
	 }
 }

function changeLang(langCode){
$('#selLanguage').val(langCode);
var selectedCnt = $('#selectedCountryDiv').html();
var langSelected = langCode;
var conLanForHome = "/"+selectedCnt.toLowerCase()+"/"+langSelected.toLowerCase();
if(document.getElementById('chkRemember').checked){
	isCookieEnabled = "true";
	//clearing cookie
	setCookie('rememberLocale','',-1,'/');
	setCookie('rememberLang','',-1,'/');
	//setting new cookie
	setCookie('rememberLocale',selectedCnt,11,'/');
	setCookie('rememberLang',langSelected,11,'/');
}else{
	isCookieEnabled = "false";
	//clearing cookie
	setCookie('rememberLocale','',-1,'/');
	setCookie('rememberLang','',-1,'/');
}
document.forms[0].action = relPath+conLanForHome+"/home!loadCountryLanguage.action?request_locale="+langSelected+"_"+selectedCnt+"&splashLocale="+langSelected+"&splashCntry="+selectedCnt+"&isCookieEnabled="+isCookieEnabled+"&langChanged=true";
document.forms[0].submit();
}

function forgetMyLoc(){
	$('#rememberDiv').attr('style','display:block');
	$('#chkRemember').removeAttr('checked');
	$('.remember span').addClass("un");
	$('#savedID').css('display','none');
//clearing cookie
	setCookie('rememberLocale','',-1,'/');
	setCookie('rememberLang','',-1,'/');
$('#forgetDiv').attr('style','display:none');
	clearCookieSession();
}
$('#chkRemember').click( function(){
if(document.getElementById('chkRemember').checked){
	document.getElementById('rememberDiv').style.display='none';
	$('#savedID').css('display','block');
	document.getElementById('forgetDiv').style.display='block';
	}
	});
function checkCookie()
{
	var isCookie = false;
	rememberLocale=getCookie('rememberLocale');
	rememberLang=getCookie('rememberLang');
if (rememberLocale!=null && rememberLocale!="" && rememberLang!=null && rememberLang!="")
  {
	//document.forms[0].txtLocation_hide.value = rememberLocale;
	// document.forms[0].language.value = rememberLang;
	// document.getElementById('dropLang').value = rememberLang;   //need to check the issue when uncommented
	isCookie = true;
  }
else
  {
	isCookie = false;
  }
return isCookie;
}

function setCookie(c_name,value,expiremonths,path)
{
var exdate=new Date();
exdate.setMonth(exdate.getMonth()+expiremonths);
// var pathString = ((path == null) ? "" : ("; path=" + path));
document.cookie=c_name+ "=" +escape(value)+ ((path == null) ? "" : ("; path=" + path))+
((expiremonths==null) ? "" : ";expires="+exdate.toUTCString());
}

function getCookie(c_name)
{
var cValue="",tmpArray;
if (document.cookie.length>0)
  {
tmpArray = document.cookie.split(";");

for(var i=0;i<tmpArray.length;i++){
if(tmpArray[i].indexOf(c_name+"=") == 1){
cValue = tmpArray[i].split("=");
}
}
  if (cValue.length>0)
  {
   return cValue[1];
  }
//return "";
}return "";
}
/*
 * This method is used to load the Global Emergency notice.
 * This should be called on page load
 */
 function populateGlobalNotice()
 {
	$.ajaxSetup ({
		cache: false
	});
	var globalNoticeLang={};
	globalNoticeLang["localeSeltd"] = locale;
	var url = relPath+"/splashPageAction!loadGlobalEmergencyXmlFile.action";
	$.get(url,globalNoticeLang,getGlobalNotice,'xml');
 }

 function getGlobalNotice(globalNotice,status)
 {
	 if(status=="success") {
		$(globalNotice).find('notice').each(function(){
				$("#globalNoticeDesc").html($(this).text());
				//$("#globalNoticeUrl").html($(this).attr('value'));
				document.forms[0].globalNoticeUrl.value = $(this).attr('value');
				$("#globalnoticediv").show();
		});
	}
 }

/*
 * This method will be called when the user clicks the 'more info'
 * tab for getting the details of the Global Emergency Notice.
 */
 function cmsPageForGlobalNotice()
 {
 	var cmsUrlForGlobalNotice = document.forms[0].globalNoticeUrl.value;

  /*
   * modifying the code assuming that the user will enter the correct url after rewriting for the news page in the below format say,
   * For pages inside ZA folder: http://www.flysaa.com/$country/$language/emergency/test.html (rewriten url)
   * For pages outside ZA folder: http://test.flysaa.com/cms/common/test.html(exact url)
   */
 	if(cmsUrlForGlobalNotice != null && $.trim(cmsUrlForGlobalNotice).length > 0) {

 		if (cmsUrlForGlobalNotice.indexOf("$") >= 0){
 			countrySel = document.getElementById('selcountry').innerHTML.toLowerCase();
 			//langSel = document.getElementById('selectedLanguage').innerHTML.toLowerCase();
 			cmsUrlForGlobalNotice=cmsUrlForGlobalNotice.replace("$country", countrySel);
 			cmsUrlForGlobalNotice=cmsUrlForGlobalNotice.replace("$language", locale);
 		    window.open(cmsUrlForGlobalNotice,"_self");
 		}else{
 			 window.open(cmsUrlForGlobalNotice,"_self");
 		}
 	} else {
 		window.location.href = "#";
 	}
 }


/*
 * This method is used to load the Local Emergency notice.
 * This should be called on page load
 */
 function populateLocalNotice()
 {
	$.ajaxSetup ({
		cache: false
	});
	var localNoticeArr={};
	localNoticeArr["localeSeltd"] = locale;
	localNoticeArr["localNoticeCntry"] = $('#user_location').text().toUpperCase();
	var url = relPath+"/splashPageAction!loadLocalEmergencyXmlFile.action";
	$.get(url,localNoticeArr,getLocalNotice,'xml');
 }

 function getLocalNotice(localNotice,status)
 {
	 if(status=="success") {
		$(localNotice).find('notice').each(function(){
				$("#localNoticeDesc").html($(this).text());
				//$("#globalNoticeUrl").html($(this).attr('value'));
				document.forms[0].localNoticeUrl.value = $(this).attr('value');
				$("#localnoticediv").show();
		});
	}
 }

/*
 * This method will be called when the user clicks the 'more info'
 * tab for getting the details of the Local Emergency Notice.
 */
 function cmsPageForLocalNotice()
 {
 	var cmsUrlForLocalNotice = document.forms[0].localNoticeUrl.value;

 	/*
 	   * modifying the code assuming that the user will enter the correct url after rewriting for the news page in the below format say,
 	   * For pages inside ZA folder: http://www.flysaa.com/$country/$language/emergency/test.html (rewriten url)
 	   * For pages outside ZA folder: http://test.flysaa.com/cms/common/test.html(exact url)
 	   */
 	if(cmsUrlForLocalNotice != null && $.trim(cmsUrlForLocalNotice).length > 0) {

 	 		if (cmsUrlForLocalNotice.indexOf("$") >= 0){
 	 			countrySel = document.getElementById('selcountry').innerHTML.toLowerCase();
 	 			//langSel = document.getElementById('selectedLanguage').innerHTML.toLowerCase();
 	 			cmsUrlForLocalNotice=cmsUrlForLocalNotice.replace("$country", countrySel);
 	 			cmsUrlForLocalNotice=cmsUrlForLocalNotice.replace("$language", locale);
 	 		    window.open(cmsUrlForLocalNotice,"_self");
 	 		}else{
 	 			 window.open(cmsUrlForLocalNotice,"_self");
 	 		}
 	} else {
 		window.location.href = "#";
 	}
  }



// From flysaa_common.js

function getKeyCode(event){
	var keyCode = event.keyCode ? event.keyCode :
		event.charCode ? event.charCode :
			event.which ? event.which : void 0;
	return keyCode;
}

/*
 * Override alert function for showing modalwindow based popup for alert mesages
 */
function alert(msg){
	modalShow('scriptAlert',document.getElementById('msgAlertDiv').innerHTML,msg);
}
/*
 * This Array is used For internationalization of validation messages in JS..
 * Required properties are read and pushed to this array such that they can be
 * accessed form js files..
 */
var propsArray= new Array();
/*
 * This method can be used to get the localized messages in javascript.This
 * method takes 2 parameters.. 1. defaultMessage - message to be used if
 * localized message is not available 2. keyIndex - index of the message in
 * propsArray
 */
function getLocalizedMessage(defaultMessage,keyIndex){

	if(propsArray[keyIndex]){
		//return propsArray[keyIndex]+'\n';
		return propsArray[keyIndex]+'<br />';
	}else{
		//return defaultMessage+'\n';
		return defaultMessage+'<br />';
	}

}

/**
 * This method is added for the Snoopy Information action
 */
function viewSnoopy()
{
	var url=relPath+conLan+"/viewSnoopy!snoopy.action";
	window.open(url,'subWin','height=800,width=1000,resizable=yes,scrollbars=yes');
}

function goToVoyHelpDesk()
{
	document.forms[0].action="/splashPageAction!goToVoyHelpDesk.action";
	document.forms[0].submit();
}

function loadHomeLangXML(){
	$.ajaxSetup ({
		cache: false
	});
	var localeArray={};
	localeArray["localeSeltd"] = $('#selectedLocaleDiv').text();
	var url = relPath+"/splashPageAction!loadLangXmlFile.action";

	$.get(url,localeArray,getHomeLangXmlValues,'xml');
}

function getHomeLangXmlValues(XMLResponse,status)
{
	if(status=="success") {
		parseHomeLangXMLResponse(XMLResponse);

	}else {
		 alert("No matching region found.");
	}
}
function parseHomeLangXMLResponse(xml){
	var selectedLang = $('#selectedLocaleDiv').html();
	var langCode = '', langDesc = '';
	var selLang = document.forms[0].selLanguage.value;
	//$('#selLanguage').removeClass('javascript');
	$('.regionLang ul').html('');
	$(xml).find('language').each(

			function(){
				langDesc = $(this).text();
				 langCode = $(this).attr('value');
				if(selLang!=langCode.toLowerCase()) {
					$('.regionLang ul').append('<li><a  onclick="changeLang('+ "'"+langCode+"'" +');" href="#'+langCode.toLowerCase()+'">' + langDesc + '</a></li>');
			}
			}
			);

	}
function updateCheckOutDay(value){$('#checkoutDay').linkselect('val',value)};
function updateCheckoutMonthYear(value){$('#checkoutMonthYear').linkselect('val',value)};
function updateDropOffDay(value){$('#dropoffDay').linkselect('val',value)};
function updateDropOffMonthYear(value){$('#dropoffMonthYear').linkselect('val',value)};
function updateReturnDay(value){$('#destDay').linkselect('val',value)};
function updateReturnMonthYear(value){$('#returnMonthYear').linkselect('val',value)};

	// delay given so that the change event is completed before the hidden field value is set
function callSetUpdatedDates() {
	setTimeout("setUpdatedDates()", 2);
}