// HOME









$(document).ready(function () {
























    var dropOffDayNew = parseInt(leadingZeroDay) + 2;
    var dropOffDayVal = '0' + dropOffDayNew;
    if (dropOffDayVal.length > 2) {
        dropOffDayVal = dropOffDayVal.substring(1);
    }
    !iDevice && $('#checkinDay').linkselect('val', leadingZeroDay);
    !iDevice && $('#checkoutDay').linkselect('val', leadingZeroDay);
    !iDevice && $('#pickDay').linkselect('val', leadingZeroDay);
    !iDevice && $('#dropoffDay').linkselect('val', dropOffDayVal);




    //TODO this is no longer needed
    $('#mb .linkselectLink').click(function (e) {
        e.stopPropagation();
        if (!$(this).hasClass('.linkselectLinkOpen')) {
            $($(this).attr('href')).linkselect('open')
        }
    });



















    $('#mb .linkselectLink').click(function (e) {
        e.stopPropagation()
    });








    //$('#midMenu .last').html('<a class="button essentialInfo"><span>Essential Info</span></a>');
    $('.essentialInfo').live('click', function (e) {
        e.stopPropagation();
        l = 20 + $(this).offset().left + ($(this).width() * 0.5) - ($('.essInfoWrap').width() * 0.5);
        t = $(this).offset().top + 26;
        $('.essInfoWrap').slideToggle(100).offset({top: t, left: l});
    });












    $('.dropLoc').click(function (e) {
        e.stopPropagation();
        !$.browser.msie &&
        $('#LoL').slideToggle(100);
        $.browser.msie &&
        $('#LoL').toggle();
        $(this).toggleClass('selected');
        $('.essInfoWrap').hide();
        $('.linkselectLink').blur()
    });









    










    $('#pickupLoc').siblings('input').autocomplete({
        select: function (event, ui) {
            select = $('#pickupLoc').siblings('select');
            if (!ui.item) {
                var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"), valid = false;
                select.children("option").each(function () {
                    if ($(this).text().match(matcher)) {
                        this.selected = valid = true;
                        return false;
                    }
                });
                if (!valid) {
                    if (Modernizr.input.placeholder) {
                        $("#pickupLoc").val('');
                        $("#pickupLoc").siblings('input').val($("#pickupHeaderId").html())
                    } else {
                        $("#pickupLoc").val('');
                        $("#pickupLoc").siblings('input').val($("#pickupHeaderId").html());
                    }
                    $(this).data("autocomplete").term = "";
                    return false;
                }
            }
            $('#pickupLoc').val(ui.item.option.value);
            $('#pickupLoc').siblings('input').val($(this).val());
            $('#dropoffLoc').val($('#pickupLoc').val());
            $('#dropoffLoc').siblings('input').val($(this).val());
        }
    });
    $('#dropoffLoc').siblings('input').autocomplete({
        select: function (event, ui) {
            select = $('#dropoffLoc').siblings('select');
            if (!ui.item) {
                var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"), valid = false;
                select.children("option").each(function () {
                    if ($(this).text().match(matcher)) {
                        this.selected = valid = true;
                        return false;
                    }
                });
                if (!valid) {
                    if (Modernizr.input.placeholder) {
                        $(this).val($('#pickupLoc').siblings('input').val());
                        select.val($('#pickupLoc').siblings('input').val());
                    } else {
                        if ($('#pickupLoc').val()) {
                            $(this).val($('#pickupLoc').siblings('input').val());
                            select.val($('#pickupLoc').siblings('input').val());
                        } else {
                            $(this).val('');
                            $("#dropoffLoc").siblings('input').val($("#dropoffHeaderId").html());
                        }
                    }

                    $(this).data("autocomplete").term = "";
                    return false;
                }
            }
            $('#dropoffLoc').val(ui.item.option.value);
            $('#dropoffLoc').siblings('input').val($(this).val());
        }
    });










//loadCarCountriesXML(locale);

    /*$.ajax({url:"xml/locales_"+locale+".xml",dataType:"xml",asunc:'true',success:function(xml){
     $('#carCountry').html('');country=$("#countryResHeaderId").html();code='';
     $('#carCountry').append('<option value="' + code + '">' + country + '</option>');
     $(xml).find('country').each(function(){
     country=$(this).text();code=$(this).attr('value');
     $('#carCountry').append('<option value="'+code+'">'+country+'</option>');});}});*/
//calling the Global Emergency Notice

});





















//TODO refactor this to a shared function call
function validateDates(id) {

    var flag = false;



    //for hotels
// commented the below code to avoide script error - USVD 2984612
    /*	if($('#checkinMonthYear').val()!=" "){
     var departDateArray = $('#checkinMonthYear').val().split('-');
     var finalDepartMonth = departDateArray[1].substring(departDateArray[1].length, 2);
     chkInDate=$('#checkinDay').val()+"-"+departDateArray[0]+" " +finalDepartMonth;
     }
     if($('#checkoutMonthYear').val()!=" "){
     var returnDateArray = $('#checkoutMonthYear').val().split('-');
     var finalReturnMonth = returnDateArray[1].substring(returnDateArray[1].length, 2);
     chkOutDate=$('#checkoutDay').val()+"-"+returnDateArray[0]+" " +finalReturnMonth;
     }*/

    //for cars

    if ($('#pickMonthYear').val() != " ") {
        var departDateArray = $('#pickMonthYear').val().split('-');
        var finalDepartMonth = departDateArray[1].substring(departDateArray[1].length, 2);
        pickupDate = $('#pickDay').val() + "-" + departDateArray[0] + " " + finalDepartMonth;
    }
    if ($('#dropoffMonthYear').val() != " ") {
        var returnDateArray = $('#dropoffMonthYear').val().split('-');
        var finalReturnMonth = returnDateArray[1].substring(returnDateArray[1].length, 2);
        dropoffDate = $('#dropoffDay').val() + "-" + returnDateArray[0] + " " + finalReturnMonth;
    }


    if (id == 'cars') {
        if (!isValidDate(pickupDate, dateFormt)) {
            flag = true;
        }
        if (!isValidDate(dropoffDate, dateFormt)) {
            flag = true;
        }

        if (compareWithCurrentDate(pickupDate, dateFormt) < 0) {
            flag = true;
        }

        if (compareWithCurrentDate(dropoffDate, dateFormt) < 0 || compareDateStrings(pickupDate, dropoffDate, dateFormt) < 0) {
            flag = true;
        }

    }
    else if (id == 'hotels') {
        //validateAndCompareDates($dateIn, $dateOut);
    }
    return flag;
}










































//TODO Is this still needed???
$(window).load(function () {
    $('#mb .loadingIndicator,#voyager .loadingIndicator').fadeOut();
    !Modernizr.input.placeholder && $('input').each(function () {
        a = $(this).attr('title');
        if (!(a == undefined) && (a.length > 0)) {
            $(this).css('color', 'darkGrey');
            $(this).val(a);
            $(this).focus(function () {
                $(this).css('color', '#444');
                if ($(this).val().length == 0 || $(this).val() == $(this).attr('title')) {
                    $(this).val('')
                }
            });
            $(this).blur(function () {
                if ($(this).val().length == 0) {
                    $(this).css('color', 'darkGrey');
                    $(this).val($(this).attr('title'))
                }
            });
        }
    });
});
























































































































































//var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
$(document).ready(function () {

    if ($('#selcountry').length > 0) {
        conLan = "/" + document.getElementById('selcountry').innerHTML.toLowerCase() + "/" + document.getElementById('selectedLanguage').innerHTML.toLowerCase();
    }
    var flexibleGrp = $('input[name="flexible"]');
    /*
     * flexible to Exact
     */
    if (flexibleGrp.val() && !checkRadioSelection(flexibleGrp)) {
        flexibleGrp[0].checked = true;
    }

    //$(":radio[name='flexible'][id='radExact']").attr('checked', 'true');
    $('#bb').tabs({
        fx: {opacity: 'toggle', duration: 'fast'}, select: function () {
            $('#bb .linkselectLink').blur()
        }
    });





    popMonths();




    !iDevice &&
    $('#adultPop').linkselect({
        change: function (li, value) {
            updatePassengers('adults', value)
        }
    });
    !iDevice &&
    $('#childPop').linkselect({
        change: function (li, value) {
            updatePassengers('children', value);
        }
    });
    !iDevice &&
    $('#infantPop').linkselect({
        change: function (li, value) {
            updatePassengers('infants', value);
        }
    });
    !iDevice &&
    updatePassengers();

    $('#departCity').siblings('input').autocomplete({

        source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($("#departCity option").map(function () {
                var text = $(this).text();
                var code = $(this).val();
                if ((matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text) && ($.trim(code).length > 0)))
                    return {
                        label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
                        value: text,
                        option: this
                    };
            }));
        },

        change: function (event, ui) {
            var select = $('#departCity').siblings('select');
            if (!ui.item) {
                var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i");
                var valid = false;
                select.children("option").each(function () {
                    if ($(this).text().match(matcher)) {
                        this.selected = valid = true;
                        return false;
                    }
                });
                if (!valid) {
                    // remove invalid value, as it didn't match anything
                    if (Modernizr.input.placeholder) {
                        $("#departCity").val('');
                        $("#departCity").siblings('input').val($("#departCityId").html());
                    }
                    else {
                        $("#departCity").val('');
                        $("#departCity").siblings('input').val($("#departCityId").html());
                    }
                    $(this).data("autocomplete").term = "";
                    return false;
                }
            }

            //checkReservationConfiguration();
            populateDestinationcities(document.getElementsByName('departCity')[0].value);

        }
    });

    $('#destCity').siblings('input').autocomplete({

        source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($("#destCity option").map(function () {
                var text = $(this).text();
                var code = $(this).val();
                if ((matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text) && ($.trim(code).length > 0)))
                    return {
                        label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
                        value: text,
                        option: this
                    };
            }));
        },

        select: function (event, ui) {
            select = $('#destCity').siblings('select');
            if (!ui.item) {
                var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"),
                    valid = false;
                select.children("option").each(function () {
                    if ($(this).text().match(matcher)) {
                        this.selected = valid = true;
                        return false;
                    }
                });
                if (!valid) {
                    // remove invalid value, as it didn't match anything
                    if (Modernizr.input.placeholder) {
                        $("#destCity").val('');
                        $("#destCity").siblings('input').val($("#departCityId").html());
                    }
                    else {
                        $("#destCity").val('');
                        $("#destCity").siblings('input').val($("#destinCityId").html());
                    }
                    $(this).data("autocomplete").term = "";
                    return false;
                }
            }
            $("#destCity").val(ui.item.option.value);


            checkReservationConfiguration();
        }
    });

    $('#IBoardPoint').siblings('input').autocomplete({

        source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($("#IBoardPoint option").map(function () {
                var text = $(this).text();
                var code = $(this).val();
                if ((matcher.test(code) && ($.trim(code).length > 0)) || (matcher.test(text) && ($.trim(code).length > 0)))
                    return {
                        label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
                        value: text,
                        option: this
                    };
            }));
        }
    });






//loadFlightStatusXML(locale);

    //TODO this is a repeat
    today = new Date();
    //TODO this is a repeat
    leadingZeroDay = ('0' + today.getDate()).slice(-2);

    if (!iDevice) {
        $('#departDay').linkselect({
            change: function (li, value) {
                checkFlexiDateConf();
                updateReturnDay(value);
                callSetUpdatedDates();
            }
        });
        $('#departMonthYear').linkselect({
            change: function (li, value) {
                checkFlexiDateConf();
                updateReturnMonthYear(value);
                callSetUpdatedDates();
            }
        });
        $('#destDay').linkselect({
            change: function (li, value) {
                checkFlexiDateConf();
                callSetUpdatedDates();
            }
        });
        $('#returnMonthYear').linkselect({
            change: function (li, value) {
                checkFlexiDateConf();
                callSetUpdatedDates();
            }
        });
        $('#departDay').linkselect('val', leadingZeroDay);
        $('#destDay').linkselect('val', leadingZeroDay);

    } else {
        $('#departDay').change(function () {
            callSetUpdatedDates();
            checkFlexiDateConf();
        });
        $('#departMonthYear').change(function (li, value) {
            callSetUpdatedDates();
            checkFlexiDateConf();
        });
        $('#destDay').change(function (li, value) {
            callSetUpdatedDates();
            checkFlexiDateConf();
        });
        $('#returnMonthYear').change(function (li, value) {
            callSetUpdatedDates();
            checkFlexiDateConf();
        });
        $('#departDay').val(today.getDate());
        $('#destDay').val(today.getDate());
    }


    // Instantiate the Booking Block's datepicker objects
    $('#bb .calendar').datepicker({
        firstDay: 1,
        minDate: 0,
        maxDate: '+1y',
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function (date) {

            // Upon selecting a date, update the relative drop downs
            updateDates($(this), date);

            // If the date selected was the departure date..
            if ($(this).parent().attr('id') == 'leaveCal') {

                // Update the drop downs of the returning date too!
                // Makes sure impossible dates don't happen by accident, basicaly.
                updateDates($('#returnCal .calendar'), date);
            }

            // Hide the datepicker that the user just clicked on
            $(this).fadeOut();

            // If the return date box is active...
            if ($(this).parent().attr('id') == 'leaveCal' && $('#chkReturn').is(':checked')) {

                // Show the return date calendar after half a second
                // The timeout is there to wait for the other calendar to fade out
                setTimeout("$('#returnCal').click()", 500);
            }
        }
    });

    $('#bb .ui-datepicker').click(function (e) {
        e.stopPropagation();
    });
    $('#bb .calendar').click(function (e) {
        e.stopPropagation();
        $(this).fadeOut();
    });
//$('#bb .calWrap').click(function(e){e.stopPropagation();closePopups();date=new Date();box=$('#'+$(this).parents('.blueBox').attr('id'));dayLinkSelect=box.find('.inputDay').children('a').attr('href');monthYearLinkSelect=box.find('.inputMonthYear').children('a').attr('href');d=new Date($(dayLinkSelect).linkselect('val')+' '+$(monthYearLinkSelect).linkselect('val'));$(this).children('.calendar').datepicker('setDate',d);$(this).children('.calendar:not(.disabled)').fadeIn();});

    // Some complicated logic & relative selection in here.
    // Note: this is basically a duplication of $('#mb .calWrap').click();
    $('#bb .calWrap').click(function (e) {

        // This checks whether the Return Date block is disabled
        if ($(this).hasClass('disabled')) return false;

        // Don't auto-close this datepicker
        e.stopPropagation();

        // Close all the other modals though
        closePopups();
        date = new Date();
        box = $('#' + $(this).parents('.blueBox').attr('id'));
        dayLinkSelect = box.find('.inputDay').children('a').attr('href');
        monthYearLinkSelect = box.find('.inputMonthYear').children('a').attr('href');

        // This fixes a bug where IE wouldn't find the correct value for the
        // 'href' attribute (above)
        if ($.browser.msie) {
            d = dayLinkSelect.split('#');
            m = monthYearLinkSelect.split('#');
            dayLinkSelect = '#' + d[1];
            monthYearLinkSelect = '#' + m[1];

        }

        // Force the string to be sure IE understands what we're doing... *sigh*
        // This spits out something like: "14-Jan-2012"
        update = $(dayLinkSelect).linkselect('val').toString() + '-' + $(monthYearLinkSelect).linkselect('val').toString();

        // Check if this if the date is a *valid* date string.
        // Fail if it is not (eg: 31 February 2011) - yay for the drop downs.
        try {
            calDate = $.datepicker.parseDate('dd-M-yy', update);
        } catch (e) {
            alert(propsArray['bookingBlock_14']);
            // the datepicker doesnt parse when M is localized,eg: Dezember,Marz,Mai in deutsch.
            return false;
        }

        // Here, we know the date is valid! So..
        // Update the datepicker.
        $(this).children('.calendar').datepicker('setDate', calDate);

        // ..and fade it in!
        $(this).children('.calendar:not(.disabled)').fadeIn()

    });

    setUpdatedDates();
    $('#bb .linkselectLink').click(function (e) {
        e.stopPropagation();
        if (!$(this).hasClass('.linkselectLinkOpen')) {
            $($(this).attr('href'), true).linkselect('open')
        }
    });
    $('#FlightChecked').iphoneStyle();
// Apply the jQuery UI button
    $('#FlightChecked').button();
    (!$.browser.msie) && $('#chkReturn, #checkInAddPax').each(function () {
        isChecked = $(this).is(':checked') ? ' checked' : '';
        $(this).wrap('<label for="' + $(this).attr('id') + '" class="checkStyle' + isChecked + '">').hide();
    });
    $('.checkStyle input').click(function () {
        isChecked = $(this).is(':checked');
        $(this).parents('label').toggleClass('checked', isChecked);
    });
    $('.chkContainer input').change(function () {
        isChecked = $(this).is(':checked');
        if (isChecked) {
            first = $('#NoFlight');
            last = $('#FlightNo');
        } else {
            first = $('#FlightNo');
            last = $('#NoFlight');
        }
        first.fadeOut(100, function () {
            last.fadeIn(100)
        })
    });


    $('#bb .depCity').css({width: 270 - $('a.mulCity').width() - 30 + 'px'});
    $('#bb .depCity input').css({width: $('.depCity').width() - 6 + 'px'});













//loadDepartureXML(locale,country);
    loadHomePageXmls(locale, country);








    $('#flights .button.submit').click(function () {
        var cnty = $('#user_location').text().toUpperCase();	//added for passing request_locale
        var valMessage = "";
        $('#departCity').siblings('input').removeClass('invalid');
        $('#destCity').siblings('input').removeClass('invalid');
        error = false;
        if (!$('#departCity').val().length) {
            $('#departCity').siblings('input').addClass('invalid');
            valMessage = getLocalizedMessage("Departure city is required.", "bookingBlock_1");
            error = true;
        }
        if (!$('#destCity').val().length) {
            $('#destCity').siblings('input').addClass('invalid');
            valMessage += getLocalizedMessage("Destination city is required.", "bookingBlock_2");
            error = true;
        }
        if ($('#departCity').val().length && $('#destCity').val().length && ($('#departCity').val() == $('#destCity').val())) {
            valMessage += getLocalizedMessage("Departure and Destination can not be the same.", "bookingBlock_3");
            error = true;
        }
        if ($("#adultPop").val() < $("#infantPop").val()) {
            valMessage += getLocalizedMessage("The adult-infant ratio should be 1:1.", "bookingBlock_5");
            error = true;
        }
        if (parseInt($("#adultPop").val()) + parseInt($("#infantPop").val()) + parseInt($("#childPop").val()) > 9) {
            valMessage += getLocalizedMessage("The total number of adults,children and infants must not exceed 9. Please consider splitting your group or complete the <a href='groupBooking.action'> Groups Request Form </a>", "bookingBlock_6");
            error = true;
        }
        $("#adultCount").val($("#adultPop").val());
        $("#childCount").val($("#childPop").val());
        $("#infantCount").val($("#infantPop").val());
        var dep = $('#departCity').val();
        var dest = $('#destCity').val();
        if ($('input:checkbox[name=chkReturn]').attr('checked')) {
            $("#tripType").val('R');
        }
        else {
            $("#tripType").val('O');
        }
        var tripType = $("#tripType").val();
        if (!$('input[name="flexible"]').val().length) {
            error = true;
        }
        //flexibleGrp=$('input[name="flexible"]').val();
        flexibleGrp = $('input:radio[name=flexible]:checked').val();
        document.forms[0].flexible.value = flexibleGrp;

        setUpdatedDates();
        var fromDate = $("#dateDepart").val();
        var toDate = $("#dateDest").val();
        var dateFormt = 'dd-M y';
        if ('R' == tripType && toDate == "") {
            valMessage += getLocalizedMessage("Please enter a valid travel return date.", "bookingBlock_17");
            error = true;
        }
        /*if('R'==tripType && (!$('#returnMonthYear').val().length && !$('#destDay').val().length ) ){
         valMessage+=getLocalizedMessage("Please enter a valid travel return date.","bookingBlock_12");
         error = true;
         }*/

        if (compareWithCurrentDate(fromDate, dateFormt) < 0) {
            valMessage += getLocalizedMessage("Please take a future departure date for travel.", "bookingBlock_10");
            error = true;
        }
        if (!isValidDate(fromDate, dateFormt)) {
            valMessage += getLocalizedMessage("Invalid Departure Date", "bookingBlock_18");
            error = true;
        }
        if ('R' == tripType && !isValidDate(toDate, dateFormt)) {
            valMessage += getLocalizedMessage("Please enter a valid travel return date.", "bookingBlock_17");
            error = true;
        }

        if ('R' == tripType) {
            if (compareWithCurrentDate(toDate, dateFormt) < 0 || compareDateStrings(fromDate, toDate, dateFormt) < 0) {
                valMessage += getLocalizedMessage("Please enter a valid travel return date.", "bookingBlock_12");
                error = true;
            }
        }
        if (compareWithCurrentDate(toDate, dateFormt) < 0) {
            valMessage += getLocalizedMessage("Please take a future return date for travel.", "bookingBlock_10");
            error = true;
        }
        if (error) {
            alert(valMessage);
            return false;
        }
        // the language selected by the user is set to the field 'selectedLang'
        document.forms[0].selectedLang.value = document.getElementById('selectedLocaleDiv').innerHTML;
        document.forms[0].preferredClass.value = $('#flightClass').val();
        showProcessing();
        var conLanFlightSearch = "/" + depCountry.toLowerCase() + "/" + locale.toLowerCase();
        cnty = depCountry.toUpperCase();
        $("#sessionCountry").val(depCountry);

        if ($('#profile').val() == 'profile') {

            if (this.id == 'link_bookByMiles')
                document.forms[0].action = relPath + conLanFlightSearch + '/flexPricerFlightSearch!milesBooking.action';
            else if (this.id == 'link_bookByCash')
                document.forms[0].action = relPath + conLanFlightSearch + '/flexPricerFlightSearch!getProfileAvailability.action';
            document.forms[0].submit();

        }
        else {
            document.forms[0].action = relPath + conLanFlightSearch + '/flexPricerFlightSearch!flexPricerFlightAvailability.action?request_locale=' + locale + "_" + cnty;
            document.forms[0].submit();

        }
    });








    $('#schedWrap span').live('click', function () {
        $('#schedWrap').slideUp(function () {
            $(this).html('')
        })
    })
});
$(window).load(function () {
    $('#bb .loadingIndicator').fadeOut();
});


























/*function updateDates(c,d){
 date=new Date(d);
 box=$('#'+c.parents('.blueBox').attr('id'));
 day=box.find('.inputDay').children('a').attr('href');
 monthYear=box.find('.inputMonthYear').children('a').attr('href');
 newDay='"'+date.getDate()+'"';
 newMonthYear='"'+months[date.getMonth()]+'-'+date.getFullYear()+'"';
 $(day).linkselect('val',newDay);
 $(monthYear).linkselect('val',newMonthYear);
 }*/


















function updatePassengers(element, value) {
    // This function will only work on linkselect elements,
    // so restrict mobile browsers.
    if (iDevice) return false;
    maxPax = 9;
    adults = parseInt($('#adultPop').linkselect('val'));
    children = parseInt($('#childPop').linkselect('val'));
    infants = parseInt($('#infantPop').linkselect('val'));
    switch (element) {
        case'adults':
            adults = parseInt(value);
            break;
        case'children':
            children = parseInt(value);
            break;
        case'infants':
            infants = parseInt(value);
            break;
        default:
            break;
    }
    passengers = [parseInt(adults), parseInt(children), parseInt(infants)];
    total = passengers[0] + passengers[1] + passengers[2];
    remainder = maxPax - total;
    $('#adultPop_list li').each(function (i) {
            $(this).removeClass('hidePassenger');
            selectable = remainder + adults;
            if (i > selectable)$(this).addClass('hidePassenger');
        }
    );
    $('#childPop_list li').each(function (i) {
        $(this).removeClass('hidePassenger');
        selectable = remainder + children;
        if (i > selectable)$(this).addClass('hidePassenger');
    });
    $('#infantPop_list li').each(function (i) {
        $(this).removeClass('hidePassenger');
        selectable = remainder + infants;
        if (i > selectable)$(this).addClass('hidePassenger');
    });
    if (total == 9) {
// Highlight the link!
        $('#flights .morePaxLink').addClass('highlight')
    }
    else {
        // un-Highlight the link!
        $('#flights .morePaxLink').removeClass('highlight')
    }
}







//function popMonths(){var dates=[],values=[],now=new Date(),ob=new String(); for(i=1;i<13;i++){values[i]=months[now.getMonth()]+'-'+now.getFullYear();dates[i]=months[now.getMonth()]+' '+now.getFullYear();ob='<option value="'+values[i]+'">'+dates[i]+'</option>';$('#departMonthYear').append(ob);$('#returnMonthYear').append(ob);if(now.getDate()==31)now.setDate(28);now.setMonth(now.getMonth()+1);}}
function popMonths() {
    var dates = [],
        values = [],
        now = new Date(),
        ob = new String();

    // This fixes a bug
    // => If the current day was over 28, February would be skipped, and would disappear from the drop downs!
    //    Meaning, people couldn't book flights in February
    now.setDate(28);

    // Loop through 13 months. (eg: December 2011 => December 2012)
    for (i = 0; i < 13; i++) {
        values[i] = months[now.getMonth()] + '-' + now.getFullYear();
        dates[i] = months[now.getMonth()] + ' ' + now.getFullYear();
        ob = '<option value="' + values[i] + '">' + dates[i] + '</option>';
        $('#departMonthYear').append(ob);
        $('#returnMonthYear').append(ob);
        now.setMonth(now.getMonth() + 1)
    }
}







function showProcessing() {
    $("#blocks,#warnings, #midMenu nav, .regionCountry, .regionLang").css("display", "none");
    setTimeout(function () {
        $("#processing").css("display", "block");
    }, 50); //modified as per BA's request for animating the processing image on flysaa.com in IE
}





















/*// work around done for voyager login. Need to modify the method
 function checkVoyagerLogin(voyagerId,pin){


 $.ajaxSetup ({
 cache: false
 });
 var detailsArray={};
 detailsArray["voyagerId"] = voyagerId;
 detailsArray["pin"] = pin;
 var url = relPath+"/splashPageAction!voyagerLogin.action";

 $.get(url,detailsArray,getVoyagerData,'text');


 }

 function getVoyagerData(Response,respStatus)
 {
 if(Response != "" && respStatus=="success") {
 var voyDetails=Response.split(',');

 $('#voyager').addClass('loggedin');
 $('#memName span').html(voyDetails[1]+voyDetails[2]);
 $('#memNum').html('<span></span>'+voyDetails[3]);
 $('#memMiles').html('<span></span>'+voyDetails[4]);
 $('#memStatus').html('<span></span>'+voyDetails[5]);
 $('#memStatus').addClass(voyDetails[5].toLowerCase());
 $('#voyager .in').css('display','block');
 $('#voyager .out').css('display','none');

 }else {
 alert("No records found");
 $('#mb #voyagerId, #mb #pin').addClass('invalid');
 }
 $('#mb .loadingIndicator').fadeOut();

 }*/









function setUpdatedDates() {

// Departure date
    dep = [];
    dep['day'] = $('#departDay').val();
    dep['month'] = $('#departMonthYear').val().split('-')[0];
    dep['year'] = $('#departMonthYear').val().split('-')[1];

    log(dep);

    // Get the final two digits of the year
    dep['year'] = dep['year'].substr(-2);

    // Get the 'en' month
    i = $.inArray(dep['month'], eval('months_' + locale));
    dep['month'] = monthArrays['en'][i];

    //
    // Set the correct value of the departure date
    try {
        $('#dateDepart').val(dep['day'] + "-" + dep['month'] + " " + dep['year']);
    } catch (e) {
        $('#dateDepart').val('');
        $('#dateDest').val('');
    }










    // Return date
    ret = [];
    ret['day'] = $('#destDay').val();
    ret['month'] = $('#returnMonthYear').val().split('-')[0];
    ret['year'] = $('#returnMonthYear').val().split('-')[1];

    log(ret);

    // Get the final two digits of the year
    ret['year'] = ret['year'].substr(-2);

    // Get the 'en' month
    i = $.inArray(ret['month'], eval('months_' + locale));
    ret['month'] = monthArrays['en'][i];

    //
    // Set the correct value of the return date
    try {
        $('#dateDest').val(ret['day'] + "-" + ret['month'] + " " + ret['year']);
    } catch (e) {
        $('#dateDest').val('');
    }










    // to update the hidden variables for carHireDep/DestMonthYear

    carDep = [];

    carDep['month'] = $('#pickMonthYear').val().split('-')[0];
    carDep['year'] = $('#pickMonthYear').val().split('-')[1];

    // Get the 'en' month
    i = $.inArray(carDep['month'], eval('months_' + locale));
    carDep['month'] = monthArrays['en'][i];

    try {
        $('#carHireDepMonthYear').val(carDep['month'] + "-" + carDep['year']);
    } catch (e) {
        $('#carHireDepMonthYear').val('');
        $('#carHireDestMonthYear').val('');
    }

    carRet = [];

    carRet['month'] = $('#dropoffMonthYear').val().split('-')[0];
    carRet['year'] = $('#dropoffMonthYear').val().split('-')[1];

    log(carRet);

    // Get the 'en' month
    i = $.inArray(carRet['month'], eval('months_' + locale));
    carRet['month'] = monthArrays['en'][i];

    try {
        $('#carHireDestMonthYear').val(carRet['month'] + "-" + carRet['year']);
    } catch (e) {
        $('#carHireDestMonthYear').val('');
    }




}


























































// delay given so that the change event is completed before the hidden field value is set
function callSetUpdatedDates() {
    setTimeout("setUpdatedDates()", 2);
}