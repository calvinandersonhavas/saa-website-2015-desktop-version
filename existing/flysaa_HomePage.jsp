<%--   
/*
 * Project	 		: FlySAA - CustomerJourneys
 * Module Name		: Homepage
 * File Name		: flysaa_homepage.jsp
 * Date				: 25-Nov-2009
 * Author(s)		: Srinivasan G
 * Revision History :
 * Date   			 Version 		Author    Purpose
 * 25-Nov-2009		  0.1			A-3151	  Added	
 * 15-July-2011		  0.2			A-3790	  Updated
 * 
 */
--%>

<!doctype html>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="<%=session.getAttribute("SELECTED_LOCALE").toString().toLowerCase()%>"> 
<!--<![endif]-->

<jsp:include page="flysaa_NewHeader.jsp" flush="true"></jsp:include>
<body class="en home">
<jsp:include page="flysaa_ContainerTags.jsp" flush="true"></jsp:include>
<s:form cssClass="siteWrap" >

<s:set var='sessCnt'><s:property value='#session.SELECTED_COUNTRY'/></s:set>
<s:hidden name="selectedLang"></s:hidden>
<s:hidden id="preferredClass" name="preferredClass"></s:hidden>
<!-- Helpers -->
	<s:set var="DAYList" value="%{@com.flysaa.common.util.CommonUtils@CALENDAR_DAYS}" />
	<div id="display_language"><%=session.getAttribute("SELECTED_LOCALE")%></div>
	<div id="user_location"><%=session.getAttribute("SELECTED_COUNTRY")%></div>
	<div id="modal"></div>
	<div class="essInfoWrap"></div>
	<!--/Helpers -->
    <div id="long_location" style="display: none"></div>
    <!--IE Notice-->
	<div class="ieNotice">
	<s:text name="label.home.ieNotice"></s:text>
        <div class="clr"></div> 
        <span class="gray"><s:text name="label.home.recommend"></s:text> &nbsp;&nbsp;
        <a href="http://www.google.com/chrome" rel="external"><span class="browsers chrome"><s:text name="label.home.chrome"></s:text></span></a>
        <a href="http://www.microsoft.com/ie" rel="external"><span class="browsers ie"><s:text name="label.home.ie"></s:text></span></a>
        <a href="http://www.mozilla.com/firefox" rel="external"><span class="browsers firefox"><s:text name="label.home.firefox"></s:text></span></a>
        <a href="http://www.apple.com/safari" rel="external"><span class="browsers safari"><s:text name="label.home.safari"></s:text></span></a>
		</span>
	</div>
    <!--/IE Notice-->
	<s:set var="departCity"><s:text name="label.departureCity"/></s:set>
    <s:set var="destinCity"><s:text name="label.destinationCity"/></s:set>
    <s:set var="destinationHeaderTxt" id="destinationHeaderTxt"><s:text name="label.destinationCity"/></s:set>
    <s:set var="hotelHeaderTxt" id="hotelHeaderTxt"><s:text name="label.hotel.header"/></s:set>
	<s:set var="pickupHeaderTxt" id="pickupHeaderTxt"><s:text name="label.carhire.pickuppoint"/></s:set>
	<s:set var="dropoffHeaderTxt" id="dropoffHeaderTxt"><s:text name="label.carhire.dropoffpoint"/></s:set>
	<s:set var="countryResHeaderTxt" id="countryResHeaderTxt"><s:text name="label.booking.common.residence"/></s:set>
	<s:set var="voyagerNumErrorTxt" id="voyagerNumErrorTxt"><s:text name="error.voyager.forgotpassword.voyagerNumber.required"/></s:set>
	<s:set var="voyagerNumErrorTxt1" id="voyagerNumErrorTxt1"><s:text name="error.voyager.forgotpassword.voyagerNumber1.required"/></s:set>
	<s:set var="voyagerNumErrorTxt2" id="voyagerNumErrorTxt2"><s:text name="error.voyager.forgotpassword.voyagerNumber2.required"/></s:set>
	<s:set var="voyagerNumErrorTxt3" id="voyagerNumErrorTxt3"><s:text name="error.voyager.forgotpassword.voyagerNumber3.required"/></s:set>
	<s:set var="voyagerEmailErrorTxt" id="voyagerEmailErrorTxt"><s:text name="error.voyager.forgotpassword.email.required"/></s:set>
	<s:set var="voyagerEmailErrorTxt1" id="voyagerEmailErrorTxt1"><s:text name="error.voyager.forgotpassword.email1.required"/></s:set>
	<div id="pickupHeaderId" style="display: none"><s:property value="#pickupHeaderTxt"/></div>
	<div id="dropoffHeaderId" style="display: none"><s:property value="#dropoffHeaderTxt"/></div>
	<div id="hotelHeaderTxtId" style="display: none"><s:property value="#hotelHeaderTxt"/></div>
	<div id="destinationHeaderId" style="display: none"><s:property value="#destinationHeaderTxt"/></div>
	
	<div id="countryResHeaderId" style="display: none"><s:property value="#countryResHeaderTxt"/></div>
	<div id="voyagerNumErrorId" style="display: none"><s:property value="#voyagerNumErrorTxt"/></div>
	<div id="voyagerNumErrorId1" style="display: none"><s:property value="#voyagerNumErrorTxt1"/></div>
	<div id="voyagerNumErrorId2" style="display: none"><s:property value="#voyagerNumErrorTxt2"/></div>
	<div id="voyagerNumErrorId3" style="display: none"><s:property value="#voyagerNumErrorTxt3"/></div>	
	<div id="voyagerEmailErrorId" style="display: none"><s:property value="#voyagerEmailErrorTxt"/></div>
	<div id="voyagerEmailErrorId1" style="display: none"><s:property value="#voyagerEmailErrorTxt1"/></div>
	<div id="departCityId" style="display: none"><s:property value="#departCity"/></div>
	<div id="destinCityId" style="display: none"><s:property value="#destinCity"/></div>	
	<div id="statusDepartCityId" style="display: none"><s:property value="#statusDepartCity"/></div>	
	<div id="statusDestCityId" style="display: none"><s:property value="#statusDestCity"/></div>	
	
	<s:hidden name="calendarSearchFlag" value="false"/>

	
<%
	String country=(String)request.getSession().getAttribute("SELECTED_COUNTRY");
	String language=(String)request.getSession().getAttribute("SELECTED_LOCALE");
	String conLan="/"+country.toLowerCase()+"/"+language.toLowerCase();
	 String relPath=request.getContextPath();
	 String serverName = request.getServerName();
%>
	

	
<div class="wrap">
	<div class="container">
		<jsp:include page="flysaa_NewTopbanner.jsp" flush="true"></jsp:include>
		
		<section id="blocks" class="siteWidth clearfix">
			
			<section id="bb" class="block">
			
				<div class="loadingIndicator"></div>
				<article class="infoPopup"><header></header><div></div></article>
				
				<ul class="tabs">
				<li class="tab-left"><a href="#flights"><span><s:text name="tab.label.flights"></s:text></span></a></li>
					<li class="sep"></li>
					<li><a href="#checkin"><span><s:text name="label.checkin.checkin"></s:text></span></a></li>
					<li class="sep"></li>
					<li class="tab-right"><a href="#status" ><span><s:text name="label.checkFltSts.fltStatus"></s:text></span>
					</a></li>
					
				</ul>
				
				<section id="flights">
				
					<div class="row topMost clearfix">
					
						<a href="<s:url value='/multiCityFlightSearch!loadMultiCityPage.action'/>" class="button silver floatRight mulCity"><span><s:text name="label.multiCity"></s:text></span></a>
					
						<div class="depCity combobox">
							
							
							
							<s:select list="%{''}" id="departCity" name="departCity" headerKey="" title="%{#departCity}"></s:select>
							
							
						</div>
						
			  </div>
					
					<div class="row clearfix">
					
						<div class="desCity combobox" id="destinationLinkSelect">
						
							<s:select list="%{''}" id="destCity" name="destCity" headerKey="" title="%{#destinCity}"></s:select>
							
							
						</div>
										
												
			  </div>
					
					<div id="leaveDate" class="blueBox">
					
						<div class="boxTitle clearfix">
							<label><s:text name="label.leaving"></s:text></label>
							<div id="leaveCal" class="calWrap">
								<div class="calendar" ><span class="title"><s:text name="label.leaving"/></span></div>
							</div>
						</div>
						
						<div class="leaveD">
							<div class="inputDay">
							<s:if test="#DAYList!=null">
								<select id="departDay">
									<s:iterator begin="1" value="#DAYList" var="dayList">
										<option value="<s:property value='#dayList'/>"><s:property value='#dayList'/></option>
									</s:iterator>
								</select>
							</s:if>
					<s:else><s:select id="departDay" list="%{''}" /></s:else>
							</div>
							
							<div class="inputMonthYear">
								
								<select id="departMonthYear"  ></select>
								  <s:hidden id="dateDepart"  name="fromDate"/>
  <s:hidden id="carHireDepMonthYear"  name="carHireDepMonthYear"/>
  <s:hidden id="carHireDestMonthYear"  name="carHireDestMonthYear"/>
							</div>
							
						</div>
						
					</div>
					
					<div id="returnDate" class="blueBox">
					
						<div class="boxTitle clearfix">
							
							<input id="chkReturn" type="checkbox" name="chkReturn" checked />
							<s:hidden id="tripType"  name="tripType"/>
							<label for="chkReturn"><s:text name="label.returning"></s:text></label>
							<div id="returnCal" class="calWrap">
								<div class="calendar"><span class="title"><s:text name="label.returning"/></span></div>
							</div>
						</div>
						
						<div class="returnD">
							<div class="inputDay">
						<s:if test="#DAYList!=null">
							<select id="destDay">
								<s:iterator begin="1" value="#DAYList" var="dayList">	
									<option value="<s:property value='#dayList'/>"><s:property value='#dayList'/></option>
							    </s:iterator>
						    </select>
					    </s:if>
					<s:else><s:select id="destDay" list="%{''}" /></s:else>	
					    
					    </div>			
					
						<div class="inputMonthYear">
								
								<select id="returnMonthYear" ></select>
								  <s:hidden id="dateDest"  name="toDate"/>
							</div>
							
						</div>
						
					</div>
					
					<div class="clr"></div>
					
						
					
					<div class="flexibility clearfix" id="flexible">
					
						<div class="exact">
					<label for="radExact">
					<input id="radExact" type="radio" name="flexible"  value="false" />
					<!--<s:radio name="flexible" id="radExact"   list="#{'false':''}" />-->
					
					<s:text name="label.exact.dates"></s:text></label>							
							
						</div>
						
						<div class="flex" id="FlexiOption">
					<label for= "radFlexible">
					<input id="radFlexible" type="radio" name="flexible"  value="true"/>
						<!--<s:radio name="flexible"  id="radFlexible" list="#{'true':''}" />
						
						--><s:text name="label.three.days"></s:text> </label>
						</div>
						
					</div>
					
					<div class="row amountPax clearfix">
					
						<div class="adults">
						
							
						<select id="adultPop" name="adultPop">
						<option value="1">1 <s:text name="label.adult"/></option>
					    <s:iterator begin="2" end="9" step="1" var="adtSize">	
						<option value="<s:property value='#adtSize'/>"><s:property value='#adtSize'/> <s:text name="label.adults"/>	</option>
					    </s:iterator>
					    </select>
						<s:hidden id="adultCount"  name="adultCount"/>
						</div>
						
						<div class="children">
						
						<select id="childPop" name="childPop">
						<option value="0"><s:text name="label.children"/>	</option>
						<option value="1">1 <s:text name="label.paxdetails.payment.enter.paxdetails.child"/>	</option>
					    <s:iterator begin="2" end="8" step="1" var="cnnSize">	
						<option value="<s:property value='#cnnSize'/>"><s:property value='#cnnSize'/> <s:text name="label.children"/>	</option>
					    </s:iterator>		
					     </select><p class="floatRight"><s:text name="label.children.ageInfo" /></p>
						<s:hidden id="childCount"  name="childCount"/>	
				
						</div>
						<div class="infants">
								
						<select id="infantPop" name="infantPop">
						<option value="0"><s:text name="label.infants"/>	</option>
						<option value="1">1 <s:text name="label.paxdetails.payment.enter.paxdetails.infant"/>	</option>
					    <s:iterator begin="2" end="4" step="1" var="infSize">	
						<option value="<s:property value='#infSize'/>"><s:property value='#infSize'/> <s:text name="label.infants"/>	</option>
					    </s:iterator>		
					    </select><p class="floatRight"><s:text name="label.infants.ageInfo" /> </p>
						<s:hidden id="infantCount"  name="infantCount"/>
						
						</div>
						
					</div>
					<!-- This block of code is for 3347562 -->
					<div class="row morePax">
						<s:if test="#sessCnt == 'US' || #sessCnt == 'CA'">
							<a href="<s:url value='http://flysaausa.com/groups'/>" target="_blank" class="morePaxLink"><s:text name="label.morethan.nine.people" /></a>
						</s:if>
						<s:else>
							<a href="<s:url value='/groupBooking.action'/>" class="morePaxLink"><s:text name="label.morethan.nine.people" /></a>
						</s:else>

					</div>
					
					<div class="row cabinClass clearfix">
					
						<label for="flightClass"><s:text name="label.cabinClass"/></label>
						
						<div class="cabinClassSelect" id="cabinClassLinkSelect">
						
						
						<s:if test="cabinClassList">
<s:select id="flightClass" name="preferredCabinClass" list="cabinClassList" listKey="cabinValue" listValue="cabinDescription"/>
							</s:if>
							<s:else>
<s:select id="flightClass" name="preferredCabinClass" list="#session.SESSION_CABIN_CLASS_LIST" listKey="cabinValue" listValue="cabinDescription" />
							</s:else>
					 </div>
						
						<a class="iconInfo">
							<span class="head"><s:text name="tooltip.leisure.travelclass.header" /></span>
							<span class="inner">
								<s:text name="tooltip.leisure.travelclass.content" />
							</span>
						</a>
					
					</div>
					
					<div class="row promoCode clearfix">
					
						<label for="promoCodeId"><s:text name="label.promotionCode"/></label>
						<div class="promoCodeInput">
							<input name="promoCode" id="promoCodeId" />
						</div>
						
						<a class="iconInfo">
							<span class="head"><s:text name="tooltip.leisure.promocode.header" /></span>
							<span class="inner"><s:text name="tooltip.leisure.promocode.content" /></span>
						</a>
					
					</div>
					
					<div class="actions">
						<a class="button submit greenUgly floatRight" ><span><s:text name="button.label.book"/></span></a>
					</div>
					
				</section>
				<!--/flights -->
				
			<section id="checkin">
<div>
<input  type="radio" name="fcheckin"  value="SaaCheckin" onclick="changeCheckin(this)" checked="checked"/><s:text name="label.saa.checkin"></s:text> &nbsp;
<input  type="radio" name="fcheckin"  value="MangoCheckin" onclick="changeCheckin(this)"/><s:text name="label.mango.checkin"></s:text>
</div>

<div id="saa_div">

<div class="tabGradient">
<div class="mobi"> <s:text name="label.checkin.mobile.info"></s:text>
 </div>
 </div>

<div class="row clearfix">
<label class="label"><s:text name="label.checkin.sur.name"></s:text></label>
<input id="ISurname" />
</div>
<div>&nbsp;</div>

<div class="row clearfix">
<label class="label"><s:text name="label.form.identification"></s:text></label>
<div class="formOfID">
<select id="checkInMethod" name="checkInMethod">
<option value="PNR"><s:text name="label.booking.refernce"></s:text></option>
<option value="FFSA"><s:text name="label.southAfrican.frequent.flyer.number"></s:text></option>
<option value="ETKT"><s:text name="label.electronic.ticket.number"></s:text></option>
</select>
</div>
<div>&nbsp;</div>
<a class="iconInfo">
<span class="head"><s:text name="label.form.identification"></s:text></span>
<span class="inner"><s:text name="label.iconinfo.form.identification"></s:text><br>
<strong>-<s:text name="label.booking.refernce"></s:text></strong>,<br>
<strong>-<s:text name="label.electronic.ticket.number"></s:text></strong><br>
<strong>-<s:text name="label.southAfrican.frequent.flyer.number"></s:text></strong>.

</span>

</a>

</div>
<div id="id_pnr" class="row idOpt clearfix">

<label class="label"><s:text name="label.booking.refernce"></s:text></label>
<a class="iconInfo">
<span class="head"><s:text name="label.booking.refernce"></s:text></span>
<span class="inner"><s:text name="label.iconinfo.booking.refernce"></s:text></span>
</a>

</div>

<div id="id_ffsa" class="row idOpt clearfix">

<label ><s:text name="label.southAfrican.frequent.flyer.number"></s:text></label>

<a class="iconInfo">

<span class="head"><s:text name="label.southAfrican.frequent.flyer.number"></s:text></span>
<span class="inner"><s:text name="label.iconinfo.southAfrican.frequent.flyer.number"></s:text></span>

</a>

</div>

<div id="id_etkt" class="row idOpt clearfix">


<label ><s:text name="label.electronic.ticket.number"></s:text></label>

<a class="iconInfo">
<!--<span class="head">Electronic Ticket</span>-->
<span class="head"><s:text name="label.electronic.ticket.number"></s:text></span>
<span class="inner"><s:text name="label.iconinfo.electronic.ticket.number"></s:text></span>

</a>

</div>

<input id="IIdentification" />
<div class="clr"></div>

<div class="row checkInAddPax clearfix">
<table><tr>
<td><label for="checkInAddPax" class="checkStyle"><input id="checkInAddPax" type="checkbox" /></label></td>
<td>&nbsp;</td>
<td><label for="checkInAddPax"><s:text name="label.checkin.additional.passengers"></s:text></label></td>
</tr></table>


</div>

<div class="actions">
<a class="button submit greenUgly floatRight"><span><s:text name="label.availability.continue"></s:text></span></a>
<a class="button reset  brownUgly floatLeft"><span><s:text name="label.booking.common.button.reset"></s:text></span></a>

</div>
</div>

<div id="mango_div" style="display: none;">

<div class="tabGradient">
<div class="mobi"> <s:text name="label.checkin.mobile.info"></s:text>
 </div>
 </div>

<div class="row clearfix">
<label class="label"><s:text name="label.checkin.sur.name"></s:text></label>
<input id="ISurnameM" />
</div>
<div>&nbsp;</div>

<div class="row clearfix">
<label class="label"><s:text name="label.form.identification"></s:text></label>
<div class="etktnumberM">
<label class="label"><s:text name="label.electronic.ticket.number"></s:text></label>
<input type="text" id="etktnumberM"/>
</div>

<a class="iconInfo"> <span class="head"><s:text
		name="label.form.identification"></s:text></span> <span class="inner"><s:text
		name="label.iconinfo.electronic.ticket.number"></s:text><br>
	</span> </a></div>





<div class="clr"></div>

<div>
<table><tr>
<td><label for="checkInAddPaxM" class="checkStyle"><input id="checkInAddPaxM" type="checkbox" /></label></td>
<td>&nbsp;</td>
<td><label for="checkInAddPaxM"><s:text name="label.checkin.additional.passengers"></s:text></label></td>
</tr></table>


</div>

<div class="actions">
<a class="button submit greenUgly floatRight"><span><s:text name="label.availability.continue"></s:text></span></a>
<a class="button reset  brownUgly floatLeft"><span><s:text name="label.booking.common.button.reset"></s:text></span></a>

</div></div>
</section>
<!--/checkin -->
	
			
				<section id="status">
				
					<div class="tabGradient clearfix">
						<label class="q" for="FlightChecked"><s:text name="label.checkFltSts.know.fltNum"/></label>
						<div class="slider">
							<input id="FlightChecked" name="FlightChecked" type="checkbox" checked />
						</div>
					</div>
					
					<div id="FlightNo">
						<div class="col">
							<label for="carrierCode"><s:text name="label.checkFltSts.carrierCode"/></label>
<input id="carrierCode" name="carrierCode" maxlength = "2" value="SA" disabled="disabled"/>
						</div>
						<div class="col">
							<label for="flightNumber"><s:text name="label.checkFltSts.flightNUmber"/></label>
							<input id="flightNumber" name="flightNumber" maxlength = "5" />
						</div>
					</div>
					
					<div id="NoFlight">
                    <div class="row clearfix combobox">
								<s:select list="%{''}"  id="statusDepartCity" title="%{#departCity}" name="departureCity"></s:select>
							</div>
                    <div class="row clearfix combobox">
                    <s:select list="%{''}"  id="statusDestCity"  title="%{#destinCity}" name="destinationCity"></s:select>
						</div>
					</div>
					
					<div class="depDate row">
					
						<label for="fromDateFLT"><s:text name="label.checkFltSts.depDate"/></label>
						<select class="DepDate" id="fromDateFLT">
							<option value="-1"><s:text name="label.checkFlt.yesterday"/></option>
							<option value="0"><s:text name="label.checkFlt.today"/></option>
							<option value="1"><s:text name="label.checkFlt.tomorrow"/></option>
						</select>

					</div>
					
					<div class="actions">
<a class="button orange floatLeft reset"><span><s:text name="label.booking.common.button.reset"/></span></a>
<a class="button orange floatLeft" href="<%=conLan%>/flightStatus/flight_status_other_airlines.html"><span><s:text name="label.checkFltSts.airlines"/></span></a>
<a class="button green floatRight submit"><span><s:text name="label.availability.continue"/></span></a>
					</div>


					
					<section class="flightSchedule">
					
						<h1 class="ir"><s:text name="label.checkFltSts.flightSchedule"/></h1>
						
						<p><s:text name="label.checkFltSts.flightSchedule.track"/></p>
						
						<div class="statusButtons">
							<a href="<%=conLan%>/Documents/flightschedules/Timetable.pdf"  rel="external" class="button orange download"><span><s:text name="label.checkFltSts.pdf"/></span></a>
							<%-- removed for cnt-1352 
							<a href="http://download.goldenware.com/airlines/sa/sa-traveldesk-en.exe"  rel="external" class="button orange download"><span><s:text name="label.checkFltSts.desktopApp"/></span></a>
							--%>
							<a class="button blue route"><span><s:text name="label.checkFltSts.online"/></span></a>
						</div>
						
					</section>
					
				</section>
				
				<!--/status--> 
				
			</section>
			<!--/#bb-->
			
			<div id="midSpace">
			
				<section id="adSpaceImage"> 
					<!-- Ad Server ad loaded here --> 
				</section>
				<!--/#specialsBlockTop-->
				
				
				<section id="specials">
					
				</section>
				<!-- /#specials -->
			
			</div>
			<!-- /#midSpace -->
			
			<section id="mb" class="block">
			
				<div class="loadingIndicator"></div>
				
			
				<ul class="tabs">
					<li class="tab-left"><a href="#voyagerWrap"><span><s:text name="label.navi.Voyager"></s:text></span></a></li>
					<li class="sep"></li>
<li><a onclick="goToHotelsURL();"><span><s:text name="tab.label.accommodation"></s:text></span></a></li>
					<li class="sep"></li>
					<li class="tab-right"><a href="#cars"><span><s:text name="tab.label.carHire"></s:text></span></a></li>
				</ul>
			
				<!--Voyager Logged Out State-->
				<section id="voyagerWrap">
				
					<iframe src="https://<%=serverName%><%=conLan%>/home!voyagerTabAction.action?country=<%=country%>&language=<%=language%>" frameborder="0" height="307" width="281"></iframe>
					
					
				</section><!--/#voyager-->
				
<!--

			
				<section id="hotels">
					
					<div class="row topMost combobox clearfix">					
					
						<s:select id="hotelDestination"  list="%{''}" title="%{#hotelHeaderTxt}" />				
						
						<input id="txtHotelDes_loc" name="txtHotelDes_loc"  type="hidden" />
					</div>
					
					<div id="hotelDateIn" class="blueBox">
				
						<div class="boxTitle clearfix"><s:text name="label.booking.accomodation.checkin.date"> </s:text>
							<div class="calWrap">
								<div class="calendar"><span class="title"><s:text name="label.booking.accomodation.checkin.date"/></span></div>
							</div>
						</div>
						
						<div class="returnD">
							<div class="inputDay">								
									<s:if test="#DAYList!=null"><s:select id="checkinDay" list="%{#DAYList}" /></s:if>
								<s:else><s:select id="checkinDay" list="%{''}" /></s:else>
							
							
								</div>
							<div class="inputMonthYear">
								<select id="checkinMonthYear">
								</select>
							</div>
						</div>
					
					</div>
					
					<div id="hotelDateOut" class="blueBox">
					
						<div class="boxTitle clearfix"><s:text name="label.booking.accomodation.checkout.date"/>
							<div class="calWrap">
								<div class="calendar"><span class="title"><s:text name="label.booking.accomodation.checkout.date"/></span></div>
							</div>
						</div>
						
						<div class="returnD">
							<div class="inputDay">								
							<s:if test="#DAYList!=null"><s:select id="checkoutDay" list="%{#DAYList}" /></s:if>
								<s:else><s:select id="checkoutDay" list="%{''}" /></s:else>
							</div>
							<div class="inputMonthYear">
								<select id="checkoutMonthYear">
								</select>
							
							</div>
						</div>
						
					</div>
					
					<div class="clr"></div>
				
					<div class="row amountPax clearfix">					
						<div class="adults">							
							<select id="hotelAdults" name="hotelAdults">
						<option value="1">1 <s:text name="label.adult"/></option>
					    <s:iterator begin="2" end="9" step="1" var="adtSize">	
						<option value="<s:property value='#adtSize'/>"><s:property value='#adtSize'/> <s:text name="label.adults"/>	</option>
					    </s:iterator>	
					    </select>
						</div>
						
						<div class="children">				
							
							<select id="hotelChildren" name="hotelChildren">
						<option value="0"><s:text name="label.children"/>	</option>
						<option value="1">1 <s:text name="label.paxdetails.payment.enter.paxdetails.child"/>	</option>
					    <s:iterator begin="2" end="8" step="1" var="cnnSize">	
						<option value="<s:property value='#cnnSize'/>"><s:property value='#cnnSize'/> <s:text name="label.children"/>	</option>
					    </s:iterator>		
					     </select>
						</div>
						
						<div class="infants">							
							
							<select id="hotelInfants" name="hotelInfants">
						<option value="0"><s:text name="label.infants"/>	</option>
						<option value="1">1 <s:text name="label.paxdetails.payment.enter.paxdetails.infant"/>	</option>
					    <s:iterator begin="2" end="4" step="1" var="infSize">	
						<option value="<s:property value='#infSize'/>"><s:property value='#infSize'/> <s:text name="label.infants"/>	</option>
					    </s:iterator>		
					    </select>
						</div>
						
					</div>
					
					<div class="row clearfix">
						<label ><s:text name="label.booking.accomodation.number.rooms"> </s:text></label>
						<div class="numberoffrooms">							
							<select id="numberoffrooms" name="numberoffrooms">						
					    <s:iterator begin="1" end="4" step="1" var="numSize">	
						<option value="<s:property value='#numSize'/>"><s:property value='#numSize'/></option>
					    </s:iterator>		
					    </select>
						</div>
					</div>
				
					<div class="actions">
						<a class="button submit greenUgly floatRight"><span><s:text name="label.availability.booknow"> </s:text></span></a>
						<a class="button reset  brownUgly floatLeft"><span><s:text name="label.booking.common.button.reset"></s:text></span></a>
					</div>
				
					<div class="hotels adSpace"></div>
					
				</section>
-->

				<!--/#hotels-->
	
				<section id="cars">
				
					<div class="row combobox topMost clearfix">
						<s:select id="pickupLoc" list="%{''}" title="%{#pickupHeaderTxt}"   name="pickupLoc"/> 	
						
						<input id="txtpick_loc" name="txtpick_loc" type="hidden" />						
					</div>
					
					<div class="row combobox clearfix">
						<s:select id="dropoffLoc"  list="%{''}" title="%{#dropoffHeaderTxt}"  name="dropoffLoc"/>						
						<input id="txtdrop_loc" name="txtdrop_loc" type="hidden" />
					</div>
					
					<div id="carDateUp" class="blueBox">
					<div class="boxTitle clearfix"> <s:text name="label.carhire.pickupdate"> </s:text><!--  Pick up	-->										
							<div class="calWrap">
								<div class="calendar"><span class="title"><s:text name="label.carhire.pickupdate"/></span></div>
							</div>
						</div>						
						
						<div class="leaveD">
							<div class="inputDay">								
									<s:if test="#DAYList!=null"><s:select id="pickDay" list="%{#DAYList}" /></s:if>
								<s:else><s:select id="pickDay" list="%{''}" /></s:else>								
							
							
					    </div>
							<div class="inputMonthYear">
								<select id="pickMonthYear">
								</select>
							</div>
						</div>
						
						<div class="clr"></div>	
						
						<label>  <s:text name="label.carhire.time"></s:text></label>
						<div class="time">							
<s:select id="pickUpTime" value="{'0900'}" list="#{'0000':'00:00','0030':'00:30','0100':'01:00','0130':'01:30','0200':'02:00','0230':'02:30','0300':'03:00','0330':'03:30','0400':'04:00','0430':'04:30','0500':'05:00','0530':'05:30','0600':'06:00','0630':'06:30','0700':'07:00','0730':'07:30','0800':'08:00','0830':'08:30','0900':'09:00','0930':'09:30','1000':'10:00','1030':'10:30','1100':'11:00','1130':'11:30','1200':'12:00','1230':'12:30','1300':'13:00','1330':'13:30','1400':'14:00','1430':'14:30','1500':'15:00','1530':'15:30','1600':'16:00','1630':'16:30','1700':'17:00','1730':'17:30','1800':'18:00','1830':'18:30','1900':'19:00','1930':'19:30','2000':'20:00','2030':'20:30','2100':'21:00','2130':'21:30','2200':'22:00','2230':'22:30','2300':'23:00','2330':'23:30'}"/>
						</div>
						
					</div>
					
					<div id="carDateOff" class="blueBox">
					
						<div class="boxTitle clearfix"><s:text name="label.carhire.dropoffdate"></s:text><!-- Drop off-->						
							<div class="calWrap">
								<div class="calendar"><span class="title"><s:text name="label.carhire.dropoffdate"/></span></div>
							</div>
						</div>
						
						<div class="returnD clearfix">
							<div class="inputDay">
						
								<s:if test="#DAYList!=null"><s:select id="dropoffDay" list="%{#DAYList}" /></s:if>
								<s:else><s:select id="dropoffDay" list="%{''}" /></s:else>	
							
							</div>
							<div class="inputMonthYear">
								<select id="dropoffMonthYear">
								</select>
							</div>
						</div>
						
						<div class="clr"></div>
						
						<!-- <label for="dropOffTime">Time</label>-->
						<label>  <s:text name="label.carhire.time"></s:text></label>
						<div class="time">							
<s:select id="dropOffTime" value="{'0900'}" list="#{'0000':'00:00','0030':'00:30','0100':'01:00','0130':'01:30','0200':'02:00','0230':'02:30','0300':'03:00','0330':'03:30','0400':'04:00','0430':'04:30','0500':'05:00','0530':'05:30','0600':'06:00','0630':'06:30','0700':'07:00','0730':'07:30','0800':'08:00','0830':'08:30','0900':'09:00','0930':'09:30','1000':'10:00','1030':'10:30','1100':'11:00','1130':'11:30','1200':'12:00','1230':'12:30','1300':'13:00','1330':'13:30','1400':'14:00','1430':'14:30','1500':'15:00','1530':'15:30','1600':'16:00','1630':'16:30','1700':'17:00','1730':'17:30','1800':'18:00','1830':'18:30','1900':'19:00','1930':'19:30','2000':'20:00','2030':'20:30','2100':'21:00','2130':'21:30','2200':'22:00','2230':'22:30','2300':'23:00','2330':'23:30'}"/>
						</div>
						
					</div>
					
					<div class="clr"></div>
					
					<div class="col clearfix">				
						
						<label ><s:text name="label.booking.common.residence"> </s:text></label>
						
						<div class="carCountry combobox">
							<s:select id="carCountry" list="%{''}" title="%{#countryResHeaderTxt}" name="carCountry"/>							
							<input id="txtcountry" name="txtcountry" type="hidden" />
						</div>
						
					</div>
					
					<div class="col clearfix">				
						
						<label ><s:text name="label.booking.common.driverage"> </s:text></label>
						
						
						<div class="carDriverAge">
							<input id="driverAge" maxlength="3"/>
						</div>
						
					</div>
					
					<div class="clr"></div>
					
					<div class="actions">
						<a class="button submit greenUgly floatRight"><span><s:text name="label.availability.booknow"> </s:text></span></a>
						<a class="button reset  brownUgly floatLeft"><span><s:text name="label.booking.common.button.reset"></s:text></span></a>
					</div>
					
<!--<div class="cars adSpace"></div>
--><a style="display: block;" class="cars adSpace" target="_blank" href="http://www.flysaa.com/za/en/manageMyTrip/insurance/travel_insurance.html"></a>

					
				</section>
				<!--/#cars-->
	
			</section>
			<!--/#mb-->
			
		</section>
		<!--/#blocks-->
			
		<div class="clr"></div>
		
		
		<!--iFrame Wrappers-->
		<div id="checkInWrap" class="siteWidth"></div>
		<div id="schedWrap"></div>
		<!--/iFrame Wrappers-->
		
		<div class="clr"></div>
		
		<section id="banners" class="siteWidth clearfix"></section>
		
		<%
        //--- Home page Links for US
       if (country.equalsIgnoreCase("US")||country.equalsIgnoreCase("CA"))
        {
        %>
          <section id="homeLinks" class="siteWidth clearfix"></section>
        <%
        }
        %>

<!-- Alliance Logos -->
<aside id="allianceLogo" class="siteWidth clearfix">
			
	<!--<a class="proudlysa ir" style="cursor: default;">Proudly South African</a>-->
	<a href="http://www.southafrica.net" class="satourism ir" target="_blank">South African Tourism</a>
	<!--<a href="http://www.staralliance.com" class="staralliance fifteenyears ir" target="_blank">Star Alliance</a>-->
	<a class="twentyYear ir" >20 Years of Freedom</a>
	<a class="anniYear ir" >80 year Anniversary</a>
	<!--<a class="saa ir" >South African Airways</a>-->
			
</aside>
		
		<!-- About SAA -->
		<article class="about">
			<header>
				<s:text name="label.home.content.header"></s:text>
			</header>
			<div class="clearfix">
			<s:text name="label.home.content"></s:text>
				
			</div>
		</article>
		
		
	</div>
</div>
<!--/wrap-->

<jsp:include page="flysaa_NewFooter.jsp" flush="true"/>
<script src="<s:url value='/scripts/lib/libraries.js'/>"> </script> 

<script src="<s:url value='/scripts/splashHome/flysaa_home.js'/>"> </script> 
<s:if test="hasActionMessages()">
   <div class="modal" id="alertWarningDiv"> 
   <a href="javascript:;" class="close" onclick="modalClose('alertWarningDiv')"><s:text name="label.popUp.close"/></a>
		<h4><s:text name="label.modal.message"/></h4>
		<div class="inner guests">
			  <s:iterator value="actionMessages"><s:property escape="false"/><br /></s:iterator> 
			  
			  <div class="buttons">
			  	<a href="#" onclick="modalClose('alertWarningDiv')" class="arrowBlue right"><font color='black'><s:text name="label.modal.ok"/></font></a>
			</div>
		</div>
		<div class="bottom"></div>
		
		<script>
		modalShow('alertWarningDiv');
		</script>
   </div>
</s:if> 
<script>	
propsArray['bookingBlock_0']	="<s:text name='error.tripType.required' />";
propsArray['bookingBlock_1']	="<s:text name='error.departure.required' />";
propsArray['bookingBlock_2']	="<s:text name='error.destination.required' />";
propsArray['bookingBlock_3']	="<s:text name='error.same.departureDestinaton' />";
propsArray['bookingBlock_4']	="<s:text name='error.atleastOneAdult.required' />";
propsArray['bookingBlock_5']	="<s:text name='error.invalid.adultInfantRatio' />";
propsArray['bookingBlock_6']	="<s:text name='error.permitedPaxCount.exceeded' />";
propsArray['bookingBlock_7']	="<s:text name='error.classOfTravel.required' />";
propsArray['bookingBlock_8']	="<s:text name='error.fromdate.required' />";
propsArray['bookingBlock_9']	="<s:text name='error.todate.required' />";
propsArray['bookingBlock_10']	="<s:text name='error.date.beforeCurrentDate' />";
propsArray['bookingBlock_11']	="<s:text name='error.advanceBookingPeriod.exceeded' />";
propsArray['bookingBlock_12']	="<s:text name='error.toDate.beforeFromDate' />";
propsArray['bookingBlock_13']	="<s:text name='label.crossSell.properLocation'/>";
propsArray['bookingBlock_14']	="<s:text name='label.crossSell.validDates'/>";
propsArray['bookingBlock_15']	="<s:text name='label.crossSell.validTime'/>";
propsArray['bookingBlock_16']	="<s:text name='label.crossSell.driverAge'/>";
propsArray['bookingBlock_17']	="<s:text name='error.toDate.validDate' />";
propsArray['bookingBlock_18']="<s:text name='error.fromDate.validDate' />";
propsArray['checkin_1']			="<s:text name='label.validate.checkin.mandatory'/>";
propsArray['statusVal_1']		="<s:text name='label.validate.status.carrierCode'/>";
propsArray['statusVal_2']		="<s:text name='label.validate.status.fltNum'/>";
propsArray['statusVal_3']		="<s:text name='label.validate.status.fltNumber'/>";
propsArray['statusVal_4']		="<s:text name='label.validate.status.depCity'/>";
propsArray['statusVal_5']		="<s:text name='label.validate.status.destCity'/>";
propsArray['statusVal_6']		="<s:text name='label.validate.status.depDest'/>";
propsArray['voylogin_usename']	="<s:text name='error.voyager.login.voyagerid.required' />";
propsArray['voylogin_usename2']	="<s:text name='label.validate.voyLogin.number' />";
propsArray['login_usename1']	="<s:text name='error.voyager.login.voyagerid1.required' />";
propsArray['login_usename2']	="<s:text name='error.voyager.login.voyagerid2.required' />";
propsArray['voylogin_pin']		="<s:text name='error.voyager.login.pin.required' />";
propsArray['voylogin_pin2']		="<s:text name='label.validate.voyLogin.pinLength' />";
propsArray['voylogin_pin3']		="<s:text name='label.validate.voyLogin.pinNum' />";
propsArray['hotelDest_1']		="<s:text name='label.validate.hotel.destination' />";
propsArray['cars_1']			="<s:text name='label.validate.cars.driverAge' />";
propsArray['cars_2']			="<s:text name='label.validate.cars.pickup' />";
propsArray['cars_3']			="<s:text name='label.validate.cars.dropoff' />";
propsArray['cars_4']			="<s:text name='label.validate.cars.country' />";
propsArray['voyLogin_usename3']	="<s:text name='label.validate.voyLogin.invalidNum' />";
propsArray['ieVersion_error']	="<s:text name='label.error.ieversion' />";
</script>
</s:form>
</body>

<script>


function setChangedLangToSession(){


<% 
if(null != request.getParameter("langChanged") && "true".equalsIgnoreCase(request.getParameter("langChanged"))){
String sessionLang = (String)session.getAttribute("SELECTED_LOCALE");
session.setAttribute("CHANGED_LOCALE",sessionLang); 
}
%>

}

</script>
</html>