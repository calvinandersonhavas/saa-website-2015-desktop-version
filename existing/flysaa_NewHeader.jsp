
<%@ taglib prefix="s" uri="/struts-tags"%>

<% 
   String path = request.getContextPath();

	String parent= request.getParameter("parent");
	String title= request.getParameter("title");
	String directory= request.getParameter("directory");
	if(parent!=null && directory==null){
		directory="screens";
	}
	//String locale_str="en_ZA";
	String locale_str=(String)request.getSession().getAttribute("CURRENT_LOCALE");
	String language=(String)request.getSession().getAttribute("SELECTED_LOCALE");
	String country=(String)request.getSession().getAttribute("SELECTED_COUNTRY");	
	String contenLang=language.toLowerCase()+"-"+country.toLowerCase();
	
	
	//for populating the meta tag content for the attribute name 'description'
	java.util.ResourceBundle messages=null;
	if(language!=null && "ZH_CN".equalsIgnoreCase(language)){
		 messages = java.util.ResourceBundle.getBundle("Leisure_MessageResources", java.util.Locale.SIMPLIFIED_CHINESE);
	} else if(language!=null){
 		messages = java.util.ResourceBundle.getBundle("Leisure_MessageResources", new 
 		java.util.Locale(language));
	} else {
		messages = java.util.ResourceBundle.getBundle("Leisure_MessageResources", new 
 		java.util.Locale("en"));
	}
	
%>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="content-language" content="<%=contenLang%>">
   <meta http-equiv="X-UA-Compatible" content="IE=10"> 
  <title><s:text name="label.homePage.title"></s:text></title>
  <meta name="description" content="<%=messages.getString("label.meta.description.home.content")%>">
  <meta name="author" content="South African Airways">
  <meta name="viewport" content="width=1050, initial-scale=1.0, maximum-scale=1.4">  
 
  <link rel="shortcut icon" href="<s:url value='/favicon.ico'/>">
  <link rel="apple-touch-icon" href="<s:url value='/images/apple-touch-icon.png'/>">
  <link rel="stylesheet" href="<s:url value='/styles/home.css'/>">

  <script src="<s:url value='/scripts/lib/modernizr-1.7.min.js'/>"></script>

  


	
 
</head>
