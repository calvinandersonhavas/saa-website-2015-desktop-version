
<%
	String country=(String)request.getSession().getAttribute("SELECTED_COUNTRY");
	String language=(String)request.getSession().getAttribute("SELECTED_LOCALE");
	String conLan="/"+country.toLowerCase()+"/"+language.toLowerCase();
	
	String locale_nojs = "en";
	String country_nojs = "za";
		if(language != null){
			locale_nojs = language.toLowerCase();
		}
		if(country != null){
			country_nojs = country.toLowerCase();
		}
		
%>
<%@ taglib prefix="s" uri="/struts-tags"%>
  	<div id="disabler"></div>
	<div id='locale_str' style='display: none;'><%=session.getAttribute("CURRENT_LOCALE")%></div>
	<s:hidden name="countrySeltd"/>
	
	<!-- Session TimeOut Messages Start here -->
	<!--<div class="alertmodal" id="sessionTimerMsg" style="position: fixed; left: 452px; top: 239px;">
        <h4></h4>
        <div class="inner"><div class="clock"></div>
			<button type="button" style="position:relative;border:0;padding:0;cursor:pointer;overflow:visible;font-size:11px;text-align:center;color:#fff;text-transform:uppercase;padding:0 26px 0 0;float:right;background:transparent url(<%=request.getContextPath()%>/images/button-green.png) no-repeat right top" onclick="sessionContinue()" ><span style="position:relative;display:block;white-space:nowrap;padding:9px 5px 0 15px;height:23px;text-shadow:0 1px 2px #226e1f;background:transparent url(<%=request.getContextPath()%>/images/button-green.png) no-repeat left top"><s:text name="label.availability.continue"/></span></button>
			<button type="button" class="brown round" onclick="goToJourneysHomePage()"><span><s:text name="label.goback"/></span></button>
			<div class="clr"></div>
		</div>
        <div class="bottom"></div>
    </div> -->
    <!-- Session TimeOut Messages end here  -->
    
     <!-- Session Expired Start here -->
	<!--<div class="alertmodal" id="sessionExpiredMessage" style="position: fixed; left: 452px; top: 239px;">
        <h4></h4>
        <div class="inner"><div class="clock"></div>
			<button type="button" class="green round" onclick="goToJourneysHomePage()"><span><s:text name="label.goback"/></span></button>
			<div class="clr"></div>
		</div>
        <div class="bottom"></div>
    </div> -->
    <!-- Session Expired end here  -->
    
    	
	
	<!-- Start of Alert-->
<s:if test="hasActionErrors()">
   <div class="modal" id="alertDiv"> 
   		<a href="javascript:;" class="close" onclick="modalClose('alertDiv')"><s:text name="label.popUp.close"/></a>
		<b><h4><s:text name="label.modal.error"/></h4></b>
		<div class="inner guests">
			<div class="guests">
			  <s:iterator value="actionErrors"><s:property escape="false"/><br /></s:iterator> 
			</div>
			
			<div class="buttons">
				<a href="#" onclick="modalClose('alertDiv')" class="arrowBlue right"><font color='black'><s:text name="label.modal.ok"/></font></a>
			</div>
		</div>
		<div class="bottom"></div>
		<script type="text/javascript">
		modalShow('alertDiv');
		</script>
   </div>
</s:if> 


<div id="scriptAlert" class="alertmodal" style="display: none; z-index:59999;">
					
    <a href="javascript:;" class="close" onclick="modalClose('scriptAlert')"><s:text name="label.popUp.close"/></a>

        <h4><s:text name="label.modal.modalBox"/></h4>
        <div class="inner">
								
        </div>

    <div class="bottom"></div>
</div> 


<!-- End of Alert Section-->
	<div id="container"><!-- closed in footer include -->
    		<div id="content" class="clearfix"><!-- Closed in footer include -->
		<header id="siteHeader">
<div id="header" class="siteWidth clearfix" >
				<nav>
					<ul>
						
					   <s:if test="#session.PRODUCT_LIST">
			
			          			<s:iterator value="#session.PRODUCT_LIST">   

			          				<s:if test='productCode!="SKINS" && productCode!="CHARTER"'>
								
								 <s:if test='productCode=="CARG"'>
								  <li >
									 <a href="<%=conLan%>/home!loadCargoHome.action"  >
											<s:property value="productDescription"/>
								     </a>
								  </li>
								 </s:if>
								 <s:elseif test='productCode=="TECH"'>
								 <li>
									 <a href="<%=conLan%>/home!loadTechnicalHome.action" >
											<s:property value="productDescription"/>
									</a>
								</li>
								</s:elseif>
								<s:elseif test='productCode=="FTS" || productCode=="SKINS"'>
								 <li class="navSelected">
 <a href="<%=conLan%>/home.action?request_locale=<%=request.getSession().getAttribute("SELECTED_LOCALE").toString().toLowerCase()%>_<%=request.getSession().getAttribute("SELECTED_COUNTRY").toString().toUpperCase()%>" >
											<s:property value="productDescription"/>
									</a>
								</li>
								</s:elseif>
								<s:elseif test='productCode=="ONBIZ"'>
								 <li>
									 <a href="<%=conLan%>/OnbizLogin!login.secured" >
											<s:property value="productDescription"/>
									</a>
								</li>
								</s:elseif>
								<s:elseif test='productCode=="VOY"'>
								 <li>
									 <a href="<%=conLan%>/voyagerLogin.secured" >
											<s:property value="productDescription"/>
									</a>
								</li>
								</s:elseif>
								<s:else>
								 <li >
  			          					<a href="<s:property value="productUrl"/>" >
			          							<s:property value="productDescription"/>
			          					</a>
			          				</li>
								</s:else>
			          			</s:if>
				
								</s:iterator>
	<li>
<a href="<%=conLan%>/myProfileLogin.action"><span>MyProfile</span></a>
 	 </li>  
			          		</s:if>
          
	
          <li><a href="#"  ></a></li><li><a href="#"  ></a></li><li><a href="#"  ></a></li><li><a href="#"  ></a></li>
<li><a href="#"  ></a></li><li><a href="#"  ></a></li>
          
  <li><a  href="<%=request.getContextPath()%>/cms/ZA/leisure/UseCookies.html" ><s:text name="label.heading.cookies"/></a></li>
			          		
					</ul>
					<s:hidden id="selectedProductIs" name="selectedProductIs" value="%{#session.SELECTED_PRODUCT}"></s:hidden>
				</nav>
				<section class="headerSearch clearfix">
					<input id="searchInput" name="searchInput" class="searchInput floatLeft" placeholder="<s:text name="label.cms.search"/>" title="<s:text name="label.cms.search"/>" onclick="this.value=''" value="<s:text name="label.cms.search"/>"  onkeyup="searchFlysaa_Enter(event)"/>
					<input type="button" class="searchButton ir floatLeft" value="Search" onclick="searchFlysaa_CMS()"/>
				</section>
<script type="text/javascript">
							function searchFlysaa_CMS(){
								var inputString=document.getElementById("searchInput").value;
								var encodedInputString=encodeURIComponent(inputString);
								if (encodedInputString == 'Search'){
								} else {
									window.location.href=relPath+"/cms/commons/flysaa_search.html?searchQuery="+encodedInputString;
								}

							}
							function searchFlysaa_Enter(event){
								
								if(getKeyCode(event)==13){
									 searchFlysaa_CMS();
									}
							}
										</script>
			</div>
		</header>
   	<!-- session expiry msg -->
			<div id="sessionExpiryMsg" style="display: none">
			<s:text name="label.sessionExpiry"/>
			</div>
			<div id="sessionHasExpired" style="display: none">
			<s:text name="label.sessionExpiredOnJourneys"/>
			</div>
			<!-- end of session expiry msg -->	     
	 <!-- processing message -->
	 
	<div id="processMsg1" style="display: none">
	<s:text name="msg.processing.request"/></div>
	<div id="processMsg2" style="display: none">
	<s:text name="msg.skip.queue"/>
	</div>
	 
	
	<article id="processing">
<img class="spinner" src="/images/splashhome/processing.gif" />
		<h1><s:text name="msg.processing.request"/></h1>
		<aside><s:text name="msg.skip.queue"/></aside>
	</article>
		
		
		<!--/#processing-->
	
	<!-- end of processing message -->
		
		<section id="pageTop" class="siteWidth clearfix">
		
<h1 class="logoContainer"><a href="<s:url value='/home.action?request_locale=<%=request.getSession().getAttribute("SELECTED_LOCALE").toString().toLowerCase()%>_<%=request.getSession().getAttribute("SELECTED_COUNTRY").toString().toUpperCase()%>'/>" class="logo ir">South African Airways</a></h1>
			
			
			
			<section id="warnings">
		  <div class="wrap">
					<div class="noticeFix">
						<div id = "globalnoticediv" style="display:none">
<a class="ntfWrap" onclick="cmsPageForGlobalNotice()">
							<span class="ntfIcon"></span>
							<span class="ntfArea" id="globalNoticeDesc" ><span></span></span>
							<s:hidden name="globalNoticeUrl"></s:hidden>
<span class="ntfMore" ><s:text name="label.moreInfo"/><span></span></span>
						</a>
						</div>
						<div class="clr"></div>
						<div id = "localnoticediv" style="display:none">
<a class="ntfWrap"  onclick="cmsPageForLocalNotice()">
							<span class="ntfIcon"></span>
							<span class="ntfArea" id="localNoticeDesc"><span></span></span>
							<s:hidden name="localNoticeUrl"></s:hidden>
<span class="ntfMore"><s:text name="label.moreInfo"/><span></span></span>
						</a>
						</div>
					</div>
				</div>
			</section>
			
			<!--/#warnings-->
		<!-- To set country and language from the Menu in home page -->	
		<s:hidden name="country" id="sessionCountry"/>
		<s:hidden name="selLanguage" id="selLanguage"/>
			<div class="regionCountry nojs">
			<div id="selectedCountryDiv" style="display:none"><%=session.getAttribute("SELECTED_COUNTRY")%></div>
			<div id="selectedLocaleDiv" style="display:none"><%=session.getAttribute("SELECTED_LOCALE")%></div>
			
			<div id="isCookieEnabledDiv" style="display: none"><%=session.getAttribute("COOKIE_ENABLED")%></div>
			
				<a class="dropLoc floatRight"><span id="currentLoc"> </span></a>
				
				<section id="LoL" class="clearfix">

					<ul class="tabs clearfix">
						<li class="americas"><a href="#list1"><s:text name="label.splash.americas"/></a></li>
						<li class="africa"><a href="#list2"><s:text name="label.splash.africa"/></a></li>
						<li class="europe"><a href="#list3"><s:text name="label.splash.europe"/></a></li>
						<li class="asiapacific"><a href="#list4"><s:text name="label.splash.asiapacific"/></a></li>
					</ul>
					
					<div class="articleWrap clearfix">
					
						<article id="list1" class="clearfix">
						
							<ul class="clearfix">

								<li><a href="/us/<%=locale_nojs%>/home.action">USA</a></li>
								<li><a href="/ca/<%=locale_nojs%>/home.action">Canada</a></li>
								<li><a href="/ar/<%=locale_nojs%>/home.action">Argentina</a></li>
								<li><a href="/bo/<%=locale_nojs%>/home.action">Bolivia</a> </li>
								<li><a href="/br/<%=locale_nojs%>/home.action">Brazil</a></li>
								<li><a href="/cl/<%=locale_nojs%>/home.action">Chile</a></li>
								<li><a href="/co/<%=locale_nojs%>/home.action">Colombia</a></li>
								<li><a href="/mx/<%=locale_nojs%>/home.action">Mexico</a></li>
								<li><a href="/py/<%=locale_nojs%>/home.action">Paraguay</a></li>
								<li><a href="/pe/<%=locale_nojs%>/home.action">Peru</a></li>
								<li><a href="/uy/<%=locale_nojs%>/home.action">Uruguay</a></li>
								<li><a href="/ve/<%=locale_nojs%>/home.action">Venezuela</a></li>
							

							</ul>
						
						</article>
						
						<article id="list2" class="clearfix">
						
							<ul class="clearfix">

							<li><a href="/za/<%=session.getAttribute("SELECTED_LOCALE").toString().toLowerCase()%>/home.action">South Africa</a></li>
							<li><a href="/za/<%=locale_nojs%>/home.action">South Africa</a></li>
							<li><a href="/eg/<%=locale_nojs%>/home.action">Egypt</a></li>
							<li><a href="/ao/<%=locale_nojs%>/home.action">Angola</a></li>
							<li><a href="/bw/<%=locale_nojs%>/home.action">Botswana</a></li>
							<li><a href="/cm/<%=locale_nojs%>/home.action">Cameroon</a></li>
							<li><a href="/cg/<%=locale_nojs%>/home.action">Congo</a></li>
							<li><a href="/cd/<%=locale_nojs%>/home.action">D.R. of Congo</a></li>
							<li><a href="/eg/<%=locale_nojs%>/home.action">Egypt</a></li>
							<li><a href="/et/<%=locale_nojs%>/home.action">Ethiopia</a></li>
							<li><a href="/ga/<%=locale_nojs%>/home.action">Gabon</a></li>
							<li><a href="/gh/<%=locale_nojs%>/home.action">Ghana</a></li>
							<li><a href="/ci/<%=locale_nojs%>/home.action">Ivory Coast</a></li>
							<li><a href="/ke/<%=locale_nojs%>/home.action">Kenya</a></li>
							<li><a href="/ls/<%=locale_nojs%>/home.action">Lesotho</a></li>
							<li><a href="/mg/<%=locale_nojs%>/home.action">Madagascar</a></li>
							<li><a href="/mw/<%=locale_nojs%>/home.action">Malawi</a></li>
							<li><a href="/mu/<%=locale_nojs%>/home.action">Mauritius</a></li>
							<li><a href="/mz/<%=locale_nojs%>/home.action">Mozambique</a></li>
							<li><a href="/na/<%=locale_nojs%>/home.action">Namibia</a></li>
							<li><a href="/ng/<%=locale_nojs%>/home.action">Nigeria</a></li>
							<li><a href="/sn/<%=locale_nojs%>/home.action">Senegal</a></li>
							<li><a href="/sz/<%=locale_nojs%>/home.action">Swaziland</a></li>
							<li><a href="/tz/<%=locale_nojs%>/home.action">Tanzania</a></li>
							<li><a href="/ug/<%=locale_nojs%>/home.action">Uganda</a></li>
							<li><a href="/zm/<%=locale_nojs%>/home.action">Zambia</a></li>
							<li><a href="/zw/<%=locale_nojs%>/home.action">Zimbabwe</a></li>
								

							</ul>
						
						</article>
						
						<article id="list3" class="clearfix">
						
							<ul class="clearfix">
											<li><a href="/uk/<%=locale_nojs%>/home.action">United Kingdom</a></li>
											<li><a href="/ie/<%=locale_nojs%>/home.action">Ireland</a></li>
											<li><a href="/fr/<%=locale_nojs%>/home.action">France</a></li>
											<li><a href="/de/<%=locale_nojs%>/home.action">Germany</a></li>
											<li><a href="/at/<%=locale_nojs%>/home.action">Austria</a></li>
											<li><a href="/by/<%=locale_nojs%>/home.action">Belarus</a></li>
											<li><a href="/be/<%=locale_nojs%>/home.action">Belgium</a></li>
											<li><a href="/bg/<%=locale_nojs%>/home.action">Bulgaria</a></li>
											<li><a href="/hr/<%=locale_nojs%>/home.action">Croatia</a></li>
											<li><a href="/cz/<%=locale_nojs%>/home.action">Czech Republic</a></li>
											<li><a href="/dk/<%=locale_nojs%>/home.action">Denmark</a></li>
											<li><a href="/fi/<%=locale_nojs%>/home.action">Finland</a></li>
											<li><a href="/hu/<%=locale_nojs%>/home.action">Hungary</a></li>
											<li><a href="/it/<%=locale_nojs%>/home.action">Italy</a></li>
											<li><a href="/lv/<%=locale_nojs%>/home.action">Latvia</a></li>
											<li><a href="/lt/<%=locale_nojs%>/home.action">Lithuania</a></li>
											<li><a href="/lu/<%=locale_nojs%>/home.action">Luxembourg</a></li>
											<li><a href="/nl/<%=locale_nojs%>/home.action">Netherlands</a></li>
											<li><a href="/no/<%=locale_nojs%>/home.action">Norway</a></li>
											<li><a href="/pl/<%=locale_nojs%>/home.action">Poland</a></li>
											<li><a href="/pt/<%=locale_nojs%>/home.action">Portugal</a></li>
											<li><a href="/ru/<%=locale_nojs%>/home.action">Russia</a></li>
											<li><a href="/sk/<%=locale_nojs%>/home.action">Slovakia</a></li>
											<li><a href="/si/<%=locale_nojs%>/home.action">Slovenia</a></li>
											<li><a href="/es/<%=locale_nojs%>/home.action">Spain</a></li>
											<li><a href="/se/<%=locale_nojs%>/home.action">Sweden</a></li>
											<li><a href="/ch/<%=locale_nojs%>/home.action">Switzerland</a></li>
											<li><a href="/ua/<%=locale_nojs%>/home.action">Ukraine</a></li>
								
							</ul>
						
						</article>
						
						<article id="list4" class="clearfix">
						
							<ul class="clearfix">

										<li><a href="/au/<%=locale_nojs%>/home.action">Australia</a></li>
										<li><a href="/hk/<%=locale_nojs%>/home.action">Hong Kong</a></li>
										<li><a href="/cn/<%=locale_nojs%>/home.action">China</a></li>
										<li><a href="/bh/<%=locale_nojs%>/home.action">Bahrain</a></li>
										<li><a href="/in/<%=locale_nojs%>/home.action">India</a></li>
										<li><a href="/ir/<%=locale_nojs%>/home.action">Iran</a></li>
										<li><a href="/il/<%=locale_nojs%>/home.action">Israel</a></li>
										<li><a href="/jp/<%=locale_nojs%>/home.action">Japan</a></li>
										<li><a href="/jo/<%=locale_nojs%>/home.action">Jordan</a></li>
										<li><a href="/kr/<%=locale_nojs%>/home.action">Korea</a></li>
										<li><a href="/kw/<%=locale_nojs%>/home.action">Kuwait</a></li>
										<li><a href="/lb/<%=locale_nojs%>/home.action">Lebanon</a></li>
										<li><a href="/my/<%=locale_nojs%>/home.action">Malaysia</a></li>
										<li><a href="/nz/<%=locale_nojs%>/home.action">New Zealand</a></li>
										<li><a href="/om/<%=locale_nojs%>/home.action">Oman</a></li>
										<li><a href="/ph/<%=locale_nojs%>/home.action">Philippines</a></li>
										<li><a href="/qa/<%=locale_nojs%>/home.action">Qatar</a></li>
										<li><a href="/sa/<%=locale_nojs%>/home.action">Saudi Arabia</a></li>
										<li><a href="/sg/<%=locale_nojs%>/home.action">Singapore</a></li>
										<li><a href="/tw/<%=locale_nojs%>/home.action">Taiwan</a></li>
										<li><a href="/th/<%=locale_nojs%>/home.action">Thailand</a></li>
										<li><a href="/ae/<%=locale_nojs%>/home.action">United Arab Emirates</a></li>
										<li><a href="/ye/<%=locale_nojs%>/home.action">Yemen</a></li>
								

							</ul>
						
						</article>
						
					</div>
					<!--/.articleWrap-->
					
					<div class="user_prefs">
						
						<div class="remember" id="rememberDiv">
							<label for="chkRemember">
								<span class="un check"><input id="chkRemember" type="checkbox" onclick="setSessionToCookie();"></span>
								<span class="bg"><s:text name="label.availability.location.remember"/></span>
							</label>
				  </div>
							<s:set var="sLang">label.selected.language_<%=session.getAttribute("SELECTED_LOCALE")%></s:set>
					
						<div id="savedID" style="display: block">					
							<p><s:text name="label.availability.preference"/>
								<span id="saved_locale">South Africa</span>
								<span id="saved_language">(<s:text name="%{sLang}"/>)</span>
							</p>
							<div class="forgetLoc floatRight"  style="display: block" id="forgetDiv">
								<a href="#" onclick="forgetMyLoc()"><s:text name="label.availability.location.forget"/><span class="pin"></span></a>
							</div>
						</div>
						
					</div>
					
				</section>
				<!--/ListOfLocations-->
				
			</div>
			<!--/regionCountry-->
			<!--<div class="regionLang">
			<s:set var="descZH">&#32321;&#39636;&#20013;&#25991;</s:set>
			<s:set var="descZH_CN">&#31616;&#20307;&#20013;&#25991;</s:set>
			<s:select id="dropLang" name="language" value="#session.SELECTED_LOCALE" list="#{'EN':'English','ZH':#descZH,'DE':'Deutsch','ES':'Espa�ol','FR':'Fran�ais','IT':'Italiano','PT':'Portugu�s','ZH_CN':#descZH_CN}" onchange="changeLang();"/>
				<select id="dropLang" name="language" onchange="changeLang();">
				
			</select>
			
			
			<s:select list="%{''}" id="dropLang" name="language" value="#session.SELECTED_LOCALE" ></s:select>
					<s:select list="%{''}" id="dropLang" name="language" value="#session.SELECTED_LOCALE" onchange="changeLang();"></s:select>
				<s:select id="dropLang" name="language" value="#session.SELECTED_LOCALE" list="%{''}" onchange="changeLang();"></s:select>
			</div>
			-->

			<nav class="regionLang" id="dropLangNav">
				<p>English</p>
				<ul id="dropLang"> 
					<li><a href="/<%=country_nojs%>/zh/home.action">&#32321;&#39636;&#20013;&#25991;</a></li> 
					<li><a href="/<%=country_nojs%>/de/home.action">Deutsch</a></li> 
					<li><a href="/<%=country_nojs%>/es/home.action">Espa�ol</a></li> 
					<li><a href="/<%=country_nojs%>/en/home.action">English</a></li> 
					<li><a href="/<%=country_nojs%>/fr/home.action">Fran�ais</a></li> 
					<li><a href="/<%=country_nojs%>/it/home.action">Italiano</a></li> 
					<li><a href="/<%=country_nojs%>/pt/home.action">Portugu�s</a></li>
					<li><a href="/<%=country_nojs%>/zh_cn/home.action">&#31616;&#20307;&#20013;&#25991;</a></li>
<li><a href="/<%=country_nojs%>/ja/home.action">&#26085;&#26412;&#35486;</a></li>
				</ul>
			</nav>

			
		</section>
		
		<!-- Forgot password  -->	
		<section id="voyForgotPassword">				
								<div class="modal" id="forgotPassword" style="position: fixed; left: 532.5px; top: 147px; display: none">
								<a onclick="modalClose('forgotPassword')" class="close" href="javascript:;">Close</a>
								<h4><s:text name="label.voy.forgotPwd"/></h4>
								<div class="inner">
								
									<label class="label"><s:text name="label.voyager.number"/>:</label>
									<div class="field"><s:textfield  cssClass='txt' name="voyagerNumber" id="voyagerNumber" maxlength="20"/></div>									
							
									<label class="label"><s:text name="label.voyager.pinreset.email"/></label>
									<div class="field"><s:textfield  cssClass='txt' name="email" id="email" maxlength="50"/></div>																
							        <div class="buttons">
										<div><button type="button"  id="forgotbutton" name="forgotbutton" class="button round green submit floatRight" ><span><s:text name="label.voy.send"/></span></button></div>
									</div>
									<div class="clr"></div>
								
								</div>
								<div class="bottom"></div>
							</div>	
		</section>
		<!--/#pageTop-->
		
		<div id="sitefx" class="siteWidth">
			
			<div class="map"></div>
			<div class="glow"></div>
		
		</div>
		
		<s:iterator var="configdCountries" value='{"AO","NA","BW","ZA","CD","SZ","LS","TZ","MW","ZM","MU","ZW","MZ","NG","KE","MG"}'>
				<s:if test='%{#session.SELECTED_COUNTRY == #configdCountries}'>
							<s:set var="isHolidayEnabled">true</s:set>
				</s:if>
		</s:iterator>
	
		<section id="midMenu" class="siteWidth">
			<nav>
				<ul>
					<li class="first"></li>
					<li><a href="<s:url value='/planMyTrip.action'/>"><s:text name="label.header.planMyTrip"/></a></li>
					<s:if test='(#session.SELECTED_PRODUCT)=="ONBIZ" && (#session.LOGGED_USER_DETAILS)!=null'>
						<li><a href="<s:url value='/ManageCurrentBookings!populateSearchCombo.action' />"><s:text name="label.header.manageMyTrip"/></a></li>
					</s:if>
					<s:else>
					<li><a href="<s:url value='/reservation.action'/>"><s:text name="label.header.manageMyTrip"/></a></li>
					</s:else>
					<li><a href="http://destinations.flysaa.com" target="_blank"><s:text name="label.header.destinations"/></a></li>
					<s:if test='#isHolidayEnabled == "true"'>
<li><a href="<%=conLan%>/home!loadSAAHolidays.action?redirect=/cms/ZA/leisure/saaHolidays.html&utm_source=ZA_Homepage&utm_medium=Banner&utm_campaign=SAAHolidaysMenuZA" target="_blank"><s:text name="label.header.holidays"/></a></li>
					</s:if>
					<li><a href="<s:url value='/flyingSAA.action'/>"><s:text name="label.header.flyingSaa"/></a></li>
					<li><a href="<%=conLan%>/customercharter/CustomerServiceCharter.html" class="subscribe"><s:text name="label.header.customerCharter"/></a></li>
					<li><a href="<%=request.getContextPath()%>/cms/commons/flysaa_registerNewsLetter.html" class="subscribe"><s:text name="label.header.newsletter"/></a></li>
					<li class="last"><a class="button essentialInfo" ><span><s:text name="label.header.essentialInfo"/></span></a>
					<!--  <li class="last"><a href="" class="button blue"><span><s:text name="label.header.essentialInfo"/></span></a></li>-->
					                                                                     
										
				</ul>
			</nav>
		</section>
		<!--/#midMenu-->
<div id="paymentORDeliverOptions" class="modal terms">

		<a href="javascript:;" class="close" onclick="modalClose('paymentORDeliverOptions')"><s:text name="label.popUp.close"/></a>
        <h4><s:text name="label.paymentOptions"/></h4>
        <div class="inner">
	<div id="paymentORDeliverOptionsModalPopUp" class="scroller">

	</div>
            <div class="buttons">
                <a href="javascript:;" class="arrowBlue right" onclick="modalClose('paymentORDeliverOptions')"><s:text name="label.popUp.close"/></a>  </div>
        </div>
        <div class="bottom"></div>
       
	</div>
	<!-- BASE MILES DISPLAY DIV. THIS WILL BE USED IN PAYMENT AND PRICING PAGES -->
<div id="showBaseMiles" class="modal terms">
    		<a href="javascript:;" class="close" onclick="modalClose('showBaseMiles')"><s:text name="label.popUp.close"/></a>
            <h4><s:text name="label.baseMiles"/></h4>
            <div class="inner">
            	<div class="whiteOnBlueInner">
            		<table class="flightDetails">
    					<tr>
    						<th class="sector"><div><s:text name="header.paxdetails.payment.flight.no"/></div></th>
    						<th class="flight"><div><s:text name="header.paxdetails.payment.sector"/></div></th>
    						<th class="arriving"><div><s:text name="header.paxdetails.payment.base.miles"/></div></th>
    					</tr>
       					<s:iterator  value="flightNo" status="fltNo">
       						<s:if test="#fltNo.odd == true"><s:set var="rowClass">odd</s:set><s:set var="finalClass">even</s:set></s:if>
       						<s:else><s:set var="rowClass">even</s:set><s:set var="finalClass">odd</s:set> </s:else>
       						<tr class="<s:property value='#rowClass'/>">
    							<td class="flight">
    								<div>
    									<s:property value="flightNo[#fltNo.index]"/>
    								</div>								
    							</td>
    							<td class="flight">
    								<div>
    									<s:property value="sectors[#fltNo.index]"/>
    								</div>
    							</td>
    							<td class="arriving">
    								<div>
    									<s:property value="baseMiles[#fltNo.index]"/>								
    								</div>								
    							</td>													
    						</tr>
    						<s:if test="#fltNo.last == true">
    							<tr class="<s:property value='#finalClass'/>">
    								<td class="flight">
    									<div>
    										<s:if test="#fltNo.last == true"><s:text name="label.paxdetails.payment.totalbase.miles"/></s:if>
    									</div>
    								</td>
    								<td  class="flight">
    								</td>
    								<td class="arriving">
    									<div>
    										<s:if test="#fltNo.last == true"><s:property value="totalBaseMiles"/></s:if>
    									</div>
    								</td>
    							</tr>
    						</s:if>												
       					</s:iterator>
       					
       				</table>
            	</div>
            	<div style="padding-left: 15px;margin-top: 10px">
            	  <ul>
            	  	<li>
            	  		<s:text name="label.paxdetails.payment.infants.not.voy"></s:text> 
            	  	</li>
            	 	<li>
            	  		<s:text name="label.paxdetails.payment.additional.miles"></s:text>
            	  	</li>
            	  	<li>
            	  		<s:text name="label.paxdetails.payment.promotion.miles"></s:text>
            	  	</li>
         	        <li>
            	  		<s:text name="label.paxdetails.payment.base.miles.differ"></s:text>
            	  	</li>
            	  </ul>	
            	</div>		
                <div class="buttons"><a href="javascript:;" class="arrowBlue right" onclick="modalClose('showBaseMiles')"><s:text name="label.popUp.close"/></a></div>
            </div>
            <div class="bottom"></div>
     </div> 	

<!-- ShowTaxInvoice Div displays the values about tax invoice  -->		

<div id="showTaxInvoice" class="modal">
	<a href="javascript:;" class="close" onclick="modalClose('showTaxInvoice')"><s:text name="label.popUp.close"/></a>
	<h4><s:text name="label.taxInvoice.about"/></h4>
	<div id="innerDiv" class="inner scrollable">
		<div class="scr">
		<p><b><s:text name="label.taxInvoice.delivery"/></b></p>
		<div class="clr"></div>
		<p><s:text name="label.taxInvoice.userRequire"></s:text></p>
		<div class="clr"></div>
		<p><b><s:text name="label.taxInvoice.SARevenue"></s:text></b></p>
		<div class="clr"></div>
		<p><s:text name="label.taxInvoice.condition"></s:text>:</p>
		<div>
			<ul>
				<li>
					<s:text name="label.taxInvoice.prominent"></s:text>
					<ul style="padding:5px;">
						<li>
							<i><s:text name="label.taxInvoice.emailEncrypted"></s:text></i> 
						</li>
						<li>
							<i><s:text name="label.taxInvoice.CGTax"></s:text></i>
						</li>
					</ul>
				</li>
				<li>
					<s:text name="label.taxInvoice.supplier"></s:text>
				</li>
				<li>
					<s:text name="label.taxInvoice.recipient"></s:text>
				</li>
				<li>
					<s:text name="label.taxInvoice.issueDate"></s:text> 
				</li>
				<li>
					<s:text name="label.taxInvoice.goods"></s:text> 
				</li>
				<li>
					<s:text name="label.taxInvoice.quantity"></s:text> 
				</li>
				<li>
					<s:text name="label.taxInvoice.either"></s:text>
					<ul style="padding:5px;">
						<li>
							<i><s:text name="label.taxInvoice.valSupply"></s:text></i> 
						</li>
						<li>
							<i><s:text name="label.taxInvoice.taxcharged"></s:text></i> 
						</li>
						<li>
							<i><s:text name="label.taxInvoice.considerEx"></s:text></i> 
						</li>
						<li>
							<i><s:text name="label.taxInvoice.consider"></s:text></i> 
						</li>
						<li>
							<i><s:text name="label.taxInvoice.statement"></s:text></i>
						</li>
					</ul>
				</li>
			</ul>
		</div>		
	</div>
	</div>
	<div class="inner">
	<div class="buttons"><a href="javascript:;" class="arrowBlue right" onclick="modalClose('showTaxInvoice')"><s:text name="label.popUp.close"/></a></div>
	</div>
	<div class="bottom"></div>
</div>									
				
<!-- Help Div for manage res retrieve and pay -->
<div id="helpModalPopUp" class="modal">
	<a href="javascript:;" class="close" onclick="modalClose('helpModalPopUp')"><s:text name="label.popUp.close"/></a>
<h4><s:text name="manageReservation.bookbymiles.button.needHelp"/></h4>
	<div class="inner scrollable">
		<div id="helpModalPopUpDetails" class="scr">
		
		</div>
	</div>
	<div class="bottom"></div>
</div>            
					
<div id="msgAlertDiv" style="display: none"><s:text name="label.modal.message"/></div>
<div id="continueDiv" style="display: none"><s:text name="label.availability.continue"/></div>
<s:hidden id="mobileUser" name="mobileUser" />
<div id="MsgMU" style="display: none"><s:text name="msg.confirm.mobileuser"/></div>
<div id="selcountry" style="display: none"><%=session.getAttribute("SELECTED_COUNTRY")%></div>
<div id="selectedLanguage" style="display: none"><%=session.getAttribute("SELECTED_LOCALE")%></div>
<script>
function clearCookieSession(){
<% session.removeAttribute("COOKIE_ENABLED"); %>
}

//setSessionToCookie
function setSessionToCookie(){

	if(document.getElementById('chkRemember').checked){
		isCookieEnabled = "true";
		//clearing cookie
		setCookie('rememberLocale','',-1,'/');
		setCookie('rememberLang','',-1,'/');
		//setting new cookie
		setCookie('rememberLocale','<%=session.getAttribute("SELECTED_COUNTRY")%>',1,'/');
		setCookie('rememberLang','<%=session.getAttribute("SELECTED_LOCALE")%>',1,'/');
		<% session.setAttribute("COOKIE_ENABLED", true); %>
	}else{
		isCookieEnabled = "false";
		//clearing cookie
		setCookie('rememberLocale','',-1,'/');
		setCookie('rememberLang','',-1,'/');
		<% session.setAttribute("COOKIE_ENABLED", false); %>
	}
}

</script>