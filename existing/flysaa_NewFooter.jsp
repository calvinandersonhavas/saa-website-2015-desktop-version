<%-- 
/***********************************************************************
* Project	 		: 
* Module Code & Name: Leisure Home Page
* File Name			: flysaa_NewFooter.jsp
* Date				: 17-Jun-2011
* Author(s)			: Merril Jose
*************************************************************************/
 --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
	</div><!--closing content div opened in Topbanner -->
	</div><!-- closing container div opened in Top banner -->
<footer class="siteFooter">

	<div class="siteWidth clearfix" style="position: static;">
		<%
				String country=(String)request.getSession().getAttribute("SELECTED_COUNTRY");
				String language=(String)request.getSession().getAttribute("SELECTED_LOCALE");
				String conLan="/"+country.toLowerCase()+"/"+language.toLowerCase();
				String con_Lan_specials=language.toLowerCase()+"_"+country.toUpperCase()+"_specials";
			%>
			
		<div id="copyright">&copy; <s:text name="label.footer.copyright"/></div>
		<nav id="footerNew">
			<ul>
				<li><a href="<%=request.getContextPath()%><%=conLan %>/about_us.action"><s:text name="label.footer.aboutUS"/></a></li>
				<li><a href="<%=request.getContextPath()%><%=conLan %>/contact_us.action"><s:text name="label.footer.contactUS"/></a></li>
				<li><a href="<%=request.getContextPath()%><%=conLan %>/careers.action"> <s:text name="label.footer.careers"/></a></li>
				<li><a href="<%=request.getContextPath()%><%=conLan %>/policies_and_Disclaimers.action"><s:text name="label.footer.policies"/></a></li>
				<li><a href="<%=request.getContextPath()%><%=conLan %>/conditions_of_Contract.action"><s:text name="label.footer.conditionsOfContract"/></a></li>
				<li><a href="<%=request.getContextPath()%><%=conLan %>/browser_Requirements.action"><s:text name="label.footer.browserRequirements"/></a></li>
				<li><a href="<%=request.getContextPath()%><%=conLan %>/accessibility.action"><s:text name="label.footer.accessibility"/></a></li>
				<li><a href="<%=request.getContextPath()%><%=conLan %>/imprint.action"><s:text name="label.footer.legal"/></a></li>
				<li><a href="<%=request.getContextPath()%><%=conLan%>/sitemap/sitemap.html"><s:text name="label.footer.siteMap"/></a></li>
				<li class="last"><a href="<%=request.getContextPath()%><%=conLan%>/planmytrip/destinations/codeshares.html "><s:text name="label.footer.codeshares"/></a></li>
				
				
				
			</ul>
		</nav>
		
		<aside class="socialLinks">
			<a onclick="window.open('<s:property value="faceBookVal"/>')"  class="facebook ir">Facebook</a>
			<a onclick="window.open('<s:property value="twitterVal"/>')" class="twitter ir">Twitter</a>
			<a href="http://www.flysaa.com/dynamicAd/GetAdvert/<%=con_Lan_specials %>" rel="external" class="rss ir">RSS</a>
			<%
			if(country.toLowerCase().equalsIgnoreCase("de"))
			{
			%>
				<a onclick="window.open('http://plus.google.com/+FlysaaGermany/posts')" class="googleplus ir">Google +</a>
			<%
			} else{
			%>
				<a onclick="window.open('http://plus.google.com/+southafricanairways')" class="googleplus ir">Google +</a>
		    <%
			} 
			%>
			<a onclick="window.open('https://itunes.apple.com/us/app/south-african-airways/id619821296?mt=8')" class="applestore ir">Apple Store</a>
            <a onclick="window.open('https://play.google.com/store/apps/details?id=com.flysaa.mobile.android')" class="googleplay ir">Google Play Store</a>
             <a onclick="window.open('http://apps.microsoft.com/windows/app/flysaa/8add1a17-1300-4045-9d24-bbde76994206')" class="windowstore ir">Window Store</a>
            <a onclick="window.open('http://pinterest.com/flysaa')" class="pinterest ir">Pinterest</a>
            
            
		</aside>
		
	</div>
	<aside id="snoopy">§</aside>
</footer>





<script>document.write('<link rel="stylesheet" media="projection" href="<%=request.getContextPath()%>/styles/unified.css">');</script>

<!--[if lt IE 7 ]>
<script src="<s:url value='/scripts/lib/dd_belatedpng.js'/>"></script>
<script>DD_belatedPNG.fix("img, .png_bg,.logo,.glow,.map,#midMenu,.vLogo,#voyager,.searchInput,.searchButton,.button,.vButton,.button span,.vButton span,.calWrap,.iconInfo,.dropLoc,.dropLoc span,.vIcon span ,.regionLang .linkselectLink,.chkLabelNo span,.chkLabelYes span,.siteFooter,.facebook,.twitter,.ieNotice span,.ui-button span,.bg,.check,.remember label,#LoL article li a:hover,.ui-datepicker-next,.ui-datepicker-prev,.pin,.linkselectLink,.block .iconInfo,#speacils header a span,#specials header A");</script>
<![endif]--> 


<img id="loadingImage12" src="/images/SAA_logo.jpg" style="visibility:hidden"/>
<img id="loadingImage1" src="/images/take-off.jpg" style="visibility:hidden"/>
<img id="loadingImage2" src="/images/take-off_DE.jpg" style="visibility:hidden"/>
<img id="loadingImage3" src="/images/take-off_ES.jpg" style="visibility:hidden"/>
<img id="loadingImage4" src="/images/take-off_FR.jpg" style="visibility:hidden"/>
<img id="loadingImage5" src="/images/take-off_JP.jpg" style="visibility:hidden"/>
<img id="loadingImage6" src="/images/take-off_IT.jpg" style="visibility:hidden"/>
<img id="loadingImage7" src="/images/take-off_PT.jpg" style="visibility:hidden"/>
<img id="loadingImage8" src="/images/take-off_zh-HK.jpg" style="visibility:hidden"/>
<img id="loadingImage9" src="/images/take-off_zh-CN.jpg" style="visibility:hidden"/>
<img id="loadingImage11" src="/images/loader.gif" style="visibility:hidden"/> 
 
