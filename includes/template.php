

<section class="content horizontal">

    <div class="wrap">
        <div class="left">
            <?php include('blocks/sidebar.php');?>
        </div>
        <div class="right">
            <div id="content" class="">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Plan &amp; Book</a></li>
                    </ul>
                </div>
                <div class="inner">
                    <!-- Content Area -->

                    <h1>H1 Heading</h1>
                    <p class="lead">A lead paragraph with the class of "lead". Consectetur adipiscing elit. Aenean vitae hendrerit felis.consectetur adipiscing elit.
                        Aenean</p>

                    <h2>H2 Heading</h2>
                    <div class="row">
                        <div class="column small-6">
                            <div class="shadow">
                                <img src="herald/images/content-img/image-1.jpg" />
                            </div>
                            <p>Image is wrapped in a div with a class of "shadow" to cause the shadow effect.</p>
                        </div>
                        <div class="column small-6">
                            <p>Right column "small-6", which is part of a 12 grid system.
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae hendrerit felis.
                                In euismod quam quam, id commodo nibh vulputate et. Phasellus egestas sit amet ipsum in
                                varius. Ut ipsum lorem, facilisis ut risus at, viverra vehicula lacus. ipsum dolor sit
                                amet, consectetur adipiscing elit.
                            </p>
                            <ul class="links">
                                <li><a href="#" class="link-arrow">These are a set of links</a></li>
                                <li><a href="#" class="link-arrow">a.link-arrow</a></li>
                                <li><a href="#" class="link-arrow gold">a.link-arrow.gold</a></li>
                            </ul>
                        </div>
                    </div>



                    <hr />


                    <p>A series of dropdown buttons</p>
                    <div>
                        <ul class="drop-down">
                            <li><a href="#" class="button drop-down"><span>.button .drop-down</span></a>
                                <ul>
                                    <li><a href="#">Link 1</a></li>
                                    <li><a href="#">Link 2</a></li>
                                    <li><a href="#">Link 3</a></li>
                                </ul>
                            </li>
                        </ul>

                        <a href="#" class="button small drop-down"><span>.button .small .drop-down</span></a>
                    </div>


                    <hr />


                    <div class="row">
                        <div class="column small-6">
                            <h3>H3 Heading</h3>
                            <div class="row">
                                <div class="column small-6">
                                    <p><strong>Aenean vitae hendrerit vitae hendrerit</strong> ipsum dolor sit amet, consectetur adipiscing elit Ut ipsum lorem, facilisis ut <strong>risus at</strong>,
                                        viverra vehicula lacus. ipsum dolor sit amet, consectetur adipiscing.</p>
                                </div>
                                <div class="column small-6">
                                    <p>ipsum dolor sit amet, consectetur adipiscing elit Ut ipsum lorem, facilisis ut <strong>risus at</strong>,
                                        viverra vehicula lacus. ipsum dolor sit amet, consectetur adipiscing.</p>
                                </div>
                            </div>
                            <hr />
                        </div>
                        <div class="column small-6">
                            <h3>H3 Heading</h3>
                            <p><strong>Aenean vitae hendrerit vitae hendrerit</strong></p>
                            <p>ipsum dolor sit amet, consectetur adipiscing elit Ut ipsum lorem, facilisis ut <strong>risus at</strong>,
                                viverra vehicula lacus. ipsum dolor sit amet, consectetur adipiscing.</p>
                            <p>ipsum dolor sit amet, consectetur adipiscing elit Ut ipsum lorem, facilisis ut <strong>risus at</strong>,
                                viverra vehicula lacus. ipsum dolor sit amet, consectetur adipiscing.</p>
                            <hr />
                        </div>
                    </div>





                    <!-- /Content Area -->
                </div>
            </div>
        </div>
    </div>

</section>




