

<section class="content horizontal">

    <div class="wrap">
        <div class="left">
            <?php include('blocks/sidebar.php');?>
        </div>
        <div class="right">
            <div id="content" class="">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Plan &amp; Book</a></li>
                    </ul>
                </div>
                <div class="inner">
                    <!-- Content Area -->

                    <h1>Plan &amp; Book Heading Text</h1>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae hendrerit
                        felis. consectetur adipiscing elit. Aenean vitae hendrerit felis.consectetur adipiscing elit.
                        Aenean</p>

                    <h2>Promotions</h2>
                    <div class="row">
                        <div class="column small-6">
                            <div class="shadow">
                                <img src="herald/images/content-img/image-1.jpg" />
                            </div>
                        </div>
                        <div class="column small-6">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae hendrerit felis.
                                In euismod quam quam, id commodo nibh vulputate et. Phasellus egestas sit amet ipsum in
                                varius. Ut ipsum lorem, facilisis ut risus at, viverra vehicula lacus. ipsum dolor sit
                                amet, consectetur adipiscing elit.
                            </p>
                            <ul class="links">
                                <li><a href="#" class="link-arrow">Egestas sit amet</a></li>
                                <li><a href="#" class="link-arrow">Lorem ipsum dolor sit amet</a></li>
                                <li><a href="#" class="link-arrow">In eisidem quam</a></li>
                            </ul>
                        </div>
                    </div>

                    <h2>Destinations</h2>
                    <div class="row">
                        <div class="column small-12">
                            <div class="shadow">
                                <div class="flexslider">
                                    <ul class="slides">
                                        <li><img src="herald/images/content-img/image-5.jpg"/></li>
                                        <li><img src="herald/images/content-img/image-5.jpg"/></li>
                                        <li><img src="herald/images/content-img/image-5.jpg"/></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="column small-6">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <strong>Aenean vitae hendrerit</strong> felis.
                                In euismod quam quam, id commodo nibh vulputate et. Phasellus egestas sit amet ipsum in
                                varius. Ut ipsum lorem, facilisis ut <strong>risus at</strong>, viverra vehicula lacus. ipsum dolor sit
                                amet, consectetur adipiscing elit.</p>
                            <ul class="links">
                                <li><a href="#" class="link-arrow">Egestas sit amet</a></li>
                                <li><a href="#" class="link-arrow">Lorem ipsum dolor sit amet</a></li>
                                <li><a href="#" class="link-arrow">In eisidem quam</a></li>
                            </ul>
                        </div>
                        <div class="column small-6">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <strong>Aenean vitae hendrerit</strong> felis.
                                In euismod quam quam, id commodo nibh vulputate et. Phasellus egestas sit amet ipsum in
                                varius. Ut ipsum lorem, facilisis ut <strong>risus at</strong>, viverra vehicula lacus. ipsum dolor sit
                                amet, consectetur adipiscing elit.</p>
                            <ul class="links">
                                <li><a href="#" class="link-arrow">Egestas sit amet</a></li>
                                <li><a href="#" class="link-arrow">Lorem ipsum dolor sit amet</a></li>
                                <li><a href="#" class="link-arrow">In eisidem quam</a></li>
                            </ul>
                        </div>
                    </div>


                    <div class="row">
                        <div class="column small-6">
                            <h2>Book flights</h2>
                            <div class="shadow">
                                <img src="herald/images/content-img/image-7.jpg" />
                            </div>
                            <div class="row">
                                <div class="column small-6">
                                    <p><strong>Aenean vitae hendrerit vitae hendrerit</strong></p>
                                    <img src="herald/images/content-img/image-8.jpg" class="responsive border" />
                                    <p>ipsum dolor sit amet, consectetur adipiscing elit Ut ipsum lorem, facilisis ut <strong>risus at</strong>,
                                        viverra vehicula lacus. ipsum dolor sit amet, consectetur adipiscing.</p>
                                </div>
                                <div class="column small-6">
                                    <p>ipsum dolor sit amet, consectetur adipiscing elit Ut ipsum lorem, facilisis ut <strong>risus at</strong>,
                                        viverra vehicula lacus. ipsum dolor sit amet, consectetur adipiscing.</p>
                                    <p>ipsum dolor sit amet, consectetur adipiscing elit Ut ipsum lorem, facilisis ut <strong>risus at</strong>,
                                        viverra vehicula lacus. ipsum dolor sit amet, consectetur adipiscing.</p>
                                </div>
                            </div>
                            <ul class="links">
                                <li><a href="#" class="link-arrow">Egestas sit amet</a></li>
                                <li><a href="#" class="link-arrow">Lorem ipsum dolor sit amet</a></li>
                                <li><a href="#" class="link-arrow">In eisidem quam</a></li>
                            </ul>
                        </div>
                        <div class="column small-6">
                            <h2>Travel advisory</h2>
                            <div class="shadow">
                                <img src="herald/images/content-img/image-6.jpg" />
                            </div>
                            <p><strong>Aenean vitae hendrerit vitae hendrerit</strong></p>
                            <p>ipsum dolor sit amet, consectetur adipiscing elit Ut ipsum lorem, facilisis ut <strong>risus at</strong>,
                                viverra vehicula lacus. ipsum dolor sit amet, consectetur adipiscing.</p>
                            <p>ipsum dolor sit amet, consectetur adipiscing elit Ut ipsum lorem, facilisis ut <strong>risus at</strong>,
                                viverra vehicula lacus. ipsum dolor sit amet, consectetur adipiscing.</p>
                            <ul class="links">
                                <li><a href="#" class="link-arrow">Egestas sit amet</a></li>
                                <li><a href="#" class="link-arrow">Lorem ipsum dolor sit amet</a></li>
                                <li><a href="#" class="link-arrow">In eisidem quam</a></li>
                                <li><a href="#" class="link-arrow">Egestas sit amet</a></li>
                                <li><a href="#" class="link-arrow">Lorem ipsum dolor sit amet</a></li>
                                <li><a href="#" class="link-arrow">In eisidem quam</a></li>
                            </ul>
                        </div>
                    </div>






                    <!-- /Content Area -->
                </div>
            </div>
        </div>
    </div>

</section>




