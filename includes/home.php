

<section class="content">

    <div class="wrap">
        <div class="left">
            <?php include('blocks/home.php');?>
        </div>
        <div class="right">
            <div id="slider" class="">
                <div class="flexslider">
                    <ul class="slides">
                        <li><img src="herald/images/content-img/header1.jpg"/></li>
                        <li><img src="herald/images/content-img/header2.jpg"/></li>
                        <li><img src="herald/images/content-img/header1.jpg"/></li>
                        <li><img src="herald/images/content-img/header2.jpg"/></li>
                        <li><img src="herald/images/content-img/header1.jpg"/></li>
                        <li><img src="herald/images/content-img/header2.jpg"/></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="wrap">
        <div id="featuredArticles" class="left">

            <section class="featured">
                <article class="feature">
                    <h2>Lorem Ipsum Dolor</h2>
                    <div class="shadow">
                        <img src="herald/images/content-img/image-1.jpg" />
                    </div>
                    <p>Lorem ipsum dolor sit amet, consecteteur adipiscing elit. Anean vitae hendrerit felis. In eusmod quam
                        quam, id commodo nibh vulputate et. Phasellus egestas sit amet ipsum in varius. Ut ipsum lorem.</p>
                    <p><a href="#" title="Lorem Ipsum Dolor"><span class="icon-arrow-right"></span>Egestas sit amet</a></p>
                </article>
                <article class="feature">
                    <h2>Lorem Ipsum Dolor</h2>
                    <div class="shadow">
                        <img src="herald/images/content-img/image-2.jpg" />
                    </div>
                    <p>Lorem ipsum dolor sit amet, consecteteur adipiscing elit. Anean vitae hendrerit felis. In eusmod quam
                        quam, id commodo nibh vulputate et. Phasellus egestas sit amet ipsum in varius. Ut ipsum lorem.</p>
                    <p><a href="#" title="Lorem Ipsum Dolor"><span class="icon-arrow-right"></span>Egestas sit amet</a></p>
                </article>
            </section>

        </div>
        <div class="right">
            <div id="flightDeals">
            <section class="flight-deals">
                <h2>Latest flight deals from Johannesburg</h2>
                <div class="row">
                    <div class="column small-6">
                        <ul>
                            <li>
                                <h3><a href="#">London</a></h3>
                                <div class="price"><span class="currency">From ZAR</span>
                                    <span class="figure">10,769*</span></div>
                                <span class="class">Economy class special</span>
                                <div class="period">Sales Period: until 31 October 2014<br />
                                    Travel Period: until 31 October 2014
                                </div>
                            </li>
                            <li>
                                <h3><a href="#">Blantyre</a></h3>
                                <div class="price"><span class="currency">From ZAR</span>
                                    <span class="figure">6,380*</span></div>
                                <span class="class">Economy class special</span>
                                <div class="period">Sales Period: until 31 October 2014<br />
                                    Travel Period: until 31 October 2014
                                </div>
                            </li>
                            <li>
                                <h3><a href="#">Beijing</a></h3>
                                <div class="price"><span class="currency">From ZAR</span>
                                    <span class="figure">10,375*</span></div>
                                <span class="class">Economy class special</span>
                                <div class="period">Sales Period: until 31 October 2014<br />
                                    Travel Period: until 31 October 2014
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="column small-6">
                        <ul>
                            <li>
                                <h3><a href="#">Mumbai</a></h3>
                                <div class="price"><span class="currency">From ZAR</span>
                                    <span class="figure">5,795*</span></div>
                                <span class="class">Economy class special</span>
                                <div class="period">Sales Period: until 31 October 2014<br />
                                    Travel Period: until 31 October 2014
                                </div>
                            </li>
                            <li>
                                <h3><a href="#">Nairobi</a></h3>
                                <div class="price"><span class="currency">From ZAR</span>
                                    <span class="figure">7,346*</span></div>
                                <span class="class">Economy class special</span>
                                <div class="period">Sales Period: until 31 October 2014<br />
                                    Travel Period: until 31 October 2014
                                </div>
                            </li>
                            <li>
                                <h3><a href="#">Abidjan</a></h3>
                                <div class="price"><span class="currency">From ZAR</span>
                                    <span class="figure">9,836*</span></div>
                                <span class="class">Economy class special</span>
                                <div class="period">Sales Period: until 31 October 2014<br />
                                    Travel Period: until 31 October 2014
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="column small-6">
                        <div class="terms">* Terms &amp; conditions apply</div>
                    </div>
                    <div class="column small-6 text-right">
                        <a href="#" class="more-specials"><span class="icon-arrow-right-large"></span>See all Specials</a>
                    </div>
                </div>
            </section>
            </div>
            <div id="specialOffers">
            <section class="offers">
                <h2>Special offers from South African Airways</h2>
                <div class="row">
                    <div class="column small-6">
                        <article class="feature">
                            <div class="shadow">
                                <img src="herald/images/content-img/image-3.jpg" />
                            </div>
                            <p>Lorem ipsum dolor sit amet, consecteteur adipiscing elit. Anean vitae hendrerit felis. In eusmod quam
                                quam, id commodo nibh vulputate et. Phasellus egestas sit amet ipsum in varius. Ut ipsum lorem.</p>
                            <p><a href="#" title="Lorem Ipsum Dolor"><span class="icon-arrow-right gold"></span>Egestas sit amet</a></p>
                        </article>
                    </div>
                    <div class="column small-6">
                        <article class="feature">
                            <div class="shadow">
                                <img src="herald/images/content-img/image-4.jpg" />
                            </div>
                            <p>Lorem ipsum dolor sit amet, consecteteur adipiscing elit. Anean vitae hendrerit felis. In eusmod quam
                                quam, id commodo nibh vulputate et. Phasellus egestas sit amet ipsum in varius. Ut ipsum lorem.</p>
                            <p><a href="#" title="Lorem Ipsum Dolor"><span class="icon-arrow-right gold"></span>Egestas sit amet</a></p>
                        </article>
                    </div>
                </div>
            </section>
            </div>
        </div>
    </div>

</section>




