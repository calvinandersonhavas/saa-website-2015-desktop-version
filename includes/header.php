<!doctype html>
<!--[if lt IE 7 ]>
<html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="content-language" content="en-za">
    <meta http-equiv="X-UA-Compatible" content="IE=10">

    <!-- NEW DEV NOTE: Replace with correct info -->
    <title>Flights to South Africa & Beyond | South African Airways </title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <meta name="author" content="South African Airways">
    <meta name="viewport" content="width=1050, initial-scale=1.0, maximum-scale=1.4">

    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="apple-touch-icon" href="/images/apple-touch-icon.png">
    <link href="herald/css/style.css" rel="stylesheet" type="text/css">

    <script src="herald/js/modernizr.js"></script>


</head>

<body class="home en">
    <!--
    <script src='//www.flysaa.com/cms/eretail/scripts/Amadeus2.js'></script>
    <!-- Google Tag Manager -->
    <!--
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-T8DLCS"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T8DLCS');</script>
    <!-- End Google Tag Manager -->




    <!--IE Notice-->
    <!-- NEW DEV NOTE: This should be implemented with JS perhaps? It will impact how SE's read the page -->
    <!-- was class="ieNotice" -->
    <div id="ieNotice">
        <p>To fully experience the functionality of our site, you will need to upgrade your browser.</p>
        <p class="grey">We recommend:
            <a href="http://www.google.com/chrome" rel="external" class="browsers chrome">Chrome</a>
            <a href="http://www.microsoft.com/ie" rel="external" class="browsers ie">Internet Explorer 9</a>
            <a href="http://www.mozilla.com/firefox" rel="external" class="browsers firefox">Firefox</a>
            <a href="http://www.apple.com/safari" rel="external" class="browsers safari">Safari</a>
        </p>
    </div>
    <!--/IE Notice-->

    <!-- Start of Site Header -->
    <header id="header">
        <div class="top">
            <div class="wrap">
                <div class="left">
                    <!-- Site Links -->
                    <nav class="siteLinks">
                        <ul>
                            <li class="selected"><a href="/za/en/home.action?request_locale=en_ZA">flysaa.com</a></li>
                            <li><a href="/za/en/OnbizLogin!login.secured">OnBiz</a></li>
                            <li><a href="/za/en/voyagerLogin.secured">Voyager</a></li>
                            <li><a href="/za/en/home!loadCargoHome.action">Cargo</a></li>
                            <li><a href="/za/en/home!loadTechnicalHome.action">Engineering</a></li>
                        </ul>
                    </nav>
                    <!--/ Site Links -->
                </div>
                <div class="right">

                    <!-- Search -->
                    <div class="search">
                        <input id="searchInput" class="input" type="text" title="Search" placeholder="Search...">
                        <input id="searchButton" value="Search" class="button" type="button">
                    </div>
                    <!--/ Search -->

                    <!-- Region Selection -->
                    <div class="region nojs">

                        <a id="currentLoc" class="za"></a>
                        <nav id="regionNav">
                            <div>
                                <div class="regions tabs">
                                    <ul>
                                        <li class="americas am"><a href="#region-am">Americas</a></li>
                                        <li class="africa af"><a href="#region-af">Africa</a></li>
                                        <li class="europe eu"><a href="#region-eu">Europe</a></li>
                                        <li class="asiapacific as"><a href="#region-as">Asia &amp; the Pacific</a></li>
                                        <li class="middleeast mi"><a href="#region-mi">Middle East</a></li>
                                    </ul>
                                    <div>
                                        <div id="region-am">
                                            <ul>
                                                <li><a href="/us/en/home.action">USA</a></li>
                                                <li><a href="/ca/en/home.action">Canada</a></li>
                                                <li><a href="/ar/en/home.action">Argentina</a></li>
                                                <li><a href="/bo/en/home.action">Bolivia</a></li>
                                                <li><a href="/br/en/home.action">Brazil</a></li>
                                                <li><a href="/cl/en/home.action">Chile</a></li>
                                                <li><a href="/co/en/home.action">Colombia</a></li>
                                                <li><a href="/mx/en/home.action">Mexico</a></li>
                                                <li><a href="/py/en/home.action">Paraguay</a></li>
                                                <li><a href="/pe/en/home.action">Peru</a></li>
                                                <li><a href="/uy/en/home.action">Uruguay</a></li>
                                                <li><a href="/ve/en/home.action">Venezuela</a></li>
                                            </ul>
                                        </div>
                                        <div id="region-af">
                                            <ul>
                                                <li><a href="/za/en/home.action">South Africa</a></li>
                                                <li><a href="/za/en/home.action">South Africa</a></li>
                                                <li><a href="/eg/en/home.action">Egypt</a></li>
                                                <li><a href="/ao/en/home.action">Angola</a></li>
                                                <li><a href="/bw/en/home.action">Botswana</a></li>
                                                <li><a href="/cm/en/home.action">Cameroon</a></li>
                                                <li><a href="/cg/en/home.action">Congo</a></li>
                                                <li><a href="/cd/en/home.action">D.R. of Congo</a></li>
                                                <li><a href="/eg/en/home.action">Egypt</a></li>
                                                <li><a href="/et/en/home.action">Ethiopia</a></li>
                                                <li><a href="/ga/en/home.action">Gabon</a></li>
                                                <li><a href="/gh/en/home.action">Ghana</a></li>
                                                <li><a href="/ci/en/home.action">Ivory Coast</a></li>
                                                <li><a href="/ke/en/home.action">Kenya</a></li>
                                                <li><a href="/ls/en/home.action">Lesotho</a></li>
                                                <li><a href="/mg/en/home.action">Madagascar</a></li>
                                                <li><a href="/mw/en/home.action">Malawi</a></li>
                                                <li><a href="/mu/en/home.action">Mauritius</a></li>
                                                <li><a href="/mz/en/home.action">Mozambique</a></li>
                                                <li><a href="/na/en/home.action">Namibia</a></li>
                                                <li><a href="/ng/en/home.action">Nigeria</a></li>
                                                <li><a href="/sn/en/home.action">Senegal</a></li>
                                                <li><a href="/sz/en/home.action">Swaziland</a></li>
                                                <li><a href="/tz/en/home.action">Tanzania</a></li>
                                                <li><a href="/ug/en/home.action">Uganda</a></li>
                                                <li><a href="/zm/en/home.action">Zambia</a></li>
                                                <li><a href="/zw/en/home.action">Zimbabwe</a></li>
                                            </ul>
                                        </div>
                                        <div id="region-eu">
                                            <ul>
                                                <li><a href="/uk/en/home.action">United Kingdom</a></li>
                                                <li><a href="/ie/en/home.action">Ireland</a></li>
                                                <li><a href="/fr/en/home.action">France</a></li>
                                                <li><a href="/de/en/home.action">Germany</a></li>
                                                <li><a href="/at/en/home.action">Austria</a></li>
                                                <li><a href="/by/en/home.action">Belarus</a></li>
                                                <li><a href="/be/en/home.action">Belgium</a></li>
                                                <li><a href="/bg/en/home.action">Bulgaria</a></li>
                                                <li><a href="/hr/en/home.action">Croatia</a></li>
                                                <li><a href="/cz/en/home.action">Czech Republic</a></li>
                                                <li><a href="/dk/en/home.action">Denmark</a></li>
                                                <li><a href="/fi/en/home.action">Finland</a></li>
                                                <li><a href="/hu/en/home.action">Hungary</a></li>
                                                <li><a href="/it/en/home.action">Italy</a></li>
                                                <li><a href="/lv/en/home.action">Latvia</a></li>
                                                <li><a href="/lt/en/home.action">Lithuania</a></li>
                                                <li><a href="/lu/en/home.action">Luxembourg</a></li>
                                                <li><a href="/nl/en/home.action">Netherlands</a></li>
                                                <li><a href="/no/en/home.action">Norway</a></li>
                                                <li><a href="/pl/en/home.action">Poland</a></li>
                                                <li><a href="/pt/en/home.action">Portugal</a></li>
                                                <li><a href="/ru/en/home.action">Russia</a></li>
                                                <li><a href="/sk/en/home.action">Slovakia</a></li>
                                                <li><a href="/si/en/home.action">Slovenia</a></li>
                                                <li><a href="/es/en/home.action">Spain</a></li>
                                                <li><a href="/se/en/home.action">Sweden</a></li>
                                                <li><a href="/ch/en/home.action">Switzerland</a></li>
                                                <li><a href="/ua/en/home.action">Ukraine</a></li>
                                            </ul>
                                        </div>
                                        <div id="region-as">
                                            <ul>
                                                <li><a href="/au/en/home.action">Australia</a></li>
                                                <li><a href="/hk/en/home.action">Hong Kong</a></li>
                                                <li><a href="/cn/en/home.action">China</a></li>
                                                <li><a href="/bh/en/home.action">Bahrain</a></li>
                                                <li><a href="/in/en/home.action">India</a></li>
                                                <li><a href="/ir/en/home.action">Iran</a></li>
                                                <li><a href="/il/en/home.action">Israel</a></li>
                                                <li><a href="/jp/en/home.action">Japan</a></li>
                                                <li><a href="/jo/en/home.action">Jordan</a></li>
                                                <li><a href="/kr/en/home.action">Korea</a></li>
                                                <li><a href="/kw/en/home.action">Kuwait</a></li>
                                                <li><a href="/lb/en/home.action">Lebanon</a></li>
                                                <li><a href="/my/en/home.action">Malaysia</a></li>
                                                <li><a href="/nz/en/home.action">New Zealand</a></li>
                                                <li><a href="/om/en/home.action">Oman</a></li>
                                                <li><a href="/ph/en/home.action">Philippines</a></li>
                                                <li><a href="/qa/en/home.action">Qatar</a></li>
                                                <li><a href="/sa/en/home.action">Saudi Arabia</a></li>
                                                <li><a href="/sg/en/home.action">Singapore</a></li>
                                                <li><a href="/tw/en/home.action">Taiwan</a></li>
                                                <li><a href="/th/en/home.action">Thailand</a></li>
                                                <li><a href="/ae/en/home.action">United Arab Emirates</a></li>
                                                <li><a href="/ye/en/home.action">Yemen</a></li>
                                            </ul>
                                        </div>
                                        <div id="region-mi">
                                            <ul>
                                                <li><a href="/bh/en/home.action">Bahrain</a></li>
                                                <li><a href="/eg/en/home.action">Egypt</a></li>
                                                <li><a href="/il/en/home.action">Israel</a></li>
                                                <li><a href="/jo/en/home.action">Jordan</a></li>
                                                <li><a href="/kw/en/home.action">Kuwait</a></li>
                                                <li><a href="/lb/en/home.action">Lebanon</a></li>
                                                <li><a href="/om/en/home.action">Oman</a></li>
                                                <li><a href="/qa/en/home.action">Qatar</a></li>
                                                <li><a href="/sa/en/home.action">Saudi Arabia</a></li>
                                                <li><a href="/ae/en/home.action">United Arab Emirates</a></li>
                                                <li><a href="/ye/en/home.action">Yemen</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="preference">
                                    <div id="rememberDiv" class="remember">
                                        <label for="chkRemember">
                                            <span class="un check"><input id="chkRemember" type="checkbox"></span>
                                            <span class="bg">Remember this location</span>
                                        </label>
                                    </div>
                                    <div id="savedDiv" class="saved">
                                        <p class="pref">My saved preferences:
                                            <span id="savedLocale"></span>
                                            <span id="savedLanguage">(English)</span>
                                        </p>
                                        <p class="forget">
                                            <a href="#">Forget my location <span class="icon-pin"></span></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <!--/Region Selection -->

                    <!-- Language Selection -->
                    <nav id="dropLangNav" class="language">
                        <span class="code"><span class="text">EN</span> <span class="icon-downarrow"></span></span>
                        <ul id="dropLang">
                            <!-- TODO Update these to work through jquery bindings-->
                            <li class="selected"><a href="#en" class="en">English</a></li>
                            <li><a href="#zh" class="zh">繁體中文</a></li>
                            <li><a href="#de" class="de">Deutsch</a></li>
                            <li><a href="#es" class="es">Español</a></li>
                            <li><a href="#fr" class="fr">Français</a></li>
                            <li><a href="#it" class="it">Italiano</a></li>
                            <li><a href="#pt" class="pt">Português</a></li>
                            <li><a href="#zh_cn" class="zh_cn">简体中文</a></li>
                            <li><a href="#ja" class="ja">日本語</a></li>
                        </ul>
                    </nav>
                    <!--/Language Selection -->

                    <!-- Context Links -->
                    <nav class="contextLinks">
                        <ul>
                            <li><a href="/us/en/myProfileLogin.action"><span>My Profile</span></a></li>
                            <li><a href="http://www.flysaa.com/cms/commons/flysaa_registerNewsLetter.html"><span>Newsletter</span></a></li>
                        </ul>
                    </nav>
                    <!--/Context Links -->
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="masthead">
                <h1 class="logo"><a href="/za/en/home.action?request_locale=en_ZA">South African Airways</a></h1>
                <div id="adSpaceImage" class="leaderboard"><a href="#"><img src="herald/images/content-img/leaderboard.jpg" width="728" height="90"></a></div>
            </div>
        </div>
    </header>
    <!-- End of Site Header -->


    <!-- Start of Warnings -->
    <!-- TODO Styling of this has not been set -->
    <div id="warnings" class="hidden">
        <div class="wrap">
            <div class="noticeFix">
                <div id="globalnoticediv">
                    <!-- TODO Move onclick to jquery bindings -->
                    <a class="ntfWrap" onclick="cmsPageForGlobalNotice()">
                        <span class="ntfIcon"></span>
                        <span class="ntfArea" id="globalNoticeDesc"><span></span></span>
                        <!-- NEW DEV NOTE: Could this be set to the href of the link above??? -->
                        <input type="hidden" name="globalNoticeUrl" value="" id="za_en_home_globalNoticeUrl"/>
                        <span class="ntfMore">more info<span></span></span>
                    </a>
                </div>
                <div class="clr"></div>
                <div id="localnoticediv">
                    <!-- TODO Move onclick to jquery bindings -->
                    <a class="ntfWrap" onclick="cmsPageForLocalNotice()">
                        <span class="ntfIcon"></span>
                        <span class="ntfArea" id="localNoticeDesc"><span></span></span>
                        <!-- NEW DEV NOTE: Could this be set to the href of the link above??? -->
                        <input type="hidden" name="localNoticeUrl" value="" id="za_en_home_localNoticeUrl"/>
                        <span class="ntfMore">more info<span></span></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--/#warnings-->



    <!-- Previous id="midMenu" -->
    <section id="menu">
        <nav>
            <ul>
                <li class="plan-and-book"><a href="/za/en/planMyTrip.action">Plan &amp; Book</a>
                    <ul>
                        <li class="promotions">
                            <h2><span class="icon-menu-speaker"></span>Promotions</h2>
                            <ul class="highlight">
                                <li><span class="icon-menu-tail"></span><a href="#" target="_self">SAA Specials</a></li>
                                <li><span class="icon-menu-car"></span><a href="#" target="_self">Car Rental</a></li>
                                <li><span class="icon-menu-hotel"></span><a href="#" target="_self">Hotels</a></li>
                            </ul>
                            <ul>
                                <li><a href="#" target="_self">Airport Transfers</a></li>
                                <li><a href="#" target="_self">Insurance</a></li>
                                <li><a href="#" target="_self">SAA Online Shop</a></li>
                                <li><a href="#" target="_self">Executive Carport</a></li>
                                <li><a href="#" target="_self">SAA Holidays</a></li>
                                <li><a href="#" target="_self">Golden Tickets</a></li>
                            </ul>
                        </li>
                        <li class="book-flights">
                            <h2><span class="icon-menu-plane"></span>Book Flights</h2>
                            <ul>
                                <li><a href="#" target="_self">Flight schedules</a></li>
                                <li><a href="#" target="_self">Book a business trip</a></li>
                                <li><a href="#" target="_self">Book a holiday</a></li>
                            </ul>
                        </li>
                        <li class="destinations">
                            <h2><span class="icon-menu-pin"></span>Destinations</h2>
                            <ul>
                                <li><a href="#" target="_self">Destination guides</a></li>
                                <li><a href="#" target="_self">Route maps</a></li>
                                <li><a href="#" target="_self">New routes</a></li>
                                <li><a href="#" target="_self">Codeshares</a></li>
                                <li><a href="#" target="_self">City guides</a></li>
                                <li><a href="#" target="_self">Discover South Africa</a></li>
                                <li><a href="#" target="_self">Eventst</a></li>
                                <li><a href="#" target="_self">Airports</a></li>
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span>Travel Advisory</h2>
                            <ul>
                                <li><a href="#" target="_self">Yellow fever warning</a></li>
                                <li><a href="#" target="_self">Visa requirements</a></li>
                                <li><a href="#" target="_self">Health</a></li>
                                <li><a href="#" target="_self">Food &amp; drink</a></li>
                                <li><a href="#" target="_self">Cabin Disinfection</a></li>
                                <li><a href="#" target="_self">NEW: Immigration changes for children</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- Submenu -->
                </li>
                <li class="manage-my-booking"><a href="/za/en/reservation.action">Manage My Booking</a>
                    <ul>
                        <li class="promotions">
                            <h2><span class="icon-menu-speaker"></span>Before my flight</h2>
                            <ul>
                                <li><a href="/za/en/manageMyTrip/onlineCheckin/aboutOnlineCheckIn.html#MobileCheckIn">Mobile
                                        Check-in</a></li>
                                <li><a href="/za/en/manageMyTrip/onlineCheckin/onlineCheckIn.html">Online check-in</a></li>
                                <li><a href="/za/en/manageMyTrip/onlineCheckin/social-check-in.html">Social check-in</a></li>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Meal request</a></li>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Select seats</a></li>
                                <li><a href="/za/en/manageMyTrip/specialneeds/SpecialTravelNeeds.html">Special travel needs</a>
                                </li>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Health tips</a></li>
                                <li><a href="/za/en/manageMyTrip/baggage/checkedBaggage.html">Baggage policies</a></li>
                                <li><a href="/za/en/manageMyTrip/Visas/visa_and_passport_info.html">Visa requirements</a></li>
                                <li><a href="/za/en/manageMyTrip/loungeinformation/airportLounges.html">Lounge information</a>
                                </li>
                            </ul>
                        </li>
                        <li class="book-flights">
                            <h2><span class="icon-menu-plane"></span>On my flight</h2>
                            <ul>
                                <li><a href="/za/en/manageMyTrip/onboardHealth/OnboardExercise.html">In-flight fitness</a></li>
                                <li><a href="/za/en/manageMyTrip/onboardShopping/onboardShoppingCatalogue.html">Preview onboard
                                        shopping</a></li>
                                <li><a href="/za/en/manageMyTrip/SAAFleet/aircraftFeatures.html">View aircraft information</a>
                                </li>
                                <li><a href="/za/en/manageMyTrip/inflightEntertainment/InFlightEntertainment.html">View
                                        in-flight entertainment information</a></li>
                            </ul>
                        </li>
                        <li class="destinations">
                            <h2><span class="icon-menu-pin"></span>At my destination</h2>
                            <ul>
                                <li><a href="/za/en/manageMyTrip/AtMyDestination/SearchAccommodation.html">Hotels</a></li>
                                <li><a href="/za/en/manageMyTrip/AtMyDestination/carHireAirportTransfers.html">Car Rentals</a>
                                </li>
                                <li><a href="/za/en/planmytrip/destinations/destinationGuides.html">Destination information</a>
                                </li>
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span>After my flight</h2>
                            <ul>
                                <li><a href="/za/en/manageMyTrip/baggage/delayedDamagedBaggage.html">Baggage tracing</a></li>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Miles balance</a></li>
                                <li><a href="/za/en/voyagerLogin.action">Signup for Voyager frequent flyer</a></li>
                                <li><a href="/za/en/viewPastTrips!showScreen.action">View past reservation</a></li>
                                <li><a href="/za/en/taxInvoiceOnline.action">Request tax invoice</a></li>
                                <li><a href="/za/en/manageRequestCreditNote.action">Request a Credit note</a></li>
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span>My Details</h2>
                            <ul>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Update contact details</a></li>
                                <li><a href="/za/en/voyagerLogin.action">Signup for Voyager frequent flyer</a></li>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Add frequent flyer number to my booking</a></li>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Add fremec number to my booking</a></li>
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span>My booking</h2>
                            <ul>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Pay for reservation</a>
                                </li>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Change booking</a></li>
                                <li><a href="/za/en/manageMyTrip/manageinfo/ManageMyBookingInfo.html">Cancel booking and request
                                        refund</a></li>
                                <li><a href="/za/en/manageMyTrip/MyBooking/AmadeusInfo.html">View itinerary</a></li>
                                <li><a href="/za/en/manageMyTrip/paymentAndTicketing/TicketCollectionOptions.html">Payment &amp;
                                        ticketing</a></li>
                                <li><a href="/za/en/manageMyTrip/insurance/travel_insurance.html">Travel insurance</a></li>
                                <li><a href="/za/en/taxInvoiceOnline.action">Request a tax invoice</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a target="_blank" href="http://destinations.flysaa.com">Destinations</a></li>
                <li><a target="_blank" href="/za/en/home!loadSAAHolidays.action?redirect=/cms/ZA/leisure/saaHolidays.html">SAA Holidays</a></li>
                <li class="saa-experience"><a href="/za/en/flyingSAA.action">SAA Experience</a>
                    <ul>
                        <li class="promotions">
                            <h2><span class="icon-menu-speaker"></span>SAA Experience</h2>
                            <ul>
                                <li><a href="/za/en/flyingSAA/loungeinformation/airportLounges.html"><span>Airport lounges</span></a>
                                </li>
                                <li><a href="/za/en/flyingSAA/SAAFleet/aircraftFeatures.html"><span>SAA fleet</span></a></li>
                                <li><a href="/za/en/flyingSAA/inflightEntertainment/InFlightEntertainment.html"><span>In flight entertainment</span></a>
                                </li>
                                <li><a href="/za/en/flyingSAA/FoodAndWine/FoodWine.html"><span>Food and wine</span></a></li>
                                <li>
                                    <a href="/za/en/flyingSAA/onboardShopping/onboardShoppingCatalogue.html"><span>Onboard shopping</span></a>
                                </li>
                                <li><a href="/za/en/flyingSAA/Sawubona/Sawubona.html"><span>Sawubona Magazine</span></a></li>
                                <li><a href="/za/en/flyingSAA/voyager/Voyager.html"><span>Voyager Frequent flyer</span></a></li>
                                <li><a href="/za/en/flyingSAA/specialneeds/SpecialTravelNeeds.html"><span>Special assistance</span></a>
                                </li>
                                <li><a href="/za/en/flyingSAA/onboardHealth/OnboardExercise.html"><span>On board health</span></a></li>
                                <li><a href="/za/en/flyingSAA/aboutUs/SAA-Environment.html"><span>Environmental</span></a></li>
                                <li><a href="/za/en/flyingSAA/mobileaccess/mobileaccess.html"><span>Mobile access</span></a></li>
                                <li><a href="/za/en/flyingSAA/News/NewsList.html"><span>Latest news</span></a></li>
                                <li><a href="/za/en/flyingSAA/NewsLetters/newsletterArchive.html"><span>Newsletter archive</span></a>
                                </li>
                                <li><a href="/za/en/flyingSAA/healthyTravel/SAANetcare.html"><span>Travel clinic</span></a></li>
                                <li>
                                    <a href="/za/en/flyingSAA/paymentAndTicketing/PaymentOptions.html"><span>Payment &amp; ticketing</span></a>
                                </li>
                                <li><a href="/za/en/flyingSAA/FAQs/FAQs.html"><span>Frequently asked questions</span></a></li>
                            </ul>
                        </li>
                        <li class="book-flights">
                            <h2><span class="icon-menu-plane"></span>Book Flights</h2>
                            <ul>
                                <li><a href="#" target="_self">Flight schedules</a></li>
                                <li><a href="#" target="_self">Book a business trip</a></li>
                                <li><a href="#" target="_self">Book a holiday</a></li>
                            </ul>
                        </li>
                        <li class="destinations">
                            <h2><span class="icon-menu-pin"></span>Destinations</h2>
                            <ul>
                                <li><a href="#" target="_self">Destination guides</a></li>
                                <li><a href="#" target="_self">Route maps</a></li>
                                <li><a href="#" target="_self">New routes</a></li>
                                <li><a href="#" target="_self">Codeshares</a></li>
                                <li><a href="#" target="_self">City guides</a></li>
                                <li><a href="#" target="_self">Discover South Africa</a></li>
                                <li><a href="#" target="_self">Eventst</a></li>
                                <li><a href="#" target="_self">Airports</a></li>
                            </ul>
                        </li>
                        <li class="travel-advisory">
                            <h2><span class="icon-menu-case"></span>Travel Advisory</h2>
                            <ul>
                                <li><a href="#" target="_self">Yellow fever warning</a></li>
                                <li><a href="#" target="_self">Visa requirements</a></li>
                                <li><a href="#" target="_self">Health</a></li>
                                <li><a href="#" target="_self">Food &amp; drink</a></li>
                                <li><a href="#" target="_self">Cabin Disinfection</a></li>
                                <li><a href="#" target="_self">NEW: Immigration changes for children</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a class="subscribe" href="/za/en/customercharter/CustomerServiceCharter.html">Customer Service</a></li>
                <li class="last essential-info"><a href="#">Essential info</a>
                    <ul>
                        <li class="promotions">
                            <h2><span class="icon-menu-speaker"></span>Essential info</h2>
                            <ul id="essentialInfoLinks">
                                <li><a href="/za/en/manageMyTrip/Visas/visa_and_passport_info.html">Visa and Passport
                                        Information</a></li>
                                <li><a href="/za/en/planmytrip/travelAdvisory/sa-immigration-under-18.html">NEW: SA immigration
                                        changes for children</a></li>
                                <li><a href="/za/en/essentialInfo/essentialinfo/SAA-and-Mango-codeshare.html">South African
                                        Airways &amp; Mango codeshare</a></li>
                                <li><a href="/za/en/essentialInfo/essentialinfo/checkinInfo.html">Essential check-in
                                        information</a></li>
                                <li><a href="/za/en/essentialInfo/essentialinfo/closureTimes.html">Flight &amp; boarding gate
                                        closure times</a></li>
                                <li><a href="/za/en/essentialInfo/essentialinfo/delayedDamagedBaggage.html">Baggage tracking</a>
                                </li>
                                <li><a href="/za/en/essentialInfo/essentialinfo/YellowFever.html">Yellow fever advisory</a></li>
                                <li><a href="/za/en/essentialInfo/essentialinfo/SAA-Environment.html">SAA and the
                                        environment</a></li>
                                <li><a href="/za/en/flyingSAA/News/SAA_launches_new_mobile_applications.html">SAA launches newly
                                        developed mobile applications</a></li>
                                <li><a href="/za/en/essentialInfo/essentialinfo/PolioVaccination.html">Polio Vaccination</a>
                                </li>
                                <li><a href="/za/en/essentialInfo/essentialinfo/brazil_cuurrency_restricions.html">Brazil
                                        Currency Restrictions</a></li>
                                <li><a href="/za/en/essentialInfo/essentialinfo/Ebola.html">Ebola Virus Disease</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <a href="#" class="best-fare-guarantee">&nbsp;</a>
        </nav>
    </section>
    <!-- Mid Menu/Nav -->
