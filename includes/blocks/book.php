<div id="bb-flights">
    <form name="bbFlights" action="" method="post">
        <input type="hidden" name="selectedLang" />
        <input type="hidden" name="selLanguage" id="selLanguage" />
        <input type="hidden" name="countrySeltd" id="countrySeltd" />
        <input type="hidden" name="country" id="sessionCountry" />
        <input type="hidden" name="preferredClass" id="preferredClass" />
        <input type="hidden" name="fromDate" id="fromDate"/>
        <input type="hidden" name="toDate" id="toDate"/>

        <h2>Book A Flight</h2>
        <!--
        <div class="input radios">
            <label class="return"><input type="radio" name="tripType" id="tpTypeR" value="R">Return</label>
            <label class="one-way"><input type="radio" name="tripType" id="tpTypeO" value="O">One Way</label>
            <label class="multi-city"><input type="radio" name="tripType" id="tpTypeM" value="M" onclick="goToMulticityFlightSearch(this.value);">Multi-city</label>
        </div>
        -->
        <div class="input select combobox">
            <select name="departCity" id="departCity" title="Departure City">
            </select>
        </div>
        <div class="input row text">
            <div class="column small-8 select combobox">
                <select name="destCity" id="destCity" title="Destination City">
                </select>
            </div>
            <div class="column small-4">
                <a href="#" class="button small no-margin multi-city"><span>Multi-City</span></a>
            </div>
        </div>
        <div class="input datepicker">
            <label>Leaving</label>
            <div>
                <div class="input day">
                    <select id="departDay" name="departDay">
                        <option></option>
                    </select>
                </div>
                <div class="input month-year">
                    <select id="departMonthYear" name="departMonthYear">
                        <option></option>
                    </select>
                </div>
                <div class="icon-calendar">
                    <div class="calendar leaving"><span class="title">Leaving</span></div>
                </div>
            </div>
        </div>
        <div class="input datepicker">
            <label>Returning<input type="checkbox" id="chkReturn" name="chkReturn" checked="checked"></label>
            <input type="hidden" id="tripType" name="tripType"/>
            <div>
                <div class="input day">
                    <select id="destDay" name="destDay">
                        <option></option>
                    </select>
                </div>
                <div class="input month-year">
                    <select id="returnMonthYear" name="returnMonthYear">
                        <option></option>
                    </select>
                </div>
                <div class="icon-calendar">
                    <div class="calendar returning"><span class="title">Returning</span></div>
                </div>
            </div>
        </div>
        <div class="flexibility">
            <div class="row">
                <div class="column small-6">
                    <div class="input radio">
                        <label for="radExact"><input id="radExact" type="radio" name="flexible" value="false">Exact Dates</label>
                    </div>
                </div>
                <div class="column small-6 flex">
                    <div class="input radio">
                        <label for="radFlexible"><input id="radFlexible" type="radio" name="flexible" value="true">+/- 3 days </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row input">
            <div class="column small-4 input select">
                <select id="adultCount" name="adultCount" class="adults">
                    <option value="1">1 Adult</option>
                    <option value="2">2 Adults</option>
                    <option value="3">3 Adults</option>
                    <option value="4">4 Adults</option>
                </select>
                <p class="tip">+12 years</p>
            </div>
            <div class="column small-4 input select">
                <select id="childCount" name="childCount" class="children">
                    <option value="0">Children</option>
                    <option value="1">1 Child</option>
                    <option value="1">2 Children</option>
                    <option value="1">3 Children</option>
                    <option value="1">4 Children</option>
                </select>
                <p class="tip">2-11 years</p>
            </div>
            <div class="column small-4 input select">
                <select id="infantCount" name="infantCount" class="infants">
                    <option value="0">Infants</option>
                    <option value="1">1 Infant</option>
                    <option value="2">2 Infants</option>
                    <option value="3">3 Infants</option>
                    <option value="4">4 Infants</option>
                </select>
                <p class="tip">0-23 months</p>
            </div>
        </div>
        <div class="row input">
            <div class="column small-7">
                <label class="inline">Cabin Class<a href="" class="icon-info">
                                            <span>
                                                <span class="head">About Cabin Class</span>
                                                <span class="inner">
                                                    <strong>Economy class</strong><br>
                                                    Travel in comfort in our relaxing cabin.<br>
                                                    <strong>Business class</strong><br>
                                                    Take to the skies in comfort and style.
                                                </span>
                                            </span>
                    </a></label>
            </div>
            <div class="column small-5">
                <select id="flightClass" name="preferredCabinClass" class="cabin-class">
                    <option>Economy</option>
                    <option>Business</option>
                </select>
            </div>
        </div>
        <div class="row input">
            <div class="column small-7">
                <label class="inline">Promo Code<a href="" class="icon-info">
                                            <span>
                                                <span class="head">Promotional code</span>
                                                <span class="inner">South African Airways will run Special Promotions from
                                                    time to time and notify flysaa.com users via E-mail. You should use the
                                                    Promotion code sent to you via e-mail in order to receive the discount
                                                    advertised at the times specified.
                                                </span>
                                            </span>
                    </a></label>
            </div>
            <div class="column small-5">
                <input type="text" id="promoCode" name="promoCode">
            </div>
        </div>
        <p class="note"><small>Booking for more than 9 people?
                <a class="morePaxLink" href="/za/en/groupBooking.action">Click here!</a></small></p>
        <div class="input submit text-right">
            <button type="submit" class="button green submit"><span>Book</span></button>
        </div>
    </form>
</div>
