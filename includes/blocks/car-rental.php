<div id="bb-car-rental">
    <form action="" method="post">
        <h2>Car Rental</h2>
        <div class="input combobox">
            <select name="pickupLoc" id="pickupLoc" title="Type your pick-up point here">
                <option value=""></option>
            </select>
            <input id="txtpick_loc" name="txtpick_loc" type="hidden" />
        </div>

        <div class="input combobox">
            <select name="dropoffLoc" id="dropoffLoc" title="Type your drop-off point here">
                <option value=""></option>
            </select>
            <input id="txtdrop_loc" name="txtdrop_loc" type="hidden" />
        </div>

        <div id="carDateUp" class="input datetimepicker">
            <label>Pick-up date</label>
            <div>
                <div class="input day">
                    <select id="pickDay" name="pickDay">
                    </select>
                </div>
                <div class="input month-year">
                    <select id="pickMonthYear" name="pickMonthYear">
                    </select>
                </div>
                <div class="icon-calendar">
                    <div class="calendar pick-up"><span class="title">Pick-up</span></div>
                </div>

                <div class="clearfix"></div>

                <label class="time">Time</label>
                <div class="input time">
                    <select name="pickUpTime" id="pickUpTime">
                        <option value="0000">00:00</option>
                        <option value="0030">00:30</option>
                        <option value="0100">01:00</option>
                        <option value="0130">01:30</option>
                        <option value="0200">02:00</option>
                        <option value="0230">02:30</option>
                        <option value="0300">03:00</option>
                        <option value="0330">03:30</option>
                        <option value="0400">04:00</option>
                        <option value="0430">04:30</option>
                        <option value="0500">05:00</option>
                        <option value="0530">05:30</option>
                        <option value="0600">06:00</option>
                        <option value="0630">06:30</option>
                        <option value="0700">07:00</option>
                        <option value="0730">07:30</option>
                        <option value="0800">08:00</option>
                        <option value="0830">08:30</option>
                        <option value="0900" selected="selected">09:00</option>
                        <option value="0930">09:30</option>
                        <option value="1000">10:00</option>
                        <option value="1030">10:30</option>
                        <option value="1100">11:00</option>
                        <option value="1130">11:30</option>
                        <option value="1200">12:00</option>
                        <option value="1230">12:30</option>
                        <option value="1300">13:00</option>
                        <option value="1330">13:30</option>
                        <option value="1400">14:00</option>
                        <option value="1430">14:30</option>
                        <option value="1500">15:00</option>
                        <option value="1530">15:30</option>
                        <option value="1600">16:00</option>
                        <option value="1630">16:30</option>
                        <option value="1700">17:00</option>
                        <option value="1730">17:30</option>
                        <option value="1800">18:00</option>
                        <option value="1830">18:30</option>
                        <option value="1900">19:00</option>
                        <option value="1930">19:30</option>
                        <option value="2000">20:00</option>
                        <option value="2030">20:30</option>
                        <option value="2100">21:00</option>
                        <option value="2130">21:30</option>
                        <option value="2200">22:00</option>
                        <option value="2230">22:30</option>
                        <option value="2300">23:00</option>
                        <option value="2330">23:30</option>
                    </select>
                </div>
            </div>
        </div>

        <div id="carDateOff" class="input datetimepicker">
            <label>Drop-off date</label>
            <div>
                <div class="input day">
                    <select id="dropoffDay" name="dropoffDay">
                    </select>
                </div>
                <div class="input month-year">
                    <select id="dropoffMonthYear" name="dropoffMonthYear" class="month-year">
                    </select>
                </div>
                <div class="icon-calendar">
                    <div class="calendar drop-off"><span class="title">Drop-off</span></div>
                </div>

                <div class="clearfix"></div>

                <label class="time">Time</label>
                <div class="input time">
                    <select id="dropOffTime" name="dropOffTime">
                        <option value="0000">00:00</option>
                        <option value="0030">00:30</option>
                        <option value="0100">01:00</option>
                        <option value="0130">01:30</option>
                        <option value="0200">02:00</option>
                        <option value="0230">02:30</option>
                        <option value="0300">03:00</option>
                        <option value="0330">03:30</option>
                        <option value="0400">04:00</option>
                        <option value="0430">04:30</option>
                        <option value="0500">05:00</option>
                        <option value="0530">05:30</option>
                        <option value="0600">06:00</option>
                        <option value="0630">06:30</option>
                        <option value="0700">07:00</option>
                        <option value="0730">07:30</option>
                        <option value="0800">08:00</option>
                        <option value="0830">08:30</option>
                        <option value="0900" selected="selected">09:00</option>
                        <option value="0930">09:30</option>
                        <option value="1000">10:00</option>
                        <option value="1030">10:30</option>
                        <option value="1100">11:00</option>
                        <option value="1130">11:30</option>
                        <option value="1200">12:00</option>
                        <option value="1230">12:30</option>
                        <option value="1300">13:00</option>
                        <option value="1330">13:30</option>
                        <option value="1400">14:00</option>
                        <option value="1430">14:30</option>
                        <option value="1500">15:00</option>
                        <option value="1530">15:30</option>
                        <option value="1600">16:00</option>
                        <option value="1630">16:30</option>
                        <option value="1700">17:00</option>
                        <option value="1730">17:30</option>
                        <option value="1800">18:00</option>
                        <option value="1830">18:30</option>
                        <option value="1900">19:00</option>
                        <option value="1930">19:30</option>
                        <option value="2000">20:00</option>
                        <option value="2030">20:30</option>
                        <option value="2100">21:00</option>
                        <option value="2130">21:30</option>
                        <option value="2200">22:00</option>
                        <option value="2230">22:30</option>
                        <option value="2300">23:00</option>
                        <option value="2330">23:30</option>
                    </select>
                </div>
            </div>
        </div>
<!--
        <div class="input combobox">
            <select name="carCountry" id="carCountry" title="Type your Country of Residence">
            </select>
            <input id="txtcountry" name="txtcountry" type="hidden" />
        </div>

        <div class="input text">
            <div class="carDriverAge">
                <input id="driverAge" maxlength="3" placeholder="Driver Age" />
            </div>
        </div>
-->
        <div class="actions">
            <button type="submit" class="button green right submit no-margin"><span>Book</span></button>
            <button type="reset" class="button orange left reset no-margin"><span>Reset</span></button>
        </div>
    </form>
    <a class="cars adSpace" target="_blank" href="http://www.flysaa.com/za/en/manageMyTrip/insurance/travel_insurance.html"></a>
</div>
