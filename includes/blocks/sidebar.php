<div id="bb" class="tabs">
    <ul>
        <li class="flights"><a href="#bb-flights"><span class="icon-bb-flight"></span><span class="text">Flights</span></a></li>
        <li class="check-in"><a href="#bb-check-in"><span class="icon-bb-checkin"></span><span class="text">Check In</span></a></li>
        <li class="car-rental"><a href="#bb-car-rental"><span class="icon-bb-car"></span><span class="text">Car Rental</span></a></li>
    </ul>
    <div class="overlay loading"></div>
    <div class="info-popup"><header></header><div></div></div>
    <div class="blocks">
        <?php include('book.php');?>
        <?php include('check-in.php');?>
        <?php include('car-rental.php');?>
    </div>
</div>
