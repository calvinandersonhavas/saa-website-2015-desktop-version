<div id="bb-log-in">
    <form name="voyagerLogin" action="" method="post">
        <div class="voyager-status-Y">
            <div class="out">
                <h2>SAA Voyager Login</h2>
                <div class="input">
                    <label for="voyagerId">Voyager Number</label>
                    <input id="voyagerId" name="voyagerId" maxlength="15">
                </div>
                <div class="input">
                    <label for="pin">PIN</label>
                    <input id="pin" name="pin" type="password" maxlength="4">
                </div>
                <div>
                    <div class="left">
                        <p class="no-margin"><small>
                                <a href="#" class="forgot"><span class="icon-arrow-right"></span>I forgot my details</a>
                                <br />
                                <a href="/za/en/voyager/AboutVoyager/flysaa_VoyagerContacts.html"><span class="icon-arrow-right"></span>Help desk</a>
                            </small>
                        </p>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="button green submit no-margin"><span>Login</span></button>
                        <a href="#" onclick="checkJoinVoyager()" class="button join"><span>Join Voyager</span></a>
                    </div>
                </div>
                <!--
                <div>
                    <h2>SAA Voyager Credit Card</h2>
                    <p>Get the best Voyager Mile earn rate with the SAA Voyager credit card. </p>
                    <p class="text-right"><a target="_blank" href="https://www.nedbank.co.za/website/content/forms/form.asp?FormsId=621&amp;Location=&amp;FormLocation=flysaa" class="button"><span>Apply online now</span></a></p>
                </div>
                -->
            </div>
            <div class="in">
                <h2>SAA Voyager</h2>
                <hr />
                <h3>Welcome, Joe</h3>
                <p class="half-margin">Membership #:<br />
                    <span class="bold blue">123456</span></p>
                <p class="half-margin">Miles:<br />
                    <span class="bold blue">576543</span></p>
                <p class="half-margin">Status:<br />
                    <span class="bold blue">Bronze</span></p>
                <hr class="dashed" />
                <nav class="meta">
                    <p>
                    <a href="#" class="link-arrow">My Details</a><br />
                    <a href="#" class="link-arrow">Awards</a><br />
                    <a href="#" class="link-arrow">Mileage</a><br />
                    <a href="#" class="link-arrow">Calculator</a><br />
                    <a href="#" class="link-arrow">News</a><br />
                    <a href="#" class="link-arrow">Help Desk</a>
                    </p>
                </nav>
                <br />
                <p class="text-right">
                    <a href="#" class="button"><span>My Voyager</span></a>
                </p>
            </div>
        </div>
    </form>

    <form name="voyagerForgot">
        <div class="clearfix">
            <h2>Forgot password</h2>

            <div class="input">
                <label for="voyagerNumber">Voyager Number</label>
                <input name="voyagerNumber" id="voyagerNumber">
            </div>

            <div class="input">
                <label for="email">E-mail Address</label>
                <input name="email" id="email">
            </div>

            <div>
                <div class="left">
                    <a class="button orange back"><span>Cancel</span></a>
                </div>
                <div class="text-right">
                    <button type="submit" class="button green submit"><span>Send</span></button>
                </div>
            </div>
        </div>
    </form>

</div>
