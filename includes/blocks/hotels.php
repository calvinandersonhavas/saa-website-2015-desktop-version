<div id="bb-hotels">
    <form action="" method="post">
        <h2>Find a Hotel</h2>
        <div class="input select combobox">
            <select name="hotelDestination" id="hotelDestination" title="Type your destination here">
                <option value=""></option>
            </select>
            <input id="txtHotelDes_loc" name="txtHotelDes_loc"  type="hidden" />
        </div>

        <div class="input datepicker">
            <label>Check In</label>
            <div>
                <div class="input day">
                    <select id="checkinDay" name="checkinDay">
                        <option></option>
                    </select>
                </div>
                <div class="input month-year">
                    <select id="checkinMonthYear" name="checkinMonthYear">
                        <option></option>
                    </select>
                </div>
                <div class="icon-calendar">
                    <div class="calendar check-in"><span class="title">Check In</span></div>
                </div>
            </div>
        </div>

        <div class="input datepicker">
            <label>Check Out</label>
            <div>
                <div class="input day">
                    <select id="checkoutDay" name="checkoutDay">
                        <option></option>
                    </select>
                </div>
                <div class="input month-year">
                    <select id="checkoutMonthYear" name="checkoutMonthYear">
                        <option></option>
                    </select>
                </div>
                <div class="icon-calendar">
                    <div class="calendar check-out"><span class="title">Check Out</span></div>
                </div>
            </div>
        </div>

        <div class="row input hotel-guests">
            <div class="column small-4 input select">
                <select id="hotelAdults" name="hotelAdults" class="adults">
                    <option value="1">1 Adult</option>
                    <option value="2">2 Adults</option>
                    <option value="3">3 Adults</option>
                    <option value="4">4 Adults</option>
                </select>
                <p class="tip">+12 years</p>
            </div>
            <div class="column small-4 input select">
                <select id="hotelChildren" name="hotelChildren" class="children">
                    <option value="0">Children</option>
                    <option value="1">1 Child</option>
                    <option value="2">2 Children</option>
                    <option value="3">3 Children</option>
                    <option value="4">4 Children</option>
                </select>
                <p class="tip">2-11 years</p>
            </div>
            <div class="column small-4 input select">
                <select id="hotelInfants" name="hotelInfants" class="infants">
                    <option value="0">Infants</option>
                    <option value="1">1 Infant</option>
                    <option value="2">2 Infants</option>
                    <option value="3">3 Infants</option>
                    <option value="4">4 Infants</option>
                </select>
                <p class="tip">0-23 months</p>
            </div>
        </div>
        <div class="row input">
            <div class="column small-6">
                <label class="inline">Number of rooms</label>
            </div>
            <div class="column small-6">
                <select id="numberoffrooms" name="numberoffrooms" class="infants">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
        </div>
        <div class="clearfix">
            <button type="submit" class="button green right submit no-margin"><span>Book now</span></button>
            <button type="reset" class="button orange left reset no-margin"><span>Reset</span></button>
        </div>
    </form>
</div>