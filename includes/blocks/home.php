<div id="bb" class="tabs">
    <ul>
        <li class="flights"><a href="#bb-flights"><span class="icon-bb-flight"></span><span class="text">Flights</span></a></li>
        <li class="check-in"><a href="#bb-check-in"><span class="icon-bb-checkin"></span><span class="text">Check In</span></a></li>
        <li class="log-in"><a href="#bb-log-in"><span class="icon-bb-user"></span><span class="text">Log In</span></a></li>
        <li class="flight-status"><a href="#bb-flight-status"><span class="icon-bb-timer"></span><span class="text">Flight Status</span></a></li>
        <li class="car-rental"><a href="#bb-car-rental"><span class="icon-bb-car"></span><span class="text">Car Rental</span></a></li>
        <li class="hotels"><a href="#bb-hotels"><span class="icon-bb-hotel"></span><span class="text">Hotels</span></a></li>
    </ul>
    <div class="overlay loading"></div>
    <div class="info-popup"><header></header><div></div></div>
    <div class="blocks">
        <?php include('book.php');?>
        <?php include('check-in.php');?>
        <?php include('log-in.php');?>
        <?php include('flight-status.php');?>
        <?php include('car-rental.php');?>
        <?php include('hotels.php');?>
    </div>
</div>
