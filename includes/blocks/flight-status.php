<div id="bb-flight-status">
    <form action="" method="post">
        <h2>Flight Status</h2>
        <div class="input checkbox">
            <label><input id="flightChecked" name="flightChecked" type="checkbox">I know the flight number</label>
        </div>

        <div id="FlightNo" class="input row hidden">
            <div class="column small-6">
                <label for="carrierCode">Carrier Code</label>
                <input id="carrierCode" name="carrierCode" maxlength="2" value="SA" disabled="disabled">
            </div>
            <div class="column small-6">
                <label for="flightNumber">Flight Number</label>
                <input id="flightNumber" name="flightNumber" maxlength="5">
            </div>
        </div>

        <div id="NoFlight">
            <div class="input select combobox">
                <select title="Departure City" id="statusDepartCity" name="departureCity">
                    <option value="">Departure City</option>
                    <option value="AAL">Aalborg, Denmark</option>
                    <option value="ABZ">Aberdeen, United Kingdom</option>
                    <option value="ABJ">Abidjan, Ivory Coast</option>
                </select>
            </div>

            <div class="input select combobox">
                <select title="Destination City" id="statusDestCity" name="destinationCity">
                    <option value="">Destination City</option>
                    <option value="AAL">Aalborg, Denmark</option>
                    <option value="ABZ">Aberdeen, United Kingdom</option>
                    <option value="ABJ">Abidjan, Ivory Coast</option>
                </select>
            </div>

            <div class="input datepicker">
                <label>Departure Date</label>
                <div class="select">
                    <select class="DepDate" id="fromDateFLT">
                        <option value="0">Today</option>
                        <option value="-1">Yesterday</option>
                        <option value="1">Tomorrow</option>
                    </select>
                    <input type="hidden" name="fromDateFLT" value="">
                </div>
            </div>

        </div>

        <div class="clearfix">
            <button type="submit" class="button green right submit no-margin"><span>Continue</span></button>
            <button type="reset" class="button orange left reset no-margin"><span>Reset</span></button>
            <a class="button orange left" href="/za/en/flightStatus/flight_status_other_airlines.html"><span>Other Airlines</span></a>
        </div>

        <footer>
            <h3>Flight Schedule</h3>
            <p>Keep track of our in and outbound flights!</p>
            <div class="text-center">
                <a href="/za/en/Documents/flightschedules/Timetable.pdf" rel="external" class="button orange download"><span>PDF</span></a>
                <a href="#" class="button route"><span>Online</span></a>
            </div>
        </footer>
    </form>
</div>
