<div id="bb-check-in">
    <form action="" method="post">
        <h2>Check In</h2>
        <header>
            <div class="input radios">
                <label><input type="radio" name="fcheckin" id="checkInSAA" value="SaaCheckin" checked="checked">SAA Check In</label>
                <label><input type="radio" name="fcheckin" id="checkInMango" value="MangoCheckin">Mango Check In</label>
            </div>
        </header>
        <hr />
        <p class="text-center no-margin">Online Check In is available <strong class="gold">24 hours</strong>
            before your flight departure time</p>
        <hr class="dashed" />

        <div id="checkInSAADiv" class="check-in">
            <div class="input text">
                <label>Surname</label>
                <input type="text" id="ISurname" name="ISurname">
            </div>
            <div class="input">
                <label class="label">Form of identification <a class="icon-info">
                                        <span>
                                            <span class="head">Form of identification</span>
                                            <span class="inner">You can use one of the following items to identify your booking:<br>
                                                <strong>- Booking reference</strong>,<br>
                                                <strong>- Electronic Ticket Number</strong><br>
                                                <strong>- South African Frequent Flyer Number</strong>.
                                            </span>
                                        </span>
                    </a>
                </label>
                <div class="input">
                    <select id="checkInMethod" name="checkInMethod">
                        <option value="PNR">Booking Reference</option>
                        <option value="FFSA">South African Frequent Flyer Number</option>
                        <option value="ETKT">Electronic Ticket Number</option>
                    </select>
                </div>
            </div>
            <div class="input">
                <label class="check-in-method pnr">Booking reference
                    <a class="icon-info">
                                            <span>
                                                <span class="head">Booking reference</span>
                                                <span class="inner">The Booking Reference is a six letters-and-figures code you can find in the booking confirmation email or printed on the ticket.<br><br><strong>Example: 3OTCHJ</strong></span>
                                            </span>
                    </a>
                </label>
                <label class="check-in-method ffsa">South African Frequent Flyer Number
                    <a class="icon-info">
                                            <span>
                                                <span class="head">South African Frequent Flyer Number</span>
                                                <span class="inner">The Frequent Flyer number is the code printed on your Frequent Flyer card.<br><br><strong>Example: 32792897.</strong></span>
                                            </span>
                    </a>
                </label>
                <label class="check-in-method etkt">Electronic Ticket Number
                    <a class="icon-info">
                                            <span>
                                                <span class="head">Electronic Ticket Number</span>
                                                <span class="inner">The Electronic Ticket number is a thirteen digit number you can find in the booking confirmation email or printed on the ticket.<br><br><strong>Example: 0832402453452.</strong></span>
                                            </span>
                    </a>
                </label>
                <input id="IIdentification" />
            </div>
            <div class="input">
                <label><input id="checkInAddPax" type="checkbox" /> Check in additional passengers</label>
            </div>
        </div>

        <div id="checkInMangoDiv" class="check-in hidden">
            <div class="input text">
                <label>Surname</label>
                <input type="text" id="ISurnameM" name="ISurnameM">
            </div>
            <div class="input">
                <label class="label">Form of identification
                    <a class="icon-info">
                                                <span>
                                                    <span class="head">Form of identification</span>
                                                    <span class="inner">The Electronic Ticket number is a thirteen digit number you
                                                        can find in the booking confirmation email or printed on the ticket.<br><br>
                                                        <strong>Example: 0832402453452.</strong>
                                                    </span>
                                                </span>
                    </a>
                </label>
                <label class="label">Electronic Ticket Number</label>
                <input type="text" id="etktnumberM" name="etktnumberM" />
            </div>
            <div class="input">
                <label><input id="checkInAddPaxM" name="checkInAddPaxM" type="checkbox" /> Check in additional passengers</label>
            </div>
        </div>
        <div class="input submit">
            <div class="left">
                <button type="reset" class="button orange reset"><span>Reset</span></button>
            </div>
            <div class="right">
                <button type="submit" class="button green submit"><span>Continue</span></button>
            </div>
        </div>
    </form>
</div>
