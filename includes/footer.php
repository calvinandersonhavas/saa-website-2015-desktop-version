


<footer id="footer">
    <div class="wrap">
        <div class="copyright">&copy; 2014 South African Airways</div>
        <nav>
            <ul>
                <li><a href="/za/en/about_us.action">About Us</a></li>
                <li><a href="/za/en/contact_us.action">Contact Us</a></li>
                <li><a href="/za/en/careers.action"> Careers</a></li>
                <li><a href="/za/en/policies_and_Disclaimers.action">Policies</a></li>
                <li><a href="/za/en/conditions_of_Contract.action">Conditions of Contract</a></li>
                <li><a href="/za/en/browser_Requirements.action">Browser Requirements</a></li>
                <li><a href="/za/en/accessibility.action">Accessibility</a></li>
                <li><a href="/za/en/imprint.action">Legal</a></li>
                <li><a href="/za/en/sitemap/sitemap.html">Site Map</a></li>
                <li><a href="/za/en/planmytrip/destinations/codeshares.html">Codeshares</a></li>
            </ul>
        </nav>
        <aside class="alliance" id="allianceLogo"><a target="_blank" class="satourism" href="http://www.southafrica.net">South African Tourism</a></aside>
        <aside class="social">
            <a href="http://www.facebook.com/FlySAA" class="facebook">Facebook</a>
            <a href="http://twitter.com/@flysaa" class="twitter">Twitter</a>
            <a href="http://www.flysaa.com/dynamicAd/GetAdvert/en_ZA_specials" class="rss" rel="external">RSS</a>
            <a href="http://plus.google.com/+southafricanairways" class="googleplus">Google +</a>
            <a href="https://itunes.apple.com/us/app/south-african-airways/id619821296?mt=8" class="applestore">Apple Store</a>
            <a href="https://play.google.com/store/apps/details?id=com.flysaa.mobile.android" class="googleplay">Google Play Store</a>
            <a href="http://apps.microsoft.com/windows/app/flysaa/8add1a17-1300-4045-9d24-bbde76994206" class="windowstore">Window Store</a>
            <a href="http://pinterest.com/flysaa" class="pinterest">Pinterest</a>
        </aside>
        <aside id="snoopy">Â§</aside>
    </div>
</footer>



<form name="submitter" action="" method="POST">
    <input type="hidden" name="country" value="ZA" />
    <!--
            <input type="hidden" name="selLanguage" value="EN" />
            <input type="hidden" name="selectedLang" value="" id="za_en_home_selectedLang"/>
            <input type="hidden" name="preferredClass" value="" id="preferredClass"/>
            <input type="hidden" name="calendarSearchFlag" value="false" id="za_en_home_calendarSearchFlag"/>
    -->
</form>




<!-- Helpers -->
<!-- Start of Alert-->


<div id="scriptAlert" class="alertmodal">
    <a href="javascript:;" class="close" onclick="modalClose('scriptAlert')">Close</a>
    <h4>label.modal.modalBox</h4>
    <div class="inner">
    </div>
</div>


<!-- End of Alert Section-->

<div id="modal"></div>

<!-- Help Div for manage res retrieve and pay -->
<div id="helpModalPopUp" class="modal">
    <a href="javascript:;" class="close" onclick="modalClose('helpModalPopUp')">Close</a>
    <h4>Need Help ?</h4>
    <div class="inner scrollable">
        <div id="helpModalPopUpDetails" class="scr">
        </div>
    </div>
    <div class="bottom"></div>
</div>


<div id="disabler"></div>

<a href="#" onclick="viewSnoopy()" style="position: absolute;left: 0;bottom: 0;z-index: 1000;"><font color="#4676a7">S</font> </a>

<!--iFrame Wrappers-->
<div id="iframePopup">
    <a href="#" class="close">Close</a>
    <div class="inner">

    </div>
</div>
<!--/iFrame Wrappers-->

<!--/Helpers -->



<!-- NEW DEV NOTE: This has been added to pass variables from the server to the browser -->
<script type="text/javascript">
    //These were pulled out of the top of the body and should be in the propsArray and not as html tags
    var config = {
//          <div id="display_language" class="hidden">EN</div>
        language: 'EN',
//          <div id="user_location" class="hidden">ZA</div>
        country: 'ZA',
//          <div id="long_location" class="hidden"></div>
        countryLong: 'South Africa',
        //<div id='locale_str' style='display: none;'>en_ZA</div>
        locale_str: 'en_ZA',

        mobileUser: false,

        //moved from region diw
//          <div id="selectedCountryDiv" class="hide">ZA</div> //There is no need for this, it is already set above
//          <div id="selectedLocaleDiv" class="hide">EN</div> //There is no need for this, it is already set above
//          <div id="isCookieEnabledDiv" class="hide">false</div>
        isCookieEnabled: false,

        //<input type="hidden" name="selectedProductIs" value="FTS" id="selectedProductIs">
        selectedProductIs: 'FTS',

        //To set country and language from the Menu in home page
        //There is no need for these, it is already set above
//          <input type="hidden" name="country" value="ZA" id="sessionCountry" />
//          <input type="hidden" name="selLanguage" value="EN" id="selLanguage" />

        //Voyager Settings
        isHomeVoyLogin: 'N'

    };

    var	voyagerUser = {
        nameInitial:    'Test',
        name:           'User',
        membrshpNum:    '1234',
        miles:          '53653',
        currenttierid:  '',
        status:         ''
    };

</script>
<script type="text/javascript" src="herald/js/libraries.js"></script>
<script type="text/javascript" src="herald/js/home.js"></script>
<script>
    /**
     * Lanugage Translations
     */
    propsArray['bookingBlock_0']	= "Trip type is required.";
    propsArray['bookingBlock_1']	= "Please select a departure city.";
    propsArray['bookingBlock_2']	= "Please select a destination city.";
    propsArray['bookingBlock_3']	= "The Origin and Destination are the same, please select a different origin or destination.";
    propsArray['bookingBlock_4']	= "Minimum of 1 adult traveler required in the booking.";
    propsArray['bookingBlock_5']	= "For safety reasons on-board, each infant under 2 years must be accompanied by a separate adult";
    propsArray['bookingBlock_6']	= "The total number of adults,children and infants must not exceed 9. Please consider splitting your group or complete the <a href=\"groupBooking.action\">Groups Request Form</a>";
    propsArray['bookingBlock_7']	= "Please select class of travel.";
    propsArray['bookingBlock_8']	= "From Date is required.";
    propsArray['bookingBlock_9']	= "To Date is required.";
    propsArray['bookingBlock_10']	= "Sorry you cannot go beyond the designated range!";
    propsArray['bookingBlock_11']	= "Sorry! you may not go beyond the designated date.";
    propsArray['bookingBlock_12']	= "The return date you have selected is before the departure date. Please review the dates selected.";
    propsArray['bookingBlock_13']	= "Please select a proper location";
    propsArray['bookingBlock_14']	= "Please select valid dates";
    propsArray['bookingBlock_15']	= "Please choose valid time";
    propsArray['bookingBlock_16']	= "Please provide Driver age";
    propsArray['bookingBlock_17']	= "Please enter a valid travel return date.";
    propsArray['bookingBlock_18']   = "Invalid Departure Date";
    propsArray['checkin_1']			= "Please fill in the mandatory field(s).";
    propsArray['statusVal_1']		= "Please enter the Carrier Code.";
    propsArray['statusVal_2']		= "Please enter the Flight number.";
    propsArray['statusVal_3']		= "Flight Number should be a number.";
    propsArray['statusVal_4']		= "Please enter the Departure City.";
    propsArray['statusVal_5']		= "Please enter the Destination City.";
    propsArray['statusVal_6']		= "Departure City and  Destination City cannot be the same.";
    propsArray['voylogin_usename']	= "The voyager number field is empty";
    propsArray['voylogin_usename2']	= "The voyager number should be a Number";
    propsArray['login_usename1']	= "Voyager number  may not be less than 6 characters.";
    propsArray['login_usename2']	= "Voyager number  exceeds 15 characters..";
    propsArray['voylogin_pin']		= "The PIN field is empty";
    propsArray['voylogin_pin2']		= "The PIN number may not be less than 4 characters.";
    propsArray['voylogin_pin3']		= "The PIN should be a Number.";
    propsArray['hotelDest_1']		= "Please provide the hotel destination";
    propsArray['cars_1']			= "Please provide a valid driver age.";
    propsArray['cars_2']			= "Please provide a pick up location.";
    propsArray['cars_3']			= "Please provide a drop-off location.";
    propsArray['cars_4']			= "Please provide the country.";
    propsArray['voyLogin_usename3']	= "The voyager number is not valid.";
    propsArray['ieVersion_error']	= "You are using a version of Internet Explorer which is no longer supported. If you are using Windows XP or older, please switch to Firefox or Chrome as Microsoft no longer supports these systems. If you are using a system newer than Windows XP, you may upgrade Internet Explorer by clicking <a href=http://windows.microsoft.com/en-ZA/internet-explorer/download-ie>here</a>";

    //These were pulled out of the top of the body and should be in the propsArray and not as html tags
    //      <div id="pickupHeaderId" class="hidden">Type your pick-up point here</div>
    propsArray['bookingBlock_pickupHeader']	        = "Type your pick-up point here.";
    //      <div id="dropoffHeaderId" class="hidden">Type your drop-off point here</div>
    propsArray['bookingBlock_dropoffHeader']	    = "Type your drop-off point here.";
    //      <div id="hotelHeaderTxtId" class="hidden">Type your destination here</div>
    propsArray['bookingBlock_hotelHeaderTxt']	    = "Type your drop-off point here.";
    //      <div id="destinationHeaderId" class="hidden">Destination City</div>
    propsArray['bookingBlock_destinationHeader']	= "Destination City";
    //      <div id="countryResHeaderId" class="hidden">Country of Residence</div>
    propsArray['bookingBlock_countryResHeader']	    = "Country of Residence";
    //      <div id="voyagerNumErrorId" class="hidden">Voyager Number is mandatory.</div>
    propsArray['bookingBlock_voyagerNumError']	    = "Voyager Number is mandatory.";
    //      <div id="voyagerNumErrorId1" class="hidden">voyager number must be between 6 and 15 characters. may not be less than 6 characters.</div>
    propsArray['bookingBlock_voyagerNumError']	    = "Voyager number must be between 6 and 15 characters. may not be less than 6 characters.";
    //      <div id="voyagerNumErrorId2" class="hidden">voyager number must be between 6 and 15 characters. exceeds 15 characters.</div>
    propsArray['bookingBlock_voyagerNumError2']	    = "Yoyager number must be between 6 and 15 characters. exceeds 15 characters.";
    //      <div id="voyagerNumErrorId3" class="hidden">error.voyager.forgotpassword.voyagerNumber3.required</div>
    propsArray['bookingBlock_voyagerNumError3']	    = "error.voyager.forgotpassword.voyagerNumber3.required";
    //      <div id="voyagerEmailErrorId" class="hidden">Either the email or mobile number is mandatory.</div>
    propsArray['bookingBlock_voyagerEmailErrorId']	= "Either the email or mobile number is mandatory.";
    //      <div id="voyagerEmailErrorId1" class="hidden">Invalid Email Id: Enter a valid Email Id, e.g. admin@saa.com.</div>
    propsArray['bookingBlock_voyagerEmailErrorId1']	= "Invalid Email Id: Enter a valid Email Id, e.g. admin@saa.com.";

    //<div id="msgAlertDiv" class="hidden">Message</div>
    propsArray['msgAlertHeader'] = "Message";
    //<div id="continueDiv" class="hidden">Continue</div>
    propsArray['msgButtonContinue'] = "Continue";

    //<div id="departCityId" class="hidden">Departure City</div>
    propsArray['bookingBlock_departCity'] =  'Departure City';

    //<div id="destinCityId" class="hidden">Destination City</div>
    propsArray['bookingBlock_destinationCity'] = 'Destination City';
    //<div id="IBoardPoint"></div>
    propsArray['bookingBlock_boardPoint'] = '';

    //<div id="statusDepartCityId" class="hidden"></div>
    propsArray['statusDepartCityId'] = '';
    //<div id="statusDestCityId" class="hidden"></div>
    propsArray['statusDestCityId'] = '';



</script>








</body>
</html>
